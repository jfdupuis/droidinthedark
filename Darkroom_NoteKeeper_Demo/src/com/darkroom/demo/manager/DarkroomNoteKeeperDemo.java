package com.darkroom.demo.manager;

import com.darkroom.library.Licensing;
import com.darkroom.manager.DarkroomNoteKeeper;

import android.os.Bundle;

public class DarkroomNoteKeeperDemo extends DarkroomNoteKeeper {
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		Licensing.setDemo(true);
		super.onCreate(savedInstanceState);
	}
}