package com.darkroom.manager;


import com.darkroom.library.FilmFrameExposure;
import com.darkroom.library.Filter;
import com.darkroom.library.Lens;
import com.darkroom.library.dao.AddNewFilterDialog;
import com.darkroom.library.dao.AddNewLensDialog;
import com.darkroom.library.dao.DarkroomDBHelper;
import com.darkroom.manager.preferences.DarkroomManagerPrefs;
import com.darkroom.manager.widget.ApertureWheelAdapter;
import com.darkroom.manager.widget.EVWheelAdapter;
import com.darkroom.manager.widget.ListUtil;
import com.darkroom.manager.widget.ShutterWheelAdapter;
import com.jfdupuis.library.math.Fraction;
import com.jfdupuis.library.math.FractionConversionException;
import com.jfdupuis.library.math.Unit;
import com.jfdupuis.library.widget.DBEditText;
import com.jfdupuis.library.widget.DBEditText.OnChangedListener;
import com.jfdupuis.library.widget.DBSpinner;
import com.jfdupuis.library.widget.DBSpinnerDialog;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.jfdupuis.library.widget.wheel.OnWheelChangedListener;
import com.jfdupuis.library.widget.wheel.WheelView;



public class FilmFrameExposureEditor  extends DarkroomDBActivity {
	static final int EXPO_WHEEL_NBITEMS = 7;
	static final int REQUEST_ADD_LENS = 0;
	static final boolean DEFAULT_BELLOW_STATE = false;
	static final boolean DEFAULT_EXPOSURELOCK_STATE = true;
	
	static final int REQUEST_EDIT_FILTER = 11;
	static final int REQUEST_ADD_FILTER = 12;
	
	final String[] mItemFrom = new String[]{
			DarkroomDBHelper.KEY_FILTER_NAME,
			DarkroomDBHelper.KEY_FILTER_FACTOR 
			};
    final int[] mItemTo = new int[]{
			R.id.name,
			R.id.factor};
	
	boolean mIsInitialized = false;
	
	long mFilmFrameId = -1;
	Lens mCurrentLens = null;
	FilmFrameExposure mCurrent = null;
	FilmFrameExposure mOldExposure = null;
	
	int mFilmISO = 0;
	Fraction mCompensatedEV;
	
//	DBEditText mHEV, mSEV, mExpEV;
	WheelView mApertureWheel, mShutterWheel, mHEV_Wheel, mSEV_Wheel, mExpEV_Wheel;
	EVWheelAdapter mEVAdapter;
	ApertureWheelAdapter mApertureAdapter;
	ShutterWheelAdapter mShutterAdapter;
	
	DBSpinner mFilterSpinner;
	DBSpinner mLensSpinner;
	
	String mBellowUnits;
	
	TextView mHighLigthRange;
	TextView mShadowRange;
	TextView mContrastRange;
	
	TextView mBellowFactor;
	CheckBox mBellowCheck;
	DBEditText mBellowExtension;
	
	CheckBox mExposureLock;
	
	DBEditText mReciprocity;
	TextView mResultingEV;
	DBEditText mNotes;
	
	ListView  mFilterList;
	SimpleCursorAdapter mFilterAdapter;
	
	public void onCreate(Bundle inBundle) {
		super.onCreate(inBundle);
		setContentView(R.layout.filmframe_expo_editor);
		
		mResultingEV = (TextView) findViewById(R.id.compensated_ev);
		
		Button lAddFilterBtn = (Button) findViewById(R.id.add_filter_btn);
		lAddFilterBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View inView) {
				addFilter();
			}
		});
		
		//Bellow
		TextView lBellowHeader = (TextView) findViewById(R.id.bellow_image);
		mBellowUnits = DarkroomManagerPrefs.getDefaultUnits(this);
		lBellowHeader.setText(lBellowHeader.getText().toString() + " [" + mBellowUnits + "]:" );
		
		
		mNotes = (DBEditText) findViewById(R.id.notes);
		mNotes.setOnChangedListener(new OnChangedListener() {
			public void changed(EditText inView) {
				changedNotes(inView.getText().toString());
			}
		});
		
		mBellowExtension = (DBEditText) findViewById(R.id.bellow);
		mBellowExtension.setEnabled(DEFAULT_BELLOW_STATE);
		mBellowExtension.setOnChangedListener(new OnChangedListener() {
			public void changed(EditText inView) {
				changedBellowExtension(Float.valueOf(inView.getText().toString()));
			}
		});
		mBellowFactor = (TextView) findViewById(R.id.bellow_factor);
		mBellowFactor.setEnabled(DEFAULT_BELLOW_STATE);
		mBellowFactor.setText("");
		mBellowCheck = (CheckBox) findViewById(R.id.havebellow);
		mBellowCheck.setChecked(DEFAULT_BELLOW_STATE);
		mBellowCheck.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(mCurrent != null)
					mCurrent.mHasBellowExtension = isChecked;
				
				mBellowFactor.setEnabled(isChecked);
				mBellowExtension.setEnabled(isChecked);
				
				if(!isChecked) {
					mCurrent.mBellowFactor = new Fraction(0);
					mBellowFactor.setText("0");
				} else {
					if(mCurrentLens != null)
						mCurrent.mBellowFactor = computeBellowFactor(mCurrent.mBellowExtension, mCurrentLens.getFocalLength());
					mBellowFactor.setText(mCurrent.mBellowFactor.toString());
				}
				updateEV();
			}
		});
		

		//Exposure
		mReciprocity = (DBEditText) findViewById(R.id.reciprocity);
		mReciprocity.setOnChangedListener(new OnChangedListener() {
			public void changed(EditText inView) {
				if(!TextUtils.isEmpty(inView.getText()))
					changedReprocity(Float.valueOf(inView.getText().toString()));
			}
		});
		
		mExposureLock = (CheckBox) findViewById(R.id.exposure_lock);
		mExposureLock.setChecked(DEFAULT_EXPOSURELOCK_STATE);
		mExposureLock.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(mCurrent != null)
					mCurrent.mHasExposureLock = isChecked;
				updateEV();
				//Save state
				//TODO
				//mDarkroomDBAdapter.updateExposureValue(mCurrent.getId(), DarkroomDBHelper.KEY_EXPOSURE_SKIPBASE, isChecked ? "1" : "0");
			}
		});
		
		setupButtons();
		setupSpinner();
		setupList();
		
		mFilmFrameId = getParent().getIntent().getLongExtra("filmframe_id", -1);
		if(mFilmFrameId != -1) {
			loadData(mFilmFrameId);
		} else
			throw new RuntimeException("A film frame id was expected.");
		
		activateListener();
		
		mIsInitialized = true;
	}
	
	private void loadData(final long inFilmFrameId) {
		long lExposurId = mDarkroomDBAdapter.getFilmFrameExposureId(mFilmFrameId);
		if(lExposurId > 0) {
			mCurrent = mDarkroomDBAdapter.getFilmFrameExposure(lExposurId);
		} else {
			//No exposure found in the database, create a new one
			Logger.d("No exposure found for id " + lExposurId + ", creating a new one");
			mCurrent = new FilmFrameExposure(mFilmFrameId);
			mCurrent.setId(mDarkroomDBAdapter.insertFilmFrameExposure(mCurrent));
			//mDarkroomDBAdapter.updateFilmFrameExposureId(mFilmFrameId, lId);
		}
		
		mOldExposure = new FilmFrameExposure(mCurrent);
		
		long lFilmId = mDarkroomDBAdapter.getFilmFrameFilmId(inFilmFrameId);
		Cursor lFilmCursor = mDarkroomDBAdapter.getFilmCursor(lFilmId, 
				new String[] {DarkroomDBHelper.KEY_FILM_SHOTISO, DarkroomDBHelper.KEY_FILM_USERID});
		if ((lFilmCursor.getCount() == 0) || !lFilmCursor.moveToFirst()) {
			throw new SQLException("No film found for filmframe id: " + inFilmFrameId);
		}
		
		TextView lUserId = (TextView) findViewById(R.id.film_userid);
		lUserId.setText(lFilmCursor.getString(lFilmCursor.getColumnIndex(DarkroomDBHelper.KEY_FILM_USERID)));
		
		mFilmISO = lFilmCursor.getInt(lFilmCursor.getColumnIndex(DarkroomDBHelper.KEY_FILM_SHOTISO));
		TextView lFilmISO = (TextView) findViewById(R.id.film_iso);
		lFilmISO.setText(String.valueOf(mFilmISO));
		lFilmCursor.close();
		
		if(mFilmISO == 0) {
			mExposureLock.setChecked(false);
			mExposureLock.setEnabled(false);
			lFilmISO.setTextColor(getResources().getColor(R.color.red));
			TextView lIsoHeader = (TextView) findViewById(R.id.film_iso_header);
			lIsoHeader.setTextColor(getResources().getColor(R.color.red));
		} else {
			mExposureLock.setChecked(mCurrent.mHasExposureLock);
		}
		
//		mHEV.setText(Double.toString(mCurrent.mHighlight.asDouble()));
//		mExpEV.setText(Double.toString(mCurrent.mExposure.asDouble()));
//		mSEV.setText(Double.toString(mCurrent.mShadow.asDouble()));
		
		mHEV_Wheel.setCurrentItem(mEVAdapter.getValueIndex(mCurrent.mHighlight));
		mSEV_Wheel.setCurrentItem(mEVAdapter.getValueIndex(mCurrent.mShadow));
		mExpEV_Wheel.setCurrentItem(mEVAdapter.getValueIndex(mCurrent.mExposure));
		
		mHighLigthRange.setText(mCurrent.mHighlight.minus(mCurrent.mExposure).toString());
		mContrastRange.setText(mCurrent.mHighlight.minus(mCurrent.mShadow).toString());
		mShadowRange.setText(mCurrent.mExposure.minus(mCurrent.mShadow).toString());
		
		
		
		mBellowFactor.setText(mCurrent.mBellowFactor.toString());
		mBellowExtension.setText(Float.toString(Unit.convertFromMM(mCurrent.mBellowExtension, DarkroomManagerPrefs.getDefaultUnits(this))));
		mReciprocity.setText(Float.toString(mCurrent.mReciprocity));
		
		if(mCurrent.mBellowFactor.asDouble() > 0)
			mBellowCheck.setChecked(true);
		else
			mBellowCheck.setChecked(false);
		
		if(mCurrent.mLens < 1) { 
			mCurrentLens = null;
			mLensSpinner.setPositionById(1); //Set to not specified
		} else {
			mCurrentLens = mDarkroomDBAdapter.getLens(mCurrent.mLens);
			mLensSpinner.setPositionById(mCurrent.mLens);
		}
		
		mNotes.setText(mCurrent.mNotes);
		
		
		Cursor lFilterCursor = mDarkroomDBAdapter.getAllExposedFilterCursor(mCurrent.getId(), mItemFrom);
        mFilterAdapter.changeCursor(lFilterCursor);
        mFilterList.setAdapter(mFilterAdapter);
		ListUtil.setListViewHeightBasedOnChildren(mFilterList);
		
		
		updateEV();
		
		mApertureWheel.setCurrentItem(mApertureAdapter.getValueIndex(mCurrent.mAperture));
		mShutterWheel.setCurrentItem(mShutterAdapter.getValueIndex(mCurrent.mShutterSpeed));
	}
	
	
	private void cancel() {
		if(!mDarkroomDBAdapter.updateFilmFrameExposureValue(mOldExposure))
			throw new SQLException("Can't update film frame exposure value!");
    	setResult(RESULT_OK);
    	finish();
	}
	
	private void setupButtons() {
		
		
		//Connect buttons
        final Button lCancelButton = (Button) findViewById(R.id.cancel_btn);
        lCancelButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	cancel();
            }
        });
        
        final Button lUndoButton = (Button) findViewById(R.id.undo_btn);
        lUndoButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
           		if(!mDarkroomDBAdapter.updateFilmFrameExposureValue(mOldExposure))
        			throw new SQLException("Can't update film frame exposure value!");
            	loadData(mFilmFrameId);
            }
        });
        
        final Button lDoneButton = (Button) findViewById(R.id.done_btn);
        lDoneButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	//All the field are saved each time they are modified. So, we just quit.
            	v.setFocusableInTouchMode(true);
            	v.requestFocus();
            	if(!mDarkroomDBAdapter.updateFilmFrameExposureValue(mCurrent))
        			throw new SQLException("Can't update film frame exposure value!");
            	mCurrent = null;
            	setResult(RESULT_OK);
            	finish();
            }
        });
	}
	
	@Override
	public void onBackPressed() {
		//cancel();
//		
//		if(!mDarkroomDBAdapter.updateFilmFrameExposureValue(mCurrent))
//			throw new SQLException("Can't update film frame exposure value!");
//		
//    	mCurrent = null;
//    	setResult(RESULT_OK);
//    	finish();
	}
	
	protected void setupSpinner() {
		//Metering
		mHighLigthRange = (TextView) findViewById(R.id.highlight2exposure);
		mShadowRange = (TextView) findViewById(R.id.shadow2exposure);
		mContrastRange = (TextView) findViewById(R.id.contrast_range);
		
		mEVAdapter = new EVWheelAdapter(this, new Fraction((long)1,(long)3), null);
		mEVAdapter.setItemResource(R.layout.ev_wheel_item);
		
		
//		mHEV = (DBEditText)  findViewById(R.id.hev);
//		mSEV = (DBEditText)  findViewById(R.id.sev);
//		mExpEV = (DBEditText)  findViewById(R.id.expev);
		
		mHEV_Wheel = (WheelView) findViewById(R.id.hev);
		mHEV_Wheel.setViewAdapter(mEVAdapter);
		mHEV_Wheel.setVisibleItems(EXPO_WHEEL_NBITEMS);
		
		mSEV_Wheel = (WheelView) findViewById(R.id.sev);
		mSEV_Wheel.setViewAdapter(mEVAdapter);
		mSEV_Wheel.setVisibleItems(EXPO_WHEEL_NBITEMS);
		
		mExpEV_Wheel = (WheelView) findViewById(R.id.expev);
		mExpEV_Wheel.setViewAdapter(mEVAdapter);
		mExpEV_Wheel.setVisibleItems(EXPO_WHEEL_NBITEMS);
		
		mApertureAdapter = new ApertureWheelAdapter(this);
		mApertureAdapter.setItemResource(R.layout.ev_wheel_item);
		mApertureWheel = (WheelView) findViewById(R.id.aperture);
		mApertureWheel.setViewAdapter(mApertureAdapter);
		mApertureWheel.setVisibleItems(EXPO_WHEEL_NBITEMS);

		mShutterAdapter = new ShutterWheelAdapter(this);
		mShutterAdapter.setItemResource(R.layout.ev_wheel_item);
		mShutterWheel = (WheelView) findViewById(R.id.shutter);
		mShutterWheel.setViewAdapter(mShutterAdapter);
		mShutterWheel.setVisibleItems(EXPO_WHEEL_NBITEMS);
		
		mLensSpinner = (DBSpinner) findViewById(R.id.lens_spinner);
		mLensSpinner.setAdapter(
				mDarkroomDBAdapter.getAllLensCursor(),
				DarkroomDBHelper.KEY_LENS_NAME);
		mLensSpinner.setOnActionRequestHandler(new DBSpinner.OnActionRequest(){
			public void onActionRequest(Spinner mInSpinner, int inAction, int inPosition, long inId) {
				Intent lIntent = new Intent(FilmFrameExposureEditor.this, AddNewLensDialog.class);
				switch(inAction) {
				case DBSpinnerDialog.ADD_REQUEST:
					changedLens(inId);
	            	break;
				case DBSpinnerDialog.EDIT_REQUEST:
					lIntent.putExtra("id", inId);
					break;
				case DBSpinnerDialog.ITEM_SELECTION:
					changedLens(inId);
					return;
				}
				startActivityForResult(lIntent,REQUEST_ADD_LENS);
			}
		});
		
	}
	
	private void activateListener() {
		mShutterWheel.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView inWheel, int inOldValue, int inNewValue) {
				changedShutterSpeed(inOldValue, inNewValue);
			}
		});
		mApertureWheel.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView inWheel, int inOldValue, int inNewValue) {
				changedAperture(inOldValue, inNewValue);
			}
		});
		
//		mExpEV.setOnChangedListener(new OnChangedListener() {
//			public void changed(EditText inView) {
//				try {
//					changedExposure(new Fraction(Double.valueOf(inView.getText().toString()), 3));
//				} catch(NumberFormatException e){
//					ScreenUtil.displayMessage(FilmFrameExposureEditor.this, "Invalid exposure number!", Toast.LENGTH_SHORT);
//				} catch (FractionConversionException e) {
//					ScreenUtil.displayMessage(FilmFrameExposureEditor.this, "Invalid exposure number!", Toast.LENGTH_SHORT);
//				}
//			}
//		});
//		mHEV.setOnChangedListener(new OnChangedListener() {
//			public void changed(EditText inView) {
//				try {
//					changedHighlight(new Fraction(Double.valueOf(inView.getText().toString()), 3));
//				} catch(NumberFormatException e){
//					ScreenUtil.displayMessage(FilmFrameExposureEditor.this, "Invalid highligh number!", Toast.LENGTH_SHORT);
//				} catch (FractionConversionException e) {
//					ScreenUtil.displayMessage(FilmFrameExposureEditor.this, "Invalid highligh number!", Toast.LENGTH_SHORT);
//				}
//			}
//		});
//		mSEV.setOnChangedListener(new OnChangedListener() {
//			public void changed(EditText inView) {
//				try {
//					changedShadow(new Fraction(Double.valueOf(inView.getText().toString()), 3));
//				} catch(NumberFormatException e){
//					ScreenUtil.displayMessage(FilmFrameExposureEditor.this, "Invalid shadow number!", Toast.LENGTH_SHORT);
//				} catch (FractionConversionException e) {
//					ScreenUtil.displayMessage(FilmFrameExposureEditor.this, "Invalid shadow number!", Toast.LENGTH_SHORT);
//				}
//			}
//		});
		mExpEV_Wheel.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView inWheel, int inOldValue, int inNewValue) {
				changedExposure(mEVAdapter.getValue(inNewValue));
			}
		});
		mHEV_Wheel.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView inWheel, int inOldValue, int inNewValue) {
				changedHighlight(mEVAdapter.getValue(inNewValue));
			}
		});
		mSEV_Wheel.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView inWheel, int inOldValue, int inNewValue) {
				changedShadow(mEVAdapter.getValue(inNewValue));
			}
		});
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Logger.d("Activity return from request " + requestCode);
		switch(requestCode) {
			case REQUEST_ADD_LENS:
				if(resultCode == Activity.RESULT_OK) {
					mLensSpinner.onAddReturn(resultCode, data);
					changedLens(data.getLongExtra("id", -1));
				}
				break;
			case REQUEST_ADD_FILTER:
				if(data != null) {
					Long lNewId = data.getLongExtra(Manager.SELECTION, -1);
					if(!mCurrent.mFilters.contains(lNewId)); {
						mCurrent.mFilters.add(lNewId);
						Logger.d("Adding filter id: " + data.getLongExtra(Manager.SELECTION, -1));
						
						Filter lFilter = mDarkroomDBAdapter.getFilter(lNewId, DarkroomDBHelper.FILTER_TABLE_NAME);
						lFilter.setFilmFrameExposureId(mCurrent.getId());
						mDarkroomDBAdapter.insertFilter(lFilter, DarkroomDBHelper.EXPOSED_FILTER_TABLE_NAME);
					}
					
					updateFilters();
				}
				break;
			case REQUEST_EDIT_FILTER:
				updateFilters();
				return;
		}

    }
	
	void changedLens(long inId) {
		if(inId > 0) {
			Logger.d("Changing lens for "+ inId);
			mCurrent.mLens = inId;
			mCurrentLens = mDarkroomDBAdapter.getLens(mCurrent.mLens);
			mCurrent.mBellowFactor = computeBellowFactor(mCurrent.mBellowExtension, mCurrentLens.getFocalLength());
			mBellowFactor.setText(mCurrent.mBellowFactor.toString());
			updateEV();
		}
	}
	
	void changedNotes(String inValues) {
		mCurrent.mNotes = inValues;
	}
	
	void changedBellowExtension(float inNewValue) {
		Logger.d("changed bellow extension");
		
		final String lUnits = DarkroomManagerPrefs.getDefaultUnits(this);
		mCurrent.mBellowExtension = Unit.convertToMM(inNewValue, lUnits);
		
		if(mCurrentLens != null) {
			mCurrent.mBellowFactor = computeBellowFactor(inNewValue, mCurrentLens.getFocalLength());
			mBellowFactor.setText(mCurrent.mBellowFactor.toString());
			updateEV();
		}
	}
	
	void changedReprocity(float inNewValue) {
		mCurrent.mReciprocity = inNewValue;
		updateEV();
	}
	
	void changedHighlight(Fraction inNewValue) {
		mCurrent.mHighlight = inNewValue;
		mHighLigthRange.setText(mCurrent.mHighlight.minus(mCurrent.mExposure).toString());
		mContrastRange.setText(mCurrent.mHighlight.minus(mCurrent.mShadow).toString());
	}
	
	void changedShadow(Fraction inNewValue) {
		mCurrent.mShadow = inNewValue;
		mShadowRange.setText(mCurrent.mExposure.minus(mCurrent.mShadow).toString());
		mContrastRange.setText(mCurrent.mHighlight.minus(mCurrent.mShadow).toString());
	}
	
	void changedExposure(Fraction inNewValue) {
		mCurrent.mExposure = inNewValue;
		mHighLigthRange.setText(mCurrent.mHighlight.minus(mCurrent.mExposure).toString());
		mShadowRange.setText(mCurrent.mExposure.minus(mCurrent.mShadow).toString());
		updateEV();
	}
	
	

	
	void updateEV() {
		Logger.d("update EV");
		mCompensatedEV = mCurrent.mExposure.minus(mCurrent.mBellowFactor);
		
		//TODO Substract all filter factors
		if(mCurrent.mFilters.size() > 0) {
			Cursor lCursor = mFilterAdapter.getCursor();
			if (lCursor != null) {
			    if (lCursor.moveToFirst()) {
			        do {
			        	mCompensatedEV = mCompensatedEV.minus(new Fraction(lCursor.getString(lCursor.getColumnIndex(DarkroomDBHelper.KEY_FILTER_FACTOR))));                 
			        } while (lCursor.moveToNext());
			    }
			}
		}
		
		mResultingEV.setText(mCompensatedEV.toString());
		if(mExposureLock.isChecked() && mIsInitialized) {
			changedEV();
		}
	}
	
	void changedEV() {
		// EV = log2 N^2/t, where N is the aperture value and t is the shutter time in seconds. EV is for ISO 100
		// EV_iso = EV_100 + log2 iso/100;
		// Source: http://en.wikipedia.org/wiki/Exposure_value
		
		//mFilmISO
		if(mFilmISO == 0)
			return;
		
		double EV = mCompensatedEV.asDouble() + Math.log(mFilmISO/100.0)/Math.log(2);
		double N = mApertureAdapter.getValue(mApertureWheel.getCurrentItem());
		
		double t = N*N / Math.pow(2.0, EV);
		
		Logger.d(String.format("Computed exposure: EV=%f, N=%f, t=%f", EV,N,t));
		
		//Match to closest shutter speed.	
		mApertureWheel.enableChangingListener(false);
		mShutterWheel.enableChangingListener(false);
		
		int lIdx = mShutterAdapter.getValueIndex(t);
		mShutterWheel.setCurrentItem(lIdx);
		mCurrent.mShutterSpeed = mShutterAdapter.getValue(lIdx);
		Logger.d(String.format("Selecting shutter: ", mCurrent.mShutterSpeed.toString()));
		
		//Adjust aperture
		t = mShutterAdapter.getValue(mShutterWheel.getCurrentItem()).asDouble();
		N = Math.sqrt(t*Math.pow(2.0,EV));
		lIdx = mApertureAdapter.getClosestValueIndex((float)N);
		mApertureWheel.setCurrentItem(lIdx);
		mCurrent.mAperture = mApertureAdapter.getValue(lIdx);
		Logger.d(String.format("Selecting aperture: ", mCurrent.mAperture));
		
		mApertureWheel.enableChangingListener(true);
		mShutterWheel.enableChangingListener(true);
	}
	
	void changedShutterSpeed(int lOldIdx, int lNewIdx) {
		mCurrent.mShutterSpeed = mShutterAdapter.getValue(lNewIdx);
		
		if(mExposureLock.isChecked() && mCompensatedEV != null) {
			double EV = mCompensatedEV.asDouble() + Math.log(mFilmISO/100.0)/Math.log(2);
			double t = mShutterAdapter.getValue(mShutterWheel.getCurrentItem()).asDouble();
			double N = Math.sqrt(t*Math.pow(2.0,EV));
			int lIdx = mApertureAdapter.getClosestValueIndex((float)N);
			mCurrent.mAperture = mApertureAdapter.getValue(lIdx);
			
			mApertureWheel.enableChangingListener(false);
			mApertureWheel.setCurrentItem(lIdx);
			mApertureWheel.enableChangingListener(true);
		}
	}
	
	void changedAperture(int lOldIdx, int lNewIdx) {
		mCurrent.mAperture = mApertureAdapter.getValue(lNewIdx);
		
		if(mExposureLock.isChecked() && mCompensatedEV != null) {
			mApertureWheel.enableChangingListener(false);
			mShutterWheel.enableChangingListener(false);
			
			double EV = mCompensatedEV.asDouble() + Math.log(mFilmISO/100.0)/Math.log(2);
			double N = mApertureAdapter.getValue(mApertureWheel.getCurrentItem());
			double t = N*N / Math.pow(2.0, EV);
			
			int lIdx = mShutterAdapter.getValueIndex(t);
			mCurrent.mShutterSpeed = mShutterAdapter.getValue(lIdx);
			mShutterWheel.setCurrentItem(lIdx);
			
			//Readjust aperture
			t = mShutterAdapter.getValue(mShutterWheel.getCurrentItem()).asDouble();
			N = Math.sqrt(t*Math.pow(2.0,EV));
			lIdx = mApertureAdapter.getClosestValueIndex((float)N);
			mApertureWheel.setCurrentItem(lIdx);
			mCurrent.mAperture = mApertureAdapter.getValue(lIdx);
			
			mApertureWheel.enableChangingListener(true);
			mShutterWheel.enableChangingListener(true);
		}
	}
	
	/**
	 * Compute the bellow extension factor give the extension in mm.
	 * @throws FractionConversionException 
	 */
	Fraction computeBellowFactor(float inExtension, float inFocalLength) {
		//Source: View Camera Technique, p.72
		try {
			return new Fraction(inExtension / inFocalLength);
		} catch (FractionConversionException e) {
			Logger.e("Can't convert fraction !", e);
			return new Fraction(0);
		}
	}
	
	
	
	void setupList() {
		
		Cursor lFilterCursor = mDarkroomDBAdapter.getAllExposedFilterCursor(-1, mItemFrom); //mCurrent is not initialized yet
		mFilterAdapter = new SimpleCursorAdapter(this, R.layout.filter_item, lFilterCursor, mItemFrom, mItemTo);
		mFilterList = (ListView) findViewById(R.id.filter_list);
		mFilterList.setAdapter(mFilterAdapter);
		mFilterList.post(new Runnable() { //Solve the issue of wrong resizing
			public void run() {
				ListUtil.setListViewHeightBasedOnChildren(mFilterList);
			}
		});
//		mFilterList.setOnItemClickListener(new OnItemClickListener(){
//			public void onItemClick(AdapterView<?> inParent, View inView, int inPosition, long inId) {
//				editFilter(inId);
//			}
//			
//		});
		registerForContextMenu(mFilterList);
	}
	

	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
		switch (item.getItemId()) {
		case R.id.context_edit:
			editFilter(info.id);
			return true;
//		case R.id.context_insert:
//			insertFilter(info.id); //Insert an item after selected id of the same type
//			return true;
		case R.id.context_delete:
			removeFilter(info.id);
			return true;
		default:
			return super.onContextItemSelected(item);
		}
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
	  super.onCreateContextMenu(menu, v, menuInfo);
	  MenuInflater inflater = getMenuInflater();
	  inflater.inflate(R.menu.filteritem_context, menu);
	}
	
	void addFilter() {
		Intent lIntent = new Intent(FilmFrameExposureEditor.this, FilterManager.class);
		lIntent.putExtra(Manager.SELECTONLY, true);
		startActivityForResult(lIntent, REQUEST_ADD_FILTER);
	}
	
	void removeFilter(long inId) {
		mCurrent.mFilters.remove(new Long(inId));
		mDarkroomDBAdapter.removeFilter(inId, DarkroomDBHelper.EXPOSED_FILTER_TABLE_NAME);
		updateFilters();
	}
	
	void editFilter(long inId) {		
		//Start film editor
		Intent lIntent = new Intent(this, AddNewFilterDialog.class);
		lIntent.putExtra("id", inId);
		lIntent.putExtra("table", DarkroomDBHelper.EXPOSED_FILTER_TABLE_NAME);
		startActivityForResult(lIntent, REQUEST_EDIT_FILTER);
	}
	
	void updateFilters() {
		//Refresh
		Cursor lFilterCursor = mDarkroomDBAdapter.getAllExposedFilterCursor(mCurrent.getId(), mItemFrom);
        mFilterAdapter.changeCursor(lFilterCursor);
		ListUtil.setListViewHeightBasedOnChildren(mFilterList);
		
		updateEV();
	}
	
}
