package com.darkroom.manager;

import com.darkroom.library.FilmFrame;
import com.darkroom.library.Logger;
import com.darkroom.library.dao.AddNewElementDialog;
import com.jfdupuis.library.screen.ScreenUtil;
import com.jfdupuis.library.widget.NumberPickerWithInvalids;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class AddNewFilmFrameDialog extends AddNewElementDialog {
	static final int REQUEST_SELECT_FILM = 3;
	static final int DATE_DIALOG_ID = 0;

	static final int FRAMERANGE_MIN = 0;
	static final int FRAMERANGE_MAX = 99;
	static final int DEFAULT_FRAME = 1;

	TextView mFilmUserId = null;
	long mFilmId = -1;

	NumberPickerWithInvalids mFrameNumberPicker = null;

	protected int mYear;
	protected int mMonth;
	protected int mDay;

	Date mDate = null;

	protected TextView mDateDisplay;
	protected Button mPickDate;

	@Override
	public void onCreate(Bundle inBundle) {
		super.onCreate(inBundle);
		setContentView(R.layout.addnew_filmframe_dialog);

		mId = getIntent().getLongExtra("filmframe_id", -1);
		mFilmId = getIntent().getLongExtra("film_id", -1);

		initialize();
		
		if(mId > 0) {
			setEditingMode(true);
		} 

		if (isEditing()) {
			setTitle(R.string.edit_filmframe_title);
			loadData();
		} else {
			setTitle(R.string.addnew_filmframe_title);
		}
	}

	@Override
	protected void initialize() {
		super.initialize();

		mFilmUserId = (TextView) findViewById(R.id.film);
		if (mFilmId > 0) {
			mFilmUserId.setText(mDarkroomDBAdapter.getFilm(mFilmId).getUserId());
		} else {
			mFilmUserId.setText("");
		}

		// Film selection
		final Button lFilmSelectButton = (Button) findViewById(R.id.select_film_btn);
		lFilmSelectButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent lIntent = new Intent(AddNewFilmFrameDialog.this, FilmManager.class);
				lIntent.putExtra(Manager.SELECTONLY, true);
				startActivityForResult(lIntent, REQUEST_SELECT_FILM);
			}
		});
		if(mFilmId > 0)
			lFilmSelectButton.setVisibility(View.GONE);

		mPickDate = (Button) findViewById(R.id.select_date_btn);
		mPickDate.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				showDialog(DATE_DIALOG_ID);
			}
		});

		// Frame selection
		mFrameNumberPicker = (NumberPickerWithInvalids) findViewById(R.id.FrameNumberPicker);
		mFrameNumberPicker.setRange(FRAMERANGE_MIN, FRAMERANGE_MAX);
		mFrameNumberPicker.setCurrent(DEFAULT_FRAME);

		// Date display
		final Calendar lCalendar = Calendar.getInstance();
		mDate = lCalendar.getTime();
		mYear = lCalendar.get(Calendar.YEAR);
		mMonth = lCalendar.get(Calendar.MONTH);
		mDay = lCalendar.get(Calendar.DAY_OF_MONTH);

		mDateDisplay = (TextView) findViewById(R.id.date);
		mDateDisplay.setText(String.format("%tF", lCalendar));
	}

	@Override
	protected void loadData() {
		FilmFrame lFrame = mDarkroomDBAdapter.getFilmFrame(mId);
		mFrameNumberPicker.setCurrent(lFrame.getFrameNumber());

		// Film id display
		mFilmId = lFrame.getFilmId();
		if (mFilmId > 0) {
			final String lFilmUserId = mDarkroomDBAdapter.getFilm(mFilmId).getUserId();
			mFilmUserId.setText(lFilmUserId);
		}

		// Date display
		final Calendar lCalendar = Calendar.getInstance();
		if (lFrame.getDate() != null)
			lCalendar.setTime(lFrame.getDate());
		mDateDisplay.setText(String.format("%tF", lCalendar));
	}

	@Override
	public void onClick(View inView) {
		// Safe the filled content in the database
		if (mFilmId < 1) {
			ScreenUtil.displayMessage(AddNewFilmFrameDialog.this, "A film should be selected", Toast.LENGTH_SHORT);
			return;
		}

		FilmFrame lFrame = new FilmFrame(mFilmId, mFrameNumberPicker.getCurrent());
		lFrame.setDate(mDate);
		try {
			if (isEditing()) {
				lFrame.setId(mId);
				mDarkroomDBAdapter.updateFilmFrameValue(lFrame);
			} else {
				mDarkroomDBAdapter.insertFilmFrame(lFrame);
			}
			setResult(RESULT_OK, new Intent().putExtra("filmframe_id", lFrame.getId()));
			finish();
		} catch (SQLiteConstraintException e) {
			ScreenUtil.displayMessage(AddNewFilmFrameDialog.this, "Frame already exist", Toast.LENGTH_SHORT);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case REQUEST_SELECT_FILM:
			if (resultCode == RESULT_OK) {
				mFilmId = data.getLongExtra(Manager.SELECTION, -1);
				final String lFilmUserId = mDarkroomDBAdapter.getFilm(mFilmId).getUserId();
				mFilmUserId.setText(lFilmUserId);
				Logger.v("Selected film : " + mFilmId);
			}
			break;
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
			return new DatePickerDialog(this, mDateSetListener, mYear, mMonth, mDay);
		}
		return null;
	}

	// the callback received when the user "sets" the date in the dialog
	protected DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

		public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
			mYear = year;
			mMonth = monthOfYear;
			mDay = dayOfMonth;
			Calendar calendar = new GregorianCalendar(mYear, mMonth, mDay);
			mDate = calendar.getTime();
			mDateDisplay.setText(String.format("%tF", calendar));
		}
	};
}
