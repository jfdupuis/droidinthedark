package com.darkroom.manager;

import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.TabHost;

public class PrintEditor extends TabActivity {
		
	public void onCreate(Bundle inBundle) {
		super.onCreate(inBundle);
		setContentView(R.layout.print_editor);
		setTitle(R.string.print_editor);
		
		Resources res = getResources(); // Resource object to get Drawables
	    TabHost tabHost = getTabHost();  // The activity TabHost
	    TabHost.TabSpec spec;  // Resusable TabSpec for each tab
	    Intent intent;  // Reusable Intent for each tab
	    
	    // Setup info tab
	    intent = new Intent().setClass(this, PrintInfoEditor.class);
	    spec = tabHost.newTabSpec("infos").setIndicator("Infos",
	                      res.getDrawable(android.R.drawable.ic_menu_info_details))
	                  .setContent(intent);
	    tabHost.addTab(spec);

	    // Setup development tab
	    intent = new Intent().setClass(this, PrintDevelopmentEditor.class);
	    spec = tabHost.newTabSpec("dev").setIndicator("Development",
	                      res.getDrawable(R.drawable.print_development))
	                  .setContent(intent);
	    tabHost.addTab(spec);
	    
	    // Setup exposure tab
	    intent = new Intent().setClass(this, PrintExposureEditor.class);
	    spec = tabHost.newTabSpec("expo").setIndicator("Exposure",
	                      res.getDrawable(android.R.drawable.ic_menu_camera))
	                  .setContent(intent);
	    tabHost.addTab(spec);

	    
	    // Set the current tab
	    tabHost.setCurrentTab(0);
        
	}
}
