package com.darkroom.manager;

import com.darkroom.library.Camera;
import com.darkroom.library.DevelopmentTime;
import com.darkroom.library.Film;
import com.darkroom.library.Logger;
import com.jfdupuis.library.screen.ScreenUtil;
import com.darkroom.library.dao.AddNewCameraDialog;
import com.darkroom.library.dao.DarkroomDBHelper;
import com.darkroom.manager.preferences.DarkroomManagerPrefs;
import com.jfdupuis.library.widget.DBSpinner;
import com.jfdupuis.library.widget.DBSpinnerDialog;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Bundle;
import android.view.View;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;


public class AddNewFilmDialog extends AddNewDatedObjectDialog {
	static final int REQUEST_ADD_CAMERA = 0;
	static final int REQUEST_ADD_FIRSTCAMERA = 1; 
		
    DBSpinner mCameraSpinner;
	
	@Override
	public void onCreate(Bundle inBundle) {
		super.onCreate(inBundle);
		setContentView(R.layout.addnew_film_dialog);
		
		initialize();
		
		
		final long lFilmId = getIntent().getLongExtra("film_id", -1);
		if(lFilmId > 0) {
			setTitle(R.string.edit_film_title);
			setEditingMode(true);
			mCurrent = mDarkroomDBAdapter.getFilm(lFilmId);
		} else {
			setTitle(R.string.addnew_film_title);
		}

		postinitialize();
		
        //Camera spinner
		mCameraSpinner = (DBSpinner) findViewById(R.id.camera_spinner);
		mCameraSpinner.setAdapter(
				mDarkroomDBAdapter.getAllCameraBrandModelCursor(),
				DarkroomDBHelper.KEY_CAMERA_BRANDMODEL);
        mCameraSpinner.setOnActionRequestHandler(new DBSpinner.OnActionRequest(){
			public void onActionRequest(Spinner mInSpinner, int inAction, int inPosition, long inId) {
				Intent lIntent = new Intent(AddNewFilmDialog.this, AddNewCameraDialog.class);
				switch(inAction) {
				case DBSpinnerDialog.ADD_REQUEST:
					break;
				case DBSpinnerDialog.EDIT_REQUEST:
					lIntent.putExtra("id", inId);
					break;
				case DBSpinnerDialog.ITEM_SELECTION:
					if ((Film)mCurrent == null)
						return;
					((Film)mCurrent).setCamera(mDarkroomDBAdapter.getCamera(inId));
					
					//Find minimum date number for this day and camera
					setDateNumberPickerRange(mCurrent.getDate(), inId);
					((Film)mCurrent).setIdNumber(mIdNumberPicker.getCurrent());
					
					updateID();
					return;
				}
				startActivityForResult(lIntent,REQUEST_ADD_CAMERA);
			}
		});

		if((Film)mCurrent != null)
			mCameraSpinner.setPositionById(((Film)mCurrent).getCamera().getId());
        //If no camera is defined
		if((mCameraSpinner.getCursorAdapter().getCursor().getCount() <= 0) || !(mCameraSpinner.getCursorAdapter().getCursor().moveToFirst())) {
        	Logger.v("No camera found, launching camera editor");
        	Intent lIntent = new Intent(AddNewFilmDialog.this, AddNewCameraDialog.class);
        	startActivityForResult(lIntent,REQUEST_ADD_FIRSTCAMERA);
        	return;
        } else {
        	//Set to default camera
        	mCameraSpinner.setPositionById(DarkroomManagerPrefs.getDefaultCamera(AddNewFilmDialog.this));
        }
		
		if((Film)mCurrent == null) {
			createNewFilm();
		} else {
			updateID();
		}
	}
	
	@Override
	protected void loadData() {
		//Already handled
	}
	
	private void createNewFilm() {
	    
	    final long lCameraId = mCameraSpinner.getSelectedItemId(); 
    	Camera lCamera = mDarkroomDBAdapter.getCamera(lCameraId);
    	DevelopmentTime lDevelopment = new DevelopmentTime("manager");
    	
    	
	    //Create new film
    	Calendar calendar = new GregorianCalendar(mYear, mMonth, mDay);
        Date lDate = calendar.getTime();
        
        //Find minimum date number for this day and camera
        setDateNumberPickerRange(lDate, lCameraId);
        
        mCurrent = new Film(lCamera, lDate, mIdNumberPicker.getCurrent(), lDevelopment);
        
        //Load default value
        ((Film)mCurrent).getDevelopment().mAgitation = DarkroomManagerPrefs.getDefaultAgitation(this);
        ((Film)mCurrent).getDevelopment().mDeveloper = DarkroomManagerPrefs.getDefaultFilmDeveloper(this);
        ((Film)mCurrent).getDevelopment().mDilution = DarkroomManagerPrefs.getDefaultFilmDeveloperDilution(this);
        ((Film)mCurrent).mInfo.setEmulsion(mDarkroomDBAdapter.getEmulsion(DarkroomManagerPrefs.getDefaultEmulsion(this)));
        ((Film)mCurrent).mInfo.mShotISO = mDarkroomDBAdapter.getEmulsion(DarkroomManagerPrefs.getDefaultEmulsion(this)).mISO;
        
        updateID();
	}


	public void onClick(View v) {
		try {
			//Flush focus from edit text to trigger onFocusChanged
			v.setFocusableInTouchMode(true);
        	v.requestFocus();
			if(isEditing()) {
				mDarkroomDBAdapter.updateFilmValue((Film)mCurrent, true);
			} else {
				((Film)mCurrent).getDevelopment().setId(mDarkroomDBAdapter.insertEmulsionDevTime(((Film)mCurrent).getDevelopment()));
				((Film)mCurrent).setId( mDarkroomDBAdapter.insertFilm((Film)mCurrent) );
			}
			setResult(RESULT_OK, new Intent().putExtra("film_id",((Film)mCurrent).getId()));
        	finish();
		} catch (SQLiteConstraintException e) {
			Logger.e(e.getMessage(),e);
			ScreenUtil.displayMessage(AddNewFilmDialog.this, "Film " + ((Film)mCurrent).getUserId() + " already exist", Toast.LENGTH_SHORT);
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Logger.v("Activity return from request " + requestCode);
		if(resultCode == RESULT_OK) {
			switch(requestCode) {
				case REQUEST_ADD_CAMERA:
					mCameraSpinner.onAddReturn(resultCode, data);
					break;
				case REQUEST_ADD_FIRSTCAMERA:
					mCameraSpinner.onAddReturn(resultCode, data);
					if(resultCode == RESULT_OK) {
						createNewFilm(); //recreate film since we have now at least one camera
					} else {
						setResult(RESULT_CANCELED);
						finish();					
					}
					break;
			}
		}

    }
	
	
	/**
	 * Looks for all film with the same date and camera. 
	 * Return the day number that is currently used.
	 */
	protected ArrayList<Integer> findUsedDayNumber(Date inDate, long inCameraId) {
		ArrayList<Integer> lUsedDayNumber = new ArrayList<Integer>();
		
				
		Cursor lCursor = mDarkroomDBAdapter.getAllFilmCursor(
				inCameraId, 
				inDate.getTime(), 
				new String[]{DarkroomDBHelper.KEY_FILM_DATENUM});
		
		if (lCursor.moveToFirst()) {
	        final int lDateNumIdx = lCursor.getColumnIndex(DarkroomDBHelper.KEY_FILM_DATENUM); 
	        do {
	        	lUsedDayNumber.add(lCursor.getInt(lDateNumIdx));
	        } while (lCursor.moveToNext());
	    }
		
		return lUsedDayNumber;
	}
	
	protected void setDateNumberPickerRange(final Date inDate, final long inCameraId) {
		ArrayList<Integer> lUsedDayNumber = findUsedDayNumber(inDate, inCameraId);
		
		Integer lMinIdx;
		for(lMinIdx = new Integer(1); ; ++lMinIdx) {
			if( !lUsedDayNumber.contains( lMinIdx ) )
				break;
		}
		
		mIdNumberPicker.setCurrent(lMinIdx);
		mIdNumberPicker.setInvalidNumbers(lUsedDayNumber);
		
	}
	
	@Override
	protected void updateIdNumberPickerRange() {
		setDateNumberPickerRange(mCurrent.getDate(), mCameraSpinner.getSelectedItemId());
		mCurrent.setIdNumber(mIdNumberPicker.getCurrent());
	}
	
}