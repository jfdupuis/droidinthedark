package com.darkroom.manager;

import com.darkroom.library.dao.AddNewFilterDialog;
import com.darkroom.library.dao.DarkroomDBHelper;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class FilterManager extends Manager {
	
	private SimpleCursorAdapter mAdapter = null;
	

	/** Called when the activity is first created. */
	public void onCreate(Bundle inBundle) {
		super.onCreate(inBundle);
		setTitle(R.string.filter_manager);
		
		// Set result CANCELED in case the user backs out
        setResult(Activity.RESULT_CANCELED);
        
        populateList();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		Logger.d("Filter manager, onResume()");
		if(mCursor != null)
			mCursor.requery();
	}

	protected void populateList() {
		mCursor = mDarkroomDBAdapter.getAllFilterCursor(
				new String[]{DarkroomDBHelper.KEY_FILTER_NAME, DarkroomDBHelper.KEY_FILTER_FACTOR}, DarkroomDBHelper.FILTER_TABLE_NAME);
        startManagingCursor(mCursor);
        
        // Create an array to specify the fields we want to display in the list (only TITLE)
        final String[] lFrom = new String[]{DarkroomDBHelper.KEY_FILTER_NAME,DarkroomDBHelper.KEY_FILTER_FACTOR};
        
        // and an array of the fields we want to bind those fields to (in this case just text1)
        final int[] lTo = new int[]{R.id.name, R.id.factor};
        // Now create a simple cursor adapter and set it to display
        mAdapter = new SimpleCursorAdapter(this, R.layout.filter_item, mCursor, lFrom, lTo);
        setListAdapter(mAdapter);
	}
	
	@Override
	protected void onListItemClick(ListView inListView, View inView, int inPosition, long inId) {
		if(mSelectOnly) {
			Intent lIntent = new Intent();
			lIntent.putExtra(SELECTION, inId);
			setResult(Activity.RESULT_OK,lIntent);
			finish();
		} else {
			Intent lIntent = new Intent(this, AddNewFilterDialog.class);
			lIntent.putExtra("id", inId);
			lIntent.putExtra("table", DarkroomDBHelper.FILTER_TABLE_NAME);
			startActivity(lIntent);
		}
	}

	

	@Override
	protected void addNewItem() {
		Intent lIntent = new Intent(this, AddNewFilterDialog.class);
		lIntent.putExtra("table", DarkroomDBHelper.FILTER_TABLE_NAME);
		Logger.v("Starting filter editor");
		startActivityForResult(lIntent,ADD_ITEM);
	}

	@Override
	protected void editItem(int inPosition, long inId) {
		Logger.d("Editing filter id:" + inId);
		
		//Start film editor
		Intent lIntent = new Intent(this, AddNewFilterDialog.class);
		lIntent.putExtra("table", DarkroomDBHelper.FILTER_TABLE_NAME);
		lIntent.putExtra("id", inId);
		startActivityForResult(lIntent,EDIT_ITEM);
	}
	
	protected void removeItem(int inPosition, long inId) {
		Logger.v(String.format("Remove item at index: %d", inPosition));
		boolean lReturn = mDarkroomDBAdapter.removeFilter(inId,DarkroomDBHelper.FILTER_TABLE_NAME);
		Logger.v(String.format("Delete results: %b", lReturn));
		mCursor.requery();
	}

}
