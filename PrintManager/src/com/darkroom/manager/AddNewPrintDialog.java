package com.darkroom.manager;

import com.darkroom.library.FilmFrame;
import com.darkroom.library.Logger;
import com.darkroom.library.Print;

import com.jfdupuis.library.screen.ScreenUtil;
import com.darkroom.library.dao.DarkroomDBHelper;
import com.darkroom.manager.preferences.DarkroomManagerPrefs;
import com.jfdupuis.library.math.Unit;
import com.jfdupuis.library.widget.DBEditText;
import com.jfdupuis.library.widget.DBEditText.OnChangedListener;
import com.jfdupuis.library.widget.NumberPicker;
import com.jfdupuis.library.widget.NumberPickerWithInvalids;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class AddNewPrintDialog extends AddNewDatedObjectDialog {
	static final int FRAMERANGE_MIN = 1;
	static final int FRAMERANGE_MAX = 99;
	static final int DEFAULT_FRAME = 1;
	
	static final int REQUEST_SELECT_FILM = 3;
	static final int DIALOG_CONFIRM_ADDNEW = 1;

	boolean mUpdateExposure = false;
	
	long mFilmId = -1;
	long mFilmFrameId = -1;
	TextView mFilmUserId = null;
	
	DBEditText mSizeH = null;
	DBEditText mSizeV = null;
	String mOldUnit = null;
	
	Spinner mUnitsSpinner;
	ArrayAdapter<CharSequence> mUnitsAdapter;

	NumberPickerWithInvalids mFrameNumberPicker = null;

	
	public void onCreate(Bundle inBundle) {
		super.onCreate(inBundle);
		setContentView(R.layout.addnew_print_dialog);
		
		initialize();

		final long lPrintId = getIntent().getLongExtra("print_id", -1);
		if(lPrintId > 0) {
			//Get the print from the intent sent by DarkroomTimer
			Print lPrint = getIntent().getParcelableExtra("print");
			mCurrent = mDarkroomDBAdapter.getPrint(lPrintId, false);
			if(getIntent().getAction() == null || !getIntent().getAction().equals("com.darkroom.saveprint")) {
				//Edit the print id as this is not a new print received
				Logger.d("Editing print ID");
				setTitle(R.string.edit_print_title);
				setEditingMode(true);
			} else {
				//Create a new print, either from the f-stop timer or the manager
				Logger.d("Create a new version of a previous print.");
				if(lPrint == null)
					throw new RuntimeException("A print was expected!");
				
				setTitle(R.string.addnew_print_title);
				if( ((Print)mCurrent).hasSameExposure(lPrint)  ) {
					//The print id received already exist, and the print have the same exposure
					showDialog(DIALOG_CONFIRM_ADDNEW); //Will quit if not
					mCurrent = lPrint;
					mCurrent.setId(-1);
					return; //Have to call postinitialize on dialog confirmation return.
				} else {
					//If the exposure was not previously defined in the database edit the print id
					if( ((Print)mCurrent).getExposures().size() == 0 ) {
						//The print didn't have any exposure defined, so we are only adding the exposure.
						//The edition screen is shown to update print size.
						Logger.d("Editing print ID of newly exposed print");
						setTitle(R.string.edit_print_title);
						setEditingMode(true);
						mUpdateExposure = true;
						mCurrent = lPrint;
					} else {
						//The print is a derivative of an existing one. Therefore, film and frame are
						//set to the same, but size might have changed.
						mCurrent = lPrint;
						mCurrent.setId(-1);
						
					}
					
				}
				
			}
			
		} else {
			Logger.d("Creating a new print.");
			mCurrent = (Print)(getIntent().getParcelableExtra("print")); //Will be null if not sent by darkroomtimer
			setTitle(R.string.addnew_print_title);
		}
		
		postinitialize();
	}
	
	@Override
	protected void postinitialize() {
		super.postinitialize();
		mIdDisplay.setVisibility(View.INVISIBLE);
		
		
		
		//Frame selection
		mFrameNumberPicker = (NumberPickerWithInvalids) findViewById(R.id.FrameNumberPicker);
        mFrameNumberPicker.setRange(FRAMERANGE_MIN, FRAMERANGE_MAX);
        mFrameNumberPicker.setOnChangeListener(new NumberPicker.OnChangedListener() {
			public void onChanged(NumberPicker mPicker, int inOldVal, int inNewVal) {
				((Print)mCurrent).setFilmFrameNumber(inNewVal);
				setIdNumberPickerRange(mFilmId, mFrameNumberPicker.getCurrent());
				updateID();
			}
		  });
        if((Print)mCurrent != null) {
        	if(((Print)mCurrent).getFilmFrameNumber() < 0) {
        		//When print comes from timer, frame = -1. It need to be set to something
        		mFrameNumberPicker.setCurrent(DEFAULT_FRAME);
        		((Print)mCurrent).setFilmFrameNumber(mFrameNumberPicker.getCurrent());
        	} else {
        		mFrameNumberPicker.setCurrent(((Print)mCurrent).getFilmFrameNumber());
        	}
        }
        
        //Film selection
        final Button lFilmSelectButton = (Button) findViewById(R.id.select_film_btn);
        lFilmSelectButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	selectFilm();
            }
        });
        
        //Film id display
        mFilmUserId = (TextView) findViewById(R.id.film_printed);
        if((Print)mCurrent != null) {
        	mFilmId = ((Print)mCurrent).getFilmId();
        } else {
        	mFilmId = getIntent().getLongExtra("film_id", -1);
        	mFilmFrameId = getIntent().getLongExtra("filmframe_id", -1);
        }
        
        if(mFilmId > 0) {
        	final String lFilmUserId = mDarkroomDBAdapter.getFilm(mFilmId).getUserId();
    		mFilmUserId.setText(lFilmUserId);
    		if(mCurrent != null)
    			((Print)mCurrent).setFilmUserId(lFilmUserId);
        	mIdDisplay.setVisibility(View.VISIBLE);
        	if(mFilmFrameId > 0) {
        		int lFrame = mDarkroomDBAdapter.getFilmFrame(mFilmFrameId).getFrameNumber();
        		mFrameNumberPicker.setCurrent(lFrame);
        	}
        } else {
    		mFilmUserId.setText("");
        }
        
        //Print size
        mUnitsSpinner = (Spinner) findViewById(R.id.units_spinner);
        mUnitsAdapter = ArrayAdapter.createFromResource(AddNewPrintDialog.this, R.array.units, android.R.layout.simple_spinner_item);
        mUnitsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mUnitsSpinner.setAdapter(mUnitsAdapter);
        mUnitsSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
				updateUnits();
			}

			public void onNothingSelected(AdapterView<?> mArg0) {
				// Do nothing
			}
		});
        mUnitsSpinner.setSelection( mUnitsAdapter.getPosition(DarkroomManagerPrefs.getDefaultUnits(this)) );
        mOldUnit = (String) mUnitsSpinner.getSelectedItem();
        
        mSizeH = (DBEditText) findViewById(R.id.size_h);
        mSizeH.setOnChangedListener(new OnChangedListener() {
			public void changed(EditText inView) {
				updateSize();
			}
		});
        mSizeV = (DBEditText) findViewById(R.id.size_v);
        mSizeV.setOnChangedListener(new OnChangedListener() {
			public void changed(EditText inView) {
				updateSize();
			}
		});
        
        if(mCurrent != null /*&& mCurrent.getId() > 0*/) { //If the print is already defined
        	float lSize = ((Print)mCurrent).getSizeH((String) mUnitsSpinner.getSelectedItem());
        	if(lSize == 0)
        		lSize = DarkroomManagerPrefs.getDefaultSizeH(this, (String) mUnitsSpinner.getSelectedItem());
        	mSizeH.setText(Float.toString(lSize));
        	
        	lSize = ((Print)mCurrent).getSizeV((String) mUnitsSpinner.getSelectedItem());
        	if(lSize == 0)
        		lSize = DarkroomManagerPrefs.getDefaultSizeV(this, (String) mUnitsSpinner.getSelectedItem());
        	mSizeV.setText(Float.toString(lSize));
        }else {
        	mSizeH.setText(Float.toString(DarkroomManagerPrefs.getDefaultSizeH(this, (String) mUnitsSpinner.getSelectedItem())));
        	mSizeV.setText(Float.toString(DarkroomManagerPrefs.getDefaultSizeV(this, (String) mUnitsSpinner.getSelectedItem())));
        }
        
		
        //Create a new print if needed
		if((Print)mCurrent == null) {
			//Create new print
			Calendar calendar = new GregorianCalendar(mYear, mMonth, mDay);
	        Date lDate = calendar.getTime();
	        setIdNumberPickerRange(mFilmId, mFrameNumberPicker.getCurrent());
	        mCurrent = new Print(mFilmId,mFrameNumberPicker.getCurrent(),mIdNumberPicker.getCurrent(),lDate);
	        if(mFilmId > 0)
	        	((Print)mCurrent).setFilmUserId(mFilmUserId.getText().toString());
	        updateSize();
	        setDefaults();
		} else if(mCurrent.getId() == -1) {
			//Fill in the basic print info
			Calendar calendar = new GregorianCalendar(mYear, mMonth, mDay);
	        Date lDate = calendar.getTime();
	        setIdNumberPickerRange(mFilmId, mFrameNumberPicker.getCurrent());
	        mCurrent.setIdNumber(mIdNumberPicker.getCurrent());
	        mCurrent.setDate(lDate);
	        ((Print)mCurrent).setFilmFrameNumber(mFrameNumberPicker.getCurrent());
	        ((Print)mCurrent).setFilmId(mFilmId);
	        updateSize();
	        setDefaults();
		}
			
		updateID();
	}
	
	@Override
	protected void loadData() {
		//Already taken care of
	}
	
	protected void setDefaults() {
		((Print)mCurrent).setDeveloper(DarkroomManagerPrefs.getDefaultPaperDeveloper(this));
		((Print)mCurrent).setPaper(DarkroomManagerPrefs.getDefaultPaper(this));
		((Print)mCurrent).setToner(DarkroomManagerPrefs.getDefaultToner(this));
		((Print)mCurrent).setCollectionId(new ArrayList<Long>()); //Collection shouldn't be carried over
	}
	

	@Override
	public void onClick(View v) {
		try {
			//Flush focus from edit text to trigger onFocusChanged
			v.setFocusableInTouchMode(true);
        	v.requestFocus();
        	
        	if(((Print)mCurrent).getFilmId() <= 0) {
        		ScreenUtil.displayMessage(AddNewPrintDialog.this, "A film should be selected!", Toast.LENGTH_SHORT);
        		return;
        	}
        	
        	if( ( TextUtils.isEmpty(mSizeH.getText().toString()) ) || ( ((Print)mCurrent).getSizeH() == 0) ) {
        		mSizeH.requestFocus();
        		ScreenUtil.displayMessage(AddNewPrintDialog.this, "Print size should be defined!", Toast.LENGTH_SHORT);
        		return;
        	}
        	
        	if( ( TextUtils.isEmpty(mSizeV.getText().toString()) ) || ( ((Print)mCurrent).getSizeV() == 0) ) {
        		mSizeV.requestFocus();
        		ScreenUtil.displayMessage(AddNewPrintDialog.this, "Print size should be defined!", Toast.LENGTH_SHORT);
        		return;
        	}
			
			if(isEditing()) {
				if(mUpdateExposure)
					mDarkroomDBAdapter.updatePrintValue((Print)mCurrent, false);
				else
					mDarkroomDBAdapter.updatePrintValue((Print)mCurrent, true);
			} else {
				((Print)mCurrent).setId( mDarkroomDBAdapter.insertPrint((Print)mCurrent) );
				//Create a new film frame if it doesn't exist
				long lFrameId = mDarkroomDBAdapter.getFilmFrameId(((Print)mCurrent).getFilmId(), ((Print)mCurrent).getFilmFrameNumber());
				if( lFrameId < 0) {
					//Create a new frame
					mDarkroomDBAdapter.insertFilmFrame(new FilmFrame(((Print)mCurrent).getFilmId(), ((Print)mCurrent).getFilmFrameNumber()));
				}

			}
			setResult(RESULT_OK, new Intent().putExtra("print_id",((Print)mCurrent).getId()));
        	finish();
		} catch (SQLiteConstraintException e) {
			Logger.e(e.getMessage(),e);
			ScreenUtil.displayMessage(AddNewPrintDialog.this, "Print " + ((Print)mCurrent).getUserId() + " already exist", Toast.LENGTH_SHORT);
		}
	}
	
	@Override
	protected Dialog onCreateDialog(int id) {
	    Dialog dialog = super.onCreateDialog(id);
	    if(dialog == null) {
		    switch(id) {
		    case DIALOG_CONFIRM_ADDNEW:
		    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
		    	builder.setMessage(R.string.confirm_new_print_with_same_expo)
		    	       .setCancelable(false)
		    	       .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
		    	           public void onClick(DialogInterface dialog, int id) {
		    	        	   postinitialize();
		    	           }
		    	       })
		    	       .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
		    	           public void onClick(DialogInterface dialog, int id) {
		    	        	   AddNewPrintDialog.this.finish();
		    	           }
		    	       });
		    	dialog = builder.create();
		        break;
		    default:
		        dialog = null;
		    }
	    }
	    return dialog;
	}
	
	void selectFilm() {
		Intent lIntent = new Intent(this, FilmManager.class);
		lIntent.putExtra(Manager.SELECTONLY, true);
		startActivityForResult(lIntent, REQUEST_SELECT_FILM);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case REQUEST_SELECT_FILM:
			if (resultCode == RESULT_OK) {
				mFilmId = data.getLongExtra(Manager.SELECTION, -1);
				((Print)mCurrent).setFilmId(mFilmId);
				final String lFilmUserId = mDarkroomDBAdapter.getFilm(((Print)mCurrent).getFilmId()).getUserId();
				mFilmUserId.setText(lFilmUserId);
				((Print)mCurrent).setFilmUserId(lFilmUserId);
				setIdNumberPickerRange(mFilmId, mFrameNumberPicker.getCurrent());
				mIdDisplay.setVisibility(View.VISIBLE);
				Logger.v("Selected film : " + mFilmId);
				updateID();
			}
			break;
		}
	}
	
	/**
	 * Looks for all film with the same date and camera. 
	 * Return the day number that is currently used.
	 */
	protected ArrayList<Integer> findUsedIdNumber(long inFilmId, int inFrame) {
		ArrayList<Integer> lUsedIdNumber = new ArrayList<Integer>();
		
				
		Cursor lCursor = mDarkroomDBAdapter.getAllPrintCursor(
				new String[] {DarkroomDBHelper.KEY_PRINT_IDNUM}, 
				DarkroomDBHelper.KEY_PRINT_FILMID + "=" + inFilmId + " and " 
				+ DarkroomDBHelper.KEY_PRINT_FRAMEID + "=" + inFrame, 
				null,null);
		
		if (lCursor.moveToFirst()) {
	        final int lIdNumIdx = lCursor.getColumnIndex(DarkroomDBHelper.KEY_PRINT_IDNUM); 
	        do {
	        	lUsedIdNumber.add(lCursor.getInt(lIdNumIdx));
	        } while (lCursor.moveToNext());
	    }
		
		return lUsedIdNumber;
	}
	
	protected void setIdNumberPickerRange(final long inFilmId, final int inFrame) {
		ArrayList<Integer> lUsedDayNumber = findUsedIdNumber(inFilmId, inFrame);
		
		Integer lMinIdx;
		for(lMinIdx = new Integer(1); ; ++lMinIdx) {
			if( !lUsedDayNumber.contains( lMinIdx ) )
				break;
		}
		mIdNumberPicker.setInvalidNumbers(lUsedDayNumber);
		mIdNumberPicker.setCurrent(lMinIdx);
		if(mCurrent!=null)
			mCurrent.setIdNumber(lMinIdx);
		updateID();
		//TODO don't behave correctly
	}
	
	@Override
	protected void updateIdNumberPickerRange() {
		setIdNumberPickerRange(mFilmId, mFrameNumberPicker.getCurrent());
	}

	protected void updateSize() {
		float lSizeH, lSizeV;
		try{
			if(mSizeH.getText().toString().equals(""))
				lSizeH = 0;
			else
				lSizeH = Float.valueOf(mSizeH.getText().toString());
		} catch(NumberFormatException e) {
			Logger.e(e.getMessage(),e);
			ScreenUtil.displayMessage(AddNewPrintDialog.this, "Invalid horizontal size!", Toast.LENGTH_SHORT);
			return;
		}
		try{
			if(mSizeV.getText().toString().equals(""))
				lSizeV = 0;
			else
				lSizeV = Float.valueOf(mSizeV.getText().toString());
		} catch(NumberFormatException e) {
			Logger.e(e.getMessage(),e);
			ScreenUtil.displayMessage(AddNewPrintDialog.this, "Invalid vertical size!", Toast.LENGTH_SHORT);
			return;
		}
		
		if(mCurrent != null) {
			((Print)mCurrent).setSize(lSizeH, lSizeV, (String)mUnitsSpinner.getSelectedItem());
		}
			
	}
	
	protected void updateUnits() {
		if(isEditing()) {
			//Update value only if in editing mode
			final String lNewUnit = (String)mUnitsSpinner.getSelectedItem();
			if(!mOldUnit.equals(lNewUnit)) {
				if(!TextUtils.isEmpty(mSizeH.getText().toString())) {
					final float lCurrentH = Float.valueOf(mSizeH.getText().toString());
					final float lNewH = Unit.convertFromMM(Unit.convertToMM(lCurrentH, mOldUnit), lNewUnit);;
					mSizeH.setText(String.valueOf(lNewH));
				}
				
				if(!TextUtils.isEmpty(mSizeV.getText().toString())) {
					final float lCurrentV = Float.valueOf(mSizeV.getText().toString());
					final float lNewV = Unit.convertFromMM(Unit.convertToMM(lCurrentV, mOldUnit), lNewUnit);;
					mSizeV.setText(String.valueOf(lNewV));
				}
				
				mOldUnit = lNewUnit;
			}
		}
	}
}
