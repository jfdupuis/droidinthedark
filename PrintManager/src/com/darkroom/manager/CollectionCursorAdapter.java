package com.darkroom.manager;

import com.darkroom.library.dao.DarkroomDBHelper;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;

public class CollectionCursorAdapter extends SimpleCursorAdapter {

	    public CollectionCursorAdapter (Context mContext, int mLayout, Cursor c, String[] from, int[] to) {
	        super(mContext, mLayout, c, from, to);
	    }

//	    @Override
//	    public View newView(Context inContext, Cursor inCursor, ViewGroup inParent) {
//	    	View lNewView = super.newView(inContext, inCursor, inParent);
//	        return lNewView;
//	    }

	    @Override
	    public void bindView(View inView, Context inContext, Cursor inCursor) {
	    	super.bindView(inView, inContext, inCursor);
	    	
	    	//Format date display
	    	TextView lDateView = (TextView) inView.findViewById(R.id.date);
	        Date lDate = new Date(inCursor.getLong(inCursor.getColumnIndex(DarkroomDBHelper.KEY_COLLECTION_DATE)));
	        
	        Calendar lCalendar = Calendar.getInstance();
	        lCalendar.setTime(lDate);
	        String dateString = String.format("%tF", lCalendar);
	        lDateView.setText(dateString);

	    }
}
