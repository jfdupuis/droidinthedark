package com.darkroom.manager;

import com.darkroom.library.Licensing;
import com.jfdupuis.library.screen.ScreenUtil;
import com.darkroom.manager.dao.ManagerDBAdapter;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.TextView;
import android.widget.Toast;

abstract public class Manager extends ListActivity {
	static protected final int ADD_ITEM = 0;
	static protected final int REMOVE_ITEM = 1;
	static protected final int EDIT_ITEM = 2;
	
	static protected final int DEMO_SIZE_LIMIT = 5;
	static protected final boolean IS_DEMO = false;
	
	//Manager intent action request
	static public final String SELECTONLY = "select_only";
	static public final String SELECTION = "selection";
	static public final String EMBEDDED = "embedded";
	
	protected ManagerDBAdapter mDarkroomDBAdapter;
	protected Cursor mCursor;
	protected boolean mSelectOnly = false;
	protected boolean mEmbedded = false; //Indicate if the print manager is used within another tab
	
	abstract protected void addNewItem();
	abstract protected void removeItem(int inPosition, long inId);
	abstract protected void editItem(int inIndex, long inId);
	abstract protected void populateList();
	
	public void onCreate(Bundle inBundle) {
		super.onCreate(inBundle);

		// Set result CANCELED in case the user backs out
        setResult(Activity.RESULT_CANCELED);
        final Intent lIntent = getIntent();
        mEmbedded = lIntent.getBooleanExtra(Manager.EMBEDDED, false);
        mSelectOnly = lIntent.getBooleanExtra(SELECTONLY,false);
        
		mDarkroomDBAdapter = new ManagerDBAdapter(this);
		mDarkroomDBAdapter.open();
		
		TextView lEmpty = new TextView(Manager.this);
		lEmpty.setText("No entry");
		lEmpty.setVisibility(View.GONE);
		((ViewGroup)getListView().getParent()).addView(lEmpty);
		getListView().setEmptyView(lEmpty);
		
		registerForContextMenu(getListView());
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();

		// Close the database
		mDarkroomDBAdapter.close();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		Logger.d("Creating option menu");
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.manager, menu);
	    return true;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);

		int lIndex = getSelectedItemPosition();

		//Set visibility according to selection
		MenuItem removeItem = menu.findItem(R.id.remove);
		removeItem.setVisible(lIndex > -1);

		MenuItem editItem = menu.findItem(R.id.edit);
		editItem.setVisible(lIndex > -1);
		
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int lIndex = getSelectedItemPosition();
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
		
	    switch (item.getItemId()) {
	    case R.id.add:
	    	addNewItem();
	        return true;
	    case R.id.edit:
	    	editItem(lIndex, info.id);
	        return true;
	    case R.id.remove:
	    	removeItem(lIndex, info.id);
	        return true;
	    default:
	        return super.onOptionsItemSelected(item);
	    }
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
	  super.onCreateContextMenu(menu, v, menuInfo);
	  Logger.d("Creating context menu");
	  MenuInflater inflater = getMenuInflater();
	  inflater.inflate(R.menu.manager_context, menu);
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
		switch (item.getItemId()) {
		case R.id.context_edit:
			editItem(info.position, info.id);
			return true;
		case R.id.context_delete:
			removeItem(info.position, info.id);
			return true;
		default:
			return super.onContextItemSelected(item);
		}
	}
	
//	protected void selectItem(int inPosition, long inId) {
//		
//		Logger.d("Selecting item id: " + inId);
//		
//		Intent lIntent = new Intent();
//		lIntent.putExtra(SELECTION, inId);
//		setResult(Activity.RESULT_OK,lIntent);
//		finish();
//	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case ADD_ITEM:
		case EDIT_ITEM:
		case REMOVE_ITEM:
			mCursor.requery();
			break;
		}
	}
	
	protected boolean isDemoLimitReach() {
		if(this.getListAdapter().getCount() >= DEMO_SIZE_LIMIT && Licensing.isDemo()) {
			ScreenUtil.displayMessage(this, "Limits of the demo application have been reached.", Toast.LENGTH_LONG);
			return true;
		}
		return false;
	}
}
