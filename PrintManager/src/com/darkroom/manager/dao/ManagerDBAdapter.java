package com.darkroom.manager.dao;

import com.darkroom.library.Agitation;
import com.darkroom.library.dao.DarkroomDBAdapter;
import com.darkroom.library.dao.StringData;
import com.darkroom.manager.Logger;
import com.darkroom.manager.preferences.DarkroomManagerPrefs;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

public class ManagerDBAdapter extends DarkroomDBAdapter {

	public ManagerDBAdapter(Context inContext) {
		super(inContext);
	}
	
	@Override
	public void onCreate(SQLiteDatabase inDatabase) {
		super.onCreate(inDatabase);
		Logger.d("ManagerDBAdapter.onCreate");
		
		Logger.w("Creating darkroom database version " + mDatabaseVersion);
		
		inDatabase.execSQL(CREATE_PRINT_TABLE);
		inDatabase.execSQL(CREATE_EXPOSURE_TABLE);
		inDatabase.execSQL(CREATE_EXPOSUREITEM_TABLE);
		inDatabase.execSQL(CREATE_FILM_TABLE);
		inDatabase.execSQL(CREATE_FILMFRAME_TABLE);
		inDatabase.execSQL(CREATE_FILMFRAME_EXPOSURE_TABLE);
		inDatabase.execSQL(CREATE_CAMERA_TABLE);

		inDatabase.execSQL(CREATE_PAPER_TABLE);
		inDatabase.execSQL(CREATE_CHEMICAL_TABLE);
		inDatabase.execSQL(CREATE_AGITATION_TABLE);
		inDatabase.execSQL(CREATE_EMULSION_TABLE);
		inDatabase.execSQL(CREATE_COLLECTION_TABLE);
		inDatabase.execSQL(CREATE_COLLECTIONPRINT_TABLE);
		
		inDatabase.execSQL(CREATE_DEVTIME_NOUNIQUE_TABLE);
		
		inDatabase.execSQL(CREATE_LENS_TABLE);
		inDatabase.execSQL(CREATE_FILTER_TABLE);
		inDatabase.execSQL(CREATE_EXPOSED_FILTER_TABLE);
		inDatabase.execSQL(CREATE_DILUTION_TABLE);
		
		//Ensure 1 index for all not specified
		insertNullChemical(inDatabase);
		insertNullEmulsion(inDatabase);
		insertNullPaper(inDatabase);
		insertNullLens(inDatabase);
		insertNullAgitation(inDatabase);
		insertNullStringData(inDatabase, DILUTION_TABLE_NAME, new StringData(NOT_SPECIFIED, null)); //used for compatibility with old db
		insertNullStringData(inDatabase, DILUTION_TABLE_NAME, new StringData("Stock", null));
		
		
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase inDatabase, int _oldVersion, int _newVersion) {
		Logger.d("ManagerDBAdapter.onUpgrade");
		// Log the version upgrade.
		Logger.w("Upgrading from version " + _oldVersion + " to " + _newVersion);

		//Data base version is set in DarkroomDBHelper
		
		//TODO change initially created agitation with "Agitation.NOTSPECIFIEDKEY" as interval
		
		if( (_oldVersion == 1 || _oldVersion == 0) && _newVersion == 3) {
			
			backupDatabase(_oldVersion, inDatabase.getPath());
			inDatabase.beginTransaction();
			try {
				
				//Create new table
				inDatabase.execSQL(CREATE_LENS_TABLE);
				inDatabase.execSQL(CREATE_FILTER_TABLE);
				inDatabase.execSQL(CREATE_EXPOSED_FILTER_TABLE);
				inDatabase.execSQL(CREATE_FILMFRAME_EXPOSURE_TABLE);
				inDatabase.execSQL(CREATE_DILUTION_TABLE);
				inDatabase.execSQL(CREATE_DEVTIME_NOUNIQUE_TABLE);
				
				//Modified film table
				//Old keys
				final String KEY_FILM_DEVID = "developper"; //Developer used
				final String KEY_FILM_DEV_NOTES = "dev_notes"; //General development notes
				final String KEY_FILM_DEV_TEMP = "dev_temp"; //Developer temperature [C]
				final String KEY_FILM_DEV_TIME = "dev_time"; //Development time [sec]
				final String KEY_FILM_DEV_AGITATIONID = "dev_agi"; //Development agitation pattern
				
				//Move development data to EMULSION_DEVTIME_TABLE_NAME
				final String DFLT_DEV_TABLE_NAME = "tmp_values"; 
				inDatabase.execSQL("create table " + DFLT_DEV_TABLE_NAME + " ("
						+ KEY_ID + " integer primary key autoincrement, "
						+ KEY_DEVTIME_DILUTIONID + " integer not null, "
						+ KEY_DEVTIME_N + " integer not null, "
						+ KEY_DEVTIME_EMULSIONFORMAT + " text not null, "
						+ KEY_DEVTIME_SOURCE + " text not null );");
				
				ContentValues newValues = new ContentValues();
			    newValues.put(KEY_DEVTIME_DILUTIONID, -1); //Not specified
			    newValues.put(KEY_DEVTIME_N, 0); 
			    newValues.put(KEY_DEVTIME_EMULSIONFORMAT, NOT_SPECIFIED);
			    newValues.put(KEY_DEVTIME_SOURCE, "");
				inDatabase.insert(DFLT_DEV_TABLE_NAME, null, newValues);
				
				//Order is important
				inDatabase.execSQL("INSERT INTO " + EMULSION_DEVTIME_TABLE_NAME + "("
						+ KEY_ID + ", "
						+ KEY_DEVTIME_EMULSIONID + ", "
						+ KEY_DEVTIME_DEVELOPERID + ", "
						+ KEY_DEVTIME_AGITATIONID + ", "
						+ KEY_DEVTIME_DILUTIONID + ", "
						+ KEY_DEVTIME_SHOTISO + ", "
						+ KEY_DEVTIME_N + ", "
						+ KEY_DEVTIME_TEMP + ", "
						+ KEY_DEVTIME_EMULSIONFORMAT + ", "
						+ KEY_DEVTIME_SOURCE + ", "
						+ KEY_DEVTIME_TIME + ", "
						+ KEY_DEVTIME_NOTE + " "
						+ ") SELECT "
						+ FILM_TABLE_NAME + "." + KEY_ID + ", "
						+ KEY_FILM_EMULSIONID + ", "
						+ KEY_FILM_DEVID + ", "
						+ KEY_FILM_DEV_AGITATIONID + ", "
						+ KEY_DEVTIME_DILUTIONID + ", "
						+ KEY_FILM_SHOTISO + ", "
						+ KEY_DEVTIME_N + ", "
						+ KEY_FILM_DEV_TEMP + ", "
						+ KEY_DEVTIME_EMULSIONFORMAT + ", "
						+ KEY_DEVTIME_SOURCE + ", "
						+ KEY_FILM_DEV_TIME + ", "
						+ KEY_FILM_DEV_NOTES + " "
						+ "FROM " + FILM_TABLE_NAME + " JOIN " + DFLT_DEV_TABLE_NAME + ";");
						
				//Remove copy to new film table
				Logger.w("Dropping unused film columns"); //ALTER don't support drop column on android
				final String FILM_TABLE_NAME_TMP = FILM_TABLE_NAME + "_tmp";
				inDatabase.execSQL("ALTER TABLE " + FILM_TABLE_NAME + " rename to " + FILM_TABLE_NAME_TMP + ";");
				inDatabase.execSQL(CREATE_FILM_TABLE);
				inDatabase.execSQL("INSERT INTO " + FILM_TABLE_NAME + " (" + ALL_FILM_TABLE_KEY	+ ") " 
						+ " SELECT " 
						+ FILM_TABLE_NAME_TMP + "." + KEY_ID + ", " + KEY_FILM_USERID + ", " + KEY_FILM_USERID_ISCUSTOM 
						+ ", " + KEY_FILM_NAME + ", " + KEY_FILM_DESC + ", " + FILM_TABLE_NAME_TMP + "." + KEY_FILM_EMULSIONID 
						+ ", " + FILM_TABLE_NAME_TMP + "." + KEY_FILM_SHOTISO + ", " + KEY_FILM_CAMERAID + ", " + KEY_FILM_DATE 
						+ ", " + KEY_FILM_DATENUM + ", " + FILM_TABLE_NAME_TMP + "." + KEY_ID + " AS " + KEY_FILM_DEVELOPMENTID 
						+ " FROM " + FILM_TABLE_NAME_TMP 
						+ ";"); //Repeat _id as they where moved to the other table with same id.
				inDatabase.execSQL("DROP TABLE IF EXISTS " + FILM_TABLE_NAME_TMP);
				
				
				//Modified film frame table
				Logger.w("Adding date to film frame, with default -1.");
				inDatabase.execSQL("ALTER TABLE " + FILMFRAME_TABLE_NAME + " ADD " + KEY_FILMFRAME_DATE + " integer not null DEFAULT \'-1\';" );
//				Logger.w("Adding exposure id to film frame, with default -1.");
//				inDatabase.execSQL("ALTER TABLE " + FILMFRAME_TABLE_NAME + " ADD " + KEY_FILMFRAME_EXPOSUREID + " INTEGER NOT NULL REFERENCES " + FILMFRAME_EXPOSURE_TABLE_NAME + " (" + KEY_ID + ")" + " DEFAULT \'-1\';" );
				Logger.w("Adding picture path to film frame, with default empty.");
				inDatabase.execSQL("ALTER TABLE " + FILMFRAME_TABLE_NAME + " ADD " + KEY_FILMFRAME_PICTURE_PATH + " text not null " + " DEFAULT \'\';");
				
				//Modified agitation
				Logger.w("Creating new agitation table.");
				//Rename old table name to temporary table
				String AGITATION_TABLE_NAME_TMP = AGITATION_TABLE_NAME + "_old";
				inDatabase.execSQL("ALTER TABLE " + AGITATION_TABLE_NAME + " rename to " + AGITATION_TABLE_NAME_TMP + ";");
				//Create a new table with new unique constraint
				inDatabase.execSQL(CREATE_AGITATION_TABLE);
				//Copy data from one table to the other
				Cursor lCursor = inDatabase.query(AGITATION_TABLE_NAME_TMP, new String[]{KEY_ID,KEY_STRING}, null, null, null, null, null);
				if(lCursor.getCount() > 0 && lCursor.moveToFirst()) {
					Logger.w("Moving data to the new agitation table.");
					int lInvalidKey = -1;
					do {
						Agitation lAgitation = new Agitation(lInvalidKey--, 0, 0); //Decrement invalid key to ensure unique 
						lAgitation.setDescription(lCursor.getString(lCursor.getColumnIndex(KEY_STRING)));
						newValues = new ContentValues();
						newValues.put(KEY_ID, lCursor.getString(lCursor.getColumnIndex(KEY_ID)));
					    newValues.put(KEY_STRING, lAgitation.toString());
					    newValues.put(KEY_CATEGORY, ""); 
					    newValues.put(KEY_AGITATION_INITIAL, lAgitation.getInitial());
					    newValues.put(KEY_AGITATION_INTERVAL, lAgitation.getInterval());
					    newValues.put(KEY_AGITATION_DURATION, lAgitation.getDuration());
						inDatabase.insertOrThrow(AGITATION_TABLE_NAME , null, newValues);
					} while(lCursor.moveToNext());
				}
				Logger.w("Deleteing the old agitation table.");
				inDatabase.execSQL("DROP TABLE IF EXISTS " + AGITATION_TABLE_NAME_TMP);
				
				//Insert special agitation pattern
				Logger.d("Inserting null agitation");
				newValues = new ContentValues();
				newValues.put(KEY_STRING, Agitation.NOTSPECIFIED);
				newValues.put(KEY_AGITATION_INITIAL, "0");
				newValues.put(KEY_AGITATION_INTERVAL, Agitation.NOTSPECIFIEDKEY);
				newValues.put(KEY_AGITATION_DURATION, "0");
				newValues.put(KEY_CATEGORY, "");
				inDatabase.update(AGITATION_TABLE_NAME, newValues, KEY_ID + "=1", null);
			    //long lId = inDb.insertOrThrow(AGITATION_TABLE_NAME, null, newValues);
				
			    newValues = new ContentValues();
				newValues.put(KEY_STRING, Agitation.NOAGITATION);
				newValues.put(KEY_AGITATION_INITIAL, "0");
				newValues.put(KEY_AGITATION_INTERVAL, "-1");
				newValues.put(KEY_AGITATION_DURATION, "0");
				newValues.put(KEY_CATEGORY, "");
				inDatabase.insertOrThrow(AGITATION_TABLE_NAME, null, newValues);
			    
			    newValues = new ContentValues();
				newValues.put(KEY_STRING, Agitation.CONTINUOUS);
				newValues.put(KEY_AGITATION_INITIAL, "0");
				newValues.put(KEY_AGITATION_INTERVAL, "0");
				newValues.put(KEY_AGITATION_DURATION, "0");
				newValues.put(KEY_CATEGORY, "");
				inDatabase.insertOrThrow(AGITATION_TABLE_NAME, null, newValues);
				
				
				//Insert null item
				insertNullLens(inDatabase);
				insertNullStringData(inDatabase, DILUTION_TABLE_NAME, new StringData(NOT_SPECIFIED, null)); //used for compatibility with old db
				insertNullStringData(inDatabase, DILUTION_TABLE_NAME, new StringData("Stock", null));
				
				inDatabase.setTransactionSuccessful();
			} catch(Exception e) {
				Logger.e("Can't upgrade database", e);
			} finally {
				inDatabase.endTransaction();
			}
			
			return;
		} else {
			throw new RuntimeException("Unknown database version, old="+_oldVersion+" new="+_newVersion+"!");
			//Create a new one.
			// The simplest case is to drop the old table and create a new one.
//			inDatabase.execSQL("DROP TABLE IF EXISTS " + PRINT_TABLE_NAME);
//			inDatabase.execSQL("DROP TABLE IF EXISTS " + EXPOSURE_TABLE_NAME);
//			inDatabase.execSQL("DROP TABLE IF EXISTS " + EXPOSUREITEM_TABLE_NAME);
//			inDatabase.execSQL("DROP TABLE IF EXISTS " + PAPER_TABLE_NAME);
//			inDatabase.execSQL("DROP TABLE IF EXISTS " + FILM_TABLE_NAME);
//			inDatabase.execSQL("DROP TABLE IF EXISTS " + FILMFRAME_TABLE_NAME);
//			inDatabase.execSQL("DROP TABLE IF EXISTS " + CAMERA_TABLE_NAME);
//			inDatabase.execSQL("DROP TABLE IF EXISTS " + CHEMICAL_TABLE_NAME);
//			inDatabase.execSQL("DROP TABLE IF EXISTS " + AGITATION_TABLE_NAME);
//			inDatabase.execSQL("DROP TABLE IF EXISTS " + EMULSION_TABLE_NAME);
//			inDatabase.execSQL("DROP TABLE IF EXISTS " + COLLECTION_TABLE_NAME);
//			inDatabase.execSQL("DROP TABLE IF EXISTS " + COLLECTIONPRINT_TABLE_NAME);
//			inDatabase.execSQL("DROP TABLE IF EXISTS " + CREATE_DEVTIME_TABLE);
//			onCreate(inDatabase);
		}
	}
	
	boolean backupDatabase(int inVersion, String inDatabasePath) {
		
		String mmNewDatabasePath;
		File dbFile = new File(inDatabasePath);

		mmNewDatabasePath = Environment.getExternalStorageDirectory() + "/" + DarkroomManagerPrefs.SDCARD_DIR;

		File exportDir = new File(mmNewDatabasePath, "");
		if (!exportDir.exists()) {
			exportDir.mkdirs();
		}

		File file = new File(exportDir, "DarkroomNoteKeeper-ver"+inVersion + "." + DarkroomManagerPrefs.DATABASE_BKP_EXT);
		if(file.exists()) {
			Logger.w("Backup file already exist");
			return true;
		}

		try {
			file.createNewFile();
			copyFile(dbFile, file);
			return true;
		} catch (IOException e) {
			Logger.e("Problem while exporting the database to sdcard", e);
			return false;
		} 
	}

	void copyFile(File src, File dst) throws IOException {
		FileChannel inChannel = new FileInputStream(src).getChannel();
		FileChannel outChannel = new FileOutputStream(dst).getChannel();
		try {
			inChannel.transferTo(0, inChannel.size(), outChannel);
		} finally {
			if (inChannel != null)
				inChannel.close();
			if (outChannel != null)
				outChannel.close();
		}
	}

}
