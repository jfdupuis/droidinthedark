package com.darkroom.manager.widget;

import com.jfdupuis.library.math.Fraction;

import android.content.Context;

import com.jfdupuis.library.widget.wheel.adapters.AbstractWheelTextAdapter;

public class FractionWheelAdapter extends AbstractWheelTextAdapter {
		/** The default min value */
	    public static final Fraction DEFAULT_MAX_VALUE = new Fraction(9);

	    /** The default max value */
	    private static final Fraction DEFAULT_MIN_VALUE = new Fraction(0);
	    
	    /** The default increment value */
	    private static final Fraction DEFAULT_INC_VALUE = new Fraction((long)1,(long)2);
	    
	    // Values
	    protected Fraction minValue;
	    protected Fraction maxValue;
	    protected Fraction incrementValue;
	    
	    // format
	    protected String format;
	    
	    /**
	     * Constructor
	     * @param context the current context
	     */
	    public FractionWheelAdapter(Context context) {
	        this(context, DEFAULT_MIN_VALUE, DEFAULT_MAX_VALUE, DEFAULT_INC_VALUE);
	    }

	    /**
	     * Constructor
	     * @param context the current context
	     * @param minValue the wheel min value
	     * @param maxValue the wheel max value
	     */
	    public FractionWheelAdapter(Context context, Fraction minValue, Fraction maxValue, Fraction increment) {
	        this(context, minValue, maxValue, increment, null);
	    }

	    /**
	     * Constructor
	     * @param context the current context
	     * @param minValue the wheel min value
	     * @param maxValue the wheel max value
	     * @param format the format string
	     */
	    public FractionWheelAdapter(Context context, Fraction minValue, Fraction maxValue, Fraction increment, String format) {
	        super(context);
	        
	        this.minValue = minValue;
	        this.maxValue = maxValue;
	        this.incrementValue = increment;
	        this.format = format;
	    }
	    
	    public Fraction getValue(int inIndex) {
	    	return minValue.plus(incrementValue.times(inIndex));
	    }
	    
	    public int getValueIndex(Fraction inValue) {
	    	return (int)((inValue.minus(minValue)).dividedBy(incrementValue).asDouble());
	    }

	    @Override
	    public CharSequence getItemText(int index) {
	        if (index >= 0 && index < getItemsCount()) {
	            Fraction value = getValue(index);
	            return format != null ? String.format(format, value) : value.toString();
	        }
	        return null;
	    }

	    @Override
	    public int getItemsCount() {
	        return (int) ((maxValue.minus(minValue)).dividedBy(incrementValue).asDouble()) + 1;
	    } 
}
