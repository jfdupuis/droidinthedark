package com.darkroom.manager.widget;

import com.jfdupuis.library.math.Fraction;

import android.content.Context;

import com.jfdupuis.library.widget.wheel.adapters.ArrayWheelAdapter;

public class ShutterWheelAdapter extends ArrayWheelAdapter<Fraction> {
		
	final static public Fraction[] ShutterFull = {
		new Fraction((long)1800),
		new Fraction((long)1200),
		new Fraction((long)600),
		new Fraction((long)240),
		new Fraction((long)120),
		new Fraction((long)60),
		new Fraction((long)30),
		new Fraction((long)20),
		new Fraction((long)15),
		new Fraction((long)10),
		new Fraction((long)8), 
		new Fraction((long)4),
		new Fraction((long)2),
		new Fraction((long)1),
		new Fraction((long)1,(long)2),
		new Fraction((long)1,(long)4),
		new Fraction((long)1,(long)8),
		new Fraction((long)1,(long)15),
		new Fraction((long)1,(long)30),
		new Fraction((long)1,(long)60),
		new Fraction((long)1,(long)125),
		new Fraction((long)1,(long)250),
		new Fraction((long)1,(long)500),
		new Fraction((long)1,(long)1000)};
	
	
	public ShutterWheelAdapter(Context mContext) {
		super(mContext, ShutterFull);
	}
	
	public ShutterWheelAdapter(Context mContext, Fraction[] mItems) {
		super(mContext, mItems);
	}
	

	
	
	public Fraction getValue(int inIndex) {
    	return items[inIndex];
    }
    
    public int getValueIndex(Fraction inValue) {
    	for(int i = 0; i < items.length; ++i) {
    		if(items[i].compareTo(inValue) == 0)
    			return i;
    	}
    	return -1;
    }
    
    /**
     * Get the closest shutter speed index to inValue
     */
    public int getValueIndex(double inValue) {
    	double lMinDiff = Double.MAX_VALUE;
    	double lDiff = Double.MAX_VALUE;
    	int lMinIdx = -1;
    	for(int i = 0; i < ShutterFull.length; ++i) {
    		lDiff = Math.abs(ShutterFull[i].asDouble() - inValue);
    		if( lDiff < lMinDiff) {
    			lMinDiff = lDiff;
    			lMinIdx  =i;
    		}
    	}
    	
    	return lMinIdx;
	}

	@Override
    public CharSequence getItemText(int index) {
        if (index >= 0 && index < items.length) {
            Fraction item = items[index];
            
            if(item.denominator() == 1) {
            	if(item.numerator() >= 60) {
            		return String.format("%.0f min", item.asDouble()/60.0); 
            	}
            }
            
            return item.toString();
            
        }
        return null;
    }
	
}
