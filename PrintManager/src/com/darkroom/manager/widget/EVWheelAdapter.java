package com.darkroom.manager.widget;

import com.jfdupuis.library.math.Fraction;

import android.content.Context;


public class EVWheelAdapter extends FractionWheelAdapter {

	public EVWheelAdapter(Context context, Fraction inIncrements, String inFormat) {
		this(context, new Fraction(0), new Fraction(20), inIncrements, inFormat);
	}
	
	public EVWheelAdapter(Context context, Fraction minValue, Fraction maxValue, Fraction increment, String format) {
		super(context, minValue, maxValue, increment, format);
	}
}
