package com.darkroom.manager.widget;

import com.darkroom.manager.R;
import com.darkroom.manager.dao.ManagerDBAdapter;

import android.content.Context;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.View;

public class ShowInfoPreference extends DialogPreference {

	public ShowInfoPreference(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        
        final String lMessage = String.format(getContext().getString(R.string.info), getContext().getDatabasePath(new ManagerDBAdapter(context).getDataBaseName())) + "\n";
        this.setDialogMessage(lMessage);
    }

    public ShowInfoPreference(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ShowInfoPreference(Context context) {
        this(context, null);
    }
    
    
    @Override
    protected void onBindDialogView(View view) {
        super.onBindDialogView(view);
   
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);
    }
    
}
