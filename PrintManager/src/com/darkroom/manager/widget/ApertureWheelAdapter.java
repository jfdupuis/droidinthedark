package com.darkroom.manager.widget;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jfdupuis.library.widget.wheel.adapters.ArrayWheelAdapter;

public class ApertureWheelAdapter extends ArrayWheelAdapter<Float> {

	final static public Float[] FStopHalf = {1.0f,1.2f,1.4f,1.7f,2f,2.4f,2.8f,3.3f,4f,4.8f,5.6f,6.7f,8f,9.5f,11f,13f,16f,19f,22f,27f,32f,38f,45f,54f,64f};
	final static public Float[] FStopThird = {1.0f,1.1f,1.2f,1.4f,1.6f,1.8f,2f,2.2f,2.5f,2.8f,3.2f,3.5f,4f,4.5f,5.0f,5.6f,6.3f,7.1f,8f,9f,10f,11f,13f,14f,16f,18f,20f,22f,25f,29f,32f,36f,40f,45f,51f,57f,64f};
	
	public static final int FRACTION_COLOR = 0xFF666666;
	public static final int BASE_COLOR = 0xFF000000;
	
	public ApertureWheelAdapter(Context mContext) {
		super(mContext, FStopThird);
	}
	
	public ApertureWheelAdapter(Context mContext, Float[] mItems) {
		super(mContext, mItems);
	}
	
	public Float getValue(int inIndex) {
    	return items[inIndex];
    }
	
	public int getClosestValueIndex(float inValue) {
		float lMinDiff = Float.MAX_VALUE;
		float lDiff = Float.MAX_VALUE;
    	int lMinIdx = -1;
    	for(int i = 0; i < items.length; ++i) {
    		lDiff = Math.abs(items[i] - inValue);
    		if( lDiff < lMinDiff) {
    			lMinDiff = lDiff;
    			lMinIdx  =i;
    		}
    	}
    	
    	return lMinIdx;
	}
    
    public int getValueIndex(Float inValue) {
    	for(int i = 0; i < items.length; ++i) {
    		Float lItem = items[i];
    		if( Math.abs(lItem - inValue) < 1e-3)
    			return i;
    	}
    	return -1;
    }
	
	@Override
    public View getItem(int index, View convertView, ViewGroup parent) {
        if (index >= 0 && index < getItemsCount()) {
            if (convertView == null) {
                convertView = getView(itemResourceId, parent);
            }
            TextView textView = getTextView(convertView, itemTextResourceId);
            if (textView != null) {
                CharSequence text = getItemText(index);
                if (text == null) {
                    text = "";
                }
                textView.setText(text);
    
                if (itemResourceId == TEXT_VIEW_ITEM_RESOURCE) {
                    configureTextView(textView);
                }

                if( ((getItemsCount() == FStopHalf.length) && (index % 2 != 0)) ||
                	((getItemsCount() == FStopThird.length) && (index % 3 != 0)) ) { 
                	textView.setTextColor(FRACTION_COLOR);
                	//textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);
                }
                else {
                	textView.setTextColor(BASE_COLOR);
                	//textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
                }
            }
            return convertView;
        }
    	return null;
    }
}
