package com.darkroom.manager;


import com.darkroom.library.Exposure;
import com.darkroom.library.ExposureItem;
import com.darkroom.library.dao.DarkroomDBAdapter;
import com.jfdupuis.library.math.Fraction;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import java.text.DecimalFormat;

public class ExposureCursorAdapter  extends SimpleCursorAdapter  {
//	private Context mContext;
//    private int mLayout;
    private boolean mUseLinear;
    private DecimalFormat mFormat;
    private Fraction mBaseTime;
    private int mExposureType;

	public ExposureCursorAdapter(Context mContext, int mLayout, Cursor mC, String[] mFrom, int[] mTo) {
		super(mContext, mLayout, mC, mFrom, mTo);
//		this.mContext = mContext;
//        this.mLayout = mLayout;
        
        this.mFormat = new DecimalFormat("0.000");
	}
	
	public void setLinear(boolean inLinear) {
		mUseLinear = inLinear;
	}
	
	public void setFormat(DecimalFormat inFormat) {
		mFormat = inFormat;
	}
	
	public void setBaseTime(Fraction inBaseTime) {
		mBaseTime = inBaseTime;
		this.notifyDataSetChanged();
	}
	
	/**
	 * Set the exposure type, i.e dodge or burn
	 * @param inExposureType DarkroomDBHelper.BURNEXPOSURE or DarkroomDBHelper.DODGEEXPOSURE
	 */
	public void setExposureType(int inExposureType) {
		mExposureType = inExposureType;
	}

//	@Override
//    public View newView(Context inContext, Cursor inCursor, ViewGroup inParent) {
//    	super.newView(inContext, inCursor, inParent);
//    	
//        final LayoutInflater inflater = LayoutInflater.from(mContext);
//        View lView = inflater.inflate(mLayout, inParent, false);
//
//        TextView lPositionField = (TextView) lView.findViewById(R.id.position);
//        final int lPosition = inCursor.getInt(inCursor.getColumnIndex(DarkroomDBAdapter.KEY_EXPOSUREITEM_POSITION));
//        lPositionField.setText(Integer.toString(lPosition));
//
//        TextView lValueField = (TextView) lView.findViewById(R.id.value);
//        final Fraction lValue = new Fraction(inCursor.getString(inCursor.getColumnIndex(DarkroomDBAdapter.KEY_EXPOSUREITEM_VALUE)));
//        if(mUseLinear) {
//        	lValueField.setText(mFormat.format(Exposure.getRelativeTime(mBaseTime, lValue, mExposureType == ExposureItem.BURNEXPOSURE )));
//        }
//        else
//        	lValueField.setText(lValue.toString());
//        
//        TextView lDescField = (TextView) lView.findViewById(R.id.desc);
//        lDescField.setText(inCursor.getString(inCursor.getColumnIndex(DarkroomDBAdapter.KEY_EXPOSUREITEM_DESC)));
//
//        return lView;
//    }

    @Override
    public void bindView(View inView, Context inContext, Cursor inCursor) {
    	super.bindView(inView, inContext, inCursor);
    	
    	TextView lPositionField = (TextView) inView.findViewById(R.id.position);
        final int lPosition = inCursor.getInt(inCursor.getColumnIndex(DarkroomDBAdapter.KEY_EXPOSUREITEM_POSITION));
        lPositionField.setText(Integer.toString(lPosition+1));
        

        TextView lValueField = (TextView) inView.findViewById(R.id.value);
        final Fraction lValue = new Fraction(inCursor.getString(inCursor.getColumnIndex(DarkroomDBAdapter.KEY_EXPOSUREITEM_VALUE)));
        if(mUseLinear)
        	lValueField.setText(mFormat.format(Exposure.getRelativeTime(mBaseTime, lValue, mExposureType == ExposureItem.BURNEXPOSURE )));
        else
        	lValueField.setText(lValue.toString());
        
        TextView lDescField = (TextView) inView.findViewById(R.id.desc);
        lDescField.setText(inCursor.getString(inCursor.getColumnIndex(DarkroomDBAdapter.KEY_EXPOSUREITEM_DESC)));
		
    }

}
