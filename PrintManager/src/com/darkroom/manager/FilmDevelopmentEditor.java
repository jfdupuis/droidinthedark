package com.darkroom.manager;

import com.darkroom.library.Chemical;
import com.darkroom.library.DevelopmentTime;
import com.darkroom.library.Film;
import com.jfdupuis.library.screen.ScreenUtil;
import com.darkroom.library.dao.AddNewAgitationDialog;
import com.darkroom.library.dao.AddNewChemicalDialog;
import com.darkroom.library.dao.AddNewStringDataDialog;
import com.darkroom.library.dao.DarkroomDBHelper;
import com.darkroom.manager.dao.ManagerDBAdapter;
import com.darkroom.manager.preferences.DarkroomManagerPrefs;
import com.jfdupuis.library.math.Unit;
import com.jfdupuis.library.widget.DBEditText;
import com.jfdupuis.library.widget.DBEditText.OnChangedListener;
import com.jfdupuis.library.widget.DBSpinner;
import com.jfdupuis.library.widget.DBSpinnerDialog;

import android.app.Activity;
import android.content.Intent;
import android.database.SQLException;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class FilmDevelopmentEditor extends Activity  {
	
	static final int REQUEST_ADD_DEVELOPER = 0;
	static final int REQUEST_ADD_AGITATION = 1;
	static final int REQUEST_ADD_DILUTION = 2;
	
	ManagerDBAdapter mDarkroomDBAdapter;
	
	long mFilmId = -1;
	
	Film mCurrentFilm = null;
	DevelopmentTime mCurrentDev = null;
	DevelopmentTime mOldDev = null;
	
	DBSpinner mDeveloperSpinner;
	DBSpinner mAgitationSpinner;
	DBSpinner mDilutionSpinner;
	
	DBEditText mTemp;
	DBEditText mTime;
	DBEditText mNotes;
	
	char mTempUnits;
	
	private TextView mIdDisplay;
	
	@Override
	public void onBackPressed() {
	}

	
	public void onCreate(Bundle inBundle) {
		super.onCreate(inBundle);
		
		setContentView(R.layout.film_dev_editor);
		
		mDarkroomDBAdapter = new ManagerDBAdapter(this);
		mDarkroomDBAdapter.open();
		
		setupSpinners();
		setupButtons();
		
		TextView lTempHeader = (TextView) findViewById(R.id.temp_header);
		mTempUnits = DarkroomManagerPrefs.getDefaultUnitsTemp(this);
		lTempHeader.setText(lTempHeader.getText().toString() + " [�" + mTempUnits + "]" );
		
		
		mTemp = (DBEditText) findViewById(R.id.temp);
		mTemp.setOnChangedListener(new OnChangedListener() {
			public void changed(EditText inView) {
				changedTemp(inView.getText().toString());
			}
		});
		
		
		mTime = (DBEditText) findViewById(R.id.time);
		mTime.setOnChangedListener(new OnChangedListener() {
			public void changed(EditText inView) {
				changedTime(inView.getText().toString());
			}
		});
		
		mNotes = (DBEditText) findViewById(R.id.notes);
		mNotes.setOnChangedListener(new OnChangedListener() {
			public void changed(EditText inView) {
				changedNotes(inView.getText().toString());
			}
		});
	
		
		mFilmId = getParent().getIntent().getLongExtra("film_id", -1);
		if(mFilmId != -1) {
			loadFilmData(mFilmId);
		}
		else
			throw new RuntimeException("A film id was expected.");
		
		// capture our View elements
		mIdDisplay = (TextView) findViewById(R.id.filmedit_userid);
		mIdDisplay.setText(mCurrentFilm.getUserId());
        
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		mDarkroomDBAdapter.close();
	}
	
	private void loadFilmData(final long inFilmId) {
		//TODO doesn't get the correct film dev temp from the DB
		mCurrentFilm = null;
		mCurrentFilm = mDarkroomDBAdapter.getFilm(inFilmId);
		mCurrentDev = mCurrentFilm.getDevelopment();
		mOldDev = new DevelopmentTime(mCurrentFilm.getDevelopment());
		
		if(mCurrentDev.mTemp > -273)
			mTemp.setText(Float.toString(
					(mTempUnits == 'F') ? Unit.convertFromCelsius(mCurrentDev.mTemp) : mCurrentDev.mTemp
					));
		else
			mTemp.setText("");

		if(mCurrentDev.mTime >= 0)
			//mTime.setText(String.format("%d:%02d", mCurrentDev.mTime/60, mCurrentDev.mTime%60));
			mTime.setText(String.format("%.2f",(float)(mCurrentDev.mTime)/60.0f));
		else
			mTime.setText("");

		mNotes.setText(mCurrentDev.mNotes);

		mDeveloperSpinner.setPositionById(mCurrentDev.mDeveloper);
		mAgitationSpinner.setPositionById(mCurrentDev.mAgitation);
		if(mCurrentDev.mDilution < 0) {
			Logger.w("Undefined dilution!");
			mDilutionSpinner.setPositionById(1); //Not specified
		} else {
			mDilutionSpinner.setPositionById(mCurrentDev.mDilution);
		}


	}
	
	private void setupButtons() {
		
		//Connect buttons
        final Button lCancelButton = (Button) findViewById(R.id.cancel_btn);
        lCancelButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	//mCurrentFilm.mDevelopment = mCurrentFilm.new Development(mOldDev); //Done in loadFilmData
            	updateDevelopmentTime(mOldDev);
            	//loadFilmData(mFilmId);
            	setResult(RESULT_OK);
            	finish();
            }
        });
        
        final Button lUndoButton = (Button) findViewById(R.id.undo_btn);
        lUndoButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	updateDevelopmentTime(mOldDev);
            	loadFilmData(mFilmId);
            }
        });
        
        final Button lDoneButton = (Button) findViewById(R.id.done_btn);
        lDoneButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	//All the field are saved each time they are modified. So, we just quit.
            	v.setFocusableInTouchMode(true);
            	v.requestFocus();
            	mCurrentFilm = null;
            	mCurrentDev = null;
            	setResult(RESULT_OK); 
            	finish();
            }
        });

	}
	
		
	private void setupSpinners() {
		// Developer spinner
		mDeveloperSpinner = (DBSpinner) findViewById(R.id.dev_spinner);
		mDeveloperSpinner.setAdapter(
				mDarkroomDBAdapter.getAllChemicalCursor(Chemical.DEVELOPER),
				DarkroomDBHelper.KEY_CHEMICAL_NAME);
		mDeveloperSpinner.setOnActionRequestHandler(new DBSpinner.OnActionRequest(){
			public void onActionRequest(Spinner mInSpinner, int inAction, int inPosition, long inId) {
				Intent lIntent = new Intent(FilmDevelopmentEditor.this, AddNewChemicalDialog.class);
            	lIntent.putExtra("category", Chemical.DEVELOPER);
				switch(inAction) {
				case DBSpinnerDialog.ADD_REQUEST:
	            	break;
				case DBSpinnerDialog.EDIT_REQUEST:
					lIntent.putExtra("id", inId);
					break;
				case DBSpinnerDialog.ITEM_SELECTION:
					changedDeveloper(inId);
					return;
				}
				startActivityForResult(lIntent,REQUEST_ADD_DEVELOPER);
			}
		});

		// Agitation spinner
		mAgitationSpinner = (DBSpinner) findViewById(R.id.agitation_spinner);
		mAgitationSpinner.setAdapter(
				mDarkroomDBAdapter.getAllStringDataCursor(DarkroomDBHelper.AGITATION_TABLE_NAME),
				DarkroomDBHelper.KEY_STRING);
		mAgitationSpinner.setOnActionRequestHandler(new DBSpinner.OnActionRequest(){
			public void onActionRequest(Spinner mInSpinner, int inAction, int inPosition, long inId) {
				Intent lIntent = new Intent(FilmDevelopmentEditor.this, AddNewAgitationDialog.class);
            	lIntent.putExtra("table", DarkroomDBHelper.AGITATION_TABLE_NAME);
            	lIntent.putExtra("category", DarkroomDBHelper.AGITATION_STRING_CATEGORY);
            	
				switch(inAction) {
				case DBSpinnerDialog.ADD_REQUEST:
	            	break;
				case DBSpinnerDialog.EDIT_REQUEST:
					lIntent.putExtra("id", inId);
					break;
				case DBSpinnerDialog.ITEM_SELECTION:
					changedAgitation(inId);
					return;
				}
				startActivityForResult(lIntent,REQUEST_ADD_AGITATION);
			}
		});
		
		mDilutionSpinner = (DBSpinner) findViewById(R.id.dilution_spinner);
		mDilutionSpinner.setAdapter(
				mDarkroomDBAdapter.getAllStringDataCursor(DarkroomDBHelper.DILUTION_TABLE_NAME),
				DarkroomDBHelper.KEY_STRING);
		mDilutionSpinner.setOnActionRequestHandler(new DBSpinner.OnActionRequest(){
			public void onActionRequest(Spinner mInSpinner, int inAction, int inPosition, long inId) {
				Intent lIntent = new Intent(FilmDevelopmentEditor.this, AddNewStringDataDialog.class);
				lIntent.putExtra("table", DarkroomDBHelper.DILUTION_TABLE_NAME);
				lIntent.putExtra("havecategory", false);
				switch(inAction) {
				case DBSpinnerDialog.ADD_REQUEST:
	            	break;
				case DBSpinnerDialog.EDIT_REQUEST:
					lIntent.putExtra("id", inId);
					break;
				case DBSpinnerDialog.ITEM_SELECTION:
					changedDilution(inId);
					return;
				}
				startActivityForResult(lIntent,REQUEST_ADD_DILUTION);
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Logger.v("Activity return from request " + requestCode);
		switch(requestCode) {
			case REQUEST_ADD_DEVELOPER:
				mDeveloperSpinner.onAddReturn(resultCode, data);
				break;
			case REQUEST_ADD_AGITATION:
				mAgitationSpinner.onAddReturn(resultCode, data);
				break;
			case REQUEST_ADD_DILUTION:
				mDilutionSpinner.onAddReturn(resultCode, data);
				break;
		}

    }
		
	void changedDeveloper(long inNewId) {
		if(mCurrentFilm.getDevelopment().mDeveloper != inNewId) {
			mCurrentFilm.getDevelopment().mDeveloper = inNewId;
			Logger.d("Update development developer to " + inNewId);
			updateDevelopmentTime(mCurrentFilm.getDevelopment());
		}
	}

	void changedAgitation(long inNewId) {
		if(mCurrentFilm.getDevelopment().mAgitation != inNewId) {
			mCurrentFilm.getDevelopment().mAgitation = inNewId;
			Logger.d("Update development agitation to " + inNewId);
			updateDevelopmentTime(mCurrentFilm.getDevelopment());
		}
	}
	
	void changedDilution(long inNewId) {
		if(mCurrentFilm.getDevelopment().mDilution != inNewId) {
			mCurrentFilm.getDevelopment().mDilution = inNewId;
			Logger.d("Update development developer dilution to " + inNewId);
			updateDevelopmentTime(mCurrentFilm.getDevelopment());
		}
	}
	
	void updateDevelopmentTime(DevelopmentTime inDevTime) {
		try{
			mDarkroomDBAdapter.updateEmulsionDevTimeValue(inDevTime);
		} catch(SQLException e) {
			//Not found, create a new devtime
			Logger.e("No dev time found for film id: " + mCurrentFilm.getId(), e);
			long lId = mDarkroomDBAdapter.insertEmulsionDevTime(inDevTime);
			mDarkroomDBAdapter.updateFilmValue(mCurrentFilm.getId(), DarkroomDBHelper.KEY_FILM_DEVELOPMENTID, lId);
		}
	}
	
	void changedTemp(String inNewValue) {
		try {
			float lTemp = Float.valueOf(inNewValue);
			switch(mTempUnits) {
				case 'C':
					break;
				case 'F':
					lTemp = Unit.convertFromFahrenheit(lTemp);
					break;
			}
        	mCurrentDev.mTemp = lTemp;
        	updateDevelopmentTime(mCurrentFilm.getDevelopment());
        	Logger.d("Update film temperature to " + mCurrentDev.mTemp);
        } catch (NumberFormatException inException) {
        	if(!TextUtils.isEmpty(inNewValue))
        		ScreenUtil.displayMessage(FilmDevelopmentEditor.this, inNewValue + " is not a valid temperature.", Toast.LENGTH_SHORT);
        }
	}
	
	void changedTime(String inNewValue) {
		try {
//			//Convert to seconds from MM:SS
//			int lIdx = inNewValue.indexOf(":");
//			int lMinutes = 0;
//			int lSeconds = 0;
//			if(lIdx != inNewValue.length() && lIdx > 0) {
//				lMinutes = Integer.valueOf(inNewValue.substring(0, lIdx));
//				lSeconds = Integer.valueOf(inNewValue.substring(lIdx+1));
//			} else {
//				lSeconds = Integer.valueOf(inNewValue);
//			}
//        	mCurrentDev.mTime = 60*lMinutes + lSeconds;
			mCurrentDev.mTime  = (int) (Float.valueOf(inNewValue)*60.0f);
        	updateDevelopmentTime(mCurrentFilm.getDevelopment());
        	Logger.d("Update film dev time to " + mCurrentDev.mTime);
        } catch (NumberFormatException inException) {
        	if(!TextUtils.isEmpty(inNewValue))
        		ScreenUtil.displayMessage(FilmDevelopmentEditor.this, inNewValue + " is not a valid time. Time should be formated MM:SS", Toast.LENGTH_SHORT);
        }
	}
	
	void changedNotes(String inNewValue) {
			mCurrentFilm.getDevelopment().mNotes = inNewValue;
			Logger.d("Update film notes to " + inNewValue);
			updateDevelopmentTime(mCurrentFilm.getDevelopment());
	}

	
}
