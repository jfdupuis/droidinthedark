package com.darkroom.manager;

import com.darkroom.manager.dao.ManagerDBAdapter;
import com.darkroom.manager.help.SimpleUsage;
import com.darkroom.manager.preferences.DarkroomManagerPrefs;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class DarkroomNoteKeeper extends Activity {
	/** Called when the activity is first created. */
	public void onCreate(Bundle inBundle) {
		super.onCreate(inBundle);
		setContentView(R.layout.main);
		
		
		//Connect buttons
        final ImageButton lShowPrintButton = (ImageButton) findViewById(R.id.show_print_btn);
        lShowPrintButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	showPrints();
            }
        });
        
        final ImageButton lShowFilmButton = (ImageButton) findViewById(R.id.show_film_btn);
        lShowFilmButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	showFilms();
            }
        });
        
        final ImageButton lShowCameraButton = (ImageButton) findViewById(R.id.show_camera_btn);
        lShowCameraButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	showCameras();
            }
        });
        
        final ImageButton lShowCollectionButton = (ImageButton) findViewById(R.id.show_collection_btn);
        lShowCollectionButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	showCollections();
            }
        });
        
        final ImageButton lShowChemicalButton = (ImageButton) findViewById(R.id.show_chemical_btn);
        lShowChemicalButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	showChemicals();
            }
        });
        
        final ImageButton lShowSettingsButton = (ImageButton) findViewById(R.id.show_settings_btn);
        lShowSettingsButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	showSettings();
            }
        });
        
        new SimpleUsage(this).show(new ManagerDBAdapter(this));
	}
	
//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//	    MenuInflater inflater = getMenuInflater();
//	    inflater.inflate(R.menu.main_menu, menu);
//	    return true;
//	}
//	
//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) {
//	    // Handle item selection
//	    switch (item.getItemId()) {
////	    case R.id.setting:
////	    	showSettings();
////	        return true; 
//	    default:
//	        return super.onOptionsItemSelected(item);
//	    }
//	}
	
	protected void showSettings() {
		startActivity(new Intent(this, DarkroomManagerPrefs.class));
	}
	
	public void showPrints() {
		Intent lIntent = new Intent(this, PrintManager.class);
		try {
			Logger.v("Starting print manager");
			startActivity(lIntent);
		} catch (Exception e) {
			AlertDialog.Builder alert = new AlertDialog.Builder(this);
			alert.setTitle("Error!");
			alert.setMessage(e.getMessage());
			alert.show();
		}
	}
	
	public void showFilms() {
		Intent lIntent = new Intent(this, FilmManager.class);
		try {
			Logger.v("Starting film manager");
			startActivity(lIntent);
		} catch (Exception e) {
			AlertDialog.Builder alert = new AlertDialog.Builder(this);
			alert.setTitle("Error!");
			alert.setMessage(e.getMessage());
			alert.show();
		}
	}

	public void showCameras() {
		Intent lIntent = new Intent(this, CameraManager.class);
		try {
			Logger.v("Starting camera manager");
			startActivity(lIntent);
		} catch (Exception e) {
			AlertDialog.Builder alert = new AlertDialog.Builder(this);
			alert.setTitle("Error!");
			alert.setMessage(e.getMessage());
			alert.show();
		}
	}
	
	public void showCollections() {
		Intent lIntent = new Intent(this, CollectionManager.class);
		try {
			Logger.v("Starting collection manager");
			startActivity(lIntent);
		} catch (Exception e) {
			AlertDialog.Builder alert = new AlertDialog.Builder(this);
			alert.setTitle("Error!");
			alert.setMessage(e.getMessage());
			alert.show();
		}
	}
	
	public void showChemicals() {
		Intent lIntent = new Intent(this, ChemicalManager.class);
		try {
			Logger.v("Starting chemical manager");
			startActivity(lIntent);
		} catch (Exception e) {
			AlertDialog.Builder alert = new AlertDialog.Builder(this);
			alert.setTitle("Error!");
			alert.setMessage(e.getMessage());
			alert.show();
		}
	}
}
