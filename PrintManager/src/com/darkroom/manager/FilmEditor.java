package com.darkroom.manager;

import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.TabHost;

public class FilmEditor extends TabActivity {
		
	public void onCreate(Bundle inBundle) {
		super.onCreate(inBundle);
		setContentView(R.layout.film_editor);
		setTitle(R.string.film_editor);
		
		Resources res = getResources(); // Resource object to get Drawables
	    TabHost tabHost = getTabHost();  // The activity TabHost
	    TabHost.TabSpec spec;  // Resusable TabSpec for each tab
	    Intent intent;  // Reusable Intent for each tab
	    
	    // Setup info tab
	    intent = new Intent().setClass(this, FilmInfoEditor.class);
	    spec = tabHost.newTabSpec("infos").setIndicator("Infos",
	                      res.getDrawable(android.R.drawable.ic_menu_info_details))
	                  .setContent(intent);
	    tabHost.addTab(spec);
	    
	    // Setup frame tab
	    intent = new Intent().setClass(this, FilmFrameManager.class);
	    intent.putExtra(Manager.EMBEDDED, true);
	    spec = tabHost.newTabSpec("frame").setIndicator("Frames",
	                      res.getDrawable(android.R.drawable.ic_menu_camera))
	                  .setContent(intent);
	    tabHost.addTab(spec);

	    // Setup development tab
	    intent = new Intent().setClass(this, FilmDevelopmentEditor.class);
	    spec = tabHost.newTabSpec("dev").setIndicator("Development",
	                      res.getDrawable(R.drawable.film_development))
	                  .setContent(intent);
	    tabHost.addTab(spec);
	    
	    // Setup print list tab
	    intent = new Intent().setClass(this, PrintManager.class);
	    intent.putExtra(Manager.EMBEDDED, true);
	    spec = tabHost.newTabSpec("prints").setIndicator("Prints",
	                      res.getDrawable(android.R.drawable.ic_menu_slideshow))
	                  .setContent(intent);
	    tabHost.addTab(spec);

	    
	    // Set the current tab
	    tabHost.setCurrentTab(0);
        
	}
}
