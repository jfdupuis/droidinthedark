package com.darkroom.manager;

import com.darkroom.library.dao.DarkroomDBAdapter;
import com.darkroom.library.dao.DarkroomDBHelper;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class FilmManager extends Manager {
	
	private SimpleCursorAdapter mFilmAdapter = null;
	

	/** Called when the activity is first created. */
	public void onCreate(Bundle inBundle) {
		super.onCreate(inBundle);
		setTitle(R.string.film_manager);
		
		// Set result CANCELED in case the user backs out
        setResult(Activity.RESULT_CANCELED);
        
        populateList();
	}

	protected void populateList() {
		mCursor = mDarkroomDBAdapter.getAllFilmCursor(new String[]{DarkroomDBAdapter.KEY_ID,DarkroomDBAdapter.KEY_FILM_USERID, DarkroomDBHelper.KEY_FILM_NAME});
        startManagingCursor(mCursor);
        
        // Create an array to specify the fields we want to display in the list (only TITLE)
        final String[] lFrom = new String[]{DarkroomDBHelper.KEY_FILM_USERID, DarkroomDBHelper.KEY_FILM_NAME};
        
        // and an array of the fields we want to bind those fields to (in this case just text1)
        final int[] lTo = new int[]{R.id.rowFilmUserId, R.id.rowFilmName};
        // Now create a simple cursor adapter and set it to display
        mFilmAdapter = new SimpleCursorAdapter(this, R.layout.film_item, mCursor, lFrom, lTo);
        setListAdapter(mFilmAdapter);
		
	}
	
	@Override
	protected void onListItemClick(ListView inListView, View inView, int inPosition, long inId) {
		if(mSelectOnly) {
			Intent lIntent = new Intent();
			lIntent.putExtra(SELECTION, inId);
			Logger.v("Selected film id " + inId);
			setResult(Activity.RESULT_OK,lIntent);
			finish();
		} else {
			Intent lIntent = new Intent(this, FilmEditor.class);
			lIntent.putExtra("film_id", inId);
			Logger.d("Will start edition of film id " + inId);
			startActivity(lIntent);
		}
	}

	

	@Override
	protected void addNewItem() {
		if(!isDemoLimitReach()) {
			Intent lIntent = new Intent(this, FilmEditor.class);
			Logger.v("Starting film editor");
			startActivityForResult(lIntent,ADD_ITEM);
		}
	}

	@Override
	protected void editItem(int inPosition, long inId) {
		Logger.d("Editing film id:" + inId);
		
		//Start film editor
		Intent lIntent = new Intent(this, FilmEditor.class);
		lIntent.putExtra("film_id", inId);
		startActivityForResult(lIntent,EDIT_ITEM);
	}
	
	protected void removeItem(int inPosition, long inId) {
		Logger.v(String.format("Remove item at index: %d", inPosition));
		boolean lReturn = mDarkroomDBAdapter.removeFilm(inId);
		Logger.v(String.format("Delete results: %b", lReturn));
		mCursor.requery();
	}
	
}