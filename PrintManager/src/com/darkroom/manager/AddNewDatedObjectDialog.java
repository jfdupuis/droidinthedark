package com.darkroom.manager;

import com.darkroom.library.DarkroomDatedObject;
import com.darkroom.library.Logger;
import com.darkroom.library.dao.AddNewElementDialog;
import com.darkroom.manager.preferences.DarkroomManagerPrefs;
import com.jfdupuis.library.widget.DBEditText;
import com.jfdupuis.library.widget.DBEditText.OnChangedListener;
import com.jfdupuis.library.widget.NumberPicker;
import com.jfdupuis.library.widget.NumberPickerWithInvalids;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public abstract class AddNewDatedObjectDialog extends AddNewElementDialog {
	static final int DATE_DIALOG_ID = 0;
	
	protected TextView mIdDisplay;
	protected TextView mDateDisplay;
    protected Button mPickDate;
    protected NumberPickerWithInvalids mIdNumberPicker;
    protected int mYear;
    protected int mMonth;
    protected int mDay;
    
    protected CheckBox mHasCustomId;
    protected DBEditText mCustomId;
	
    DarkroomDatedObject  mCurrent;

    
	// the callback received when the user "sets" the date in the dialog
    protected DatePickerDialog.OnDateSetListener mDateSetListener =
            new DatePickerDialog.OnDateSetListener() {

                public void onDateSet(DatePicker view, int year, 
                                      int monthOfYear, int dayOfMonth) {
                    mYear = year;
                    mMonth = monthOfYear;
                    mDay = dayOfMonth;
                    Calendar calendar = new GregorianCalendar(mYear, mMonth, mDay);
                    Date lDate = calendar.getTime();
                    
//                                 mCurrentPrint.setPrintingDate(lDate);
                    mDateDisplay.setText(String.format("%tF", calendar));
                    mCurrent.setDate(lDate);
                    updateIdNumberPickerRange();
                    updateID();
                }
            };
            
    public void onCreate(Bundle inBundle) {
		super.onCreate(inBundle);
    }

	@Override
	protected void initialize() {
		super.initialize();
		
	}
	
	protected void postinitialize() {
		final Calendar lCalendar = Calendar.getInstance();
		if(mCurrent != null)
			lCalendar.setTime(mCurrent.getDate());
        mYear = lCalendar.get(Calendar.YEAR);
        mMonth = lCalendar.get(Calendar.MONTH);
        mDay = lCalendar.get(Calendar.DAY_OF_MONTH);
		
		mIdDisplay = (TextView) findViewById(R.id.userid);
		if(mCurrent != null)
			mIdDisplay.setText(mCurrent.getUserId());
		
        mDateDisplay = (TextView) findViewById(R.id.date);
        mPickDate = (Button) findViewById(R.id.select_date_btn);
        mPickDate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                showDialog(DATE_DIALOG_ID);
            }
        });
        mDateDisplay.setText(String.format("%tF", lCalendar));
        
        mHasCustomId = (CheckBox) findViewById(R.id.use_custom_userid);
        if(mCurrent != null)
        	mHasCustomId.setChecked(mCurrent.hasCustomId());
        mHasCustomId.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				mCustomId.setEnabled(isChecked);
				if(mCurrent != null)
					mCustomId.setText(mCurrent.getUserId());
				mCustomId.requestFocus();
				updateID();
			}
        	
        });
        
        
        mCustomId = (DBEditText) findViewById(R.id.customid);
        if(mCurrent != null && mCurrent.hasCustomId())
        	mCustomId.setText(mCurrent.getUserId());
        mCustomId.setOnChangedListener(new OnChangedListener() {
			public void changed(EditText inView) {
				updateID();
			}
		});
        
        if(!mHasCustomId.isChecked()) mCustomId.setEnabled(false);
        if(!DarkroomManagerPrefs.displayCustomId(this)) {
        	findViewById(R.id.customid_header).setVisibility(View.GONE);
        	mHasCustomId.setVisibility(View.GONE);
        	mCustomId.setVisibility(View.GONE);
        }
        
		// Connect buttons
		mIdNumberPicker = (NumberPickerWithInvalids) findViewById(R.id.IDNumberPicker);
		mIdNumberPicker.setRange(0, 99);
		mIdNumberPicker.setOnChangeListener(new NumberPicker.OnChangedListener() {
			public void onChanged(NumberPicker mPicker, int inOldVal, int inNewVal) {
				if(mCurrent != null)
					mCurrent.setIdNumber(inNewVal);
				updateID();
			}
		});
		if(mCurrent != null && mCurrent.getIdNumber() != -1) {
			mIdNumberPicker.setCurrent(mCurrent.getIdNumber());
		}
	}
	
	@Override
	protected Dialog onCreateDialog(int id) {
	    switch (id) {
	    case DATE_DIALOG_ID:
	        return new DatePickerDialog(this, mDateSetListener, mYear, mMonth, mDay);
	    }
	    return null;
	}
	
	// updates the date in the TextView
    protected void updateID() {
    	if(mCurrent == null) 
    		return;
    	
    	//Update user id
    	if(mHasCustomId.isChecked()) {
    		mCurrent.setUserId(mCustomId.getText().toString(), true);
			Logger.d("Item as custom user id: " + mCurrent.getUserId());
		} else {
			//Get rid of the custom id
			mCurrent.setUserId(null,false);
			Logger.d("Item as generated user id: " + mCurrent.getUserId());
		}
    	
    	mIdDisplay.setText(mCurrent.getUserId());
    	Logger.v("New film userid: " + mCurrent.getUserId());
    }
    
    abstract protected void updateIdNumberPickerRange();
    
}
