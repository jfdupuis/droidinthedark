package com.darkroom.manager;

import com.darkroom.library.dao.AddNewCameraDialog;
import com.darkroom.library.dao.DarkroomDBHelper;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class CameraManager extends Manager {
	
	private SimpleCursorAdapter mCameraAdapter = null;
	

	/** Called when the activity is first created. */
	public void onCreate(Bundle inBundle) {
		super.onCreate(inBundle);
		setTitle(R.string.camera_manager);
		
		// Set result CANCELED in case the user backs out
        setResult(Activity.RESULT_CANCELED);
        
        populateList();
	}

	protected void populateList() {
		mCursor = mDarkroomDBAdapter.getAllCameraBrandModelCursor();
        startManagingCursor(mCursor);
        
        // Create an array to specify the fields we want to display in the list (only TITLE)
        final String[] lFrom = new String[]{DarkroomDBHelper.KEY_CAMERA_BRANDMODEL, DarkroomDBHelper.KEY_CAMERA_SIZE};
        
        // and an array of the fields we want to bind those fields to (in this case just text1)
        final int[] lTo = new int[]{R.id.rowCameraBrandModel, R.id.rowCameraFilmSize};
        // Now create a simple cursor adapter and set it to display
        mCameraAdapter = new SimpleCursorAdapter(this, R.layout.camera_item, mCursor, lFrom, lTo);
        setListAdapter(mCameraAdapter);
		
	}
	
	@Override
	protected void onListItemClick(ListView inListView, View inView, int inPosition, long inId) {		
		if(mSelectOnly) {
			Intent lIntent = new Intent();
			lIntent.putExtra(SELECTION, inId);
			setResult(Activity.RESULT_OK,lIntent);
			finish();
		} else {
			Intent lIntent = new Intent(this, AddNewCameraDialog.class);
			lIntent.putExtra("id", inId);
			startActivity(lIntent);
		}
	}

	

	@Override
	protected void addNewItem() {
		Intent lIntent = new Intent(this, AddNewCameraDialog.class);
		Logger.v("Starting camera editor");
		startActivityForResult(lIntent,ADD_ITEM);
	}

	@Override
	protected void editItem(int inPosition, long inId) {
		Logger.d("Editing camera id:" + inId);
		
		//Start film editor
		Intent lIntent = new Intent(this, AddNewCameraDialog.class);
		lIntent.putExtra("id", inId);
		startActivityForResult(lIntent,EDIT_ITEM);
	}
	
	protected void removeItem(int inPosition, long inId) {
		Logger.v(String.format("Remove item at index: %d", inPosition));
		boolean lReturn = mDarkroomDBAdapter.removeCamera(inId);
		Logger.v(String.format("Delete results: %b", lReturn));
		mCursor.requery();
	}
	
	
}