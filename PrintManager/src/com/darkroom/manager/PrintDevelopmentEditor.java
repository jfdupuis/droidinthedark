package com.darkroom.manager;

import com.darkroom.library.Chemical;
import com.darkroom.library.Print;
import com.darkroom.library.dao.AddNewChemicalDialog;
import com.darkroom.library.dao.AddNewPaperDialog;
import com.darkroom.library.dao.DarkroomDBHelper;
import com.darkroom.manager.dao.ManagerDBAdapter;
import com.jfdupuis.library.widget.DBEditText;
import com.jfdupuis.library.widget.DBEditText.OnChangedListener;
import com.jfdupuis.library.widget.DBSpinner;
import com.jfdupuis.library.widget.DBSpinnerDialog;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class PrintDevelopmentEditor extends Activity {
	static final int REQUEST_ADD_DEV = 0;
	static final int REQUEST_ADD_TONER = 1;
	static final int REQUEST_ADD_PAPER = 2;
	
	ManagerDBAdapter mDarkroomDBAdapter;
	
	DBSpinner mDevSpinner;
	DBSpinner mTonerSpinner;
	DBSpinner mPaperSpinner;
	
	DBEditText mNotes;
	
	long mPrintId;
	Print mCurrentPrint;
	Print mOldPrint;
	
	@Override
	public void onBackPressed() {
	}

	
	@Override
	public void onCreate(Bundle inBundle) {
		super.onCreate(inBundle);
		setContentView(R.layout.print_dev_editor);
		
		mDarkroomDBAdapter = new ManagerDBAdapter(this);
		mDarkroomDBAdapter.open();
		
		setupSpinners();
		setupButtons();
		
		mNotes = (DBEditText) findViewById(R.id.notes);
		mNotes.setOnChangedListener(new OnChangedListener() {
			public void changed(EditText inView) {
				changedNotes(inView.getText().toString());
			}
		});

		
		mPrintId = getParent().getIntent().getLongExtra("print_id", -1);
		if(mPrintId != -1) {
			loadPrintData(mPrintId);
		}
		else
			throw new RuntimeException("A print id was expected.");
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		mDarkroomDBAdapter.close();
	}
	
	private void loadPrintData(final long inPrintId) {
		mCurrentPrint = mDarkroomDBAdapter.getPrint(inPrintId, true);
		mOldPrint = new Print(mCurrentPrint);
		
		
		mNotes.setText(mCurrentPrint.getNotes());

		mDevSpinner.setPositionById(mCurrentPrint.getDeveloper());
		mTonerSpinner.setPositionById(mCurrentPrint.getToner());
		mPaperSpinner.setPositionById(mCurrentPrint.getPaper());
	}
		
	private void setupButtons() {
		//Connect buttons
       
        final Button lCancelButton = (Button) findViewById(R.id.cancel_btn);
        lCancelButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	//mCurrentFilm.mDevelopment = mCurrentFilm.new Development(mOldDev); //Done in loadFilmData
            	mDarkroomDBAdapter.updatePrintValue(mOldPrint, true);
            	//loadFilmData(mFilmId);
            	setResult(RESULT_OK);
            	finish();
            }
        });
        
        final Button lUndoButton = (Button) findViewById(R.id.undo_btn);
        lUndoButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	mDarkroomDBAdapter.updatePrintValue(mOldPrint, true);
            	loadPrintData(mPrintId);
            }
        });
        
        final Button lDoneButton = (Button) findViewById(R.id.done_btn);
        lDoneButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	//All the field are saved each time they are modified. So, we just quit.
            	v.setFocusableInTouchMode(true);
            	v.requestFocus();
            	setResult(RESULT_OK);
            	finish();
            }
        });
        
	}
	
	private void setupSpinners() {
		//Developer spinner
		mDevSpinner = (DBSpinner) findViewById(R.id.dev_spinner); 
		mDevSpinner.setAdapter(
				mDarkroomDBAdapter.getAllChemicalCursor(
						new String []{DarkroomDBHelper.KEY_CHEMICAL_NAME}, 
						DarkroomDBHelper.KEY_CHEMICAL_NAME,
						Chemical.DEVELOPER), 
						DarkroomDBHelper.KEY_CHEMICAL_NAME);
        mDevSpinner.setOnActionRequestHandler(new DBSpinner.OnActionRequest(){
			public void onActionRequest(Spinner mInSpinner, int inAction, int inPosition, long inId) {
				Intent lIntent = new Intent(PrintDevelopmentEditor.this, AddNewChemicalDialog.class);
				lIntent.putExtra("category", Chemical.DEVELOPER);
				switch(inAction) {
				case DBSpinnerDialog.ADD_REQUEST:
	            	break;
				case DBSpinnerDialog.EDIT_REQUEST:
					lIntent.putExtra("id", inId);
					break;
				case DBSpinnerDialog.ITEM_SELECTION:
					changedDeveloper(inId);
					return;
				}
				startActivityForResult(lIntent, REQUEST_ADD_DEV);
			}
		});
        
        //Toner spinner
        mTonerSpinner = (DBSpinner) findViewById(R.id.toner_spinner);
       	mTonerSpinner.setAdapter(
       			mDarkroomDBAdapter.getAllChemicalCursor(
       					new String []{DarkroomDBHelper.KEY_CHEMICAL_NAME},
       					DarkroomDBHelper.KEY_CHEMICAL_NAME,
       					Chemical.TONER), 
       			DarkroomDBHelper.KEY_CHEMICAL_NAME);
        mTonerSpinner.setOnActionRequestHandler(new DBSpinner.OnActionRequest(){
			public void onActionRequest(Spinner mInSpinner, int inAction, int inPosition, long inId) {
				Intent lIntent = new Intent(PrintDevelopmentEditor.this, AddNewChemicalDialog.class);
				lIntent.putExtra("category", Chemical.TONER);
				switch(inAction) {
				case DBSpinnerDialog.ADD_REQUEST:
	            	break;
				case DBSpinnerDialog.EDIT_REQUEST:
					lIntent.putExtra("id", inId);
					break;
				case DBSpinnerDialog.ITEM_SELECTION:
					changedToner(inId);
					return;
				}
            	startActivityForResult(lIntent, REQUEST_ADD_TONER);
			}
		});
        
        //Paper spinner
		mPaperSpinner = (DBSpinner) findViewById(R.id.paper_spinner);
        mPaperSpinner.setAdapter(
        		mDarkroomDBAdapter.getAllPapersCursor(
        				new String[]{ DarkroomDBHelper.KEY_PAPER_NAME}), 
        		DarkroomDBHelper.KEY_PAPER_NAME); 
        mPaperSpinner.setOnActionRequestHandler(new DBSpinner.OnActionRequest(){
			public void onActionRequest(Spinner mInSpinner, int inAction, int inPosition, long inId) {
				Intent lIntent = new Intent(PrintDevelopmentEditor.this, AddNewPaperDialog.class);
				switch(inAction) {
				case DBSpinnerDialog.ADD_REQUEST:
	            	break;
				case DBSpinnerDialog.EDIT_REQUEST:
					lIntent.putExtra("id", inId);
					break;
				case DBSpinnerDialog.ITEM_SELECTION:
					changedPaper(inId);
					return;
				}
            	startActivityForResult(lIntent, REQUEST_ADD_PAPER);
			}
		});
        
	}
	
	void changedToner(long inNewId) {
		if(mCurrentPrint.getToner() != inNewId) {
			mCurrentPrint.setToner(inNewId);
			Logger.d("Update print toner to " + inNewId);
			mDarkroomDBAdapter.updatePrintValue(mCurrentPrint.getId(), DarkroomDBHelper.KEY_PRINT_TONERID, inNewId);
		}
	}
	
	void changedDeveloper(long inNewId) {
		if(mCurrentPrint.getDeveloper() != inNewId) {
			mCurrentPrint.setDeveloper(inNewId);
			Logger.d("Update print developer to " + inNewId);
			mDarkroomDBAdapter.updatePrintValue(mCurrentPrint.getId(), DarkroomDBHelper.KEY_PRINT_DEVID, inNewId);
		}
	}
	
	void changedPaper(long inNewId) {
		if(mCurrentPrint != null && mCurrentPrint.getPaper() != inNewId) {
			mCurrentPrint.setPaper(inNewId);
			Logger.d("Update print paper to " + inNewId);
			mDarkroomDBAdapter.updatePrintValue(mCurrentPrint.getId(), DarkroomDBHelper.KEY_PRINT_PAPERID, inNewId);
		}
	}
	
	void changedNotes(String inNewValue) {
		mCurrentPrint.setNotes(inNewValue);
		Logger.d("Update film notes to " + inNewValue);
		mDarkroomDBAdapter.updatePrintValue(mCurrentPrint.getId(), DarkroomDBHelper.KEY_PRINT_NOTES, inNewValue);
	}
	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch(requestCode) {
			case REQUEST_ADD_DEV:
				mDevSpinner.onAddReturn(resultCode, data);
				break;
			case REQUEST_ADD_TONER:
				mTonerSpinner.onAddReturn(resultCode, data);
				break;
			case REQUEST_ADD_PAPER:
				mPaperSpinner.onAddReturn(resultCode, data);
				break;
		}
    }
}

