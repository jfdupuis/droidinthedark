package com.darkroom.manager;

import com.darkroom.library.dao.DarkroomDBAdapter;
import com.darkroom.library.dao.DarkroomDBHelper;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class CollectionManager extends Manager {
	
	private SimpleCursorAdapter mAdapter = null;
	

	/** Called when the activity is first created. */
	public void onCreate(Bundle inBundle) {
		super.onCreate(inBundle);
		setTitle(R.string.collection_manager);
		
		// Set result CANCELED in case the user backs out
        setResult(Activity.RESULT_CANCELED);
        
        populateList();
	}

	protected void populateList() {
		mCursor = mDarkroomDBAdapter.getAllCollectionCursor(
				new String[]{DarkroomDBAdapter.KEY_COLLECTION_NAME, DarkroomDBAdapter.KEY_COLLECTION_DATE},
				new String(DarkroomDBAdapter.KEY_COLLECTION_DATE + ", " + DarkroomDBAdapter.KEY_COLLECTION_NAME));
        startManagingCursor(mCursor);
        
        // Create an array to specify the fields we want to display in the list (only TITLE)
        final String[] lFrom = new String[]{DarkroomDBHelper.KEY_COLLECTION_NAME, DarkroomDBHelper.KEY_COLLECTION_DATE};
        
        // and an array of the fields we want to bind those fields to (in this case just text1)
        final int[] lTo = new int[]{R.id.name, R.id.date};
        // Now create a simple cursor adapter and set it to display
        mAdapter = new CollectionCursorAdapter(this, R.layout.collection_item, mCursor, lFrom, lTo);
        setListAdapter(mAdapter);
		
	}
	
	@Override
	protected void onListItemClick(ListView inListView, View inView, int inPosition, long inId) {
		if(mSelectOnly) {
			Intent lIntent = new Intent();
			lIntent.putExtra(SELECTION, inId);
			Logger.v("Selected collection id " + inId);
			setResult(Activity.RESULT_OK,lIntent);
			finish();
		} else {
			Intent lIntent = new Intent(this, CollectionEditor.class);
			lIntent.putExtra("collection_id", inId);
			Logger.d("Will start edition of collection id " + inId);
			startActivity(lIntent);
		}
	}

	

	@Override
	protected void addNewItem() {
		Intent lIntent = new Intent(this, CollectionEditor.class);
		Logger.v("Starting collection editor");
		startActivityForResult(lIntent,ADD_ITEM);
	}

	@Override
	protected void editItem(int inPosition, long inId) {
		Logger.d("Editing collection id:" + inId);
		
		//Start collection editor
		Intent lIntent = new Intent(this, CollectionEditor.class);
		lIntent.putExtra("collection_id", inId);
		startActivityForResult(lIntent,EDIT_ITEM);
	}
	
	protected void removeItem(int inPosition, long inId) {
		Logger.v(String.format("Remove item at index: %d", inPosition));
		boolean lReturn = mDarkroomDBAdapter.removeCollection(inId);
		Logger.v(String.format("Delete results: %b", lReturn));
		mCursor.requery();
	}
}
