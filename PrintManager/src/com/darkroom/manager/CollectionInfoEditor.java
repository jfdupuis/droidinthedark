package com.darkroom.manager;

import com.darkroom.library.Collection;
import com.darkroom.library.Logger;

import com.jfdupuis.library.screen.ScreenUtil;
import com.darkroom.library.dao.AddNewCollectionDialog;
import com.darkroom.library.dao.DarkroomDBHelper;
import com.darkroom.manager.dao.ManagerDBAdapter;
import com.jfdupuis.library.widget.DBEditText;
import com.jfdupuis.library.widget.DBEditText.OnChangedListener;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class CollectionInfoEditor extends Activity{
	static final int DATE_DIALOG_ID = 0;
	static final int REQUEST_CREATE_COLLECTION = 1;
	
	protected ManagerDBAdapter mDarkroomDBAdapter;
	protected long mCollectionId = -1;
	protected Collection mCurrent;
	protected Collection mOld;
	DBEditText mName;
	
	protected TextView mDateDisplay;
    protected Button mPickDate;
    protected int mYear;
    protected int mMonth;
    protected int mDay;
    
	// the callback received when the user "sets" the date in the dialog
    protected DatePickerDialog.OnDateSetListener mDateSetListener =
            new DatePickerDialog.OnDateSetListener() {

                public void onDateSet(DatePicker view, int year, 
                                      int monthOfYear, int dayOfMonth) {
                    mYear = year;
                    mMonth = monthOfYear;
                    mDay = dayOfMonth;
                    Calendar calendar = new GregorianCalendar(mYear, mMonth, mDay);
                    mDateDisplay.setText(String.format("%tF", calendar));
                    changedDate(calendar.getTime());
                }
            };
	
	public void onCreate(Bundle inBundle) {
		super.onCreate(inBundle);
		setContentView(R.layout.collection_info_editor);
		
		mDarkroomDBAdapter = new ManagerDBAdapter(this);
		mDarkroomDBAdapter.open();
		
		setupButtons();
		
		mDateDisplay = (TextView) findViewById(R.id.date);
        mPickDate = (Button) findViewById(R.id.select_date_btn);
        mPickDate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                showDialog(DATE_DIALOG_ID);
            }
        });

		mName = (DBEditText) findViewById(R.id.name);
		
		mCollectionId = getParent().getIntent().getLongExtra("collection_id", -1);
		if(mCollectionId != -1) {
			loadData(mCollectionId);
		}
		else {
			//Create new collection
			Intent lIntent = new Intent(CollectionInfoEditor.this, AddNewCollectionDialog.class);
        	startActivityForResult(lIntent,REQUEST_CREATE_COLLECTION);
		}
        
	}
	
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		mDarkroomDBAdapter.close();
	}
	
	void loadData(long inId) {
		
		//Get film from DB
		mCurrent = mDarkroomDBAdapter.getCollection(inId);
		mOld = new Collection(mCurrent);
		
        
        //Set date
        Calendar lCalendar = Calendar.getInstance();
        mYear = lCalendar.get(Calendar.YEAR);
        mMonth = lCalendar.get(Calendar.MONTH);
        mDay = lCalendar.get(Calendar.DAY_OF_MONTH);
        lCalendar.setTime(mCurrent.getDate());
        mDateDisplay.setText(String.format("%tF", lCalendar));
        mDateDisplay.setVisibility(View.VISIBLE);
        
		//Set film name edit box to film
        mName.setText(mCurrent.getName());
        mName.setOnChangedListener(new OnChangedListener() {
			public void changed(EditText inView) {
				changedName(inView.getText().toString());
			}
		});
        
        //Set film description edit box to film
        DBEditText lDescription = (DBEditText) findViewById(R.id.description);
        lDescription.setText(mCurrent.getDescription());
        lDescription.setOnChangedListener(new OnChangedListener() {
			public void changed(EditText inView) {
				changedDescription(inView.getText().toString());
			}
		});
        
	}
	
	void changedName(String inNewValue) {
		Logger.d("Update collection name to " + inNewValue);
		mCurrent.setName(inNewValue);
		try {
			mDarkroomDBAdapter.updateCollectionValue(mCollectionId, DarkroomDBHelper.KEY_COLLECTION_NAME, inNewValue);
		} catch (SQLiteConstraintException e) {
			Logger.v(e.getMessage());
			ScreenUtil.displayMessage(CollectionInfoEditor.this, "Collection of name " + mCurrent.getName() + " already exist", Toast.LENGTH_SHORT);
			mName.requestFocus();
		}
		
	}
	
	void changedDescription(String inNewValue){
		Logger.d("Update collection description to " +inNewValue);
		mCurrent.setDescription(inNewValue);
		mDarkroomDBAdapter.updateCollectionValue(mCollectionId, DarkroomDBHelper.KEY_COLLECTION_DESC, inNewValue);
	    
	}
	
	void changedDate(Date inNewValue){
		Logger.d("Update collection description to " +inNewValue);
		mCurrent.setDate(inNewValue);
		mDarkroomDBAdapter.updateCollectionValue(mCollectionId, DarkroomDBHelper.KEY_COLLECTION_DATE, inNewValue.getTime());
	    
	}
	
	private void setupButtons() {
		
		//Connect buttons
	    final Button lCancelButton = (Button) findViewById(R.id.cancel_btn);
	    lCancelButton.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	        	mDarkroomDBAdapter.updateCollectionValue(mOld);
	        	setResult(RESULT_OK);
	        	finish();
	        }
	    });
	    
	    final Button lUndoButton = (Button) findViewById(R.id.undo_btn);
	    lUndoButton.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	        	mDarkroomDBAdapter.updateCollectionValue(mOld);
	        	loadData(mCollectionId);
	        }
	    });
	    
	    final Button lDoneButton = (Button) findViewById(R.id.done_btn);
	    lDoneButton.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	        	//All the field are saved each time they are modified. So, we just quit.
	        	v.setFocusableInTouchMode(true);
	        	v.requestFocus();
	        	setResult(RESULT_OK);
	        	finish();
	        }
	    });

	}
	
	@Override
	protected Dialog onCreateDialog(int id) {
	    switch (id) {
	    case DATE_DIALOG_ID:
	        return new DatePickerDialog(this, mDateSetListener, mYear, mMonth, mDay);
	    }
	    return null;
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Logger.v("Activity return from request " + requestCode);
		switch(requestCode) {
		case REQUEST_CREATE_COLLECTION:
			if(resultCode == RESULT_OK) {
				final long lId = data.getLongExtra("id", -1);
				getParent().getIntent().putExtra("collection_id", lId);
				loadData(lId);
			} else {
				setResult(RESULT_CANCELED);
				finish();
			}
			break;
		}
    }
}
