package com.darkroom.manager;

import com.darkroom.manager.dao.ManagerDBAdapter;

import android.app.Activity;
import android.os.Bundle;

public class DarkroomDBActivity extends Activity {

	ManagerDBAdapter mDarkroomDBAdapter;
	
	@Override
	public void onCreate(Bundle inBundle) {
		super.onCreate(inBundle);
		mDarkroomDBAdapter = new ManagerDBAdapter(this);
		mDarkroomDBAdapter.open();
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		mDarkroomDBAdapter.close();
	}

}
