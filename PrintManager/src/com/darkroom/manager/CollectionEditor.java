package com.darkroom.manager;

import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.TabHost;

public class CollectionEditor extends TabActivity {

	public void onCreate(Bundle inBundle) {
		super.onCreate(inBundle);
		setContentView(R.layout.editor);
		setTitle(R.string.collection_editor);
		
		Resources res = getResources(); // Resource object to get Drawables
	    TabHost tabHost = getTabHost();  // The activity TabHost
	    TabHost.TabSpec spec;  // Resusable TabSpec for each tab
	    Intent intent;  // Reusable Intent for each tab
	    
	    // Setup info tab
	    intent = new Intent().setClass(this, CollectionInfoEditor.class);
	    spec = tabHost.newTabSpec("infos").setIndicator("Infos",
	                      res.getDrawable(android.R.drawable.ic_menu_info_details))
	                  .setContent(intent);
	    tabHost.addTab(spec);

	    // Setup print list tab
	    intent = new Intent().setClass(this, PrintManager.class);
	    intent.putExtra(Manager.EMBEDDED, true);
	    spec = tabHost.newTabSpec("prints").setIndicator("Prints",
	                      res.getDrawable(android.R.drawable.ic_menu_slideshow))
	                  .setContent(intent);
	    tabHost.addTab(spec);

	    
	    // Set the current tab
	    tabHost.setCurrentTab(0);
        
	}
}