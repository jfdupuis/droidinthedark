package com.darkroom.manager;

import com.darkroom.library.dao.DarkroomDBAdapter;
import com.darkroom.library.dao.DarkroomDBHelper;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class FilmFrameManager extends Manager {
	
	private SimpleCursorAdapter mFilmFrameAdapter = null;
	long mFilmId = -1;

	/** Called when the activity is first created. */
	public void onCreate(Bundle inBundle) {
		super.onCreate(inBundle);
		setTitle(R.string.filmframe_manager);
		
		// Set result CANCELED in case the user backs out
        setResult(Activity.RESULT_CANCELED);
        
        
        
        //Retrieve the film id
        if(mEmbedded) {
        	mFilmId = getParent().getIntent().getLongExtra("film_id", -1);
        } else {
        	mFilmId = getIntent().getLongExtra("film_id", -1);
        }
        
        populateList();
	}

	protected void populateList() {
		//TODO insert image
		
		mCursor = mDarkroomDBAdapter.getAllFilmFrameCursor(mFilmId, new String[]{DarkroomDBAdapter.KEY_FILMFRAME_FRAME, DarkroomDBHelper.KEY_FILMFRAME_DESC});
        startManagingCursor(mCursor);
        
        // Create an array to specify the fields we want to display in the list (only TITLE)
        final String[] lFrom = new String[]{DarkroomDBHelper.KEY_FILMFRAME_FRAME, DarkroomDBHelper.KEY_FILMFRAME_DESC};
        
        // and an array of the fields we want to bind those fields to (in this case just text1)
        final int[] lTo = new int[]{R.id.rowFilmFrameNumber, R.id.rowFilmFrameDesc};
        // Now create a simple cursor adapter and set it to display
        mFilmFrameAdapter = new SimpleCursorAdapter(this, R.layout.filmframe_item, mCursor, lFrom, lTo);
        setListAdapter(mFilmFrameAdapter);
		
	}
	
	@Override
	protected void onListItemClick(ListView inListView, View inView, int inPosition, long inId) {
		if(mSelectOnly) {
			Intent lIntent = new Intent();
			lIntent.putExtra(SELECTION, inId);
			Logger.v("Selected film frame id " + inId);
			setResult(Activity.RESULT_OK,lIntent);
			finish();
		} else {
			Intent lIntent = new Intent(this, FilmFrameEditor.class);
			lIntent.putExtra("filmframe_id", inId);
			Logger.d("Will start edition of film frame id " + inId);
			startActivity(lIntent);
		}
	}

	

	@Override
	protected void addNewItem() {
		if(!isDemoLimitReach()) {
			Intent lIntent = new Intent(this, FilmFrameEditor.class);
			lIntent.putExtra("film_id", mFilmId);
			Logger.v("Starting film frame editor");
			startActivityForResult(lIntent,ADD_ITEM);
		}
	}

	@Override
	protected void editItem(int inPosition, long inId) {
		Logger.d("Editing film id:" + inId);
		
		//Start film editor
		Intent lIntent = new Intent(this, FilmFrameEditor.class);
		lIntent.putExtra("filmframe_id", inId);
		startActivityForResult(lIntent,EDIT_ITEM);
	}
	
	protected void removeItem(int inPosition, long inId) {
		Logger.v(String.format("Remove item at index: %d", inPosition));
		boolean lReturn = mDarkroomDBAdapter.removeFilmFrame(inId);
		Logger.v(String.format("Delete results: %b", lReturn));
		mCursor.requery();
	}
	
}