package com.darkroom.manager;

import com.darkroom.library.Film;
import com.darkroom.library.Logger;
import com.darkroom.library.Print;
import com.jfdupuis.library.screen.ScreenUtil;
import com.darkroom.library.dao.AddNewCollectionDialog;
import com.darkroom.library.dao.DarkroomDBHelper;
import com.darkroom.manager.dao.ManagerDBAdapter;
import com.darkroom.manager.preferences.DarkroomManagerPrefs;
import com.darkroom.manager.widget.ListUtil;
import com.jfdupuis.library.math.Unit;
import com.jfdupuis.library.widget.DBEditText;
import com.jfdupuis.library.widget.DBEditText.OnChangedListener;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

public class PrintInfoEditor extends Activity  {
	static final int REQUEST_EDIT_PRINTID = 0;
	static final int REQUEST_CREATE_PRINT = 1;
	static final int REQUEST_ADD_COLLECTION = 3;
	static final int REQUEST_EDIT_COLLECTION = 4;
	
	class CollectionListUpdater implements Runnable {
		public void run() {
			ListUtil.setListViewHeightBasedOnChildren(mCollectionList);
		}
	}
	
	CollectionListUpdater mListUpdater = new CollectionListUpdater();
	
	ManagerDBAdapter mDarkroomDBAdapter;
	
	ListView mCollectionList;
	CollectionCursorAdapter mCollectionAdapter;
	
	long mPrintId = -1;
	Print mCurrentPrint;
	Print mOldPrint;
	
	DBEditText mDescription, mTitle;
	
	@Override
	public void onBackPressed() {
	}

	
	@Override
	public void onCreate(Bundle inBundle) {
		super.onCreate(inBundle);
		setContentView(R.layout.print_info_editor);
		
		mDarkroomDBAdapter = new ManagerDBAdapter(this);
		mDarkroomDBAdapter.open();
		
		
		setupButtons();
		setupList();
		
		mTitle = (DBEditText) findViewById(R.id.title);
		mTitle.setOnChangedListener(new OnChangedListener() {
			public void changed(EditText inView) {
				changedTitle(inView.getText().toString());
			}
		});
		
				
		mDescription = (DBEditText) findViewById(R.id.description);
		mDescription.setOnChangedListener(new OnChangedListener() {
			public void changed(EditText inView) {
				changedDescription(inView.getText().toString());
			}
		});
		
		final String lAction = getParent().getIntent().getAction();
		if(lAction != null && lAction.equals("com.darkroom.saveprint")) {
			//Call new print dialog
			Intent lIntent = new Intent(PrintInfoEditor.this, AddNewPrintDialog.class);
			lIntent.setAction(getParent().getIntent().getAction());
			lIntent.putExtra("film_id", getParent().getIntent().getLongExtra("film_id", -1));
			lIntent.putExtra("print_id", getParent().getIntent().getLongExtra("print_id", -1));
			lIntent.putExtra("print", getParent().getIntent().getParcelableExtra("print"));
        	startActivityForResult(lIntent,REQUEST_CREATE_PRINT);
		} else {
			mPrintId = getParent().getIntent().getLongExtra("print_id", -1);
			Logger.v("Print info editor, got print_id="+mPrintId);
			if(mPrintId != -1) {
				loadData(mPrintId);
			}
			else {
				//Create new print
				Intent lIntent = new Intent(PrintInfoEditor.this, AddNewPrintDialog.class);
				lIntent.putExtra("film_id", getParent().getIntent().getLongExtra("film_id", -1));
				lIntent.putExtra("filmframe_id", getParent().getIntent().getLongExtra("filmframe_id", -1));
	        	startActivityForResult(lIntent,REQUEST_CREATE_PRINT);
			}
		}
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		mDarkroomDBAdapter.close();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		Logger.d("Creating option menu");
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.info_editor, menu);
	    return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    case R.id.edit_id:
	    	//Create new film
			Intent lIntent = new Intent(PrintInfoEditor.this, AddNewPrintDialog.class);
			lIntent.putExtra("print_id", mPrintId);
        	startActivityForResult(lIntent,REQUEST_EDIT_PRINTID);
	        return true;
	    default:
	        return super.onOptionsItemSelected(item);
	    }
	}
	
	void loadData(long inPrintId) {
		//Get film from DB
		mCurrentPrint = mDarkroomDBAdapter.getPrint(inPrintId, true);
		mOldPrint = new Print(mCurrentPrint);
		Logger.v("Editing print info : " + mCurrentPrint.getId());
		
		TextView lPrintId = (TextView) findViewById(R.id.print_userid);
		lPrintId.setText(mCurrentPrint.getUserId());
		lPrintId.setVisibility(View.VISIBLE);
		
		
		TextView lFilmId = (TextView) findViewById(R.id.film_printed);
		final Film lFilmPrinted = mDarkroomDBAdapter.getFilm(mCurrentPrint.getFilmId());
		lFilmId.setText(lFilmPrinted.getUserId());
		lFilmId.setVisibility(View.VISIBLE);
		
		TextView lFrameNum = (TextView) findViewById(R.id.frame_printed);
		lFrameNum.setText(Integer.toString(mCurrentPrint.getFilmFrameNumber()));
		lFrameNum.setVisibility(View.VISIBLE);
		
		mDescription.setText(mCurrentPrint.getDescription());
		mTitle.setText(mCurrentPrint.getTitle());
		
		TextView lDate = (TextView) findViewById(R.id.date_printed);
		Calendar lCal = Calendar.getInstance();
		lCal.setTime(mCurrentPrint.getDate());
		lDate.setText(String.format("%tF", lCal));
		lDate.setVisibility(View.VISIBLE);
		
		TextView lPrintSize = (TextView) findViewById(R.id.print_size);
		final String lUnits = DarkroomManagerPrefs.getDefaultUnits(this);
		lPrintSize.setText(String.format("%.2f X %.2f %s", Unit.convertFromMM(mCurrentPrint.getSizeH(), lUnits),Unit.convertFromMM(mCurrentPrint.getSizeV(), lUnits), lUnits));
		lPrintSize.setVisibility(View.VISIBLE);
		
		updateCollectionCursor();
	}
	
	private void updateCollectionCursor() {
		Cursor lCursor = mDarkroomDBAdapter.getAllCollectionCursor(
				new String[]{DarkroomDBHelper.KEY_COLLECTION_NAME, DarkroomDBHelper.KEY_COLLECTION_DATE},
				new String(DarkroomDBHelper.KEY_COLLECTIONPRINT_PRINTID + "=" + mPrintId),
				new String(DarkroomDBHelper.KEY_COLLECTION_DATE + ", " + DarkroomDBHelper.KEY_COLLECTION_NAME),
				new String[]{DarkroomDBHelper.COLLECTIONPRINT_TABLE_NAME}
				);
		mCollectionAdapter.changeCursor(lCursor);
	}
	
	
	private void setupButtons() {
                
        final Button lCancelButton = (Button) findViewById(R.id.cancel_btn);
        lCancelButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	mDarkroomDBAdapter.updatePrintValue(mOldPrint, true);
            	//loadFilmData(mFilmId);
            	setResult(RESULT_OK);
            	finish();
            }
        });
        
        final Button lUndoButton = (Button) findViewById(R.id.undo_btn);
        lUndoButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	mDarkroomDBAdapter.updatePrintValue(mOldPrint, true);
            	loadData(mPrintId);
            }
        });
        
        final Button lDoneButton = (Button) findViewById(R.id.done_btn);
        lDoneButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	//All the field are saved each time they are modified. So, we just quit.
            	v.setFocusableInTouchMode(true);
            	v.requestFocus();
            	setResult(RESULT_OK);
            	finish();
            }
        });
        
        Button lAddCollectionButton = (Button) findViewById(R.id.add_collection_btn);
        lAddCollectionButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	addCollection();
            }
        });
	}
	
		
	private void setupList() {
		//TODO done for nothing at the beginning
		Cursor lCursor = mDarkroomDBAdapter.getAllCollectionCursor(
				new String[]{DarkroomDBHelper.KEY_COLLECTION_NAME, DarkroomDBHelper.KEY_COLLECTION_DATE},
				new String(DarkroomDBHelper.KEY_COLLECTIONPRINT_PRINTID + "=" + mPrintId),
				new String(DarkroomDBHelper.KEY_COLLECTION_DATE + ", " + DarkroomDBHelper.KEY_COLLECTION_NAME),
				new String[]{DarkroomDBHelper.COLLECTIONPRINT_TABLE_NAME}
				);
        
        final String[] lFrom = new String[]{DarkroomDBHelper.KEY_COLLECTION_NAME, DarkroomDBHelper.KEY_COLLECTION_DATE};
        final int[] lTo = new int[]{R.id.name, R.id.date};
        mCollectionAdapter = new CollectionCursorAdapter(this, R.layout.collection_item, lCursor, lFrom, lTo);
        
		mCollectionList = (ListView) findViewById(R.id.collection_list);
		mCollectionList.setAdapter(mCollectionAdapter);
		mCollectionList.post(mListUpdater);
		mCollectionList.setOnItemClickListener(new OnItemClickListener(){
			public void onItemClick(AdapterView<?> inParent, View inView, int inPosition, long inId) {
				openCollectionItem(inId);
			}
		});
		mCollectionList.setEmptyView(findViewById(R.id.empty_collection_list));
		registerForContextMenu(mCollectionList);
	}
	
	
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
	  super.onCreateContextMenu(menu, v, menuInfo);
	  MenuInflater inflater = getMenuInflater();
	  inflater.inflate(R.menu.manager_context, menu);
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
		switch (item.getItemId()) {
		case R.id.context_edit:
			editCollectionItem(info.id);
			return true;
		case R.id.context_delete:
			removeCollectionItem(info.id);
			return true;
		default:
			return super.onContextItemSelected(item);
		}
	}
	
	void changedTitle(String inNewValue) {
		mCurrentPrint.setTitle(inNewValue);
		mDarkroomDBAdapter.updatePrintValue(mCurrentPrint.getId(), DarkroomDBHelper.KEY_PRINT_TITLE, inNewValue);
	}
	
	void changedDescription(String inNewValue) {
		mCurrentPrint.setDescription(inNewValue);
		mDarkroomDBAdapter.updatePrintValue(mCurrentPrint.getId(), DarkroomDBHelper.KEY_PRINT_DESC, inNewValue);
	}
	
	void addCollection() {
    	Intent lIntent = new Intent(PrintInfoEditor.this, CollectionManager.class);
		lIntent.putExtra(Manager.SELECTONLY, true);
		startActivityForResult(lIntent, REQUEST_ADD_COLLECTION);
	}
	
	void editCollectionItem(long inId) {
		Intent lIntent = new Intent(PrintInfoEditor.this, AddNewCollectionDialog.class);
		lIntent.putExtra("id", inId);
		startActivityForResult(lIntent, REQUEST_ADD_COLLECTION);
	}
	
	void removeCollectionItem(long inId) {
		mDarkroomDBAdapter.removePrintFromCollection(inId, mCurrentPrint.getId());
		mCurrentPrint.removeCollectionId(inId);
		mCollectionAdapter.getCursor().requery();
	}
	
	void openCollectionItem(long inId) {
		Intent lIntent = new Intent(PrintInfoEditor.this, CollectionEditor.class);
		lIntent.putExtra("collection_id", inId);
		startActivityForResult(lIntent, REQUEST_ADD_COLLECTION);
	}

		
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case REQUEST_CREATE_PRINT:
			if (resultCode == RESULT_OK) {
				mPrintId = data.getLongExtra("print_id", -1);
				getParent().getIntent().putExtra("print_id", mPrintId);
				loadData(mPrintId);
			} else {
				setResult(RESULT_CANCELED);
				finish();
			}
			break;
		case REQUEST_EDIT_PRINTID:
			if (resultCode == RESULT_OK) {
				loadData(mPrintId);
			}
			break;
		case REQUEST_ADD_COLLECTION:
			if (resultCode == RESULT_OK) {
				final long lCollectionId = data.getLongExtra(Manager.SELECTION, -1);
				try{
					mDarkroomDBAdapter.insertCollectionPrintPair(lCollectionId, mPrintId);
					updateCollectionCursor();
					mCollectionList.post(mListUpdater);
				} catch (SQLiteConstraintException e) {
					Logger.e(e.getMessage(),e);
					ScreenUtil.displayMessage(PrintInfoEditor.this, "This print is already the selected collection", Toast.LENGTH_SHORT);
				}
				loadData(mPrintId);
			}
			break;
		
		case REQUEST_EDIT_COLLECTION:
			if (resultCode == RESULT_OK) {
				updateCollectionCursor();
				mCollectionList.post(mListUpdater);
			}
			break;
		}
	}

	
}
