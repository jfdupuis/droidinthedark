package com.darkroom.manager;

import com.darkroom.library.Exposure;
import com.darkroom.library.ExposureItem;
import com.jfdupuis.library.screen.ScreenUtil;
import com.darkroom.library.dao.AddNewElementDialog;
import com.darkroom.library.dao.DarkroomDBHelper;
import com.jfdupuis.library.math.Fraction;
import com.jfdupuis.library.text.FractionInputFilter;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.DigitsKeyListener;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class AddNewExposureItemDialog extends AddNewElementDialog {

	ExposureItem mItem;
	EditText mValue, mDescription;
	Fraction mBase = null;
	boolean mIsSeconds = false;
	boolean mIsInserting = false;
	int mExposureType = -1;
	int mPosition;
	long mPrintId;
	float mGrade;
	static final DecimalFormat mFormat = new DecimalFormat("0.000");

	FractionInputFilter mNumberFilter;

	@Override
	public void onCreate(Bundle inBundle) {
		super.onCreate(inBundle);
		setContentView(R.layout.addnew_exposureitem_dialog);

		initialize();

		mValue = (EditText) findViewById(R.id.value);
		mDescription = (EditText) findViewById(R.id.desc);

		mBase = new Fraction(getIntent().getStringExtra("basetime"));
		mIsSeconds = getIntent().getBooleanExtra("isSeconds", false);
		mGrade = getIntent().getFloatExtra("exposure_grade", -3);
		mPrintId = getIntent().getLongExtra("print_id", -1);
		
		if(getIntent().getAction().equals(PrintExposureEditor.ADD_EXPOSUREITEM)) {
			mExposureType = getIntent().getIntExtra("exposure_type", -1);
			mPosition = getIntent().getIntExtra("exposure_position", -1);			
		} else {
			//Edit or insert
			final ExposureItem lExposureItem = mDarkroomDBAdapter.getExposureItem(mId);
			mExposureType = lExposureItem.getType();
			mPosition = lExposureItem.getPosition();
			
			if(getIntent().getAction().equals(PrintExposureEditor.INSERT_EXPOSUREITEM)) {
				mId = -1;
				setEditingMode(false);
				mIsInserting = true;
			}
		}
		
		
//		mNumberFilter = new FractionWithLimitDecimalInputFilter(1);
		if(mIsSeconds) {  
//			mNumberFilter.setActive(true);
			mValue.setKeyListener(new DigitsKeyListener(true,true));
		} else {
			mValue.setKeyListener(new FractionInputFilter(true));
		}
		
		if (isEditing()) {
			setTitle(R.string.edit_exposureitem_title);
			loadData();
		} else {
			setTitle(R.string.addnew_exposureitem_title);
		}
	}

	@Override
	protected void loadData() {
		ExposureItem lExposureItem = mDarkroomDBAdapter.getExposureItem(mId);
		
		setValue(lExposureItem.getValue());
		mDescription.setText(lExposureItem.getDescription());
		
	}

	public Fraction getValue() {
		Fraction lExposureValue;
		if (mIsSeconds) {
			lExposureValue = Exposure.getStop(Exposure.getTime(mBase), Double.valueOf(mValue.getText().toString()).doubleValue(), mExposureType == ExposureItem.BURNEXPOSURE);
		} else {
			lExposureValue = new Fraction(mValue.getText().toString());
		}

		return lExposureValue;
	}
	
	public void setValue(Fraction inValue) {
        if(mIsSeconds) {
        	mValue.setText(mFormat.format(Exposure.getRelativeTime(mBase, inValue, mExposureType == ExposureItem.BURNEXPOSURE )));
        }
        else
        	mValue.setText(inValue.toString());
	}

	@Override
	public void onClick(View mV) {
		// Add exposure

		if (TextUtils.isEmpty(mValue.getText().toString())) {
			ScreenUtil.displayMessage(AddNewExposureItemDialog.this, "An exposure value is needed !", Toast.LENGTH_SHORT);
			return;
		}

		final String lDescStr = mDescription.getText().toString();

		try {
			final Fraction lValue = getValue();

			long lId = mId;
			if (isEditing()) {
				Logger.d("Updating exposure item id " + mId);
				mDarkroomDBAdapter.updateExposureItem(mId, lValue, lDescStr);
			} else {
				if(mIsInserting) {
					//Change position of the previous entry
					ArrayList<ExposureItem> lItems = mDarkroomDBAdapter.getExposureItems(mPrintId, mGrade, mExposureType, DarkroomDBHelper.KEY_EXPOSUREITEM_POSITION + ">=" + mPosition);
					for(int i = lItems.size()-1; i >= 0; --i) {
						//Need to go reverse, if not SQL constraint will raise
						lItems.get(i).setPosition(lItems.get(i).getPosition()+1);
						mDarkroomDBAdapter.updateExposureItem(mPrintId, mGrade, mExposureType, lItems.get(i));
					}
				} 
				
				Logger.d("Creating new exposure item at position " + mPosition);
				lId = mDarkroomDBAdapter.insertExposureItem(mPrintId, mGrade, mExposureType, new ExposureItem(mPosition, mExposureType, lDescStr, lValue));
				Logger.d("Created exposure item id " + lId);
				
			}
			setResult(RESULT_OK, new Intent().putExtra("id", lId));
			finish();
		} catch (NumberFormatException e) {
			ScreenUtil.displayMessage(AddNewExposureItemDialog.this, "Invalide exposure value!", Toast.LENGTH_SHORT);
			mValue.requestFocus();
			return;
		}

	}

}
