package com.darkroom.manager;

import com.darkroom.library.Exposure;
import com.darkroom.library.ExposureItem;
import com.jfdupuis.library.screen.ScreenUtil;
import com.darkroom.library.dao.AddNewGradeDialog;
import com.darkroom.library.dao.DarkroomDBHelper;
import com.darkroom.manager.dao.ManagerDBAdapter;
import com.darkroom.manager.preferences.DarkroomManagerPrefs;
import com.darkroom.manager.widget.ListUtil;
import com.jfdupuis.library.math.Fraction;
import com.jfdupuis.library.widget.DBEditText;
import com.jfdupuis.library.widget.DBEditText.OnChangedListener;
import com.jfdupuis.library.widget.DBSpinner;
import com.jfdupuis.library.widget.DBSpinnerDialog;
import com.jfdupuis.library.text.FractionInputFilter;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.DigitsKeyListener;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import java.text.DecimalFormat;

public class PrintExposureEditor extends Activity {
	
	static final int REQUEST_ADD_GRADE = 0;
	static final int REQUEST_EDIT_GRADE = 1;
	static final int EXPOSE_PRINT = 2;
	
	static final int REQUEST_EDIT_EXPOSUREITEM = 11;
	
	static final String EDIT_EXPOSUREITEM = "edit_exposureitem";
	static final String ADD_EXPOSUREITEM = "add_exposureitem";
	static final String INSERT_EXPOSUREITEM = "insert_exposureitem";
	
	static final DecimalFormat mFormat = new DecimalFormat("0.000");
	
	ManagerDBAdapter mDarkroomDBAdapter;
	
	FractionInputFilter mFractionFilter;
	DigitsKeyListener mDecimalFilter;
	
	ListView mDodgeList;
	ListView mBurnList;
	ExposureCursorAdapter mBurnAdapter;
	ExposureCursorAdapter mDodgeAdapter;
	DBSpinner mGradeSpinner;
	
	CheckBox mSkipBase;
	DBEditText mBase;
	float mGradeValue;
	boolean mUseLinearTiming = false;
	
	RadioButton mSeconds;
	RadioButton mFStop;
	
	long mPrintId;
	Exposure mCurrent; //Not use for ExposureItem
	Exposure mOldExposure;

	Button mAddDodgeButton;
	Button mAddBurnButton;
	
	EditText mmEditItemValue = null;
	EditText mmEditItemDesc = null;
	
    
    final String[] mItemFrom = new String[]{
			DarkroomDBHelper.KEY_EXPOSUREITEM_POSITION,
			DarkroomDBHelper.KEY_EXPOSUREITEM_VALUE, 
			DarkroomDBHelper.KEY_EXPOSUREITEM_DESC };;
    final int[] mItemTo = new int[]{
			R.id.position,
			R.id.value,
			R.id.desc };
    
    final private OnClickListener mTimeBaseChangedListener = new OnClickListener() {
	    public void onClick(View inView) {
	        // Perform action on clicks
	        RadioButton lButton = (RadioButton) inView;
	        switch (lButton.getId()) {
	        	case R.id.time_seconds:
	        		mUseLinearTiming = true;
	        		changedTimeBase();
	        		return;
				case R.id.time_fstop:
					mUseLinearTiming = false;
					changedTimeBase();
					return; 
				default:
					return;
	        }
	    }
	};

	@Override
	public void onBackPressed() {
	}

	
	@Override
	public void onCreate(Bundle inBundle) {
		super.onCreate(inBundle);
		setContentView(R.layout.print_expo_editor);
		
		mDarkroomDBAdapter = new ManagerDBAdapter(this);
		mDarkroomDBAdapter.open();
		
		mPrintId = getParent().getIntent().getLongExtra("print_id", -1);
		
		//mFractionFilter = new FractionWithLimitDecimalInputFilter(1);
		mFractionFilter = new FractionInputFilter(false);
		mDecimalFilter = new DigitsKeyListener(false,true);
		
		setupSpinners();
		setupButtons();
		setupList();
		
		
		
		mSeconds = (RadioButton) findViewById(R.id.time_seconds);
		mFStop = (RadioButton) findViewById(R.id.time_fstop);
		mSeconds.setOnClickListener(mTimeBaseChangedListener);
		mFStop.setOnClickListener(mTimeBaseChangedListener);
		mUseLinearTiming = !DarkroomManagerPrefs.useFStop(this);
		if(mUseLinearTiming)  {
			mSeconds.toggle();
		}
		else {
			mFStop.toggle();
		}
		
		mSkipBase = (CheckBox) findViewById(R.id.skip_base);
		mSkipBase.setEnabled(false);
		mSkipBase.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(mCurrent == null)
					throw new RuntimeException("A grade should be first defined!");
				mCurrent.setSkipBase(isChecked);
				//Save state
				mDarkroomDBAdapter.updateExposureValue(mCurrent.getId(), DarkroomDBHelper.KEY_EXPOSURE_SKIPBASE, isChecked ? "1" : "0");
			}
		});
		
		mBase = (DBEditText) findViewById(R.id.exposure_base);
		mBase.setEnabled(false);
		mBase.setKeyListener(mFractionFilter);
		mBase.setOnChangedListener(new OnChangedListener() {
			public void changed(EditText inView) {
				changedBase(inView.getText().toString());
			}
		});
		
		if(mPrintId != -1) {
			loadData();
		}
		else
			throw new RuntimeException("A print id was expected.");
	}
	
	@Override
	public void onRestart() {
		super.onRestart();
		loadData();
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		mDarkroomDBAdapter.close();
	}
	
	private void createNewGrade() {
		Logger.d("Creating new exposure with grade " + mGradeValue);
		mCurrent = new Exposure(mGradeValue, "");
		mCurrent.setLinear(mUseLinearTiming);
		mDarkroomDBAdapter.insertExposure(mPrintId,mCurrent);
		loadData(); //To be sure all the default created field are loaded properly
	}
	
	private void editGrade() {
		mCurrent.setGrade(mGradeValue);
		mDarkroomDBAdapter.updateExposureValue(mCurrent.getId(),DarkroomDBHelper.KEY_EXPOSURE_GRADE, Float.toString(mCurrent.getGrade()));
	}
	
	private void loadData() {
		Logger.d("Loading exposure data");
		mCurrent = mDarkroomDBAdapter.getExposure(mPrintId, mGradeValue);
		if(mCurrent == null) //No grade exist 
			return;
		
		//Save for undo
		mOldExposure = new Exposure(mCurrent);
		
		//Enable exposure related input
		mBase.setEnabled(true);
		mAddDodgeButton.setEnabled(true);
		mAddBurnButton.setEnabled(true);
		mSkipBase.setEnabled(true);
		
		//Load data
		updateBaseDisplay();
		mSkipBase.setChecked(mCurrent.isSkipBase());
		
		Cursor lBurnCursor = mDarkroomDBAdapter.getAllExposureItemCursor(
				mPrintId, mGradeValue,ExposureItem.BURNEXPOSURE, 
				mItemFrom,DarkroomDBHelper.KEY_EXPOSUREITEM_POSITION);
		mBurnAdapter.changeCursor(lBurnCursor);
		mBurnList.setAdapter(mBurnAdapter);
        ListUtil.setListViewHeightBasedOnChildren(mBurnList);
        
        
        Cursor lDodgeCursor = mDarkroomDBAdapter.getAllExposureItemCursor(
        		mPrintId, mGradeValue,ExposureItem.DODGEEXPOSURE, 
        		mItemFrom,DarkroomDBHelper.KEY_EXPOSUREITEM_POSITION);
        mDodgeAdapter.changeCursor(lDodgeCursor);
        mDodgeList.setAdapter(mDodgeAdapter);
		ListUtil.setListViewHeightBasedOnChildren(mDodgeList);
		
		
		mUseLinearTiming = mCurrent.isLinear();
		if(mUseLinearTiming) 
			mSeconds.toggle(); 
		else mFStop.toggle();
		changedTimeBase();
	}
		
	private void setupButtons() {
		//Connect buttons
        final Button lCancelButton = (Button) findViewById(R.id.cancel_btn);
        lCancelButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	undo();
            	setResult(RESULT_OK);
            	finish();
            }
        });
        
        final Button lUndoButton = (Button) findViewById(R.id.undo_btn);
        lUndoButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	undo();
            	loadData();
            }
        });
        
        final Button lDoneButton = (Button) findViewById(R.id.done_btn);
        lDoneButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	//All the field are saved each time they are modified. So, we just quit.
            	v.setFocusableInTouchMode(true);
            	v.requestFocus();
            	setResult(RESULT_OK);
            	finish();
            }
        });
        
        final Button lExposeButton = (Button) findViewById(R.id.expose_btn);
        lExposeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	exposePrint();
            }
        });
        
        mAddDodgeButton = (Button) findViewById(R.id.add_dodge_btn);
        mAddDodgeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	addExposureItem(mDodgeAdapter.getCount(),ExposureItem.DODGEEXPOSURE);
            }
        });
        mAddDodgeButton.setEnabled(false);
		
        mAddBurnButton = (Button) findViewById(R.id.add_burn_btn);
        mAddBurnButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	addExposureItem(mBurnAdapter.getCount(), ExposureItem.BURNEXPOSURE);
            }
        });
        mAddBurnButton.setEnabled(false);
	}
	
		
	private void undo() {
		//Remove added exposures
		
		for(ExposureItem lNewItem : mDarkroomDBAdapter.getExposureItems(mPrintId, mGradeValue, ExposureItem.BURNEXPOSURE, null)) {
			if( ExposureItem.find(mOldExposure.getBurnExposureItems(), lNewItem) < 0 ) {
				//Remove item
				mDarkroomDBAdapter.removeExposureItem(mPrintId, mCurrent.getGrade(), lNewItem.getId());
			}
		}
		for(ExposureItem lNewItem : mDarkroomDBAdapter.getExposureItems(mPrintId, mGradeValue, ExposureItem.DODGEEXPOSURE, null)) {
			if( ExposureItem.find(mOldExposure.getDodgeExposureItems(), lNewItem) < 0 ) {
				//Remove item
				mDarkroomDBAdapter.removeExposureItem(mPrintId, mCurrent.getGrade(), lNewItem.getId());
			}
		}
		
		//Add removed exposures
		for(ExposureItem lItem : mOldExposure.getBurnExposureItems()) {
			if( ExposureItem.find(mDarkroomDBAdapter.getExposureItems(mPrintId, mGradeValue, ExposureItem.BURNEXPOSURE, null), lItem) < 0 ) {
				//Add item
				mDarkroomDBAdapter.insertExposureItem(mPrintId, mOldExposure.getGrade(), ExposureItem.BURNEXPOSURE, lItem);
			}
		}
		for(ExposureItem lItem : mOldExposure.getDodgeExposureItems()) {
			if( ExposureItem.find(mDarkroomDBAdapter.getExposureItems(mPrintId, mGradeValue, ExposureItem.DODGEEXPOSURE, null), lItem) < 0 ) {
				//Add missing item
				mDarkroomDBAdapter.insertExposureItem(mPrintId, mOldExposure.getGrade(), ExposureItem.DODGEEXPOSURE, lItem);
			}
		}
		
		//Update the remaining entry to the old values
		mDarkroomDBAdapter.updateExposureValue(mPrintId, mOldExposure);
		
	}
	
	protected void exposePrint() {
		Intent lIntent = new Intent();
		lIntent.setAction( "com.darkroom.print" );
		lIntent.putExtra("print", mDarkroomDBAdapter.getPrint(mPrintId, false));
		lIntent.putExtra("print_id", mPrintId);
		try {
			startActivityForResult(lIntent,EXPOSE_PRINT);
			finish();
		} catch(ActivityNotFoundException inException) {
			ScreenUtil.displayMessage(PrintExposureEditor.this, "Unable to start the F-Stop Timer application", Toast.LENGTH_SHORT);
		}
	}
	
	
	void setupList() {
		
		Cursor lDodgeCursor = mDarkroomDBAdapter.getAllExposureItemCursor(mPrintId, mGradeValue,ExposureItem.DODGEEXPOSURE, mItemFrom,DarkroomDBHelper.KEY_EXPOSUREITEM_POSITION);
		mDodgeAdapter = new ExposureCursorAdapter(this, R.layout.exposurelist_item, lDodgeCursor, mItemFrom, mItemTo);
		mDodgeList = (ListView) findViewById(R.id.dodge_exposure_list);
		mDodgeList.setAdapter(mDodgeAdapter);
		mDodgeAdapter.setFormat(mFormat);
		mDodgeAdapter.setExposureType(ExposureItem.DODGEEXPOSURE);
		mDodgeList.post(new Runnable() { //Solve the issue of wrong resizing
			public void run() {
				ListUtil.setListViewHeightBasedOnChildren(mDodgeList);
			}
		});
		mDodgeList.setOnItemClickListener(new OnItemClickListener(){
			public void onItemClick(AdapterView<?> inParent, View inView, int inPosition, long inId) {
				editExposureItem(inId);
			}
			
		});
		registerForContextMenu(mDodgeList);
		
        
        Cursor lBurnCursor = mDarkroomDBAdapter.getAllExposureItemCursor(mPrintId, mGradeValue,ExposureItem.BURNEXPOSURE, mItemFrom,DarkroomDBHelper.KEY_EXPOSUREITEM_POSITION);
        mBurnAdapter = new ExposureCursorAdapter(this, R.layout.exposurelist_item, lBurnCursor, mItemFrom, mItemTo);
        mBurnList = (ListView) findViewById(R.id.burn_exposure_list);
        mBurnList.setAdapter(mBurnAdapter);
        mBurnAdapter.setFormat(mFormat);
        mBurnAdapter.setExposureType(ExposureItem.BURNEXPOSURE);
        mBurnList.post(new Runnable() { //Solve the issue of wrong resizing
			public void run() {
				ListUtil.setListViewHeightBasedOnChildren(mBurnList);
			}
		});
        mBurnList.setOnItemClickListener(new OnItemClickListener(){
			public void onItemClick(AdapterView<?> inParent, View inView, int inPosition, long inId) {
				editExposureItem(inId);
			}
			
		});
        registerForContextMenu(mBurnList);
	}
	

	
	void setupSpinners() {
		//Grade spinner
		mGradeSpinner = (DBSpinner) findViewById(R.id.grade_spinner); 
        mGradeSpinner.setAdapter(
        		mDarkroomDBAdapter.getAllExposureCursor(
        				mPrintId, new String []{DarkroomDBHelper.KEY_EXPOSURE_GRADE}, 
        				DarkroomDBHelper.KEY_EXPOSURE_GRADE), 
				DarkroomDBHelper.KEY_EXPOSURE_GRADE);
        mGradeSpinner.setOnActionRequestHandler(new DBSpinner.OnActionRequest(){
			public void onActionRequest(Spinner mInSpinner, int inAction, int inPosition, long inId) {
				Intent lIntent = new Intent(PrintExposureEditor.this, AddNewGradeDialog.class);
				lIntent.putExtra("print_id", mPrintId);
				Cursor lCursor;
				switch(inAction) {
				case DBSpinnerDialog.ADD_REQUEST:
					startActivityForResult(lIntent, REQUEST_ADD_GRADE);
	            	return;
				case DBSpinnerDialog.EDIT_REQUEST:
					lCursor = mGradeSpinner.getCursorAdapter().getCursor();
					lIntent.putExtra("grade", lCursor.getFloat(lCursor.getColumnIndex(DarkroomDBHelper.KEY_EXPOSURE_GRADE)));
					startActivityForResult(lIntent, REQUEST_EDIT_GRADE);
					return;
				case DBSpinnerDialog.ITEM_SELECTION:
					lCursor = mGradeSpinner.getCursorAdapter().getCursor();
					changedGrade(lCursor.getFloat(lCursor.getColumnIndex(DarkroomDBHelper.KEY_EXPOSURE_GRADE)));
					return;
				}
				
			}
		});
        Cursor lCursor = mGradeSpinner.getCursorAdapter().getCursor();
        if(lCursor.getCount() > 0)
        	mGradeValue = lCursor.getFloat(lCursor.getColumnIndex(DarkroomDBHelper.KEY_EXPOSURE_GRADE));
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
	  super.onCreateContextMenu(menu, v, menuInfo);
	  MenuInflater inflater = getMenuInflater();
	  inflater.inflate(R.menu.exposureitem_context, menu);
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
		switch (item.getItemId()) {
		case R.id.context_edit:
			editExposureItem(info.id);
			return true;
		case R.id.context_insert:
			insertExposureItem(info.id); //Insert an item after selected id of the same type
			return true;
		case R.id.context_delete:
			removeExposureItem(info.position, info.id);
			return true;
		default:
			return super.onContextItemSelected(item);
		}
	}
	
	private Intent getExposureItemIntent(long inId) {
		Intent lIntent = new Intent(this, AddNewExposureItemDialog.class);
		lIntent.putExtra("id", inId);
		lIntent.putExtra("print_id", mPrintId);
		lIntent.putExtra("exposure_grade", mCurrent.getGrade());
		lIntent.putExtra("basetime", mCurrent.getBaseExposure().toStringShort());
		lIntent.putExtra("isSeconds", mSeconds.isChecked());
		return lIntent;
	}
	
	void insertExposureItem(long inId) {
		Intent lIntent = getExposureItemIntent(inId);
		lIntent.setAction(INSERT_EXPOSUREITEM);
		startActivityForResult(lIntent, REQUEST_EDIT_EXPOSUREITEM);
	}
	
	void addExposureItem(int inPosition, int inType) {
		Intent lIntent = getExposureItemIntent(-1);
		lIntent.putExtra("exposure_type", inType);
		lIntent.putExtra("exposure_position", inPosition);
		lIntent.setAction(ADD_EXPOSUREITEM);
		startActivityForResult(lIntent, REQUEST_EDIT_EXPOSUREITEM);
	}
	
	void editExposureItem(long inId) {
		Intent lIntent = getExposureItemIntent(inId);
		lIntent.setAction(EDIT_EXPOSUREITEM);
		startActivityForResult(lIntent, REQUEST_EDIT_EXPOSUREITEM);
	}
	
	void removeExposureItem(int inPosition, long inId) {
		//Remove item from DB
		ExposureItem lItem = mDarkroomDBAdapter.getExposureItem(inId);
		mDarkroomDBAdapter.removeExposureItem(mPrintId, mCurrent.getGrade(), inId);
		switch(lItem.getType()) {
		case ExposureItem.BURNEXPOSURE:
			mBurnAdapter.getCursor().requery();
			ListUtil.setListViewHeightBasedOnChildren(mBurnList);
			break;
		case ExposureItem.DODGEEXPOSURE:
			mDodgeAdapter.getCursor().requery();
			ListUtil.setListViewHeightBasedOnChildren(mDodgeList);
			break;
		}
	}
		
	void changedGrade(float inNewValue) {
		mGradeValue = inNewValue;
		Logger.d("Changing grade to " + inNewValue);
		loadData();
		
	}
	
	void changedBase(String inNewValue) {
		if(mCurrent == null && mBase.isEnabled()) 
			throw new RuntimeException("An exposure should be first defined by adding a grade.");
		if(!TextUtils.isEmpty(inNewValue)) {
			try {
				Fraction lBaseExposure;
				if(mUseLinearTiming){
					lBaseExposure = Exposure.getStop(Double.valueOf(mBase.getText().toString()).doubleValue());
				}
				else {
					lBaseExposure = new Fraction(inNewValue);
				}
				mCurrent.setBaseExposure(lBaseExposure);
				Logger.d("Update film base exposure to " + lBaseExposure.toString());
				mDarkroomDBAdapter.updateExposureValue(mCurrent.getId(), DarkroomDBHelper.KEY_EXPOSURE_BASE, lBaseExposure.toString());
				updateBaseDisplay();
			} catch(NumberFormatException e) {
				ScreenUtil.displayMessage(PrintExposureEditor.this, "Invalide base exposure input.", Toast.LENGTH_SHORT);
				updateBaseDisplay();
				return;
			} 
		} else {
			//Put old value back
			updateBaseDisplay();
			return;
		}
	}
	
	void changedTimeBase() {
		mDodgeAdapter.setLinear(mUseLinearTiming);
		mDodgeAdapter.notifyDataSetChanged();
		mBurnAdapter.setLinear(mUseLinearTiming);
		mBurnAdapter.notifyDataSetChanged();
		
		if(mUseLinearTiming)
			mBase.setKeyListener(mDecimalFilter);
		else
			mBase.setKeyListener(mFractionFilter);
			
		if(mCurrent != null) {
			mCurrent.setLinear(mUseLinearTiming);
			mDarkroomDBAdapter.updateExposureValue(mCurrent.getId(), DarkroomDBHelper.KEY_EXPOSURE_ISLINEAR, new Integer(mUseLinearTiming ? 1 : 0).toString());
			updateBaseDisplay();
			updateExposureItemDisplay();
		}
		
		
	}
	
	void updateBaseDisplay() {
		if(mUseLinearTiming) {
			mBase.setText(mFormat.format(Exposure.getTime(mCurrent.getBaseExposure())));
		}
		else {	
			mBase.setText(mCurrent.getBaseExposure().toString());
		}
		
		mDodgeAdapter.setBaseTime(mCurrent.getBaseExposure());
		mBurnAdapter.setBaseTime(mCurrent.getBaseExposure());
	}
	
	void updateExposureItemDisplay() {
		//TODO Usefull ??
	}
	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Logger.v("Activity return from request " + requestCode);
		if(resultCode != RESULT_OK)
			return;
		
		switch(requestCode) {
		case REQUEST_EDIT_GRADE:
			mGradeValue = data.getFloatExtra("grade", -2);
			if(mGradeValue > -2)
				editGrade();
			break;
		case REQUEST_ADD_GRADE:
			mGradeValue = data.getFloatExtra("grade", -2);
			if(mGradeValue > -2)
				createNewGrade(); //Add exposure with new grade in DB
			break;
		case REQUEST_EDIT_EXPOSUREITEM:
			mBurnAdapter.getCursor().requery();
			mDodgeAdapter.getCursor().requery();
			ListUtil.setListViewHeightBasedOnChildren(mDodgeList);
			ListUtil.setListViewHeightBasedOnChildren(mBurnList);
			return;
		case EXPOSE_PRINT:
			Cursor lCursor = mGradeSpinner.getCursorAdapter().getCursor();
			lCursor.requery();
			loadData();
			return;
		}
		
		
		if(requestCode == REQUEST_EDIT_GRADE || requestCode == REQUEST_ADD_GRADE) {
			//Update spinner list and set selection to the newly added grade
			Cursor lCursor = mGradeSpinner.getCursorAdapter().getCursor();
			lCursor.requery();
			if (lCursor.moveToFirst()) {
		        final int lIdx = lCursor.getColumnIndex(DarkroomDBHelper.KEY_EXPOSURE_GRADE);
		        int lPosition = 0;
		        do {
		        	if(lCursor.getFloat(lIdx) == mGradeValue) {
		        		mGradeSpinner.setSelection(lPosition);
		        		return;
		        	}
		        	lPosition++;
		        } while (lCursor.moveToNext());
		    }
		}
    }
	
	
}