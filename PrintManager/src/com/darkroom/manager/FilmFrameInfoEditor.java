package com.darkroom.manager;

import com.darkroom.library.FilmFrame;
import com.darkroom.library.Logger;

import com.darkroom.manager.dao.ManagerDBAdapter;
import com.darkroom.manager.preferences.DarkroomManagerPrefs;
import com.jfdupuis.library.screen.ScreenUtil;
import com.jfdupuis.library.widget.DBEditText;
import com.jfdupuis.library.widget.DBEditText.OnChangedListener;
import com.jfdupuis.library.BitmapUtils;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class FilmFrameInfoEditor extends Activity {
	final int IMAGE_MAX_SIZE = 1200000;
	
	static final int DATE_DIALOG_ID = 0;
	static final int REQUEST_CREATE_FILMFRAME = 1;
	static final int REQUEST_EDIT_FILMFRAMEID = 2;
	
	ManagerDBAdapter mDarkroomDBAdapter;

	FilmFrame mOld = null;
	FilmFrame mCurrent = null;
	long mFilmFrameId = -1;
	long mFilmId = -1;
	String mImagePath = "";
	
	ImageView mImageView = null;
	
	protected TextView mDateDisplay;
    protected Button mPickDate;
    protected int mYear;
    protected int mMonth;
    protected int mDay;
    
    boolean mReceivedNewImage = false;
    
	@Override
	public void onBackPressed() {
	}
	
    @Override
	public void onCreate(Bundle inBundle) {
		super.onCreate(inBundle);
		setContentView(R.layout.filmframe_info_editor);
		
		mDarkroomDBAdapter = new ManagerDBAdapter(this);
		mDarkroomDBAdapter.open();
		
		setupButtons();
		
		initialize();

		final String lAction = getParent().getIntent().getAction();
		
		mFilmFrameId = getParent().getIntent().getLongExtra("filmframe_id", -1);
		mFilmId = getParent().getIntent().getLongExtra("film_id", -1);
		
		if(lAction != null && lAction.equals("com.darkroom.savepicture")) {
			//Called from external program like ViewFinder 
			mImagePath = getParent().getIntent().getStringExtra("picture_filename");
			//TODO get focal length and image size 
			Logger.d("Receiving image path" + mImagePath);
			
			if(mImagePath == null)
				throw new RuntimeException("Image path was expected!");
			
			mReceivedNewImage = true;
			
			if(mFilmFrameId < 0) {
				Intent lIntent = new Intent(FilmFrameInfoEditor.this, AddNewFilmFrameDialog.class);
				lIntent.setAction(getParent().getIntent().getAction());
				lIntent.putExtra("film_id", mFilmId);
				startActivityForResult(lIntent, REQUEST_CREATE_FILMFRAME);
			} else {
				loadData(mFilmFrameId);
			}
		} else {
			//Called from within NoteKeeper
			
			
			if(mFilmFrameId != -1) {
				loadData(mFilmFrameId);
			}
			else {
				//Create new film frame
				Intent lIntent = new Intent(FilmFrameInfoEditor.this, AddNewFilmFrameDialog.class);
				lIntent.putExtra("film_id", mFilmId);
	        	startActivityForResult(lIntent,REQUEST_CREATE_FILMFRAME);
			}
		}
        
	}
	
    @Override
    protected void onNewIntent (Intent intent) {
    	Logger.d("FilmFrameInfoEditor.onNewIntent");
    }
    
	@Override
	public void onDestroy() {
		super.onDestroy();
		mDarkroomDBAdapter.close();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		Logger.d("Creating option menu");
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.info_editor, menu);
	    return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    case R.id.edit_id:
	    	//Create new film
			Intent lIntent = new Intent(FilmFrameInfoEditor.this, AddNewFilmFrameDialog.class);
			lIntent.putExtra("filmframe_id", mFilmFrameId);
			lIntent.putExtra("id", mFilmFrameId);
			lIntent.putExtra("film_id", mFilmId);
        	startActivityForResult(lIntent,REQUEST_EDIT_FILMFRAMEID);
	        return true;
	    default:
	        return super.onOptionsItemSelected(item);
	    }
	}
	
	
	
	void loadData(long inId) {
		mCurrent = mDarkroomDBAdapter.getFilmFrame(inId);
		mOld = new FilmFrame(mCurrent);
		
		//Set id
        TextView lIdDisplay = (TextView) findViewById(R.id.userid);
        lIdDisplay.setText(mDarkroomDBAdapter.getFilm(mCurrent.getFilmId()).getUserId());
        
        //Frame
        TextView lFrameNumber = (TextView) findViewById(R.id.framenumber);
        lFrameNumber.setText(Integer.toString(mCurrent.getFrameNumber()));
        
        //Date
        Calendar lCalendar = Calendar.getInstance();
        mYear = lCalendar.get(Calendar.YEAR);
        mMonth = lCalendar.get(Calendar.MONTH);
        mDay = lCalendar.get(Calendar.DAY_OF_MONTH);
        if(mCurrent.getDate().getTime() < 0) {
        	lCalendar.setTime(mCurrent.getDate()); //TODO what happen with date = -1
        	mDateDisplay.setText("0000-00-00");
        	//mDateDisplay.setTextColor(getResources().getColor(R.color.red));
        } else {
        	lCalendar.setTime(mCurrent.getDate()); //TODO what happen with date = -1
        	mDateDisplay.setText(String.format("%tF", lCalendar));
        }
        
        //Description
        DBEditText lDescription = (DBEditText) findViewById(R.id.description);
        lDescription.setText(mCurrent.getDescription());
        
        //Display linked image
        if(mReceivedNewImage) {
        	receivedNewImage(mImagePath);
        }
        if(!TextUtils.isEmpty(mCurrent.getImagePath())) {
        	File lImageFile = new  File(mCurrent.getImagePath());
    		if(lImageFile.exists()){
    			mImageView.setImageBitmap(BitmapUtils.getBitmap(mCurrent.getImagePath(), IMAGE_MAX_SIZE));
    		} else {
    			Logger.w("Unable to retreive image file: " + mCurrent.getImagePath());
    		}
        }
        
	}
	
	/**
	 * Move received image to picture directory and link path in database
	 * @param lImagePath
	 */
	protected void receivedNewImage(String lImagePath) {
		//Rename file
		File lPhotoFile = new File(lImagePath);
		if (lPhotoFile.exists()) {
			File lTargetDir = new File(DarkroomManagerPrefs.getPictureSaveDir(this));
			if (!lTargetDir.exists()) {
				Logger.d("Creating directiory");
				lTargetDir.mkdirs();
			}
			
			String lFilename = mDarkroomDBAdapter.getFilm(mCurrent.getFilmId()).getUserId() + "-" + mCurrent.getFrameNumber();
			
			File lNewFile = new File(lTargetDir, lFilename+".jpg");
			Logger.d("Moving picture file " + lPhotoFile.getAbsolutePath() + " to: " + lNewFile.getAbsolutePath());
			lPhotoFile.renameTo(lNewFile);
			mImagePath = lNewFile.getAbsolutePath();
			
			mCurrent.setImagePath(lNewFile.getAbsolutePath());
			mDarkroomDBAdapter.updateFilmFrameValue(mCurrent);
		} else {
			throw new RuntimeException(mImagePath + " doesn't exist!");
		}
	}
	
	private void initialize() {
		mImageView = (ImageView) findViewById(R.id.imageView);
		
		
		mDateDisplay = (TextView) findViewById(R.id.date);
        mPickDate = (Button) findViewById(R.id.select_date_btn);
        mPickDate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                showDialog(DATE_DIALOG_ID);
            }
        });
        
        //Set film description edit box to film
        DBEditText lDescription = (DBEditText) findViewById(R.id.description);
        lDescription.setOnChangedListener(new OnChangedListener() {
			public void changed(EditText inView) {
				mCurrent.setDescription(inView.getText().toString());
				mDarkroomDBAdapter.updateFilmFrameValue(mCurrent);
			}
		});
	}
	
	@Override
	protected Dialog onCreateDialog(int id) {
	    switch (id) {
	    case DATE_DIALOG_ID:
	        return new DatePickerDialog(this, mDateSetListener, mYear, mMonth, mDay);
	    }
	    return null;
	}
	
	protected DatePickerDialog.OnDateSetListener mDateSetListener =
        new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, 
                                  int monthOfYear, int dayOfMonth) {
                mYear = year;
                mMonth = monthOfYear;
                mDay = dayOfMonth;
                Calendar calendar = new GregorianCalendar(mYear, mMonth, mDay);
                mDateDisplay.setText(String.format("%tF", calendar));
                
        		mCurrent.setDate(calendar.getTime());
        		mDarkroomDBAdapter.updateFilmFrameValue(mCurrent);
            }
        };
	
	private void setupButtons() {
		
		
		final ImageButton lPictureButton = (ImageButton) findViewById(R.id.take_picture_btn);
		lPictureButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent lIntent = new Intent();
    			lIntent.setAction( "com.darkroom.takepicture" );
    			lIntent.putExtra("film_id", mCurrent.getFilmId());
    			lIntent.putExtra("filmframe_id", mCurrent.getId());
    			//TODO Exchange image size and focal length
    			
    			try {
    				startActivity(lIntent);
    				finish();
    			} catch(ActivityNotFoundException inException) {
    				ScreenUtil.displayMessage(FilmFrameInfoEditor.this, "Unable to start the ViewFinder application.", Toast.LENGTH_SHORT);
    			}
			}
		});

		// Connect buttons
		final Button lCancelButton = (Button) findViewById(R.id.cancel_btn);
		lCancelButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				mDarkroomDBAdapter.updateFilmFrameValue(mOld);
				setResult(RESULT_OK);
				finish();
			}
		});

		final Button lUndoButton = (Button) findViewById(R.id.undo_btn);
		lUndoButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				mDarkroomDBAdapter.updateFilmFrameValue(mOld);
				loadData(mFilmFrameId);
			}
		});

		final Button lDoneButton = (Button) findViewById(R.id.done_btn);
		lDoneButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// All the field are saved each time they are modified. So, we
				// just quit.
				v.setFocusableInTouchMode(true);
				v.requestFocus();
				setResult(RESULT_OK);
				finish();
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Logger.v("Activity return from request " + requestCode);
		switch(requestCode) {
		case REQUEST_CREATE_FILMFRAME:
			if(resultCode == RESULT_OK) {
				mFilmFrameId = data.getLongExtra("filmframe_id", -1);
				getParent().getIntent().putExtra("filmframe_id", mFilmFrameId);
				loadData(mFilmFrameId);
			} else {
				//TODO If image provided delete it
				File lPhotoFile = new File(mImagePath);
				if (lPhotoFile.exists()) {
					lPhotoFile.delete();
				}
				
				setResult(RESULT_CANCELED);
				finish();
			}
			break;
		case REQUEST_EDIT_FILMFRAMEID:
			if(resultCode == RESULT_OK) {
				loadData(mFilmFrameId);
			}
		}
    }
    
}
