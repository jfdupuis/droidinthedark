package com.darkroom.manager;

import android.util.Log;

public class Logger {
    private static final String TAG = "DarkroomNoteKeeper";
    private static final boolean DEBUG = false;

    public static void v(String message) {
    	if(DEBUG)
    		Log.v(TAG, message);
    }

    public static void d(String message) {
    	if(DEBUG)
    		Log.d(TAG, message);
    }

    public static void i(String message) {
   		Log.i(TAG, message);
    }

    public static void w(String message) {
   		Log.w(TAG, message);
    }

    public static void e(String message, Throwable inException) {
        Log.e(TAG, message, inException);
    }
}
