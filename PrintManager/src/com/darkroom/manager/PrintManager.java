package com.darkroom.manager;

import com.darkroom.library.Print;
import com.jfdupuis.library.screen.ScreenUtil;
import com.darkroom.library.dao.DarkroomDBHelper;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.Toast;

public class PrintManager extends Manager {
	static final private int SORT_BY_FILMID = 0;
	static final private int SORT_BY_DATE = 1;
	static final private int SORT_BY_SIZE = 2;
	static final private int SORT_BY_TITLE = 3;
	
	private PrintCursorAdapter mPrintAdapter = null;
	private boolean mShowFilmInfo = true;
	
	private long mFilmId = -1; //Used only if embedded
	private long mFilmFrameId = -1; //Used only if embedded
	private long mCollectionId = -1; //Used only if embedded
	
	
	private View mHeader = null;
	
	/** Called when the activity is first created. */
	public void onCreate(Bundle inBundle) {
		super.onCreate(inBundle);
		setTitle(R.string.print_manager);
		
		// Set result CANCELED in case the user backs out
        setResult(Activity.RESULT_CANCELED);
        
        //Retrieve the film id
        if(mEmbedded) {
        	mFilmId = getParent().getIntent().getLongExtra("film_id", -1);
        	mCollectionId = getParent().getIntent().getLongExtra("collection_id", -1);
        	mFilmFrameId = getParent().getIntent().getLongExtra("filmframe_id", -1);
        	if(mFilmId <= 0 && mCollectionId <= 0 && mFilmFrameId <= 0)
        		mEmbedded = false;
        	
        	//Logger.d("Creating embedded PrintManager with film id " + mFilmId);
        	
        	
        	if(mFilmFrameId > 0 && mFilmId < 0) {
        		//Retrive film id
        		mFilmId = mDarkroomDBAdapter.getFilmFrameFilmId(mFilmFrameId);
        	}
        }
        
        mHeader = (View)getLayoutInflater().inflate(R.layout.print_item_header, null);
//        if(mEmbedded) {
//        	if(mFilmId > 0) {
//        		TextView lFilmHeader = (TextView)mHeader.findViewById(R.id.rowFilmUserId);
//        		lFilmHeader.setVisibility(View.GONE);
//        	}
//        }
        this.getListView().addHeaderView(mHeader,null,false);
        
        this.getListView().setHorizontalScrollBarEnabled(true);
        
        populateList();

        registerForContextMenu(getListView());
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
	  // ignore orientation/keyboard change
	  super.onConfigurationChanged(newConfig);
	}

	protected void populateList() {
		getNewCursor(SORT_BY_FILMID);
        
        startManagingCursor(mCursor);
        
        // and an array of the fields we want to bind those fields
        final int[] lTo = new int[]{
        		//R.id.rowPrintUserId, 
        		//R.id.rowPrintSize, //Handled in custom adapter
        		R.id.rowFilmUserId, 
        		R.id.rowFilmFrame, 
        		R.id.rowIDNumber,
        		//R.id.rowDate //Handled in custom adapter
        		R.id.rowTitle
        		};
        
        final String[] lFromCursor = new String[]{
				//DarkroomDBHelper.KEY_PRINT_USERID, 
				//DarkroomDBHelper.KEY_PRINT_SIZE, //Handled in custom adapter
				DarkroomDBHelper.KEY_FILM_USERID, 
				DarkroomDBHelper.KEY_PRINT_FRAMEID,
				DarkroomDBHelper.KEY_PRINT_IDNUM,
				//DarkroomDBHelper.KEY_PRINT_DATE //Handled in custom adapter
				DarkroomDBHelper.KEY_PRINT_TITLE
			};
        
       
        
        // Now create a simple cursor adapter and set it to display
        int [] lHidden;
        if(mFilmId > 0) {
        	if(mFilmFrameId > 0)
        		lHidden = new int [] {0,1}; //R.id.rowFilmUserId, R.id.rowFilmFrame
        	else
        		lHidden = new int [] {0}; //R.id.rowFilmUserId
        } else {
        	lHidden = new int [] {};
        }
        
        mPrintAdapter = new PrintCursorAdapter(this, R.layout.print_item, mCursor, lFromCursor, lTo, lHidden, (TableLayout) mHeader);
        setListAdapter(mPrintAdapter);
	}
	
	@Override
	protected void onListItemClick(ListView inListView, View inView, int inPosition, long inId) {
		
		if(inId > 0) {
			if(mSelectOnly) {
				Intent lIntent = new Intent();
				lIntent.putExtra(SELECTION, inId);
				setResult(Activity.RESULT_OK,lIntent);
				finish();
			} else {
				Intent lIntent = new Intent(this, PrintEditor.class);
				lIntent.putExtra("print_id", inId);
				Logger.v("Starting print editor from print id " + inId);
				startActivity(lIntent);
			}
		}
	}
	
	private void getNewCursor(int inOrder) {
		final String[] lFrom = new String[]{
				//DarkroomDBHelper.PRINT_TABLE_NAME + "." + DarkroomDBHelper.KEY_PRINT_USERID, 
				//DarkroomDBHelper.KEY_PRINT_SIZE_H + "||'x'|| " + DarkroomDBHelper.KEY_PRINT_SIZE_V + " AS " + DarkroomDBHelper.KEY_PRINT_SIZE,
				DarkroomDBHelper.KEY_PRINT_SIZE_H, DarkroomDBHelper.KEY_PRINT_SIZE_V,
				DarkroomDBHelper.FILM_TABLE_NAME + "." + DarkroomDBHelper.KEY_FILM_USERID, 
				DarkroomDBHelper.PRINT_TABLE_NAME + "." + DarkroomDBHelper.KEY_PRINT_FRAMEID, 
				DarkroomDBHelper.PRINT_TABLE_NAME + "." + DarkroomDBHelper.KEY_PRINT_IDNUM,
				DarkroomDBHelper.PRINT_TABLE_NAME + "." + DarkroomDBHelper.KEY_PRINT_DATE,
				DarkroomDBHelper.PRINT_TABLE_NAME + "." + DarkroomDBHelper.KEY_PRINT_TITLE
			};
		
		//Define the default ordering of the print
		String lOrder = null;
		switch(inOrder) {
		case SORT_BY_FILMID:
			lOrder = 
				DarkroomDBHelper.FILM_TABLE_NAME + "." + DarkroomDBHelper.KEY_FILM_USERID 
				+ ", " + DarkroomDBHelper.PRINT_TABLE_NAME + "." + DarkroomDBHelper.KEY_PRINT_FRAMEID
				+ ", " + DarkroomDBHelper.PRINT_TABLE_NAME + "." + DarkroomDBHelper.KEY_PRINT_IDNUM
				+ ", " + DarkroomDBHelper.KEY_PRINT_SIZE_H + ", " + DarkroomDBHelper.KEY_PRINT_SIZE_H
				+ ", " + DarkroomDBHelper.PRINT_TABLE_NAME + "." + DarkroomDBHelper.KEY_PRINT_DATE;
			break;
		case SORT_BY_DATE:
			lOrder = 
				DarkroomDBHelper.PRINT_TABLE_NAME + "." + DarkroomDBHelper.KEY_PRINT_DATE
				+ ", " +DarkroomDBHelper.FILM_TABLE_NAME + "." + DarkroomDBHelper.KEY_FILM_USERID 
				+ ", " + DarkroomDBHelper.PRINT_TABLE_NAME + "." + DarkroomDBHelper.KEY_PRINT_FRAMEID
				+ ", " + DarkroomDBHelper.PRINT_TABLE_NAME + "." + DarkroomDBHelper.KEY_PRINT_IDNUM
				+ ", " + DarkroomDBHelper.KEY_PRINT_SIZE_H + ", " + DarkroomDBHelper.KEY_PRINT_SIZE_H
				;
			break;
		case SORT_BY_SIZE:
			lOrder = 
				DarkroomDBHelper.KEY_PRINT_SIZE_H + ", " + DarkroomDBHelper.KEY_PRINT_SIZE_H
				+ ", " + DarkroomDBHelper.FILM_TABLE_NAME + "." + DarkroomDBHelper.KEY_FILM_USERID 
				+ ", " + DarkroomDBHelper.PRINT_TABLE_NAME + "." + DarkroomDBHelper.KEY_PRINT_FRAMEID
				+ ", " + DarkroomDBHelper.PRINT_TABLE_NAME + "." + DarkroomDBHelper.KEY_PRINT_IDNUM
				+ ", " + DarkroomDBHelper.PRINT_TABLE_NAME + "." + DarkroomDBHelper.KEY_PRINT_DATE;
			break;
		case SORT_BY_TITLE:
			lOrder = 
				DarkroomDBHelper.PRINT_TABLE_NAME + "." + DarkroomDBHelper.KEY_PRINT_TITLE
				+ ", " + DarkroomDBHelper.FILM_TABLE_NAME + "." + DarkroomDBHelper.KEY_FILM_USERID 
				+ ", " + DarkroomDBHelper.PRINT_TABLE_NAME + "." + DarkroomDBHelper.KEY_PRINT_FRAMEID
				+ ", " + DarkroomDBHelper.PRINT_TABLE_NAME + "." + DarkroomDBHelper.KEY_PRINT_IDNUM
				+ ", " + DarkroomDBHelper.KEY_PRINT_SIZE_H + ", " + DarkroomDBHelper.KEY_PRINT_SIZE_H
				+ ", " + DarkroomDBHelper.PRINT_TABLE_NAME + "." + DarkroomDBHelper.KEY_PRINT_DATE;
			break;
		}
		
		if(mFilmId > 0) {
			if(mFilmFrameId > 0) {
				//Get the prints associated to a film frame
				//Table is on frame number, not id
				mCursor = mDarkroomDBAdapter.getAllPrintCursor(lFrom, 
						DarkroomDBHelper.FILM_TABLE_NAME + "." + DarkroomDBHelper.KEY_ID + "=" + mFilmId
						+ " and " + DarkroomDBHelper.PRINT_TABLE_NAME + "." + DarkroomDBHelper.KEY_PRINT_FRAMEID + "=" + mDarkroomDBAdapter.getFilmFrame(mFilmFrameId).getFrameNumber()
						,lOrder, new String[]{DarkroomDBHelper.FILM_TABLE_NAME});
			} else {
				//Get the prints associated to a film 
				mCursor = mDarkroomDBAdapter.getAllPrintCursor(lFrom, DarkroomDBHelper.FILM_TABLE_NAME + "." + DarkroomDBHelper.KEY_ID + "=" + mFilmId, 
						lOrder, new String[]{DarkroomDBHelper.FILM_TABLE_NAME});
			}
		}
		else if(mCollectionId > 0) {
			//Get the prints associated to a collection
			mCursor = mDarkroomDBAdapter.getAllPrintCursor(lFrom, 
					DarkroomDBHelper.COLLECTIONPRINT_TABLE_NAME + "." + DarkroomDBHelper.KEY_COLLECTIONPRINT_COLLECTIONID + "=" + mCollectionId, 
        			lOrder, new String[]{DarkroomDBHelper.FILM_TABLE_NAME, DarkroomDBHelper.COLLECTIONPRINT_TABLE_NAME});
		}
        else {
        	mCursor = mDarkroomDBAdapter.getAllPrintCursor(lFrom, null, lOrder, new String[]{DarkroomDBHelper.FILM_TABLE_NAME});
        }
		
		if(mPrintAdapter != null)
			mPrintAdapter.changeCursor(mCursor);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.manager_print, menu);
	    return true;
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);
		
		MenuItem lItem = menu.findItem(R.id.filminfo);
		if(mShowFilmInfo) {
			lItem.setTitle(R.string.hidefilminfo);
		} else {
			lItem.setTitle(R.string.showfilminfo);
		}
		
		return true;
	}

	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int lIndex = getSelectedItemPosition();
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
		
	    switch (item.getItemId()) {
	    case R.id.add:
	    	addNewItem();
	        return true;
	    case R.id.edit:
	    	editItem(lIndex, info.id);
	        return true;
	    case R.id.remove:
	    	removeItem(lIndex, info.id);
	        return true;
	    case R.id.filminfo:
	    	mShowFilmInfo = !mShowFilmInfo;
	    	toggleFilmInfoDisplay(mShowFilmInfo);
	        return true;
	    default:
	        return super.onOptionsItemSelected(item);
	    }
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
	  //super.onCreateContextMenu(menu, v, menuInfo);
	  Logger.d("Creating print manager context menu");
	  MenuInflater inflater = getMenuInflater();
	  inflater.inflate(R.menu.manager_context_print, menu);
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
		switch (item.getItemId()) {
//		case R.id.context_select:
//			selectItem(info.position);
//			return true;
		case R.id.context_print:
			printItem(info.position, info.id);
			return true;
		case R.id.context_edit:
			editItem(info.position, info.id);
			return true;
		case R.id.context_delete:
			removeItem(info.position, info.id);
			return true;
		case R.id.context_sort_filmid:
			getNewCursor(SORT_BY_FILMID);
			return true;
		case R.id.context_sort_date:
			getNewCursor(SORT_BY_DATE);
			return true;
		case R.id.context_sort_size:
			getNewCursor(SORT_BY_SIZE);
			return true;
		case R.id.context_sort_title:
			getNewCursor(SORT_BY_TITLE);
			return true;
		default:
			return super.onContextItemSelected(item);
		}
	}

	protected void addNewItem() {
		if(!isDemoLimitReach()) {
			Intent lIntent = new Intent(this, PrintEditor.class);
			//Add the film id. The default -1 will indicate that now film selected.
			lIntent.putExtra("film_id", mFilmId);
			lIntent.putExtra("filmframe_id", mFilmFrameId);
			Logger.v("Starting print editor");
			startActivityForResult(lIntent,ADD_ITEM);
		}
	}

	protected void toggleFilmInfoDisplay(boolean inShow) {
	        
        // Now create a simple cursor adapter and set it to display
        int [] lHidden;
        if(!inShow)
        	lHidden = new int []{0,1,2};
        else {
        	if(mEmbedded && mFilmId > 0) {
        		lHidden = new int [] {0};
        	} else {
        		lHidden = new int [] {};
        	}
        }
        
        mPrintAdapter.updateHidden(lHidden);
	}


	@Override
	protected void editItem(int inPosition, long inId) {		
		Logger.d("Editing print id:" + inId);
		
		//Start print  editor
		Intent lIntent = new Intent(this, PrintEditor.class);
		lIntent.putExtra("print_id", inId);
		startActivityForResult(lIntent,EDIT_ITEM);
	}
	
	protected void removeItem(int inPosition, long inId) {
		Logger.v(String.format("Remove item at index: %d", inPosition));
		boolean lReturn = mDarkroomDBAdapter.removePrint(inId);
		Logger.v(String.format("Delete results: %b", lReturn));
		mCursor.requery();
	}

	protected void printItem(int inPosition, long inId) {
		Logger.v(String.format("Print item at index: %d", inPosition));
		Print lPrint = mDarkroomDBAdapter.getPrint(inId, false);
		Intent lIntent = new Intent();
		lIntent.setAction( "com.darkroom.print" );
		lIntent.putExtra("print", lPrint);
		lIntent.putExtra("print_id", inId);
		try {
			startActivity(lIntent);
		} catch(ActivityNotFoundException inException) {
			ScreenUtil.displayMessage(PrintManager.this, "Unable to start the F-Stop Timer application", Toast.LENGTH_SHORT);
		}
	}

}