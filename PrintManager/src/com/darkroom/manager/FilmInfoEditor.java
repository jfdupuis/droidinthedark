package com.darkroom.manager;

import com.darkroom.library.Emulsion;
import com.darkroom.library.Film;
import com.darkroom.library.dao.AddNewEmulsionDialog;
import com.darkroom.library.dao.DarkroomDBHelper;
import com.darkroom.manager.dao.ManagerDBAdapter;
import com.jfdupuis.library.widget.DBEditText;
import com.jfdupuis.library.widget.DBEditText.OnChangedListener;
import com.jfdupuis.library.widget.DBSpinner;
import com.jfdupuis.library.widget.DBSpinnerDialog;

import android.app.Activity;
import android.content.Intent;
import android.database.SQLException;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Calendar;

public class FilmInfoEditor extends Activity {
		
	static final int REQUEST_CREATE_FILM = 1;
	static final int REQUEST_EDIT_FILMID = 2;
	static final int REQUEST_ADD_EMULSION = 3;
	
	ManagerDBAdapter mDarkroomDBAdapter;

	Spinner mIsoSpinner;
	DBSpinner mEmulsionSpinner;
	
	ArrayAdapter<CharSequence> mISOAdapter;
	
	Film mCurrentFilm = null;
	Film.Info mOldInfo = null;
	long mFilmId = -1;
	
	@Override
	public void onBackPressed() {
	}

	
	public void onCreate(Bundle inBundle) {
		super.onCreate(inBundle);
		setContentView(R.layout.film_info_editor);
		
		mDarkroomDBAdapter = new ManagerDBAdapter(this);
		mDarkroomDBAdapter.open();
		
		setupSpinners();
		setupButtons();		

		mFilmId = getParent().getIntent().getLongExtra("film_id", -1);
		if(mFilmId != -1) {
			loadFilmData(mFilmId);
		}
		else {
			//Create new film
			Intent lIntent = new Intent(FilmInfoEditor.this, AddNewFilmDialog.class);
        	startActivityForResult(lIntent,REQUEST_CREATE_FILM);
		}
        
	}
	
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		mDarkroomDBAdapter.close();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		Logger.d("Creating option menu");
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.info_editor, menu);
	    return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    case R.id.edit_id:
	    	//Create new film
			Intent lIntent = new Intent(FilmInfoEditor.this, AddNewFilmDialog.class);
			lIntent.putExtra("film_id", mFilmId);
        	startActivityForResult(lIntent,REQUEST_EDIT_FILMID);
	        return true;
	    default:
	        return super.onOptionsItemSelected(item);
	    }
	}
	
	
	
	void loadFilmData(long inFilmId) {
		int lPosition = -1;
		
		//Get film from DB
		mCurrentFilm = mDarkroomDBAdapter.getFilm(inFilmId);
		mOldInfo = mCurrentFilm.new Info(mCurrentFilm.mInfo);
		
		Logger.v("Editing film info : " + mCurrentFilm.getUserId());

		//Set id
        TextView lIdDisplay = (TextView) findViewById(R.id.userid);
        lIdDisplay.setText(mCurrentFilm.getUserId());
        
        //Set date
        TextView lDateView = (TextView) findViewById(R.id.date);
        Calendar lCalendar = Calendar.getInstance();
        lCalendar.setTime(mCurrentFilm.getDate());
        lDateView.setText(String.format("%tF", lCalendar));
        lDateView.setVisibility(View.VISIBLE);
		
        //Set day number 
        TextView lDayNumberView = (TextView) findViewById(R.id.datenumber);
        lDayNumberView.setText(Integer.toString(mCurrentFilm.getIdNumber()));
                
        //Set the camera
        TextView lCameraView = (TextView) findViewById(R.id.camera);
        lCameraView.setText(mCurrentFilm.getCamera().getBrand() + " " + mCurrentFilm.getCamera().getModel());
		
        		
		//Set iso spinner to film
		lPosition = mISOAdapter.getPosition(Integer.toString(mCurrentFilm.mInfo.mShotISO));
		mIsoSpinner.setSelection(lPosition);
		
		//Set film name edit box to film
        DBEditText lName = (DBEditText) findViewById(R.id.name);
        lName.setText(mCurrentFilm.mInfo.mName);
        lName.setOnChangedListener(new OnChangedListener() {
			public void changed(EditText inView) {
				changedName(inView.getText().toString());
			}
		});
        
        //Set film description edit box to film
        DBEditText lDescription = (DBEditText) findViewById(R.id.description);
        lDescription.setText(mCurrentFilm.mInfo.mDescription);
        lDescription.setOnChangedListener(new OnChangedListener() {
			public void changed(EditText inView) {
				changedDescription(inView.getText().toString());
			}
		});

		
		//Set the emulsion
        mEmulsionSpinner.setPositionById(mCurrentFilm.mInfo.getEmulsion().getId());

	}
	
private void setupButtons() {
		
	//Connect buttons
    final Button lCancelButton = (Button) findViewById(R.id.cancel_btn);
    lCancelButton.setOnClickListener(new View.OnClickListener() {
        public void onClick(View v) {
        	//mCurrentFilm.mDevelopment = mCurrentFilm.new Development(mOldDev); //Done in loadFilmData
        	mDarkroomDBAdapter.updateFilmValue(mFilmId, mOldInfo);
        	//loadFilmData(mFilmId);
        	setResult(RESULT_OK);
        	finish();
        }
    });
    
    final Button lUndoButton = (Button) findViewById(R.id.undo_btn);
    lUndoButton.setOnClickListener(new View.OnClickListener() {
        public void onClick(View v) {
        	mDarkroomDBAdapter.updateFilmValue(mFilmId, mOldInfo);
        	loadFilmData(mFilmId);
        }
    });
    
    final Button lDoneButton = (Button) findViewById(R.id.done_btn);
    lDoneButton.setOnClickListener(new View.OnClickListener() {
        public void onClick(View v) {
        	//All the field are saved each time they are modified. So, we just quit.
        	v.setFocusableInTouchMode(true);
        	v.requestFocus();
        	setResult(RESULT_OK);
        	finish();
        }
    });

}
	
	
	private void setupSpinners() {
        
        //Emulsion spinner
        mEmulsionSpinner = (DBSpinner) findViewById(R.id.emulsion_spinner);
        mEmulsionSpinner.setAdapter(
        		mDarkroomDBAdapter.getAllEmulsionCursor(new String[]{DarkroomDBHelper.KEY_EMULSION_NAME}), 
				DarkroomDBHelper.KEY_EMULSION_NAME);
        mEmulsionSpinner.setOnActionRequestHandler(new DBSpinner.OnActionRequest(){
			public void onActionRequest(Spinner mInSpinner, int inAction, int inPosition, long inId) {
				Intent lIntent = new Intent(FilmInfoEditor.this, AddNewEmulsionDialog.class);
				switch(inAction) {
				case DBSpinnerDialog.ADD_REQUEST:
	            	break;
				case DBSpinnerDialog.EDIT_REQUEST:
					lIntent.putExtra("id", inId);
					break;
				case DBSpinnerDialog.ITEM_SELECTION:
					changedEmulsion(inId);
					return;
				}
				startActivityForResult(lIntent,REQUEST_ADD_EMULSION);
			}
		});
        
        //ISO spinner
        mIsoSpinner = (Spinner) findViewById(R.id.shotiso_spinner);
        mISOAdapter = ArrayAdapter.createFromResource(
        		FilmInfoEditor.this, R.array.iso, android.R.layout.simple_spinner_item);
        mISOAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mIsoSpinner.setAdapter(mISOAdapter);
        mIsoSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
				changedISO(Integer.valueOf((String) mIsoSpinner.getSelectedItem()));
			}

			public void onNothingSelected(AdapterView<?> mArg0) {
				// Do nothing
			}
		});
        
        matchShotISOToEmulsion();
	}
	
	void matchShotISOToEmulsion() {
		final long lEmulsionId = mEmulsionSpinner.getSelectedItemId();
		final String lIso = (String) mIsoSpinner.getSelectedItem();
		if(lIso.equals("0")) {
			int lPos = mISOAdapter.getPosition("0");
			try {
				Emulsion lSelectedEmulsion = mDarkroomDBAdapter.getEmulsion(lEmulsionId);
				lPos = mISOAdapter.getPosition(Integer.toString(lSelectedEmulsion.mISO));
			} catch (SQLException inException) {
				Logger.v("Film editor: No emulsion found in database");
			}
			mIsoSpinner.setSelection(lPos);
		}
	}
	
	void changedName(String inNewValue) {
		Logger.d("Update film name to " + inNewValue);
		mCurrentFilm.mInfo.mName = inNewValue;
		mDarkroomDBAdapter.updateFilmValue(mCurrentFilm.getId(), DarkroomDBHelper.KEY_FILM_NAME, inNewValue);
		
	}
	
	void changedDescription(String inNewValue){
		Logger.d("Update film description to " +inNewValue);
		mCurrentFilm.mInfo.mDescription = inNewValue;
		mDarkroomDBAdapter.updateFilmValue(mCurrentFilm.getId(), DarkroomDBHelper.KEY_FILM_DESC, inNewValue);
	    
	}
	
	
	void changedEmulsion(long inNewId) {
		if (mCurrentFilm != null) {
			if(mCurrentFilm.mInfo.getEmulsion().getId() != inNewId) {
				Logger.d("Update film emulsion to " + inNewId);
				mCurrentFilm.mInfo.setEmulsion(mDarkroomDBAdapter.getEmulsion(inNewId));
				mDarkroomDBAdapter.updateFilmValue(mCurrentFilm.getId(), DarkroomDBHelper.KEY_FILM_EMULSIONID, inNewId);
			}
		}
		matchShotISOToEmulsion();
	}

	void changedISO(int inNewValue) {
		if (mCurrentFilm != null) {
			if(mCurrentFilm.mInfo.mShotISO != inNewValue) {
				Logger.d("Update film iso to " + inNewValue);
				mCurrentFilm.mInfo.mShotISO = inNewValue;
				mDarkroomDBAdapter.updateFilmValue(mCurrentFilm.getId(), DarkroomDBHelper.KEY_FILM_SHOTISO, inNewValue);
			}
		}
	}
	

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Logger.v("Activity return from request " + requestCode);
		switch(requestCode) {
		case REQUEST_CREATE_FILM:
			if(resultCode == RESULT_OK) {
				mFilmId = data.getLongExtra("film_id", -1);
				getParent().getIntent().putExtra("film_id", mFilmId);
				loadFilmData(mFilmId);
			} else {
				setResult(RESULT_CANCELED);
				finish();
			}
			break;
		case REQUEST_EDIT_FILMID:
			if(resultCode == RESULT_OK) {
				loadFilmData(mFilmId);
			}
		case REQUEST_ADD_EMULSION:
			mEmulsionSpinner.onAddReturn(resultCode, data);
			matchShotISOToEmulsion();
			break;
		}
    }
    
}
