package com.darkroom.manager;

import com.darkroom.library.dao.DarkroomDBAdapter;
import com.darkroom.manager.preferences.DarkroomManagerPrefs;
import com.jfdupuis.library.math.Unit;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleCursorAdapter;
import android.widget.TableLayout;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;

public class PrintCursorAdapter extends SimpleCursorAdapter  {

    private int [] mHidden;
    protected Context mContext; //Since it's hidden in CursorAdapter
    
    private TableLayout mHeader;

    public PrintCursorAdapter (Context mContext, int mLayout, Cursor c, String[] from, int[] to, int [] inHidden, TableLayout inHeader) {
        super(mContext, mLayout, c, from, to);
        this.mContext = mContext;
        this.mHidden = inHidden;
        this.mHeader = inHeader;
        updateHidden(inHidden);
    }
    
    public void updateHidden(int [] inHidden) {
    	mHidden = inHidden;
    	this.notifyDataSetInvalidated();
    	
    	//Update list header
    	final int lNbColumns = ((ViewGroup)mHeader.getChildAt(0)).getChildCount(); //Get the number of column in the table row
    	for(int i = 0; i < lNbColumns; ++i) {
    		//Restore column visibility
    		mHeader.setColumnCollapsed(i, false);
    	}
    	for(int id : mHidden) {
    		mHeader.setColumnCollapsed(id, true);
        }
    	
    }
    
//    @Override
//    public View newView(Context inContext, Cursor inCursor, ViewGroup inParent) {
//    	View lPrintView = super.newView(inContext, inCursor, inParent);
//    	
//        return lPrintView;
//    }

    @Override
    public void bindView(View inView, Context inContext, Cursor inCursor) {
    	super.bindView(inView, inContext, inCursor);
    	
    	TableLayout lTable = (TableLayout) inView;
    	final int lNbColumns = ((ViewGroup)lTable.getChildAt(0)).getChildCount(); //Get the number of column in the table row
    	for(int i = 0; i < lNbColumns; ++i) {
    		//Restore column visibility
    		lTable.setColumnCollapsed(i, false);
    	}
    	for(int id : mHidden) {
    		lTable.setColumnCollapsed(id, true);
        }
		
    	
    	
    	//Format date display
    	TextView dateView = (TextView) inView.findViewById(R.id.rowDate);
        Date lDate = new Date(inCursor.getLong(inCursor.getColumnIndex(DarkroomDBAdapter.KEY_PRINT_DATE)));
        
        Calendar lCalendar = Calendar.getInstance();
        lCalendar.setTime(lDate);
        String dateString = String.format("%tF", lCalendar);
		dateView.setText(dateString);
		
		//Format size display
        TextView lSizeView = (TextView) inView.findViewById(R.id.rowPrintSize);
        float lSizeH = inCursor.getFloat(inCursor.getColumnIndex(DarkroomDBAdapter.KEY_PRINT_SIZE_H));
        float lSizeV = inCursor.getFloat(inCursor.getColumnIndex(DarkroomDBAdapter.KEY_PRINT_SIZE_V));
        //Apply conversion from mm
        final String lUnits = DarkroomManagerPrefs.getDefaultUnits(mContext);
        lSizeView.setText(String.format("%.1f X %.1f", Unit.convertFromMM(lSizeH, lUnits),Unit.convertFromMM(lSizeV, lUnits)));
    }
}
