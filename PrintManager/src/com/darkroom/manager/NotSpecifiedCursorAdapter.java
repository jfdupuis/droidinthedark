package com.darkroom.manager;

import com.darkroom.library.dao.DarkroomDBHelper;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

public class NotSpecifiedCursorAdapter extends SimpleCursorAdapter {

    public NotSpecifiedCursorAdapter (Context mContext, int mLayout, Cursor c, String[] from, int[] to) {
        super(mContext, mLayout, c, from, to);
    }
    
    @Override
    public View newView(Context inContext, Cursor inCursor, ViewGroup inParent) {
    	View lPrintView = super.newView(inContext, inCursor, inParent);
    	
        return lPrintView;
    }

    @Override
    public void bindView(View inView, Context inContext, Cursor inCursor) {
    	super.bindView(inView, inContext, inCursor);
    	  		    	
        //Hide the not in collection
    	TextView lNameView =  (TextView) inView.findViewById(R.id.name);
    	if(lNameView == null)
    		return;
    	if(lNameView.getText().toString().equals(DarkroomDBHelper.NOT_SPECIFIED)) {
    		inView.setVisibility(View.GONE); //Set the layout to gone
    		for(int i = 0; i < ((ViewGroup) inView).getChildCount(); ++i) {
    			//Go through all row layout child and set visibility to gone
    			((ViewGroup) inView).getChildAt(i).setVisibility(View.GONE);
    		}
    	}
    	else {
    		inView.setVisibility(View.VISIBLE);
    		for(int i = 0; i < ((ViewGroup) inView).getChildCount(); ++i) {
    			//Go through all row layout child and set visibility to gone
    			((ViewGroup) inView).getChildAt(i).setVisibility(View.VISIBLE);
    		}
    	}

    }

}
