package com.darkroom.manager.preferences;



import com.darkroom.library.Chemical;
import com.darkroom.library.dao.DarkroomDBHelper;
import com.darkroom.manager.Logger;
import com.darkroom.manager.R;
import com.darkroom.manager.dao.ManagerDBAdapter;
import com.jfdupuis.library.math.Unit;
import com.jfdupuis.library.preferences.CursorListPreference;
import com.jfdupuis.library.widget.FileDialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.preference.Preference.OnPreferenceClickListener;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DarkroomManagerPrefs extends PreferenceActivity{
	static final private int REQUEST_SELECT_SAVE_DIR = 10;
	static final private int REQUEST_SELECT_RESTORE_FILE = 11;
	
	public static final String SDCARD_DIR = "DarkroomNoteKeeper";
	public static final String DATABASE_BKP_EXT = "bkpDB";
	
	private static final String UNITS = "units_length";
    private static final String UNITS_DEFAULT = "mm";
    
    private static final String UNITS_TEMP = "units_temp";
    private static final String UNITS_TEMP_DEFAULT = "C";
    
    private static final String FSTOP = "fstop";
    private static final boolean FSTOP_DEFAULT = true;
    
    String mDatabasePath;
    
    String mRestoreFile = null;
    
    //Default emulsion, paper, film dev, paper dev, agitation, toner, camera
    
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);
        
        
        //Populate with database
        PreferenceScreen preferenceScreen = getPreferenceScreen();
        ((CursorListPreference) preferenceScreen.findPreference("camera")).setData(DarkroomDBHelper.CAMERA_TABLE_NAME, DarkroomDBHelper.KEY_CAMERA_SHORTID);
        ((CursorListPreference) preferenceScreen.findPreference("emulsion")).setData(DarkroomDBHelper.EMULSION_TABLE_NAME, DarkroomDBHelper.KEY_EMULSION_NAME);
        ((CursorListPreference) preferenceScreen.findPreference("agitation")).setData(DarkroomDBHelper.AGITATION_TABLE_NAME, DarkroomDBHelper.KEY_STRING);
        ((CursorListPreference) preferenceScreen.findPreference("film_dev_dilution")).setData(DarkroomDBHelper.DILUTION_TABLE_NAME, DarkroomDBHelper.KEY_STRING);
        ((CursorListPreference) preferenceScreen.findPreference("paper")).setData(DarkroomDBHelper.PAPER_TABLE_NAME, DarkroomDBHelper.KEY_PAPER_NAME);
        
        ManagerDBAdapter lDarkroomDBAdapter = new ManagerDBAdapter(this);
        lDarkroomDBAdapter.open();
        
        mDatabasePath = lDarkroomDBAdapter.getDataBase().getPath();
        
        Cursor lDevCursor = lDarkroomDBAdapter.getAllChemicalCursor(Chemical.DEVELOPER);
        Cursor lTonerCursor = lDarkroomDBAdapter.getAllChemicalCursor(Chemical.TONER);
        
        ((CursorListPreference) preferenceScreen.findPreference("paper_dev")).setData(lDevCursor, DarkroomDBHelper.KEY_CHEMICAL_NAME);
        ((CursorListPreference) preferenceScreen.findPreference("film_dev")).setData(lDevCursor, DarkroomDBHelper.KEY_CHEMICAL_NAME);
        ((CursorListPreference) preferenceScreen.findPreference("toner")).setData(lTonerCursor, DarkroomDBHelper.KEY_CHEMICAL_NAME);
        
        lDarkroomDBAdapter.close();
        
//        Preference myPref = (Preference) findPreference("picture_dir");
//        myPref.setOnPreferenceClickListener(new OnPreferenceClickListener() {
//            public boolean onPreferenceClick(Preference preference) {
//            	selectDirectory();
//            	return true;
//            }
//        });
        
        Preference lExportDbPref = (Preference) findPreference("exportdb");
        lExportDbPref.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
            	new ExportDatabaseFileTask().execute();
            	return true;
            }
        });
        Preference lRestoreDbPref = (Preference) findPreference("restoredb");
        lRestoreDbPref.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
            	selectRestoreFile();
            	return true;
            }
        });
    }
	
	public static float getDefaultSizeH(Context context, String inUnits) {
		float lValue = Float.valueOf(SizePreference.getSizeH(context,"size"));
		final String lUnits = SizePreference.getSizeUnits(context,"size");
		if(lUnits != inUnits) {
			return Unit.convertFromMM(Unit.convertToMM(lValue, lUnits), inUnits);
		} else {
			return lValue;
		}
	}
	
	public static float getDefaultSizeV(Context context, String inUnits) {
		float lValue = Float.valueOf(SizePreference.getSizeV(context,"size"));
		final String lUnits = SizePreference.getSizeUnits(context,"size");
		if(lUnits != inUnits) {
			return Unit.convertFromMM(Unit.convertToMM(lValue, lUnits), inUnits);
		} else {
			return lValue;
		}
	}
	
	public static long getDefaultCamera(Context context) {
		return Long.valueOf(PreferenceManager.getDefaultSharedPreferences(context).getString("camera", "1"));
	}
	public static long getDefaultEmulsion(Context context) {
		return Long.valueOf(PreferenceManager.getDefaultSharedPreferences(context).getString("emulsion", "1"));
	}
	public static long getDefaultAgitation(Context context) {
		return Long.valueOf(PreferenceManager.getDefaultSharedPreferences(context).getString("agitation", "1"));
	}
	public static long getDefaultPaper(Context context) {
		return Long.valueOf(PreferenceManager.getDefaultSharedPreferences(context).getString("paper", "1"));
	}
	public static long getDefaultPaperDeveloper(Context context) {
		return Long.valueOf(PreferenceManager.getDefaultSharedPreferences(context).getString("paper_dev", "1"));
	}
	public static long getDefaultFilmDeveloper(Context context) {
		return Long.valueOf(PreferenceManager.getDefaultSharedPreferences(context).getString("film_dev", "1"));
	}
	public static long getDefaultFilmDeveloperDilution(Context context) {
		return Long.valueOf(PreferenceManager.getDefaultSharedPreferences(context).getString("film_dev_dilution", "1"));
	}
	public static long getDefaultToner(Context context) {
		return Long.valueOf(PreferenceManager.getDefaultSharedPreferences(context).getString("toner", "1"));
	}
	
	public static String getDefaultUnits(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(UNITS, UNITS_DEFAULT);
    }
    
    public static char getDefaultUnitsTemp(Context context) {
    	return PreferenceManager.getDefaultSharedPreferences(context).getString(UNITS_TEMP, UNITS_TEMP_DEFAULT).charAt(0);
	}
    
    public static boolean useFStop(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(FSTOP, FSTOP_DEFAULT);
    }
    
    public static boolean displayCustomId(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("custom_id", true);
    }

    public static String getPictureSaveDir(Context context) {
    	return Environment.getExternalStorageDirectory().getPath()+"/"+SDCARD_DIR;
		//return PreferenceManager.getDefaultSharedPreferences(context).getString("picture_dir", Environment.getExternalStorageDirectory().getPath()+"/"+SDCARD_DIR);
    }
    
    public static void setPictureSaveDir(Context context, String inDir) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("picture_dir", inDir).commit();
    }

    protected boolean selectDirectory() {
		Intent intent = new Intent(this, FileDialog.class);
        intent.putExtra(FileDialog.START_PATH, Environment.getExternalStorageDirectory().getPath());
        
        //can user select directories or not
        intent.putExtra(FileDialog.CAN_SELECT_DIR, true);
        
        //alternatively you can set file filter
        //intent.putExtra(FileDialog.FORMAT_FILTER, new String[] { "csv" });
        intent.putExtra(FileDialog.SELECTION_MODE, FileDialog.SelectionMode.MODE_CREATE);
        
        startActivityForResult(intent, REQUEST_SELECT_SAVE_DIR);
		return true;
	}
    
    protected boolean selectRestoreFile() {
    	Intent intent = new Intent(this, FileDialog.class);
        intent.putExtra(FileDialog.START_PATH, Environment.getExternalStorageDirectory().getPath());
        intent.putExtra(FileDialog.CAN_SELECT_DIR, false);
        intent.putExtra(FileDialog.FORMAT_FILTER, new String[] { DATABASE_BKP_EXT });
        intent.putExtra(FileDialog.SELECTION_MODE, FileDialog.SelectionMode.MODE_OPEN);
        startActivityForResult(intent, REQUEST_SELECT_RESTORE_FILE);
		return true;
    }
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch(requestCode) {
		case REQUEST_SELECT_SAVE_DIR:
			if (resultCode == Activity.RESULT_OK) {
				String filePath = data.getStringExtra(FileDialog.RESULT_PATH);
				File lFile = new File(filePath);
				if(!lFile.isDirectory()) {
					filePath = lFile.getParent();
				}
				Logger.d("Setting picture save dir to " + filePath);
				setPictureSaveDir(this, filePath);

			} else if (resultCode == Activity.RESULT_CANCELED) {
				Logger.d("file not selected");
			}
			break;
		case REQUEST_SELECT_RESTORE_FILE:
			if (resultCode == Activity.RESULT_OK) {
				restoreDatabase(data.getStringExtra(FileDialog.RESULT_PATH));
			} else if (resultCode == Activity.RESULT_CANCELED) {
				Logger.d("File not selected");
			}
			break;
		}
    }
	
	void restoreDatabase(String inFilename) {
		mRestoreFile = inFilename;
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(String.format(getString(R.string.restore_warning), inFilename))
		.setCancelable(false)
		.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				new RestoreDatabaseFileTask().execute();
				dialog.dismiss();
			}})
		.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}});
		
		AlertDialog alert = builder.create();
		alert.show();
	}
	
	void copyFile(File src, File dst) throws IOException {
        FileChannel inChannel = new FileInputStream(src).getChannel();
        FileChannel outChannel = new FileOutputStream(dst).getChannel();
        try {
           inChannel.transferTo(0, inChannel.size(), outChannel);
        } finally {
           if (inChannel != null)
              inChannel.close();
           if (outChannel != null)
              outChannel.close();
        }
     }
	
	private class ExportDatabaseFileTask extends AsyncTask<String, Void, Boolean> {
        private final ProgressDialog dialog = new ProgressDialog(DarkroomManagerPrefs.this);
        
        String mmNewDatabasePath;

        // can use UI thread here
        protected void onPreExecute() {
           this.dialog.setMessage("Exporting database...");
           this.dialog.show();
        }

        // automatically done on worker thread (separate from UI thread)
        protected Boolean doInBackground(final String... args) {
           File dbFile = new File(mDatabasePath);
           mmNewDatabasePath = Environment.getExternalStorageDirectory()+"/"+SDCARD_DIR;

           File exportDir = new File(mmNewDatabasePath, "");
           if (!exportDir.exists()) {
              exportDir.mkdirs();
           }
           
           //Get date
           SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
           Calendar cal = Calendar.getInstance();
           String lDateStr = dateFormat.format(cal.getTime());
           String lExportFilename = "DarkroomNoteKeeper-" + lDateStr + "." + DarkroomManagerPrefs.DATABASE_BKP_EXT;
           Logger.i("Exporting database to " + lExportFilename);
           
           File file = new File(exportDir, lExportFilename);

           try {
              file.createNewFile();
              copyFile(dbFile, file);
              return true;
           } catch (IOException e) {
              Logger.e("Problem while exporting the database to sdcard", e);
              return false;
           }
        }

        // can use UI thread here
        protected void onPostExecute(final Boolean success) {
           if (this.dialog.isShowing()) {
              this.dialog.dismiss();
           }
           if (success) {
              Toast.makeText(DarkroomManagerPrefs.this, "Export successful!\n"+mmNewDatabasePath, Toast.LENGTH_LONG).show();
           } else {
              Toast.makeText(DarkroomManagerPrefs.this, "Export failed", Toast.LENGTH_SHORT).show();
           }
        }

     }
	
	private class RestoreDatabaseFileTask extends AsyncTask<String, Void, Boolean> {
        private final ProgressDialog dialog = new ProgressDialog(DarkroomManagerPrefs.this);
        
        // can use UI thread here
        protected void onPreExecute() {
           this.dialog.setMessage("Restoring database...");
           this.dialog.show();
        }

		// automatically done on worker thread (separate from UI thread)
		protected Boolean doInBackground(final String... args) {

			if(mRestoreFile == null) {
				Logger.w("No restoration file selected!");
				return false;
			}
			
			File dbFile = new File(mDatabasePath);
			File exportDir = new File(dbFile.getParent());
			
			File lRestoreFile = new File(mRestoreFile);
			if(!lRestoreFile.exists()) {
				Logger.w("Restauration file doesn't exist!");
				return false;
			}

			if (!exportDir.exists()) {
				exportDir.mkdirs();
			}
			
			File lNewFile = new File(exportDir, dbFile.getName());
			

			try {
				lNewFile.createNewFile();
				Logger.w("Restauring " + lRestoreFile.getAbsolutePath() + " to " + lNewFile.getAbsolutePath());
				copyFile(lRestoreFile, lNewFile);
				return true;
			} catch (IOException e) {
				Logger.e("Problem while exporting the database to sdcard", e);
				return false;
			}
		}

        // can use UI thread here
        protected void onPostExecute(final Boolean success) {
           if (this.dialog.isShowing()) {
              this.dialog.dismiss();
           }
           if (success) {
              Toast.makeText(DarkroomManagerPrefs.this, "Restore successful!", Toast.LENGTH_SHORT).show();
           } else {
              Toast.makeText(DarkroomManagerPrefs.this, "Restore failed", Toast.LENGTH_LONG).show();
           }
        }
	}	
	
}
