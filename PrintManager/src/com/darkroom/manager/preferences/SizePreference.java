package com.darkroom.manager.preferences;

import com.darkroom.manager.R;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.preference.DialogPreference;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

public class SizePreference extends DialogPreference {
    /**
     * The edit text shown in the dialog.
     */
    private EditText mEditTextH, mEditTextV;
    private Spinner mUnitsSpinner;
    private ArrayAdapter<CharSequence> mUnitsAdapter;
    
    private String mText;
    
    public SizePreference(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        
        setDialogLayoutResource(R.layout.sizepreference_dialog);

    }
    

    public SizePreference(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SizePreference(Context context) {
        this(context, null, 0);
    }
    
    public static String getSizeH(Context context, String inKey) {
    	String lText = PreferenceManager.getDefaultSharedPreferences(context).getString(inKey, "8x10 in");
    	return lText.substring(0, lText.indexOf('x'));
    }
    
    public static String getSizeV(Context context, String inKey) {
    	String lText = PreferenceManager.getDefaultSharedPreferences(context).getString(inKey, "8x10 in");
        return lText.substring(lText.indexOf('x')+1, lText.indexOf(' '));
    }
    
    public static String getSizeUnits(Context context, String inKey) {
    	String lText = PreferenceManager.getDefaultSharedPreferences(context).getString(inKey, "8x10 in");
        return lText.substring(lText.indexOf(' ')+1);
    }
    
    /**
     * Saves the text to the {@link SharedPreferences}.
     * 
     * @param text The text to save
     */
    public void setSizeText(String texth, String textv, String units) {
        final boolean wasBlocking = shouldDisableDependents();
        
        mText = texth + 'x' + textv + ' ' + units;
        
        persistString(mText);
        
        final boolean isBlocking = shouldDisableDependents(); 
        if (isBlocking != wasBlocking) {
            notifyDependencyChange(isBlocking);
        }
    }
    
    public void setSizeText(String text) {
        final boolean wasBlocking = shouldDisableDependents();
        
        mText = text;
        
        persistString(mText);
        
        final boolean isBlocking = shouldDisableDependents(); 
        if (isBlocking != wasBlocking) {
            notifyDependencyChange(isBlocking);
        }
    }
    
    /**
     * Gets the text from the {@link SharedPreferences}.
     * 
     * @return The current preference value.
     */
    public String getSizeTextH() {
        return mText.substring(0, mText.indexOf('x'));
    }
    
    public String getSizeTextV() {
    	try {
    		return mText.substring(mText.indexOf('x')+1, mText.indexOf(' '));
    	} catch(StringIndexOutOfBoundsException e) {
    		return mText.substring(mText.indexOf('x')+1);
    	}
    }
    
    public String getSizeUnits() {
        return mText.substring(mText.indexOf(' ')+1);
    }

    @Override
    protected View onCreateDialogView() {
    	View lView = super.onCreateDialogView();
   	
    	mEditTextH = (EditText) lView.findViewById(R.id.size_h);
    	mEditTextV = (EditText) lView.findViewById(R.id.size_v);

    	//Print size
        mUnitsSpinner = (Spinner) lView.findViewById(R.id.units_spinner);
        mUnitsAdapter = ArrayAdapter.createFromResource(lView.getContext(), R.array.units, android.R.layout.simple_spinner_item);
        mUnitsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mUnitsSpinner.setAdapter(mUnitsAdapter);
    	
    	return lView;
    }
    
    @Override
    protected void onPrepareDialogBuilder(AlertDialog.Builder builder) {
    	builder.setPositiveButton("Ok", this)
        .setNegativeButton("Cancel", this);
    }
    
    @Override
    protected void onBindDialogView(View view) {
        super.onBindDialogView(view);
        
        mEditTextH.setText(getSizeTextH());
        mEditTextV.setText(getSizeTextV());

        mUnitsSpinner.setSelection( mUnitsAdapter.getPosition(getSizeUnits()) );
    }

    
    protected void updateSummary() {
    	setSummary(getContext().getString(R.string.default_value_summary, mText));
    }
    
    @Override
    protected void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);
        
        
        
        if (positiveResult) {
        	String value = new String();
        	value = mEditTextH.getText().toString() + 'x' + mEditTextV.getText().toString() + ' ' + mUnitsSpinner.getSelectedItem();
            
            if (callChangeListener(value)) {
                setSizeText(value);
            }
        }
        
        updateSummary();
        
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        return a.getString(index);
    }

    @Override
    protected void onSetInitialValue(boolean restoreValue, Object defaultValue) {
        setSizeText(restoreValue ? getPersistedString(mText) : (String) defaultValue);
        updateSummary();
    }


    public EditText getSizeHEditText() {
        return mEditTextH;
    }

    public EditText getSizeVEditText() {
        return mEditTextV;
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        final Parcelable superState = super.onSaveInstanceState();
        if (isPersistent()) {
            // No need to save instance state since it's persistent
            return superState;
        }
        
        final SavedState myState = new SavedState(superState);
        myState.texth = getSizeTextH();
        myState.textv = getSizeTextV();
        myState.units = getSizeUnits();
        return myState;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        if (state == null || !state.getClass().equals(SavedState.class)) {
            // Didn't save state for us in onSaveInstanceState
            super.onRestoreInstanceState(state);
            return;
        }
         
        SavedState myState = (SavedState) state;
        super.onRestoreInstanceState(myState.getSuperState());
        setSizeText(myState.texth, myState.textv, myState.units);
    }
    
    private static class SavedState extends BaseSavedState {
        String texth;
        String textv;
        String units;
        
        public SavedState(Parcel source) {
            super(source);
            texth = source.readString();
            textv = source.readString();
            units = source.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            dest.writeString(texth);
            dest.writeString(textv);
            dest.writeString(units);
        }

        public SavedState(Parcelable superState) {
            super(superState);
        }

        @SuppressWarnings("unused")
		public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            public SavedState createFromParcel(Parcel in) {
                return new SavedState(in);
            }

            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        };
    }
    
}
