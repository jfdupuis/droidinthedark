package com.darkroom.library;

import com.darkroom.library.dao.DBObject;

public class Lens extends DBObject {
	private int mFocalLength; //[mm]
	private String mName;
	//private float mMinAperture;
	//private float mMaxAperture;
	
	public Lens(String inName, int inFocalLength) {
		mName = inName;
		mFocalLength = inFocalLength;
	}
	
	public String getName() {
		return mName;
	}
	
	public int getFocalLength() {
		return mFocalLength;
	}
}
