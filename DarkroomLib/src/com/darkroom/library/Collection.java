package com.darkroom.library;

import com.darkroom.library.dao.DBObject;

import java.util.Date;

public class Collection extends DBObject {
	private String mName;
	private String mDescription = "";
	private Date mDate;
	
	public Collection(String inName) {
		mName = inName;
	}
	
	public Collection(Collection inCollection) {
		super(inCollection);
		this.mName = inCollection.mName;
		this.mDate = inCollection.mDate;
		this.mDescription = inCollection.mDescription;
	}
	
	public void setName(String inName) {
		mName = inName;
	}
	public String getName() {
		return mName;
	}

	public void setDate(Date inDate) {
		this.mDate = inDate;
	}

	public Date getDate() {
		return mDate;
	}

	public void setDescription(String inDescription) {
		this.mDescription = inDescription;
	}

	public String getDescription() {
		return mDescription;
	}
}
