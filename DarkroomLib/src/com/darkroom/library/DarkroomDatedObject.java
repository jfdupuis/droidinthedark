package com.darkroom.library;

import com.darkroom.library.dao.DBObject;

import android.os.Parcel;

import java.util.Date;

public abstract class DarkroomDatedObject extends DBObject {
	protected Date mDate = null; //Date this film was finish
	protected int mIDNumber = -1; //Unique number identification for this object
	protected String mUserId; //User identifier
	protected boolean mHasCustomId;
	
	abstract protected String generateUniqueId();
	
	public DarkroomDatedObject() {
		
	}
	
	public DarkroomDatedObject(DarkroomDatedObject inObject) {
		super(inObject);
		this.mDate = inObject.mDate;
		this.mIDNumber = inObject.mIDNumber;
		this.mUserId = inObject.mUserId;
		this.mHasCustomId = inObject.hasCustomId();
	}
	
	@Override
	public void writeToParcel(Parcel outParcel, int inFlags) {
		super.writeToParcel(outParcel, inFlags);
		outParcel.writeLong(mDate.getTime());
		outParcel.writeInt(mIDNumber);
		outParcel.writeString(mUserId);
		outParcel.writeBooleanArray(new boolean[] {mHasCustomId});
	}
	
	@Override
    public void readFromParcel(Parcel inParcel) {
    	super.readFromParcel(inParcel);
    	mDate = new Date();
    	mDate.setTime(inParcel.readLong());
    	mIDNumber = inParcel.readInt();
    	mUserId = inParcel.readString();
    	boolean[] lBools = new boolean [1];
    	inParcel.readBooleanArray(lBools);
    	mHasCustomId = lBools[0];
    	
	}
	
	public Date getDate() {
		return mDate;
	}
	
	public void setDate(Date inDate) {
		mDate = inDate;
	}
	
	public int getIdNumber() {
		return mIDNumber;
	}
	
	public void setIdNumber(int inNumber) {
		mIDNumber = inNumber;
	}
	
	/**
	 * Set the id of the film.
	 * @param inCustomId The custom id of the film, if null, a generateUniqueId is called;
	 */
	public void setUserId(String inCustomId, boolean inIsCustomId) {
		if(inCustomId == null || inCustomId.equals("")) {
			mHasCustomId = false;
			generateUniqueId();
		} else {
			mHasCustomId = inIsCustomId;
			mUserId = inCustomId;
		}
	}
	
	public boolean hasCustomId() {
		return mHasCustomId;
	}
	
	public String getUserId() {
		return mUserId;
	}
}
