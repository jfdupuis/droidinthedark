package com.darkroom.library;

import com.darkroom.library.dao.DBObject;

import android.os.Parcel;
import android.os.Parcelable;

public class Agitation extends DBObject implements Parcelable {
	static public final String NOAGITATION = "Standing";
	static public final String CONTINUOUS = "Continuous";
	static public final String NOTSPECIFIED = "Not Specified";
	
	public static final int NOTSPECIFIEDKEY = -2; //need to be negative
	
	private String mOldDescription = null; //Hold the old agitation description
	
	private int mInitial; // [s] if 0, no initial, first agitation will be at t0 + mInterval
	private int mInterval; // [s] if 0:continuous, -1:standing, NOTSPECIFIEDKEY:not specified
	private int mDuration; // [s] duration of each periodic agitation

	public Agitation(int initial, int interval, int duration) {
		mInitial = initial;
		mInterval = interval;
		mDuration = duration;
	}
	
	/** Used to export old database. Should not be used otherwise.
	 */
	public void setDescription(String inDescription) {
		mOldDescription = inDescription;
	}
	
	public String toString() {
		if(mInitial < 0 && mOldDescription != null){
			return mOldDescription; //Compatibility with old database
		}
		
		if(mInterval == NOTSPECIFIEDKEY)
			return NOTSPECIFIED;
		
		if(mInitial == 0 && mInterval < 0 && mDuration == 0)
			return NOAGITATION;
		if(mInterval == 0)
			return CONTINUOUS;
		
		if(mInterval < 0)
			return String.format("%d - %s", mInitial, NOAGITATION);
		else
			return String.format("%d - %d - %d", mInitial, mInterval, mDuration);
	}
	
	public void setInterval(int interval) {
		this.mInterval = interval;
	}

	public int getInterval() {
		return mInterval;
	}

	public void setDuration(int duration) {
		this.mDuration = duration;
	}

	public int getDuration() {
		return mDuration;
	}

	public void setInitial(int initial) {
		this.mInitial = initial;
	}

	public int getInitial() {
		return mInitial;
	}

	// /////Parcel related methods
	public static final Parcelable.Creator<Agitation> CREATOR = new Parcelable.Creator<Agitation>() {
		public Agitation createFromParcel(Parcel inParcel) {
			return new Agitation(inParcel);
		}

		public Agitation[] newArray(int size) {
			return new Agitation[size];
		}
	};

	private Agitation(Parcel inParcel) {
		readFromParcel(inParcel);
	}

	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel outParcel, int inFlags) {
		super.writeToParcel(outParcel, inFlags);
		outParcel.writeInt(mInitial);
		outParcel.writeInt(mInterval);
		outParcel.writeInt(mDuration);
	}

	@Override
	public void readFromParcel(Parcel inParcel) {
		super.readFromParcel(inParcel);
		mInitial = inParcel.readInt();
		mInterval = inParcel.readInt();
		mDuration = inParcel.readInt();
	}

}
