package com.darkroom.library;

import java.util.ArrayList;

import android.os.Parcel;
import android.os.Parcelable;

import com.darkroom.library.dao.DBObject;
import com.jfdupuis.library.math.Fraction;


public class ExposureItem extends DBObject implements Parcelable {
	public static final int BURNEXPOSURE = 1;
	public static final int DODGEEXPOSURE = 0;
	
	private String mDescription = "";
	private int mPosition;
	private Fraction mValue;
	private int mType;
	private static final Fraction DEFAULT_MAX = new Fraction(10);
    private static final Fraction DEFAULT_MIN = new Fraction(0);

    static public int find(ArrayList<ExposureItem> inArray, ExposureItem inObject) {
		for(int i = 0; i < inArray.size(); ++i) {
			if(inArray.get(i).mId == inObject.mId)
				return i;
		}
		return -1;
	}
    
    public ExposureItem(int inPosition, int inType, String inDescription, Fraction inValue) {
		mValue = inValue;
		mDescription = inDescription;
		mPosition = inPosition;
		mType = inType;
	}
	
	public ExposureItem(ExposureItem inItem) {
		this.mDescription = inItem.mDescription;
		this.mPosition = inItem.mPosition;
		this.mValue = inItem.mValue;
		this.mType = inItem.mType;
	}
		
	public String getDescription() {
	    return mDescription;
	}
	
	public void setDescription(String inDescription) {
		mDescription = inDescription;
	}
	
	public Fraction getValue() {
		return mValue;
	}
	
	public String getValueStr() {
		return getValue().toString();
	}
	
	public void setValue(Fraction inValue) {
		mValue = inValue;
	}
	
	public int getType() {
		return mType;
	}
	
	public void setType(int inType) {
		mType = inType;
	}
	
	public void setPosition(int inPosition) {
		this.mPosition = inPosition;
	}

	public int getPosition() {
		return mPosition;
	}
	
	public void incValue(Fraction inValue) {
		mValue = (Fraction) mValue.plus(inValue);
		
		if (mValue.compareTo(DEFAULT_MAX) > 0) {
			mValue = DEFAULT_MIN;
        } else if (mValue.compareTo(DEFAULT_MIN) < 0) {
        	mValue = DEFAULT_MAX;
        }
	}
	
	public void decValue(Fraction inValue) {
		mValue = (Fraction) mValue.minus(inValue);
		
		if (mValue.compareTo(DEFAULT_MAX) > 0) {
			mValue = DEFAULT_MIN;
        } else if (mValue.compareTo(DEFAULT_MIN) < 0) {
        	mValue = DEFAULT_MAX;
        }
	}
	
	///////Parcel related methods
	public static final Parcelable.Creator<ExposureItem> CREATOR = new
	Parcelable.Creator<ExposureItem>() {
	        public ExposureItem createFromParcel(Parcel inParcel) {
	            return new ExposureItem(inParcel);
	        }

	        public ExposureItem[] newArray(int size) {
	            return new ExposureItem[size];
	        }
	    };

	    private ExposureItem(Parcel inParcel) {
	        readFromParcel(inParcel);
	    }
	
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel outParcel, int inFlags) {
		super.writeToParcel(outParcel, inFlags);
		outParcel.writeParcelable(mValue, inFlags);
		outParcel.writeString(mDescription);
		outParcel.writeInt(mPosition);
		outParcel.writeInt(mType);
	}

	@Override
    public void readFromParcel(Parcel inParcel) {
		super.readFromParcel(inParcel);
    	mValue = inParcel.readParcelable(Fraction.class.getClassLoader());
    	mDescription = inParcel.readString();
    	mPosition = inParcel.readInt();
    	mType = inParcel.readInt();
    }


}
