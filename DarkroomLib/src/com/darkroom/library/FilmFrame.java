package com.darkroom.library;

import com.darkroom.library.dao.DBObject;
import java.util.Date;


public class FilmFrame extends DBObject {
	
	private final int mFrameNumber;
	private String mContentDescription = "";
	
	private long mFilmId = -1;
	protected Date mDate = null;
	protected String mImagePath = "";
	
	protected long mExposureId = -1;
	
	public FilmFrame(FilmFrame inFrame) {
		mId = inFrame.mId;
		mFilmId = inFrame.mFilmId;
		mFrameNumber = inFrame.mFrameNumber;
		mContentDescription = inFrame.mContentDescription;
		mDate = inFrame.mDate;
		mImagePath = inFrame.mImagePath;
		mExposureId = inFrame.mExposureId;
	}
	
	public FilmFrame(long inFilmId, int inFrameNumber) { 
		mFrameNumber = inFrameNumber;
		mFilmId = inFilmId;
	}
	
	public long getExposure() { return mExposureId; }
	public void setExposure(long inExposure) { mExposureId = inExposure; }
	
	public int getFrameNumber() { return mFrameNumber; }
	
	public long getFilmId() { return mFilmId; }
	public void setFilmId(long inId) { mFilmId = inId; }
	
	public void setDate(Date inDate) { mDate = inDate; }
	public Date getDate() { return mDate; }
	
	public void setDescription(String inText) {
		mContentDescription = inText;
	}
	public String getDescription() { 
		return mContentDescription;
	}
	
	public void setImagePath(String inPath) {
		mImagePath = inPath;
	}
	public String getImagePath() {
		return mImagePath;
	}
	
	
	
	
}