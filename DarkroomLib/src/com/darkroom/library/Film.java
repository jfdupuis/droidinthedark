package com.darkroom.library;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


public class Film extends DarkroomDatedObject  {

//	private static final long serialVersionUID = 1L;
	private Camera mCamera = null; //Camera use to shot this film

	ArrayList<FilmFrame> mFilmFrames;

	public Info mInfo;
	private DevelopmentTime mDevelopment;
	//public Exposure mExposure;
	
	@Override
	public void setId(long inId) {
		super.setId(inId);
		for(FilmFrame lFrame : mFilmFrames) {
			lFrame.setFilmId(inId);
		}
	}
	
	public Film(Camera inCamera, Date inDate, int inDayNumber, DevelopmentTime inDevelopment) {
		mDevelopment = inDevelopment;
		
		setCamera(inCamera);
		mDate = inDate;
		mIDNumber = inDayNumber;
		generateUniqueId();
		
		mFilmFrames = new ArrayList<FilmFrame>(); 
		mInfo = this.new Info();
		//mExposure = this.new Exposure();
	}
	
	public FilmFrame addFrame(int inNumber) {
		mFilmFrames.ensureCapacity(inNumber);
		mFilmFrames.set( inNumber, new FilmFrame(mId, inNumber) );
		return mFilmFrames.get(inNumber);
	}
	
	public FilmFrame getFrame(int inNumber) {
		return mFilmFrames.get(inNumber);
	}
	
	public ArrayList<FilmFrame> getFrames() {
		return mFilmFrames;
	}
	
	public void addFrame(FilmFrame inFrame) {
		mFilmFrames.add(inFrame);
	}
	
	public DevelopmentTime getDevelopment() {
		return mDevelopment;
	}
	
	public void setDevelopment(DevelopmentTime inDevelopment) {
		mDevelopment = inDevelopment;
	}
	
	public Camera getCamera() {
		return mCamera;
	}
	
	public void setCamera(Camera inCamera){
		mCamera = inCamera;
		
		//Set dev format
		int lSize = mCamera.getSize();
		if(lSize == 135)
			mDevelopment.mFormat = "135";
		else if(lSize == 120)
			mDevelopment.mFormat = "120";
		else
			mDevelopment.mFormat = "Sheet";
	}
	
	
	protected String generateUniqueId() {
		Calendar lCalendar = Calendar.getInstance();
        lCalendar.setTime(mDate);
        
		mUserId = String.format("%tF-%s-%d", lCalendar, mCamera.getShortId(), mIDNumber);
		Logger.d("New user id: " + mUserId);
		return mUserId;
	}
	
	public class Info {
		
		public String mName = ""; //Short name of the film type
		public String mDescription = ""; //Longer film name
		public int mShotISO; //Iso shot at
		private Emulsion mEmulsion = null; //TODO change to id
		
		public Info() {}
		public Info(Info inInfo) {
			this.mName = inInfo.mName;
			this.mDescription = inInfo.mDescription;
			this.mShotISO = inInfo.mShotISO;
			setEmulsion(inInfo.mEmulsion);
		}
	
		public void setEmulsion(Emulsion inEmulsion) {
			this.mEmulsion = inEmulsion;
			mDevelopment.mEmulsion = inEmulsion.getId();
		}
		public Emulsion getEmulsion() {
			return mEmulsion;
		}
	}
	
}
