package com.darkroom.library;

import com.darkroom.library.dao.DBObject;
import com.jfdupuis.library.math.Fraction;

public class Filter extends DBObject {
	private String mName;
	private Fraction mFactor;
	private long mFilmFrameExposureId = -1;
	
	public Filter(String inName, Fraction inFactor) {
		mName = inName;
		mFactor = inFactor;
	}
	
	public void setName(String inName) {
		this.mName = inName;
	}
	public String getName() {
		return mName;
	}
	
	public void setFactor(Fraction inFactor) {
		this.mFactor = inFactor;
	}
	public Fraction getFactor() {
		return mFactor;
	}
	
	public void setFilmFrameExposureId(long inId) {
		mFilmFrameExposureId = inId;
	}
	
	public long getFilmFrameExposureId() {
		return mFilmFrameExposureId;
	}
	
	
}
