package com.darkroom.library;

public class Licensing {

	private static boolean mIsDemo = false;
	
	public static void setDemo(boolean inDemoState) {
		mIsDemo = inDemoState;
	}
	
	public static boolean isDemo() {
		return mIsDemo;
	}
}
