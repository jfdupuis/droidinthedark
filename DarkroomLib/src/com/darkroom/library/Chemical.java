package com.darkroom.library;

import com.darkroom.library.dao.DBObject;

public class Chemical extends DBObject {
	//Chemical category
	public final static String DEVELOPER = "Developer";
	public final static String TONER = "Toner";
	public final static String FIXER = "Fixer";
	public final static String OTHER = "Other";
	
	private final String mName;
	private String mFormula = "";
	private String mCategory = "";
	
	public Chemical(String inName) {
		mName = inName;
	}

	public String getName() {
		return mName;
	}
	
	public void setFormula(String mFormula) {
		this.mFormula = mFormula;
	}
	public String getFormula() {
		return mFormula;
	}
	public void setCategory(String inCategory) {
		this.mCategory = inCategory;
	}
	public String getCategory() {
		return mCategory;
	}
}
