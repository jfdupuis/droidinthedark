package com.darkroom.library;

import com.darkroom.library.dao.DBObject;

import android.text.TextUtils;

public class Camera extends DBObject {
	private String mBrand;
	private String mModel;
	private int mYear;
	private String mSerial;
	private String mShortId;
	private int mSize; //135, 120, 45, 810
	
	
	public Camera(String inBrand, String inModel) {
		if(TextUtils.isEmpty(inModel))
			throw new RuntimeException("Camera model can't be empty");
		if(TextUtils.isEmpty(inBrand))
			throw new RuntimeException("Camera model can't be empty");
		
		mModel = inModel;
		mBrand = inBrand;
	}
	public void setBrand(String mBrand) {
		this.mBrand = mBrand;
	}
	public String getBrand() {
		return mBrand;
	}
	public void setModel(String mModel) {
		this.mModel = mModel;
	}
	public String getModel() {
		return mModel;
	}
	public void setYear(int mYear) {
		this.mYear = mYear;
	}
	public int getYear() {
		return mYear;
	}
	public void setSerial(String mSerial) {
		this.mSerial = mSerial;
	}
	public String getSerial() {
		return mSerial;
	}
	public void setSize(int mSize) {
		this.mSize = mSize;
	}
	public int getSize() {
		return mSize;
	}
	public void setShortId(String inShortId) {
		this.mShortId = inShortId;
	}
	public String getShortId() {
		return mShortId;
	}
}
