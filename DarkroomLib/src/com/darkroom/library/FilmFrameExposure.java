package com.darkroom.library;

import com.darkroom.library.dao.DBObject;
import com.jfdupuis.library.math.Fraction;

import java.util.ArrayList;

public class FilmFrameExposure extends DBObject {
	static final protected long DEFAULT_HIGHLIGHT = 20;
	static final protected long DEFAULT_SHADOW = 0;
	static final protected long DEFAULT_EXPOSURE = 0;
	static final protected float DEFAULT_APERTURE = 5.6f;
	static final protected long DEFAULT_SHUTTERSPEED = 60;

	public long mFilmFrameId = -1;
	public long mLens = -1;
	public boolean mHasExposureLock = true;
	public boolean mHasBellowExtension = false;
	public float mBellowExtension = 0; //[mm]
	public Fraction mBellowFactor = new Fraction(0);
	public Fraction mHighlight = new Fraction(DEFAULT_HIGHLIGHT); //Metering of the highlight [EV]
	public Fraction mShadow = new Fraction(DEFAULT_SHADOW); //Metering of the shadow [EV]
	public Fraction mExposure = new Fraction(DEFAULT_EXPOSURE); //Exposure chosen for the exposure [EV]
	public float mAperture = DEFAULT_APERTURE; //Aperture used in f-stops
	public Fraction mShutterSpeed = new Fraction(1,DEFAULT_SHUTTERSPEED); //Shutter speed used [s]
	public float mReciprocity = 0; //Shutter speed after reciprocity compensation [s]
	
	public ArrayList<Long> mFilters; //List of the filter used
	public String mNotes = "";
		
	public FilmFrameExposure(long inFilmFrameId) {
		mFilmFrameId = inFilmFrameId;
		mFilters = new ArrayList<Long>();
	}
	
	public FilmFrameExposure(FilmFrameExposure inExposure) {
		this.mId = inExposure.mId;
		this.mFilmFrameId = inExposure.mFilmFrameId;
		this.mLens = inExposure.mLens;
		this.mHasExposureLock = inExposure.mHasExposureLock;
		this.mHasBellowExtension = inExposure.mHasBellowExtension;
		this.mBellowExtension = inExposure.mBellowExtension;
		this.mBellowFactor = inExposure.mBellowFactor;
		this.mHighlight = inExposure.mHighlight;
		this.mShadow = inExposure.mShadow;
		this.mExposure = inExposure.mExposure;
		this.mAperture = inExposure.mAperture;
		this.mShutterSpeed = inExposure.mShutterSpeed;
		this.mReciprocity = inExposure.mReciprocity;
		this.mNotes = inExposure.mNotes;
	}
}
