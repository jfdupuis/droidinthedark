package com.darkroom.library;

import com.darkroom.library.dao.DBObject;


public class Emulsion extends DBObject {
	private String mName;
	public  boolean mColor;
	public String mBrand; //User identifier
	public int mISO; //Box iso
	
	
	public Emulsion(String inCodeName) {
		mName = inCodeName;
	}
	
	public String getName() { return mName; }
}
