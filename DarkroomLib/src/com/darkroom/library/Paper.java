package com.darkroom.library;

import com.darkroom.library.dao.DBObject;

public class Paper extends DBObject { 
	public static final int RC_PAPER = 0;
	public static final int FB_PAPER = 1;
	
	public static final float MULTIGRADE = -2;
	public static final float GRADE00 = -1;
	
	public String mName = ""; //Short paper name
	public String mDescription = ""; //Long paper name
	public int mType = -1; //Short paper name, 0 = RC, 1 = FB, -1 not specified
	public float mGrade = -3; //Paper grade, -2 = multi, -1 = 00, -3 not specified
	
	public void setType(String inString) {
		if(inString.equals("RC")) 
			mType = RC_PAPER;
		else if(inString.equals("FB"))
			mType = FB_PAPER;
		else
			throw new RuntimeException("Unknown paper type " + inString);
	}
	
	public void setGrade(String inString) {
		if(inString.equals("Multi-grade")) 
			mGrade = MULTIGRADE;
		else if(inString.equals("00"))
			mGrade = GRADE00;
		else
			mGrade = Float.valueOf(inString);
	}

}