package com.darkroom.library.dao;

import com.darkroom.library.Filter;
import com.darkroom.library.R;
import com.jfdupuis.library.math.Fraction;
import com.jfdupuis.library.math.FractionConversionException;
import com.jfdupuis.library.screen.ScreenUtil;

import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class AddNewFilterDialog extends AddNewElementDialog {
		
	String mTableName = null;
	
	@Override
	public void onCreate(Bundle inBundle) {
		super.onCreate(inBundle);
		setContentView(R.layout.addnew_filter_dialog);
		
		mTableName = getIntent().getStringExtra("table");
		if(mTableName == null)
			throw new RuntimeException("table was expected in the intent!");
		
		initialize();
   
        if(isEditing()) {
        	setTitle(R.string.edit_filter_title);
			loadData();
        } else {
        	setTitle(R.string.addnew_filter_title);
        }
	}
	
	@Override
	protected void loadData() {
		Filter lFilter = mDarkroomDBAdapter.getFilter(mId, mTableName);
		EditText lName = (EditText) findViewById(R.id.name);
		lName.setText(lFilter.getName());
		EditText lFactor = (EditText) findViewById(R.id.filter_factor);
		lFactor.setText(Double.toString(lFilter.getFactor().asDouble()));
	}


	public void onClick(View v) {
		//Safe the filled content in the database
		final EditText lName = (EditText) findViewById(R.id.name);
		final EditText lFactor = (EditText) findViewById(R.id.filter_factor);

		if(TextUtils.isEmpty(lName.getText().toString())) {
			ScreenUtil.displayMessage(AddNewFilterDialog.this, "Filter name can't be empty !", Toast.LENGTH_SHORT);
			return;
		}
		
		Fraction lFilterFactor;
		try {
			lFilterFactor = new Fraction(Double.valueOf(lFactor.getText().toString()));
		} catch (NumberFormatException e1) {
			ScreenUtil.displayMessage(AddNewFilterDialog.this, "Invalid filter factor!", Toast.LENGTH_SHORT);
			return;
		} catch (FractionConversionException e1) {
			ScreenUtil.displayMessage(AddNewFilterDialog.this, "Invalid filter factor!", Toast.LENGTH_SHORT);
			return;
		}
		
		Filter lFilter = new Filter(lName.getText().toString(), lFilterFactor);
			
		try {	
			if(isEditing()) {
				lFilter.setId(mId);
				mDarkroomDBAdapter.updateFilterValue(lFilter, mTableName);
			} else {
				mDarkroomDBAdapter.insertFilter(lFilter, mTableName);
			}
			setResult(RESULT_OK, new Intent().putExtra("id",lFilter.getId()));
			finish();
		} catch (SQLiteConstraintException e) {
			ScreenUtil.displayMessage(AddNewFilterDialog.this, "Filter already exist", Toast.LENGTH_SHORT);
		}
	}


}