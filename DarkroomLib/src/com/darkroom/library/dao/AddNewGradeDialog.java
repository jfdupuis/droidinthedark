package com.darkroom.library.dao;

import com.darkroom.library.Logger;
import com.darkroom.library.R;
import com.jfdupuis.library.screen.ScreenUtil;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class AddNewGradeDialog extends AddNewElementDialog {
	
	EditText mInputString = null;
	long mPrintId;
	
	@Override
	public void onCreate(Bundle inBundle) {
		super.onCreate(inBundle);
		setContentView(R.layout.addnew_stringdata_dialog);
		
		Intent lIntent = getIntent();
		mPrintId = lIntent.getLongExtra("print_id", -1);
		if(mPrintId <= 0)
			throw new RuntimeException("A print ID should be provided!");
		
		
		
		initialize();
		
		TextView lStringHeader = (TextView) findViewById(R.id.string_header);
		lStringHeader.setVisibility(View.GONE);
		TextView lCategoryHeader = (TextView) findViewById(R.id.category_header);
		lCategoryHeader.setVisibility(View.GONE);
		EditText lInputCategory = (EditText) findViewById(R.id.category);
		lInputCategory.setVisibility(View.GONE);
		
		mInputString = (EditText) findViewById(R.id.string);
		mInputString.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED);
		
		float lGrade = lIntent.getFloatExtra("grade", -2);
		if(lGrade != -2) {
			setEditingMode(true);
			setTitle(R.string.edit_grade_title);
			mInputString.setText(Float.toString(lGrade));
		} else {
			setTitle(R.string.addnew_grade_title);
		}
	}
	
	@Override
	protected void loadData() {	}


	public void onClick(View v) {
		//Safe the filled content in the database
		final String lString = mInputString.getText().toString();
		
		if(TextUtils.isEmpty(lString)) {
			ScreenUtil.displayMessage(AddNewGradeDialog.this, "" +	"A grade should be defined", Toast.LENGTH_SHORT);
			mInputString.requestFocus();
			return;
		}
					
		try {
			float lGradeValue = Float.valueOf(lString);
			//Round to every 0.5
			lGradeValue = Math.round(lGradeValue*2)/2f;
			
			Cursor lCursor = mDarkroomDBAdapter.getDataBase().query(
					DarkroomDBHelper.EXPOSURE_TABLE_NAME, 
					new String[]{DarkroomDBHelper.KEY_ID,DarkroomDBHelper.KEY_EXPOSURE_GRADE},
					DarkroomDBHelper.KEY_EXPOSURE_PRINTID + "=" + mPrintId 
					+ " and " + DarkroomDBHelper.KEY_EXPOSURE_GRADE + "=" + lGradeValue, null, null, null, null, null);
			
			if(lCursor.getCount() > 0) {
				ScreenUtil.displayMessage(AddNewGradeDialog.this, "An exposure with grade " + lString + " already exist for this print", Toast.LENGTH_SHORT);
				mInputString.requestFocus();
				return;
			}
			Logger.d("Return new grade: " + lGradeValue);
			setResult(RESULT_OK, new Intent().putExtra("grade",lGradeValue));
			finish();
		} catch (SQLiteConstraintException e) {
			ScreenUtil.displayMessage(AddNewGradeDialog.this, "An exposure with grade " + lString + " already exist for this print", Toast.LENGTH_SHORT);
			mInputString.requestFocus();
			return;
		} catch (NumberFormatException e) {
			ScreenUtil.displayMessage(AddNewGradeDialog.this, lString + " is not a valid input for grade", Toast.LENGTH_SHORT);
			mInputString.requestFocus();
			return;
		}
	}


}