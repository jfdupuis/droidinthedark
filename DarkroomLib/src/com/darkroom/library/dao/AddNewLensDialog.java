package com.darkroom.library.dao;

import com.darkroom.library.Lens;

import com.darkroom.library.R;
import com.jfdupuis.library.screen.ScreenUtil;


import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class AddNewLensDialog extends AddNewElementDialog {
		
	@Override
	public void onCreate(Bundle inBundle) {
		super.onCreate(inBundle);
		setContentView(R.layout.addnew_lens_dialog);
		
		initialize();
   
        if(isEditing()) {
        	setTitle(R.string.edit_lens_title);
			loadData();
        } else {
        	setTitle(R.string.addnew_lens_title);
        }
	}
	
	@Override
	protected void loadData() {
		Lens lLens = mDarkroomDBAdapter.getLens(mId);
		EditText lName = (EditText) findViewById(R.id.name);
		lName.setText(lLens.getName());
		EditText lFocal = (EditText) findViewById(R.id.focal_length);
		lFocal.setText(lLens.getFocalLength());
	}


	public void onClick(View v) {
		//Safe the filled content in the database
		final EditText lName = (EditText) findViewById(R.id.name);
		final EditText lFocal = (EditText) findViewById(R.id.focal_length);

		if(TextUtils.isEmpty(lName.getText().toString())) {
			ScreenUtil.displayMessage(AddNewLensDialog.this, "Lens name can't be empty !", Toast.LENGTH_SHORT);
			return;
		}
							
		try {	
			Lens lLens = new Lens(lName.getText().toString(), Integer.valueOf(lFocal.getText().toString()));
			
			if(lLens.getFocalLength() == 0) {
				ScreenUtil.displayMessage(AddNewLensDialog.this, "Invalid focal lenght", Toast.LENGTH_SHORT);
				return;
			}
			
			if(isEditing()) {
				lLens.setId(mId);
				mDarkroomDBAdapter.updateLensValue(lLens);
			} else {
				mDarkroomDBAdapter.insertLens(lLens);
			}
			setResult(RESULT_OK, new Intent().putExtra("id",lLens.getId()));
			finish();
		} catch (SQLiteConstraintException e) {
			ScreenUtil.displayMessage(AddNewLensDialog.this, "Lens already exist", Toast.LENGTH_SHORT);
		} catch (NumberFormatException e) {
			ScreenUtil.displayMessage(AddNewLensDialog.this, "Invalid focal lenght", Toast.LENGTH_SHORT);
			return;
		}
		
	}

}