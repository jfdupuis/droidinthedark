package com.darkroom.library.dao;

import com.darkroom.library.Agitation;
import com.darkroom.library.Camera;
import com.darkroom.library.Chemical;
import com.darkroom.library.Collection;
import com.darkroom.library.DevelopmentTime;
import com.darkroom.library.Emulsion;
import com.darkroom.library.Exposure;
import com.darkroom.library.ExposureItem;
import com.darkroom.library.Film;
import com.darkroom.library.FilmFrame;
import com.darkroom.library.FilmFrameExposure;
import com.darkroom.library.Filter;
import com.darkroom.library.Lens;
import com.darkroom.library.Logger;
import com.darkroom.library.Paper;
import com.darkroom.library.Print;
import com.jfdupuis.library.math.Fraction;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.database.sqlite.SQLiteDatabase.CursorFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

public class DarkroomDBAdapter extends DarkroomDBHelper{

	public DarkroomDBAdapter(Context inContext) {
		super(inContext);
		Logger.d("DarkroomDBAdapter.DarkroomDBAdapter");
	}
	
	public DarkroomDBAdapter(Context inContext, String name, CursorFactory factory, int version) {
		super(inContext, name, factory, version);
	}
	
//	@Override
//	public void onCreate(SQLiteDatabase _db) {
//		super.onCreate(_db);
//		
//		//Ensure 1 index for all not specified
//		insertNullChemical(_db);
//		insertNullEmulsion(_db);
//		insertNullPaper(_db);
//		insertNullLens(_db);
//		insertNullFilter(_db);
//		insertNullAgitation(_db);
//		insertNullStringData(_db, DILUTION_TABLE_NAME, new StringData("Stock", null));
//		
//	}
	
	//TODO Implement Agitation table
	
	private String[] addKeyId(String[] inColumns) {
		return addKeyId(inColumns,null);
	}
	
	private String[] addKeyId(String[] inColumns, String inTableName) {
		ArrayList<String> lColumns = new ArrayList<String>(Arrays.asList(inColumns));
		if(inTableName != null)
			lColumns.add(inTableName + "." + KEY_ID);
		else
			lColumns.add(KEY_ID);
		String[] myArray = (String[]) lColumns.toArray(new String[0]);
		return myArray;
	}

	/////////////
	// Print
	/////////////
	private ContentValues fillPrintValue(Print inPrint) {
		ContentValues newPrintValues = new ContentValues();
	    // Assign values for each row.
	    newPrintValues.put(KEY_PRINT_USERID, inPrint.getUserId());
	    newPrintValues.put(KEY_PRINT_USERID_ISCUSTOM, inPrint.hasCustomId() ? 1 : 0);
	    newPrintValues.put(KEY_PRINT_DATE, inPrint.getDate().getTime());
	    newPrintValues.put(KEY_PRINT_FILMID,  inPrint.getFilmId());
	    newPrintValues.put(KEY_PRINT_IDNUM,  inPrint.getIdNumber());
	    newPrintValues.put(KEY_PRINT_FRAMEID,  inPrint.getFilmFrameNumber());
	    newPrintValues.put(KEY_PRINT_TITLE, inPrint.getTitle());
	    newPrintValues.put(KEY_PRINT_DESC, inPrint.getDescription());
	    newPrintValues.put(KEY_PRINT_NOTES, inPrint.getNotes());
	    newPrintValues.put(KEY_PRINT_SIZE_H, inPrint.getSizeH());
	    newPrintValues.put(KEY_PRINT_SIZE_V, inPrint.getSizeV());
	    
	    //Deal with unspecified paper
	    if(inPrint.getPaper() >= 0) {
	    	newPrintValues.put(KEY_PRINT_PAPERID, inPrint.getPaper());
	    }
	    else {
	    	//Find "Not Specified" developer
	    	Cursor lCursor = mDataBase.query(true, PAPER_TABLE_NAME, null,
	    			KEY_PAPER_NAME + " = \"" + NOT_SPECIFIED + "\"", null, null, null, null, null);
	    	long lId;
	    	if((lCursor.getCount() == 0) || !lCursor.moveToFirst())
	    		lId = insertNullPaper(mDataBase);
	    	else {
	    		lCursor.moveToFirst();
	    		lId = lCursor.getLong(lCursor.getColumnIndex(KEY_ID));
	    	}
	    	
	    	newPrintValues.put(KEY_PRINT_PAPERID, lId);
	    }
	    
	    //Deal with unspecified developer
	    if(inPrint.getDeveloper() >= 0) {
	    	newPrintValues.put(KEY_PRINT_DEVID, inPrint.getDeveloper());
	    }
	    else {
	    	//Find "Not Specified" developer
	    	Cursor lCursor = mDataBase.query(true, CHEMICAL_TABLE_NAME, null,
	    			KEY_CHEMICAL_NAME + " = \"" + NOT_SPECIFIED + "\"", null, null, null, null, null);
	    	long lId;
	    	if(lCursor.getCount() == 0)
	    		lId = insertNullChemical(mDataBase);
	    	else {
	    		lCursor.moveToFirst();
	    		lId = lCursor.getLong(lCursor.getColumnIndex(KEY_ID));
	    	}
	    	
	    	newPrintValues.put(KEY_PRINT_DEVID, lId);
	    }
	    
	    //Deal with unspecified toner
	    if(inPrint.getToner() >= 0) {
	    	newPrintValues.put(KEY_PRINT_TONERID, inPrint.getToner());
	    }
	    else {
	    	//Find "Not Specified" developer
	    	Cursor lCursor = mDataBase.query(true, CHEMICAL_TABLE_NAME, null,
	    			KEY_CHEMICAL_NAME + " = \"" + NOT_SPECIFIED + "\"", null, null, null, null, null);
	    	long lId;
	    	if(lCursor.getCount() == 0)
	    		lId = insertNullChemical(mDataBase);
	    	else {
	    		lCursor.moveToFirst();
	    		lId = lCursor.getLong(lCursor.getColumnIndex(KEY_ID));
	    	}
	    	
	    	newPrintValues.put(KEY_PRINT_TONERID, lId);
	    }
	    
	    return newPrintValues;
	}
	
	public long insertPrint(Print inPrint) {
		ContentValues newPrintValues = fillPrintValue(inPrint);
	    
	    Logger.v("Inserting new print with user id: " + inPrint.getUserId());
	    inPrint.setId(mDataBase.insertOrThrow(DarkroomDBHelper.PRINT_TABLE_NAME, null, newPrintValues));
	    
	    //add the collection id to the link table
	    for(Long lCollectionId : inPrint.getCollectionId())
	    	insertCollectionPrintPair(lCollectionId, inPrint.getId());
	    
	    //add the exposure to exposure table
	    for (Exposure lExposure : inPrint.getExposures())
    		insertExposure(inPrint.getId(), lExposure);
	    
	    return inPrint.getId();
	}

	public boolean removePrint(long inPrintId) {
		Logger.d("Removing print id " + inPrintId);
		return mDataBase.delete(DarkroomDBHelper.PRINT_TABLE_NAME, KEY_ID + "=" + inPrintId, null) > 0;
	}
	
	public Cursor getAllPrintsCursor() {
		return mDataBase.query(DarkroomDBHelper.PRINT_TABLE_NAME, null, null, null, null, null, null);
	}
	
	public Cursor getAllPrintsCursor(String[] inColumns) {
		return mDataBase.query(DarkroomDBHelper.PRINT_TABLE_NAME, addKeyId(inColumns), null, null, null, null, null);
	}
		
	/**
	 * 
	 * @param inColumns If has KEY_PRINT_SIZE, an alias for size_h x size_v will be done
	 * @param inFilmId If set < 0, no film alias is done
	 * @return
	 */
	public Cursor getAllPrintCursor(String[] inColumns, String inWhere, String inOrder, String [] inJoinedTable) {
		
		String[] lColumns = addKeyId(inColumns,PRINT_TABLE_NAME);
		String lQuery = SQLiteQueryBuilder.buildQueryString(false, PRINT_TABLE_NAME, lColumns, null, null, null, null, null);
				
		//Create the join statement
		if(inJoinedTable != null) {
			for(String lTable : inJoinedTable) {
				if(lTable.equals(FILM_TABLE_NAME)) {
					lQuery += " JOIN " + FILM_TABLE_NAME 
							+ " ON (" + PRINT_TABLE_NAME + "." + KEY_PRINT_FILMID + "=" + FILM_TABLE_NAME + "." + KEY_ID + ")";
				} else if(lTable.equals(PAPER_TABLE_NAME)) {
					lQuery += " JOIN " + PAPER_TABLE_NAME 
					+ " ON (" + PRINT_TABLE_NAME + "." + KEY_PRINT_PAPERID + "=" + PAPER_TABLE_NAME + "." + KEY_ID + ")";
				} else if(lTable.equals(CHEMICAL_TABLE_NAME)) {
					lQuery += " JOIN " + CHEMICAL_TABLE_NAME 
					+ " ON (" + PRINT_TABLE_NAME + "." + KEY_PRINT_DEVID + "=" + CHEMICAL_TABLE_NAME + "." + KEY_ID + ")";
				} else if(lTable.equals(COLLECTIONPRINT_TABLE_NAME)) {
					lQuery += " JOIN " + COLLECTIONPRINT_TABLE_NAME 
					+ " ON (" + PRINT_TABLE_NAME + "." + KEY_ID + "=" + COLLECTIONPRINT_TABLE_NAME + "." + KEY_COLLECTIONPRINT_PRINTID + ")";
				}
			}
		}
		
		//Add the where 
		if(inWhere != null)
			lQuery += " WHERE "+ inWhere;
		
		//Add the order by
		if(inOrder != null)
			lQuery += " ORDER BY "+	inOrder;
		
		//Query the database
		return mDataBase.rawQuery(lQuery,null);
		
	}
	/**
	 * @param inPrintId The row index to be modified
	 * @param inWhat The key to be changed
	 * @param inNewValue The new value for the given key
	 */
	public boolean updatePrintValue(long inPrintId, String inWhat, String inNewValue) {
		ContentValues newValue = new ContentValues();
		newValue.put(inWhat, inNewValue);
		
		return mDataBase.update(DarkroomDBHelper.PRINT_TABLE_NAME, newValue, KEY_ID + "=" + inPrintId, null) > 0;
		
		//TODO create update method for foreign keys and exposures
	}
	
	public boolean updatePrintValue(long inPrintId, String inWhat, long inNewValue) {
		ContentValues newValue = new ContentValues();
		newValue.put(inWhat, inNewValue);
		
		return mDataBase.update(DarkroomDBHelper.PRINT_TABLE_NAME, newValue, KEY_ID + "=" + inPrintId, null) > 0;
	}
	
	public boolean updatePrintValue(Print inPrint, boolean inInfoOnly) {
		ContentValues newPrintValues = fillPrintValue(inPrint);
	    
		boolean lSuccess = mDataBase.update(DarkroomDBHelper.PRINT_TABLE_NAME, newPrintValues, KEY_ID + "=" + inPrint.getId(), null) > 0;
		if(!lSuccess) return lSuccess;
				
		//TODO deal with collection id
		
	    if(!inInfoOnly) {
	    	for (Exposure lExposure : inPrint.getExposures()) {
	    		if( !updateExposureValue(inPrint.getId(), lExposure) )
	    			insertExposure(inPrint.getId(), lExposure);
	    	}
	    }
	    
		return lSuccess;
		
	}

	public Print getPrint(long inPrintId, boolean inInfoOnly) throws SQLException {
		Cursor lCursor = mDataBase.query(true, PRINT_TABLE_NAME, null,
                KEY_ID + "=" + inPrintId, null, null, null, null, null);
		if ((lCursor.getCount() == 0) || !lCursor.moveToFirst()) {
			throw new SQLException("No print found for row: " + inPrintId);
		}
		
		
	    long lDate = lCursor.getLong(lCursor.getColumnIndex(KEY_PRINT_DATE));
	    int lIdNumber = lCursor.getInt(lCursor.getColumnIndex(KEY_PRINT_IDNUM));
	    long lFilmId = lCursor.getLong(lCursor.getColumnIndex(KEY_PRINT_FILMID));
	    int lFilmFrameNumber = lCursor.getInt(lCursor.getColumnIndex(KEY_PRINT_FRAMEID));
	    Print lPrint = new Print(lFilmId, lFilmFrameNumber, lIdNumber, new Date(lDate));
	    
	    lPrint.setId(inPrintId);

	    lPrint.setUserId(lCursor.getString(lCursor.getColumnIndex(KEY_PRINT_USERID)), lCursor.getInt(lCursor.getColumnIndex(KEY_PRINT_USERID_ISCUSTOM)) == 1);
	    
	    lPrint.setPaper( lCursor.getLong(lCursor.getColumnIndex(KEY_PRINT_PAPERID)) );
	    //lPrint.setCollectionId(lCursor.getLong(lCursor.getColumnIndex(KEY_PRINT_COLLECTIONID)) );
	    lPrint.setCollectionId(getCollections(lPrint.getId()));
	    
	    if(!inInfoOnly)
	    	getExposures(lPrint);
	    
	    
	    lPrint.setSize(lCursor.getFloat(lCursor.getColumnIndex(KEY_PRINT_SIZE_H)), lCursor.getFloat(lCursor.getColumnIndex(KEY_PRINT_SIZE_V)));
	    lPrint.setDeveloper(lCursor.getLong(lCursor.getColumnIndex(KEY_PRINT_DEVID)));
	    lPrint.setToner(lCursor.getLong(lCursor.getColumnIndex(KEY_PRINT_TONERID)));
	    
	    lPrint.setTitle(lCursor.getString(lCursor.getColumnIndex(KEY_PRINT_TITLE)));
	    lPrint.setDescription(lCursor.getString(lCursor.getColumnIndex(KEY_PRINT_DESC)));
	    lPrint.setNotes(lCursor.getString(lCursor.getColumnIndex(KEY_PRINT_NOTES)));
		
	    lCursor.close();
		return lPrint;
	}
	
	/////////////////
	// Chemical
	/////////////////
	private ContentValues fillChemicalValue(Chemical inChemical) {
		// Create a new row of values to insert.
	    ContentValues newChemicalValues = new ContentValues();
	    // Assign values for each row.
	    newChemicalValues.put(KEY_CHEMICAL_NAME, inChemical.getName());
	    newChemicalValues.put(KEY_CHEMICAL_FORMULA, inChemical.getFormula());
	    newChemicalValues.put(KEY_CHEMICAL_CATEGORY, inChemical.getCategory());
	    return newChemicalValues;
	}
	
	public long insertChemical(Chemical inChemical) {
		ContentValues newChemicalValues = fillChemicalValue(inChemical);
	    	    
	    // Insert the row.
	    inChemical.setId(mDataBase.insertOrThrow(CHEMICAL_TABLE_NAME, null, newChemicalValues));
	    Logger.v("Inserted new chemical with id " + inChemical.getId());
	    return inChemical.getId();
	}
	
	public boolean updateChemicalValue(Chemical inChemical) {
		ContentValues newValues = fillChemicalValue(inChemical);	    
		return mDataBase.update(DarkroomDBHelper.CHEMICAL_TABLE_NAME, newValues, KEY_ID + "=" + inChemical.getId(), null) > 0;
	}
	
	protected long insertNullChemical(SQLiteDatabase inDb) {
		Logger.d("Inserting null chemical");
		ContentValues newChemicalValues = new ContentValues();
	    newChemicalValues.put(KEY_CHEMICAL_NAME, NOT_SPECIFIED);
	    newChemicalValues.put(KEY_CHEMICAL_FORMULA, "");
	    newChemicalValues.put(KEY_CHEMICAL_CATEGORY, "");
	    return inDb.insertOrThrow(CHEMICAL_TABLE_NAME, null, newChemicalValues);
	}
		
	public Cursor getAllChemicalCursor() {
		return mDataBase.query(DarkroomDBHelper.CHEMICAL_TABLE_NAME, null, null , null, null, null, null);
	}
	
	public Cursor getAllChemicalCursor(String inCategory) {
		return mDataBase.query(DarkroomDBHelper.CHEMICAL_TABLE_NAME, null, 
				KEY_CHEMICAL_CATEGORY + "=" + "\"" + inCategory + "\"" 
				+ " OR " + KEY_CHEMICAL_CATEGORY + " = " + "\"\"", null, null, null, null);
	}
	
	public Cursor getAllChemicalCursor(String [] inColumns, String inOrder, String inCategory) {
		return mDataBase.query(DarkroomDBHelper.CHEMICAL_TABLE_NAME, addKeyId(inColumns), 
				KEY_CHEMICAL_CATEGORY + "=" + "\"" + inCategory + "\"" 
				+ " OR " + KEY_CHEMICAL_CATEGORY + " = " + "\"\"", null, null, null, inOrder);
	}
	
	public Cursor getAllChemicalCursor(String [] inColumns, String inOrder) {
		return mDataBase.query(DarkroomDBHelper.CHEMICAL_TABLE_NAME, addKeyId(inColumns), null, null, null, null, inOrder);
	}
	
	public Cursor getAllChemicalCursor(String [] inColumns) {
		return mDataBase.query(DarkroomDBHelper.CHEMICAL_TABLE_NAME, addKeyId(inColumns), null, null, null, null, null);
	}

	public boolean removeChemical(long inId) {
		return mDataBase.delete(CHEMICAL_TABLE_NAME, KEY_ID + "=" + inId, null) > 0;
	}
	
	public Chemical getChemical(long inId) {
		Cursor lCursor = mDataBase.query(CHEMICAL_TABLE_NAME, null, KEY_ID + "=" + inId, null, null, null, null, null);
		if ((lCursor.getCount() == 0) || !lCursor.moveToFirst()) {
			throw new SQLException(String.format("No chemical found with id %d", inId));
		}
		
		Chemical lChemical = new Chemical(lCursor.getString(lCursor.getColumnIndex(KEY_CHEMICAL_NAME)));
		lChemical.setId(inId);
		lChemical.setFormula(lCursor.getString(lCursor.getColumnIndex(KEY_CHEMICAL_FORMULA)));
		lChemical.setCategory(lCursor.getString(lCursor.getColumnIndex(KEY_CHEMICAL_CATEGORY)));
		lCursor.close();
		return lChemical;
	}
	
	/////////////////
	// Agitation
	/////////////////
	private ContentValues fillAgitationValue(Agitation inValue) {
		ContentValues newValues = new ContentValues();
	    // Assign values for each row.
	    newValues.put(KEY_STRING, inValue.toString());
	    newValues.put(KEY_CATEGORY, ""); //TODO usefull ?
	    newValues.put(KEY_AGITATION_INITIAL, inValue.getInitial());
	    newValues.put(KEY_AGITATION_INTERVAL, inValue.getInterval());
	    newValues.put(KEY_AGITATION_DURATION, inValue.getDuration());
	    return newValues;
	}
	
	public long insertAgitation(Agitation inValue) {
		ContentValues newValues = fillAgitationValue(inValue);
	    
		inValue.setId(mDataBase.insertOrThrow(AGITATION_TABLE_NAME, null, newValues));
	    Logger.v("Inserted new aggidtation in table " + AGITATION_TABLE_NAME + " with id " + inValue.getId());
	    return inValue.getId();
	}
	
	public boolean updateAgitationValue(Agitation inValue) {
		ContentValues newValues = fillAgitationValue(inValue);
		return mDataBase.update(AGITATION_TABLE_NAME, newValues, KEY_ID + "=" + inValue.getId(), null) > 0;
	}
	
	protected long insertNullAgitation(SQLiteDatabase inDb) {
		Logger.d("Inserting null agitation");
		ContentValues newValues = new ContentValues();
		newValues.put(KEY_STRING, Agitation.NOTSPECIFIED);
		newValues.put(KEY_AGITATION_INITIAL, "0");
		newValues.put(KEY_AGITATION_INTERVAL, Agitation.NOTSPECIFIEDKEY);
		newValues.put(KEY_AGITATION_DURATION, "0");
		newValues.put(KEY_CATEGORY, "");
	    long lId = inDb.insertOrThrow(AGITATION_TABLE_NAME, null, newValues);
		
	    newValues = new ContentValues();
		newValues.put(KEY_STRING, Agitation.NOAGITATION);
		newValues.put(KEY_AGITATION_INITIAL, "0");
		newValues.put(KEY_AGITATION_INTERVAL, "-1");
		newValues.put(KEY_AGITATION_DURATION, "0");
		newValues.put(KEY_CATEGORY, "");
	    inDb.insertOrThrow(AGITATION_TABLE_NAME, null, newValues);
	    
	    newValues = new ContentValues();
		newValues.put(KEY_STRING, Agitation.CONTINUOUS);
		newValues.put(KEY_AGITATION_INITIAL, "0");
		newValues.put(KEY_AGITATION_INTERVAL, "0");
		newValues.put(KEY_AGITATION_DURATION, "0");
		newValues.put(KEY_CATEGORY, "");
	    inDb.insertOrThrow(AGITATION_TABLE_NAME, null, newValues);

	    return lId;
	}
		
	public Cursor getAllAgitationCursor() {
		return mDataBase.query(DarkroomDBHelper.AGITATION_TABLE_NAME, null, null , null, null, null, null);
	}
	
	
	public Cursor getAllAgitationCursor(String [] inColumns, String inOrder) {
		return mDataBase.query(DarkroomDBHelper.AGITATION_TABLE_NAME, addKeyId(inColumns), null, null, null, null, inOrder);
	}
	
	public Cursor getAllAgitationCursor(String [] inColumns) {
		return mDataBase.query(DarkroomDBHelper.AGITATION_TABLE_NAME, addKeyId(inColumns), null, null, null, null, null);
	}

	public boolean removeAgitation(long inId) {
		return mDataBase.delete(AGITATION_TABLE_NAME, KEY_ID + "=" + inId, null) > 0;
	}
	
	public Agitation getAgitation(long inId) {
		Cursor lCursor = mDataBase.query(AGITATION_TABLE_NAME, null, KEY_ID + "=" + inId, null, null, null, null, null);
		if ((lCursor.getCount() == 0) || !lCursor.moveToFirst()) {
			throw new SQLException(String.format("No agitation found with id %d", inId));
		}
		
		int lIni = lCursor.getInt(lCursor.getColumnIndex(KEY_AGITATION_INITIAL));
		int lInter = lCursor.getInt(lCursor.getColumnIndex(KEY_AGITATION_INTERVAL));
		int lDura = lCursor.getInt(lCursor.getColumnIndex(KEY_AGITATION_DURATION));
		
		Agitation lValue = new Agitation(lIni, lInter, lDura);
		lValue.setId(inId);
		lCursor.close();
		return lValue;
	}
	
	
	/////////////////
	// String data
	/////////////////
	
	private ContentValues fillStringData(StringData inString) {
		ContentValues newValues = new ContentValues();
	    // Assign values for each row.
	    newValues.put(KEY_STRING, inString.getString());
	    if(inString.mCategory != null)
	    	newValues.put(KEY_CATEGORY, inString.getCategory()); 
	    return newValues;
	}
	
	public long insertStringData(String inTableName, StringData inString) {
		ContentValues newValues = fillStringData(inString);
	    
	    inString.setId(mDataBase.insertOrThrow(inTableName, null, newValues));
	    Logger.v("Inserted new string data in table " + inTableName + " with id " + inString.getId());
	    return inString.getId();
	}
	
	public boolean updateStringData(String inTableName, StringData inString) {
		ContentValues newValues = fillStringData(inString);
		return mDataBase.update(inTableName, newValues, KEY_ID + "=" + inString.getId(), null) > 0;
	}
	
	protected long insertNullStringData(SQLiteDatabase inDb, String inTableName, StringData inValue) {
		Logger.d("Inserting null string data");
	    ContentValues newValues = new ContentValues();
	    newValues.put(KEY_STRING, inValue.mValue);
	    
	    if(inValue.mCategory != null)
	    	newValues.put(KEY_CATEGORY, inValue.mCategory);
	    
	    return inDb.insertOrThrow(inTableName, null, newValues);
	}
	
	public Cursor getAllStringDataCursor(String inTableName) {
		return mDataBase.query(inTableName, null, null, null, null, null, KEY_STRING);
	}
    
	public Cursor getAllStringDataCursor(String inTableName, String inCategory) {
		String where = KEY_CATEGORY + "=\"" + inCategory + "\"";
		return mDataBase.query(inTableName, null, where, null, null, null, KEY_STRING);
	}
	
	public boolean removeStringData(String inTableName, long inId) {
		return mDataBase.delete(inTableName, KEY_ID + "=" + inId, null) > 0;
	}
	
	public StringData getStringData(String inTableName, long inId) {
		Cursor lCursor = mDataBase.query(true, inTableName, null, KEY_ID + "=" + inId, null, null, null, null, null);
		if ((lCursor.getCount() == 0) || !lCursor.moveToFirst()) {
			throw new SQLException(String.format("No string data found in table %s with id %d", inTableName, inId));
		}
		
		StringData lData;
		int lCatIdx = lCursor.getColumnIndex(KEY_CATEGORY);
		if(lCatIdx > -1)
			lData = new StringData(lCursor.getString(lCursor.getColumnIndex(KEY_STRING)),lCursor.getString(lCatIdx));
		else
			lData = new StringData(lCursor.getString(lCursor.getColumnIndex(KEY_STRING)));
		lData.setId(inId);
		return lData;
	}
	
	/////////////////
	// Collection
	/////////////////
	private ContentValues fillCollectionValue(Collection inCollection) {
		// Create a new row of values to insert.
	    ContentValues newValues = new ContentValues();
	    // Assign values for each row.
	    newValues.put(KEY_COLLECTION_NAME, inCollection.getName());
	    newValues.put(KEY_COLLECTION_DATE, inCollection.getDate().getTime());
	    newValues.put(KEY_COLLECTION_DESC, inCollection.getDescription());
	    return newValues;
	}
	
	public long insertCollection(Collection inCollection) {
		ContentValues newValues = fillCollectionValue(inCollection);
	    
	    inCollection.setId(mDataBase.insertOrThrow(COLLECTION_TABLE_NAME, null, newValues));
	    Logger.v("Inserted new collection with id " + inCollection.getId());
	    return inCollection.getId();
	}
	
	public boolean updateCollectionValue(Collection inCollection) {
		ContentValues newValues = fillCollectionValue(inCollection);	    
		return mDataBase.update(DarkroomDBHelper.COLLECTION_TABLE_NAME, newValues, KEY_ID + "=" + inCollection.getId(), null) > 0;
	}
	
	public boolean updateCollectionValue(long inId, String inWhat, String inNewValue) {
		ContentValues newValue = new ContentValues();
		newValue.put(inWhat, inNewValue);
		return mDataBase.update(DarkroomDBHelper.COLLECTION_TABLE_NAME, newValue, KEY_ID + "=" + inId, null) > 0;
	}
	
	public boolean updateCollectionValue(long inId, String inWhat, long inNewValue) {
		ContentValues newValue = new ContentValues();
		newValue.put(inWhat, inNewValue);
		return mDataBase.update(DarkroomDBHelper.COLLECTION_TABLE_NAME, newValue, KEY_ID + "=" + inId, null) > 0;
	}
	
	
	
//	protected long insertNullCollection() {
//		Logger.d("Inserting null collection");
//	    ContentValues newValues = new ContentValues();
//	    newValues.put(KEY_COLLECTION_NAME, NOT_INCOLLECTION);
//	    newValues.put(KEY_COLLECTION_DATE, 0);
//	    newValues.put(KEY_COLLECTION_DESC, "");
//	    
//	    return mDataBase.insertOrThrow(COLLECTION_TABLE_NAME, null, newValues);
//	}
	
	
	public Cursor getAllCollectionCursor(String[] inColumns, String inOrder) {
		return mDataBase.query(COLLECTION_TABLE_NAME, addKeyId(inColumns), null, null, null, null, inOrder);
	}
	
	public Cursor getAllCollectionCursor(String[] inColumns) {
		return getAllCollectionCursor(inColumns, null);
	}
    
	public Cursor getAllCollectionCursor(String[] inColumns, String inWhere, String inOrder, String [] inJoinedTable) {
		
		String[] lColumns = addKeyId(inColumns,COLLECTION_TABLE_NAME);
		String lQuery = SQLiteQueryBuilder.buildQueryString(false, COLLECTION_TABLE_NAME, lColumns, null, null, null, null, null);
				
		//Create the join statement
		if(inJoinedTable != null) {
			for(String lTable : inJoinedTable) {
				if(lTable.equals(COLLECTIONPRINT_TABLE_NAME)) {
					lQuery += " JOIN " + COLLECTIONPRINT_TABLE_NAME 
							+ " ON (" + COLLECTION_TABLE_NAME + "." + KEY_ID + "=" + COLLECTIONPRINT_TABLE_NAME + "." + KEY_COLLECTIONPRINT_COLLECTIONID + ")";
				} else {
					throw new RuntimeException("Unsupported join on " + inJoinedTable);
				}
			}
		}
		
		//Add the where 
		if(inWhere != null)
			lQuery += " WHERE "+ inWhere;
		
		//Add the order by
		if(inOrder != null)
			lQuery += " ORDER BY "+	inOrder;
		
		//Query the database
		return mDataBase.rawQuery(lQuery,null);
		
	}
    

	public boolean removeCollection(long inId) {
		return mDataBase.delete(COLLECTION_TABLE_NAME, KEY_ID + "=" + inId, null) > 0;
	}
	
	public Collection getCollection(long inId) {
		Cursor lCursor = mDataBase.query(true, COLLECTION_TABLE_NAME, null, KEY_ID + "=" + inId, null, null, null, null, null);
		if ((lCursor.getCount() == 0) || !lCursor.moveToFirst()) {
			throw new SQLException(String.format("No collection found with id %d", inId));
		}
		
		Collection lCollection = new Collection(lCursor.getString(lCursor.getColumnIndex(KEY_COLLECTION_NAME)));
		lCollection.setDate(new Date(lCursor.getLong(lCursor.getColumnIndex(KEY_COLLECTION_DATE))));
		lCollection.setDescription(lCursor.getString(lCursor.getColumnIndex(KEY_COLLECTION_DESC)));
		lCollection.setId(inId);
		return lCollection;
	}
	
	public long insertCollectionPrintPair(long inCollectionId, long inPrintId) {
		Logger.d("Inserting collection-print pair");
		if(inCollectionId <= 0 || inPrintId <= 0)
			throw new RuntimeException("Id must be greater than 0");
		
		ContentValues newValues = new ContentValues();
	    newValues.put(KEY_COLLECTIONPRINT_PRINTID, inPrintId);
	    newValues.put(KEY_COLLECTIONPRINT_COLLECTIONID, inCollectionId);
	    return mDataBase.insertOrThrow(COLLECTIONPRINT_TABLE_NAME, null, newValues);
	}
	
	protected ArrayList<Long> getCollections(long inPrintId) {
		Cursor lCursor = mDataBase.query(true, COLLECTIONPRINT_TABLE_NAME, null, KEY_COLLECTIONPRINT_PRINTID + "=" + inPrintId, null, null, null, null, null);
		ArrayList<Long> lCollections = new ArrayList<Long>();
		if (lCursor.moveToFirst()) {
			do {
				lCollections.add(lCursor.getLong(lCursor.getColumnIndex(KEY_COLLECTIONPRINT_COLLECTIONID)));
			} while (lCursor.moveToNext());
		}
		return lCollections;
	}
	
	public boolean removePrintFromCollection(long inCollectionId, long inPrintId) {
		return mDataBase.delete(COLLECTIONPRINT_TABLE_NAME, 
				KEY_COLLECTIONPRINT_PRINTID + "=" + inPrintId
				+ " and " + KEY_COLLECTIONPRINT_COLLECTIONID + "=" + inCollectionId
				, null) > 0;
	}

	
	/////////////////
	// Exposure
	/////////////////
	private ContentValues fillExposureValues(long inPrintId, Exposure inExposure) {
		ContentValues newValues = new ContentValues();
		newValues.put(KEY_EXPOSURE_PRINTID, inPrintId);
		newValues.put(KEY_EXPOSURE_GRADE, inExposure.getGrade());
		newValues.put(KEY_EXPOSURE_DESC, inExposure.getDescription());
		newValues.put(KEY_EXPOSURE_NOTES, inExposure.getNotes());
		newValues.put(KEY_EXPOSURE_BASE, inExposure.getBaseExposure().toString());
		newValues.put(KEY_EXPOSURE_SKIPBASE, inExposure.isSkipBase() ? 1 : 0);
		newValues.put(KEY_EXPOSURE_ISLINEAR, inExposure.isLinear() ? 1 : 0);
		return newValues;
	}
	
	public long insertExposure(long inPrintId, Exposure inExposure) {		
		ContentValues newValues = fillExposureValues(inPrintId, inExposure);
		
		//Insert dodge exposure item
		for (ExposureItem lItem : inExposure.getDodgeExposureItems())
    		insertExposureItem(inPrintId, inExposure.getGrade(), ExposureItem.DODGEEXPOSURE, lItem);
		//Insert burn exposure item 
		for (ExposureItem lItem : inExposure.getBurnExposureItems())
    		insertExposureItem(inPrintId, inExposure.getGrade(), ExposureItem.BURNEXPOSURE, lItem);
		
		inExposure.setId(mDataBase.insertOrThrow(DarkroomDBHelper.EXPOSURE_TABLE_NAME, null, newValues));
		Logger.v("Inserted new exposure with id " + inExposure.getId());
		return inExposure.getId();
	}
	
	public boolean updateExposureValue(final long inPrintId, final Exposure inExposure) {
		ContentValues newValues = fillExposureValues(inPrintId, inExposure);
				
		boolean lSuccess = mDataBase.update(DarkroomDBHelper.EXPOSURE_TABLE_NAME, newValues, KEY_ID + "=" + inExposure.getId(), null) > 0;
		if(!lSuccess) return lSuccess;
		
		//Update all the exposure items
		final float lGrade = inExposure.getGrade();
		for(ExposureItem lItem : inExposure.getBurnExposureItems()) {
			if( !updateExposureItem(inPrintId, lGrade, ExposureItem.BURNEXPOSURE, lItem) )
				insertExposureItem(inPrintId, inExposure.getGrade(), ExposureItem.BURNEXPOSURE, lItem); //Can't update if not in the db
		}
		for(ExposureItem lItem : inExposure.getDodgeExposureItems()) {
			if( !updateExposureItem(inPrintId, lGrade, ExposureItem.DODGEEXPOSURE, lItem) )
				insertExposureItem(inPrintId, inExposure.getGrade(), ExposureItem.DODGEEXPOSURE, lItem); //Can't update if not in the db
		}
		
		return lSuccess;
	}
	
	/**
	 * 
	 * @param inId Exposure id
	 * @param inWhat The column to be modified
	 * @param inNewValue The new value
	 * @return
	 */
	public boolean updateExposureValue(long inId, String inWhat, String inNewValue) {
		ContentValues newValue = new ContentValues();
		newValue.put(inWhat, inNewValue);
		return mDataBase.update(DarkroomDBHelper.EXPOSURE_TABLE_NAME, newValue, KEY_ID + "=" + inId, null) > 0;
	}
	
	public boolean removeExposure(long inPrintId, float inGrade) {
		mDataBase.delete(DarkroomDBHelper.EXPOSUREITEM_TABLE_NAME, KEY_EXPOSUREITEM_PRINTID + "=" + inPrintId + " and " + KEY_EXPOSUREITEM_GRADE + "=" + inGrade, null);
		//Remove exposure
		return mDataBase.delete(DarkroomDBHelper.EXPOSURE_TABLE_NAME, KEY_EXPOSURE_PRINTID + "=" + inPrintId + " and " + KEY_EXPOSURE_GRADE + "=" + inGrade, null) > 0;
	}
	
	public Cursor getAllExposureCursor(long inPrintId, String[] inColumns, String inOrder) {
		Cursor lCursor = mDataBase.query(DarkroomDBHelper.EXPOSURE_TABLE_NAME, addKeyId(inColumns),
				KEY_EXPOSURE_PRINTID + "=" + inPrintId, null, null, null, inOrder, null);
		return lCursor;
	}
	
	private Exposure getExposure(Cursor inCursor) {
		final String lDescription = inCursor.getString(inCursor.getColumnIndex(KEY_EXPOSURE_DESC));
		final float lGrade = inCursor.getFloat(inCursor.getColumnIndex(KEY_EXPOSURE_GRADE));
		Exposure lExposure = new Exposure(lGrade, lDescription);
		lExposure.setId(inCursor.getLong(inCursor.getColumnIndex(KEY_ID)));
		lExposure.setNotes(inCursor.getString(inCursor.getColumnIndex(KEY_EXPOSURE_NOTES)));
		Fraction lBaseExposure = new Fraction(inCursor.getString(inCursor.getColumnIndex(KEY_EXPOSURE_BASE)));
		lExposure.setBaseExposure(lBaseExposure);
		lExposure.setSkipBase(inCursor.getInt(inCursor.getColumnIndex(KEY_EXPOSURE_SKIPBASE)) == 1);
		lExposure.setLinear(inCursor.getInt(inCursor.getColumnIndex(KEY_EXPOSURE_ISLINEAR)) == 1);
		return lExposure;
	}
	
	private void getExposures(Print inPrint) {
		Cursor lCursor = mDataBase.query(true, DarkroomDBHelper.EXPOSURE_TABLE_NAME, null,
				KEY_EXPOSURE_PRINTID + "=" + inPrint.getId(), null, null, null, null, null); 
		if ((lCursor.getCount() == 0) || !lCursor.moveToFirst()) {
			//throw new SQLException("No frame found for film : " + inFilm.getId());
			Logger.v("No exposure found for print : " + inPrint.getId());
			return;
		}
		
		if (lCursor.moveToFirst()) {
			do {
			
				Exposure lExposure = getExposure(lCursor);
				//Get burn exposures
				lExposure.setBurnExposureItems(getExposureItems(inPrint.getId(),lExposure.getGrade(),ExposureItem.BURNEXPOSURE,null));
				//Get dodge exposures
				lExposure.setDodgeExposureItems(getExposureItems(inPrint.getId(),lExposure.getGrade(),ExposureItem.DODGEEXPOSURE,null));

				inPrint.addExposure(lExposure);
			} while (lCursor.moveToNext());
		}
		lCursor.close();
	}
	
	public Exposure getExposure(long inPrintId, float inGrade) {
		Cursor lCursor = mDataBase.query(true, DarkroomDBHelper.EXPOSURE_TABLE_NAME, null,
				KEY_EXPOSURE_PRINTID + "=" + inPrintId + " and " + KEY_EXPOSURE_GRADE + "=" + inGrade, null, null, null, null, null); 
		if ((lCursor.getCount() == 0) || !lCursor.moveToFirst()) {
			Logger.v("No exposure found for print : " + inPrintId + " of grade " + inGrade);
			return null;
		}
				
		Exposure lExposure = getExposure(lCursor);
		//Get burn exposures
		lExposure.setBurnExposureItems(getExposureItems(inPrintId,lExposure.getGrade(),ExposureItem.BURNEXPOSURE,null));
		//Get dodge exposures
		lExposure.setDodgeExposureItems(getExposureItems(inPrintId,lExposure.getGrade(),ExposureItem.DODGEEXPOSURE,null));

		lCursor.close();
		return lExposure;
	}
	
	
	
	/////////////////
	// Exposure Item
	/////////////////
	private ContentValues fillExposureItemValues(long inPrintId, float inGrade, int inType,ExposureItem inItem) {
		ContentValues newItemValues = new ContentValues();
		newItemValues.put(KEY_EXPOSUREITEM_PRINTID, inPrintId);
		newItemValues.put(KEY_EXPOSUREITEM_GRADE,inGrade);
		newItemValues.put(KEY_EXPOSUREITEM_DESC,inItem.getDescription());
		newItemValues.put(KEY_EXPOSUREITEM_POSITION,inItem.getPosition());
		newItemValues.put(KEY_EXPOSUREITEM_VALUE,inItem.getValue().toStringShort());
		newItemValues.put(KEY_EXPOSUREITEM_TYPE,inType);
		return newItemValues;
	}
	
	public long insertExposureItem(long inPrintId, float inGrade, int inType, ExposureItem inItem) {
		if(inItem.getValue().equals(0))
			return -1;
		final ContentValues newItemValues = fillExposureItemValues(inPrintId, inGrade, inType, inItem);
		inItem.setId(mDataBase.insertOrThrow(DarkroomDBHelper.EXPOSUREITEM_TABLE_NAME, null, newItemValues));
		return inItem.getId();
	}
	
	public boolean updateExposureItem(long inPrintId, float inGrade, int inType, ExposureItem inItem) {
		final ContentValues newValues = fillExposureItemValues(inPrintId, inGrade, inType, inItem);
		return mDataBase.update(DarkroomDBHelper.EXPOSUREITEM_TABLE_NAME, newValues, KEY_ID + "=" + inItem.getId(), null) > 0;
	}
	
		
	public boolean updateExposureItem(long inId, Fraction inValue, String inDescription) {
		ContentValues newValue = new ContentValues();
		newValue.put(KEY_EXPOSUREITEM_VALUE, inValue.toStringShort());
		newValue.put(KEY_EXPOSUREITEM_DESC,inDescription);
		
		return mDataBase.update(DarkroomDBHelper.EXPOSUREITEM_TABLE_NAME, newValue, KEY_ID + "=" + inId, null) > 0;
	}
	
	public boolean removeExposureItem(long inPrintId, float inGrade, int inType, int inPosition) {
		//TODO can be optimized to reduce comparison
		return mDataBase.delete(DarkroomDBHelper.EXPOSUREITEM_TABLE_NAME, 
				KEY_EXPOSUREITEM_PRINTID + "=" + inPrintId
				+ " and " + KEY_EXPOSUREITEM_GRADE + "=" + inGrade
				+ " and " + KEY_EXPOSUREITEM_TYPE + "=" + inType
				+ " and " + KEY_EXPOSUREITEM_POSITION + "=" + inPosition, null) > 0;
	}
	
	public boolean removeExposureItem(long inPrintId, float inGrade, long inId) {
		ExposureItem lItem = getExposureItem(inId);
		if(mDataBase.delete(EXPOSUREITEM_TABLE_NAME, KEY_ID + "=" + inId, null) > 0) {
			//Update position of the other exposure item
			ArrayList<ExposureItem> lItems = getExposureItems(inPrintId, inGrade, lItem.getType(), KEY_EXPOSUREITEM_POSITION + ">=" + lItem.getPosition());
			for(int i = 0; i < lItems.size(); ++i) {
				//Need not to go reverse, if not SQL constraint will raise
				lItems.get(i).setPosition(lItems.get(i).getPosition()-1);
				updateExposureItem(inPrintId, inGrade, lItem.getType(), lItems.get(i));
			}
			return true;
		}
		return false; 
	}
	
	public Cursor getAllExposureItemCursor(long inPrintId, float inGrade, int inType, String[] inColumns, String inOrder) {
		Cursor lCursor = mDataBase.query(EXPOSUREITEM_TABLE_NAME, addKeyId(inColumns),
				KEY_EXPOSUREITEM_PRINTID + "=" + inPrintId
				+ " and " + KEY_EXPOSUREITEM_GRADE + "=" + inGrade
				+ " and " + KEY_EXPOSUREITEM_TYPE + "=" + inType, 
				null, null, null, inOrder, null);
		return lCursor;
	}
	
	public ExposureItem getExposureItem(long inId) {
		Cursor lCursor = mDataBase.query(true, EXPOSUREITEM_TABLE_NAME, null,
				KEY_ID + "=" + inId, null, null, null, null, null);
		
		if ((lCursor.getCount() == 0) || !lCursor.moveToFirst()) {
			Logger.v("No exposure item found with id "+ inId);
			return null;
		}
				
		final String lDescription = lCursor.getString(lCursor.getColumnIndex(KEY_EXPOSURE_DESC));
		final Fraction lValue = new Fraction(lCursor.getString(lCursor.getColumnIndex(KEY_EXPOSUREITEM_VALUE)));
		final int lPosition = lCursor.getInt(lCursor.getColumnIndex(KEY_EXPOSUREITEM_POSITION));
		final int lType = lCursor.getInt(lCursor.getColumnIndex(KEY_EXPOSUREITEM_TYPE));
		ExposureItem lExposureItem = new ExposureItem(lPosition, lType, lDescription, lValue);
		lExposureItem.setId(lCursor.getLong(lCursor.getColumnIndex(KEY_ID)));
		lExposureItem.setPosition(lCursor.getInt(lCursor.getColumnIndex(KEY_EXPOSUREITEM_POSITION)));
		lCursor.close();
		return lExposureItem;
	}
	
	public ArrayList<ExposureItem> getExposureItems(long inPrintId, float inGrade, int inType, String inCondition) {
		boolean lIsOrdered = true;;
		String lWhere = KEY_EXPOSUREITEM_PRINTID + "=" + inPrintId 
			+ " and " + KEY_EXPOSUREITEM_GRADE + "=" + inGrade
			+ " and " + KEY_EXPOSUREITEM_TYPE + "=" + inType;
		if(inCondition != null && inCondition.length() != 0) {
			lWhere += " and " + inCondition;
			lIsOrdered = false;
		}
		
		Cursor lCursor = mDataBase.query(true, DarkroomDBHelper.EXPOSUREITEM_TABLE_NAME, null,
				lWhere, null, null, null, KEY_EXPOSUREITEM_POSITION, null); 
		
		ArrayList<ExposureItem> lArray = new ArrayList<ExposureItem>();
		
		if (lCursor.moveToFirst()) {
			lArray.ensureCapacity(lCursor.getCount());
			do {
				final String lDescription = lCursor.getString(lCursor.getColumnIndex(KEY_EXPOSURE_DESC));
				final Fraction lValue = new Fraction(lCursor.getString(lCursor.getColumnIndex(KEY_EXPOSUREITEM_VALUE)));
				final int lPosition = lCursor.getInt(lCursor.getColumnIndex(KEY_EXPOSUREITEM_POSITION));
				int lType = lCursor.getInt(lCursor.getColumnIndex(KEY_EXPOSUREITEM_TYPE));
				ExposureItem lExposureItem = new ExposureItem(lPosition, lType, lDescription, lValue);
				lExposureItem.setId(lCursor.getLong(lCursor.getColumnIndex(KEY_ID)));
				if(lIsOrdered)
					lArray.add(lPosition, lExposureItem);
				else
					lArray.add(lExposureItem);
				
			} while (lCursor.moveToNext());
		}
		
		lCursor.close();
		return lArray;
	}
	
	////////////////////////////
	// Emulsion Development Time
	////////////////////////////
	private ContentValues fillEmulsionDevTimeValue(DevelopmentTime inTime) {
		ContentValues newValues = new ContentValues();
	    // Assign values for each row.
		newValues.put(KEY_DEVTIME_EMULSIONFORMAT, inTime.mFormat);
	    newValues.put(KEY_DEVTIME_EMULSIONID, inTime.mEmulsion);
	    newValues.put(KEY_DEVTIME_AGITATIONID, inTime.mAgitation);
	    newValues.put(KEY_DEVTIME_SHOTISO, inTime.mISO);
	    newValues.put(KEY_DEVTIME_DEVELOPERID, inTime.mDeveloper);
	    newValues.put(KEY_DEVTIME_TEMP, inTime.mTemp);
	    newValues.put(KEY_DEVTIME_TIME, inTime.mTime);
	    newValues.put(KEY_DEVTIME_N, inTime.mAlteration);
	    //Don't recorde Fix time
	    newValues.put(KEY_DEVTIME_SOURCE, inTime.mSource);
	    newValues.put(KEY_DEVTIME_DILUTIONID, inTime.mDilution);
	    newValues.put(KEY_DEVTIME_NOTE, inTime.mNotes);
	    
		return newValues;
	}
	
	public long insertEmulsionDevTime(DevelopmentTime inTime) {
		ContentValues newValues = fillEmulsionDevTimeValue(inTime);
		inTime.setId(mDataBase.insertOrThrow(DarkroomDBHelper.EMULSION_DEVTIME_TABLE_NAME, null, newValues));
	    Logger.v("Inserted new development time with id " + inTime.getId());
	    return inTime.getId();
	}
	
	public boolean updateEmulsionDevTimeValue(DevelopmentTime inTime)  throws SQLException {
		if(inTime.getId() == -1) {
			//Find the id of existing entry
			inTime.setId(findEmulstionDevTimeId(inTime));
		}
		
		if(inTime.getId() == -1)
			throw new SQLException("No development time found, can't update!");
		
		ContentValues newValues = fillEmulsionDevTimeValue(inTime);	    
		return mDataBase.update(DarkroomDBHelper.EMULSION_DEVTIME_TABLE_NAME, newValues, KEY_ID + "=" + inTime.getId(), null) > 0;
	}
	
	public boolean removeEmulsionDevTime(long inId) {
		return mDataBase.delete(DarkroomDBHelper.EMULSION_DEVTIME_TABLE_NAME, KEY_ID + "=" + inId, null) > 0;
	}
	
	public Cursor getAllEmulsionDevTimeCursor() {
		return mDataBase.query(DarkroomDBHelper.EMULSION_DEVTIME_TABLE_NAME, null, null, null, null, null, null);
	}

	public Cursor getAllEmulsionDevTimeCursor(String [] inColumns) {
		return mDataBase.query(DarkroomDBHelper.EMULSION_DEVTIME_TABLE_NAME, addKeyId(inColumns), null, null, null, null, null);
	}
	
	public int findEmulstionDevTimeId(DevelopmentTime inTime) {
		Cursor lCursor = getAllEmulsionDevTimeCursor(inTime,null);
		if ((lCursor.getCount() == 0) || !lCursor.moveToFirst()) {
			return -1;
		}
		assert(lCursor.getCount() == 1);
		int lId = lCursor.getInt(lCursor.getColumnIndex(KEY_ID));
		lCursor.close();
		return lId;
	}
	
	public Cursor getAllEmulsionDevTimeCursor(String inWhere, String[] inColumns, String inOrder) throws SQLException {
		Cursor lCursor = mDataBase.query(EMULSION_DEVTIME_TABLE_NAME, inColumns, inWhere, null, null, null, inOrder, null);
		if ((lCursor.getCount() == 0) || !lCursor.moveToFirst()) {
			throw new SQLException("No development time found");
		}
		return lCursor;
	}
	
	public Cursor getAllEmulsionDevTimeAllSourceCursor(DevelopmentTime lTime, String[] inColumns) throws SQLException {
		//Ignoring notes, source
		Cursor lCursor = mDataBase.query(EMULSION_DEVTIME_TABLE_NAME, inColumns,
				KEY_DEVTIME_DEVELOPERID + "=" + lTime.mDeveloper
				+ " and " + KEY_DEVTIME_EMULSIONID + "=" + lTime.mEmulsion
				+ " and " + KEY_DEVTIME_SHOTISO + "=" + lTime.mISO
				+ " and " + KEY_DEVTIME_N + "=" + lTime.mAlteration
				+ " and " + KEY_DEVTIME_TEMP + "=" + lTime.mTemp
				+ " and " + KEY_DEVTIME_DILUTIONID + "=\"" + lTime.mDilution + "\""
				+ " and " + KEY_DEVTIME_AGITATIONID + "=\"" + lTime.mAgitation + "\""
				//+ " and " + KEY_DEVTIME_SOURCE + "=\"" + lTime.mSource + "\""
				+ " and " + KEY_DEVTIME_EMULSIONFORMAT + "=\"" + lTime.mFormat + "\"",
				null, null, null, null, null);
		if ((lCursor.getCount() == 0) || !lCursor.moveToFirst()) {
			throw new SQLException("No development time found");
		}
		return lCursor;
	}
	
	public Cursor getAllEmulsionDevTimeCursor(DevelopmentTime lTime, String[] inColumns) throws SQLException {
		//Ignoring notes, source
		Cursor lCursor = mDataBase.query(EMULSION_DEVTIME_TABLE_NAME, inColumns,
				KEY_DEVTIME_DEVELOPERID + "=" + lTime.mDeveloper
				+ " and " + KEY_DEVTIME_EMULSIONID + "=" + lTime.mEmulsion
				+ " and " + KEY_DEVTIME_SHOTISO + "=" + lTime.mISO
				+ " and " + KEY_DEVTIME_N + "=" + lTime.mAlteration
				+ " and " + KEY_DEVTIME_TEMP + "=" + lTime.mTemp
				+ " and " + KEY_DEVTIME_DILUTIONID + "=\"" + lTime.mDilution + "\""
				+ " and " + KEY_DEVTIME_AGITATIONID + "=\"" + lTime.mAgitation + "\""
				+ " and " + KEY_DEVTIME_SOURCE + "=\"" + lTime.mSource + "\""
				+ " and " + KEY_DEVTIME_EMULSIONFORMAT + "=\"" + lTime.mFormat + "\"",
				null, null, null, null, null);
		if ((lCursor.getCount() == 0) || !lCursor.moveToFirst()) {
			throw new SQLException("No development time found");
		}
		return lCursor;
	}
	
	public int getEmulsionDevTime(DevelopmentTime lTime) {
		try {
			Cursor lCursor = getAllEmulsionDevTimeCursor(lTime, addKeyId(new String[]{KEY_DEVTIME_TIME}));
			assert(lCursor.getCount() == 1);
			int lId = lCursor.getInt(lCursor.getColumnIndex(KEY_DEVTIME_TIME));
			lCursor.close();
			return lId;
		} catch(SQLException e) {
			return 0;
		}
	}

	public DevelopmentTime getEmulsionDevTime(long inId) throws SQLException {
		Cursor lCursor = mDataBase.query(true, DarkroomDBHelper.EMULSION_DEVTIME_TABLE_NAME, null,
                KEY_ID + "=" + inId, null, null, null, null, null);
		if ((lCursor.getCount() == 0) || !lCursor.moveToFirst()) {
			throw new SQLException("No development time found for row: " + inId);
		}
					  
		DevelopmentTime lTime = new DevelopmentTime("manager");
		lTime.setId(inId);
		
		lTime.mEmulsion = lCursor.getInt(lCursor.getColumnIndex(KEY_DEVTIME_EMULSIONID));
		lTime.mISO = lCursor.getInt(lCursor.getColumnIndex(KEY_DEVTIME_SHOTISO));
		lTime.mDeveloper = lCursor.getInt(lCursor.getColumnIndex(KEY_DEVTIME_DEVELOPERID));
		lTime.mAlteration = lCursor.getInt(lCursor.getColumnIndex(KEY_DEVTIME_N));
		lTime.mFormat = lCursor.getString(lCursor.getColumnIndex(KEY_DEVTIME_EMULSIONFORMAT));
		lTime.mTemp = lCursor.getFloat(lCursor.getColumnIndex(KEY_DEVTIME_TEMP));
		lTime.mTime = lCursor.getInt(lCursor.getColumnIndex(KEY_DEVTIME_TIME));
		lTime.mAgitation = lCursor.getLong(lCursor.getColumnIndex(KEY_DEVTIME_AGITATIONID));
		lTime.mDilution = lCursor.getLong(lCursor.getColumnIndex(KEY_DEVTIME_DILUTIONID));
		lTime.mSource = lCursor.getString(lCursor.getColumnIndex(KEY_DEVTIME_SOURCE));
		lTime.mNotes = lCursor.getString(lCursor.getColumnIndex(KEY_DEVTIME_NOTE));
		
		lCursor.close();
		return lTime;
	}

	/////////////////
	// Emulsion
	/////////////////
	private ContentValues fillEmulsionValue(Emulsion inEmulsion) {
		ContentValues newValues = new ContentValues();
	    // Assign values for each row.
	    newValues.put(KEY_EMULSION_BRAND, inEmulsion.mBrand);
	    newValues.put(KEY_EMULSION_NAME, inEmulsion.getName());
	    newValues.put(KEY_EMULSION_ISO, inEmulsion.mISO);
	    newValues.put(KEY_EMULSION_ISCOLOR, inEmulsion.mColor);
		return newValues;
	}
	
	public long insertEmulsion(Emulsion inEmulsion) {
		ContentValues newValues = fillEmulsionValue(inEmulsion);
	    inEmulsion.setId(mDataBase.insertOrThrow(DarkroomDBHelper.EMULSION_TABLE_NAME, null, newValues));
	    Logger.v("Inserted new emulsion with id " + inEmulsion.getId());
	    return inEmulsion.getId();
	}
	
	public boolean updateEmulsionValue(Emulsion inEmulsion) {
		ContentValues newValues = fillEmulsionValue(inEmulsion);	    
		return mDataBase.update(DarkroomDBHelper.EMULSION_TABLE_NAME, newValues, KEY_ID + "=" + inEmulsion.getId(), null) > 0;
	}
	
	protected long insertNullEmulsion(SQLiteDatabase inDb) {
		Logger.d("Inserting null emulsion");
		ContentValues newValues = new ContentValues();
		newValues.put(KEY_EMULSION_NAME, NOT_SPECIFIED);
	    newValues.put(KEY_EMULSION_BRAND, "");
	    newValues.put(KEY_EMULSION_ISO, 0);
	    newValues.put(KEY_EMULSION_ISCOLOR, false);
	    return inDb.insertOrThrow(DarkroomDBHelper.EMULSION_TABLE_NAME, null, newValues);
	}
	
	
	public boolean removeEmulsion(long inId) {
		//Remove the film
		return mDataBase.delete(DarkroomDBHelper.EMULSION_TABLE_NAME, KEY_ID + "=" + inId, null) > 0;
		
	}
	
	public Cursor getAllEmulsionCursor() {
		return mDataBase.query(DarkroomDBHelper.EMULSION_TABLE_NAME, null, null, null, null, null, null);
	}

	public Cursor getAllEmulsionCursor(String [] inColumns) {
		return mDataBase.query(DarkroomDBHelper.EMULSION_TABLE_NAME, addKeyId(inColumns), null, null, null, null, null);
	}
	
	public Cursor getAllEmulsionCursor(String [] inColumns, String inOrder) {
		return mDataBase.query(DarkroomDBHelper.EMULSION_TABLE_NAME, addKeyId(inColumns), null, null, null, null, inOrder);
	}

	public Emulsion getEmulsion(long inId) throws SQLException {
		Cursor lCursor = mDataBase.query(true, DarkroomDBHelper.EMULSION_TABLE_NAME, null,
                KEY_ID + "=" + inId, null, null, null, null, null);
		if ((lCursor.getCount() == 0) || !lCursor.moveToFirst()) {
			throw new SQLException("No emulsion found for row: " + inId);
		}
		
		
		String lCodeName = lCursor.getString(lCursor.getColumnIndex(KEY_EMULSION_NAME));
			  
	    Emulsion lEmulsion = new Emulsion(lCodeName);
	    lEmulsion.setId(inId);
	    
	    lEmulsion.mBrand = lCursor.getString(lCursor.getColumnIndex(KEY_EMULSION_BRAND));
	    lEmulsion.mISO = lCursor.getInt(lCursor.getColumnIndex(KEY_EMULSION_ISO));
	    lEmulsion.mColor = lCursor.getInt(lCursor.getColumnIndex(KEY_EMULSION_ISCOLOR)) != 0;
		lCursor.close();
		return lEmulsion;
	}
	
	/////////////////
	// Film
	/////////////////
	private void fillFilmIdValue(Film inFilm, ContentValues inNewValues) {
		inNewValues.put(KEY_FILM_USERID, inFilm.getUserId());
		inNewValues.put(KEY_FILM_CAMERAID, inFilm.getCamera().getId());
		inNewValues.put(KEY_FILM_DEVELOPMENTID, inFilm.getDevelopment().getId());
		inNewValues.put(KEY_FILM_DATE, inFilm.getDate().getTime());
		inNewValues.put(KEY_FILM_DATENUM, inFilm.getIdNumber());
		inNewValues.put(KEY_FILM_USERID_ISCUSTOM, inFilm.hasCustomId() ? 1 : 0);
		return;
	}
	
	private void fillFilmInfoValue(Film.Info inInfo, ContentValues inNewValues) {
		inNewValues.put(KEY_FILM_NAME, inInfo.mName);
		inNewValues.put(KEY_FILM_DESC, inInfo.mDescription);
		inNewValues.put(KEY_FILM_SHOTISO, inInfo.mShotISO);
		//Deal with unspecified emulsion
	    if(inInfo.getEmulsion() != null) {
	    	inNewValues.put(KEY_FILM_EMULSIONID, inInfo.getEmulsion().getId());
	    }
	    else {
	    	//Find "Not Specified" developer
	    	Cursor lCursor = mDataBase.query(true, EMULSION_TABLE_NAME, null,
	    			KEY_EMULSION_NAME + " = \"" + NOT_SPECIFIED + "\"", null, null, null, null, null);
	    	long lId;
	    	if((lCursor.getCount() == 0) || !lCursor.moveToFirst())
	    		lId = insertNullEmulsion(mDataBase);
	    	else {
	    		lCursor.moveToFirst();
	    		lId = lCursor.getLong(lCursor.getColumnIndex(KEY_ID));
	    	}
	    	
	    	inNewValues.put(KEY_FILM_EMULSIONID, lId);
	    	lCursor.close();
	    }
	}
	
	
	public long insertFilm(Film inFilm) {
		// Create a new row of values to insert.
	    ContentValues newFilmValues = new ContentValues();
	    // Assign values for each row.
	    fillFilmIdValue(inFilm, newFilmValues);
	    fillFilmInfoValue(inFilm.mInfo, newFilmValues);
	    
	    for (FilmFrame lFrame : inFilm.getFrames())
	    	if(lFrame != null)
	    		insertFilmFrame(lFrame);
	    	
	    
	    inFilm.setId(mDataBase.insertOrThrow(DarkroomDBHelper.FILM_TABLE_NAME, null, newFilmValues));
	    Logger.v("Inserted new film with id " + inFilm.getId());
	    return inFilm.getId();
	}
	
	/**
	 * @param inFilmId The row index to be modified
	 * @param inWhat The key to be changed
	 * @param inNewValue The new value for the given key
	 */
	public boolean updateFilmValue(long inFilmId, String inWhat, String inNewValue) {
		ContentValues newValue = new ContentValues();
		newValue.put(inWhat, inNewValue);
		
		return mDataBase.update(DarkroomDBHelper.FILM_TABLE_NAME, newValue, KEY_ID + "=" + inFilmId, null) > 0;
		
	}
	
	public boolean updateFilmValue(long inFilmId, String inWhat, long inNewValue) {
		ContentValues newValue = new ContentValues();
		newValue.put(inWhat, inNewValue);
		
		return mDataBase.update(DarkroomDBHelper.FILM_TABLE_NAME, newValue, KEY_ID + "=" + inFilmId, null) > 0;
	}
	
	public boolean updateFilmValue(long inFilmId, String inWhat, float inNewValue) {
		ContentValues newValue = new ContentValues();
		newValue.put(inWhat, inNewValue);
		
		return mDataBase.update(DarkroomDBHelper.FILM_TABLE_NAME, newValue, KEY_ID + "=" + inFilmId, null) > 0;
	}
	
	public boolean updateFilmValue(long inFilmId, Film.Info inInfo) {
		ContentValues newFilmValues = new ContentValues();
		fillFilmInfoValue(inInfo, newFilmValues);
	    
	    return mDataBase.update(DarkroomDBHelper.FILM_TABLE_NAME, newFilmValues, KEY_ID + "=" + inFilmId, null) > 0;
	}
	
	public boolean updateFilmValue(Film inFilm, boolean inIdOnly) {
		ContentValues newFilmValues = new ContentValues();
	    // Assign values for each row.
		fillFilmIdValue(inFilm, newFilmValues);
	    
	    if(!inIdOnly) {
	    	updateFilmValue(inFilm.getId(), inFilm.mInfo);
	    	updateEmulsionDevTimeValue(inFilm.getDevelopment());
	    }
	    
//	    for (FilmFrame lFrame : inFilm)
//	    	if(lFrame != null)
//	    		updateFrameValue(inFilm.getId(), lFrame);
	    
		return mDataBase.update(DarkroomDBHelper.FILM_TABLE_NAME, newFilmValues, KEY_ID + "=" + inFilm.getId(), null) > 0;
		
	}
	
	
	public boolean removeFilm(long inFilmId) {
		Logger.d("Removing film id " + inFilmId);
		//Remove film frame associated to this film
		mDataBase.delete(DarkroomDBHelper.FILMFRAME_TABLE_NAME, KEY_FILMFRAME_FILMID + "=" + inFilmId, null);
		//Remove the film
		return mDataBase.delete(DarkroomDBHelper.FILM_TABLE_NAME, KEY_ID + "=" + inFilmId, null) > 0;
		
	}
	
	/**
	 * Get a cursor base on the for a specific camera id and date. 
	 * @param inCameraId If <= 0, all camera will be retrieved
	 * @param inDate if <= 0, all dates will be retrieved
	 * @param inColumns which columns to return
	 * @return
	 */
	public Cursor getAllFilmCursor(final long inCameraId, final long inDate, String [] inColumns) {
		String lWhere = "";
		if(inCameraId > 0 && inDate > 0) {
			lWhere = KEY_FILM_CAMERAID + "=" + inCameraId 
			         + " AND " + KEY_FILM_DATE + "=" + inDate;
		} else {
			if(inCameraId > 0)
				lWhere = KEY_FILM_CAMERAID + "=" + inCameraId;
			else if(inDate > 0)
				lWhere = KEY_FILM_DATE + "=" + inDate;
		}
		
		return mDataBase.query(DarkroomDBHelper.FILM_TABLE_NAME, addKeyId(inColumns), lWhere, null, null, null, DarkroomDBHelper.KEY_FILM_USERID);
	}

	public Cursor getAllFilmCursor() {
		return mDataBase.query(DarkroomDBHelper.FILM_TABLE_NAME, null, null, null, null, null, DarkroomDBHelper.KEY_FILM_USERID);
	}
	
	public Cursor getAllFilmCursor(String [] inColumns) {
		return mDataBase.query(DarkroomDBHelper.FILM_TABLE_NAME, addKeyId(inColumns), null, null, null, null, DarkroomDBHelper.KEY_FILM_USERID);
	}
	
	
	public Cursor getFilmCursor(long inFilmId, String[] inColumns) {
		return mDataBase.query(true, DarkroomDBHelper.FILM_TABLE_NAME, inColumns,
                KEY_ID + "=" + inFilmId, null, null, null, null, null);
	}

	public Film getFilm(long inFilmId) throws SQLException {
		Cursor lCursor = mDataBase.query(true, DarkroomDBHelper.FILM_TABLE_NAME, null,
                KEY_ID + "=" + inFilmId, null, null, null, null, null);
		if ((lCursor.getCount() == 0) || !lCursor.moveToFirst()) {
			throw new SQLException("No film found for row: " + inFilmId);
		}
		
		
		long lCameraId = lCursor.getLong(lCursor.getColumnIndex(KEY_FILM_CAMERAID));
	    long lDate = lCursor.getLong(lCursor.getColumnIndex(KEY_FILM_DATE));
	    int lDateNum= lCursor.getInt(lCursor.getColumnIndex(KEY_FILM_DATENUM));
			  
	    Camera lCamera = getCamera(lCameraId);
	    DevelopmentTime lDevelopment;
	    try{
	    	lDevelopment = this.getEmulsionDevTime(lCursor.getLong(lCursor.getColumnIndex(KEY_FILM_DEVELOPMENTID)));
		} catch(SQLException e) { //No development found, create a new one
			Logger.e("Error while getting film id " + inFilmId + ". Can't find development. ", e);
			lDevelopment = new DevelopmentTime("undefined");
		}
		
	    Film lFilm = new Film(lCamera, new Date(lDate), lDateNum, lDevelopment);
	    lFilm.setId(inFilmId);
	    lFilm.setUserId(lCursor.getString(lCursor.getColumnIndex(KEY_FILM_USERID)), lCursor.getInt((lCursor.getColumnIndex(KEY_FILM_USERID_ISCUSTOM))) == 1);
	    getFilmFrame(lFilm);
	    
	    lFilm.mInfo = lFilm.new Info();
	    Emulsion lEmulsion = getEmulsion(lCursor.getInt(lCursor.getColumnIndex(KEY_FILM_EMULSIONID)));
	    lFilm.mInfo.setEmulsion(lEmulsion);
		lFilm.mInfo.mName = lCursor.getString(lCursor.getColumnIndex(KEY_FILM_NAME));
		lFilm.mInfo.mDescription= lCursor.getString(lCursor.getColumnIndex(KEY_FILM_DESC));
		lFilm.mInfo.mShotISO = lCursor.getInt(lCursor.getColumnIndex(KEY_FILM_SHOTISO));
	    
		
		
		lCursor.close();
		return lFilm;
	}
	
	/////////////////
	// Film frame
	/////////////////
	private ContentValues fillFilmFrameValue(FilmFrame inFrame) {
		ContentValues newFrameValues = new ContentValues();
		newFrameValues.put(KEY_FILMFRAME_FILMID, inFrame.getFilmId());
		newFrameValues.put(KEY_FILMFRAME_FRAME, inFrame.getFrameNumber());
		newFrameValues.put(KEY_FILMFRAME_DESC, inFrame.getDescription());
		if( inFrame.getDate() == null) {
			Logger.w("Inserting film with null date");
			newFrameValues.put(KEY_FILMFRAME_DATE, -1);
		} else
			newFrameValues.put(KEY_FILMFRAME_DATE, inFrame.getDate().getTime());
		newFrameValues.put(KEY_FILMFRAME_PICTURE_PATH, inFrame.getImagePath());
		//newFrameValues.put(KEY_FILMFRAME_EXPOSUREID, inFrame.getExposure());
		return newFrameValues;
	}
	
	public long insertFilmFrame(FilmFrame inFrame) {
		ContentValues newFrameValues = fillFilmFrameValue(inFrame);
		inFrame.setId(mDataBase.insertOrThrow(DarkroomDBHelper.FILMFRAME_TABLE_NAME, null, newFrameValues));
		return inFrame.getId();
	}
	
	public boolean updateFilmFrameValue(FilmFrame inFrame) {
		ContentValues newValues = fillFilmFrameValue(inFrame);	    
		return mDataBase.update(DarkroomDBHelper.FILMFRAME_TABLE_NAME, newValues, KEY_ID + "=" + inFrame.getId(), null) > 0;
	}
	
//	public boolean updateFilmFrameExposureId(long inFilmFrameId, long inFilmFrameExposureId) {
//		ContentValues newValue = new ContentValues();
//		newValue.put(KEY_FILMFRAME_EXPOSUREID, inFilmFrameExposureId);
//		return mDataBase.update(DarkroomDBHelper.FILMFRAME_TABLE_NAME, newValue, KEY_ID + "=" + inFilmFrameId, null) > 0;
//	}
	
	public boolean removeFilmFrame(long inFilmId, long inFrameNumber) {
		//Delete associated exposure
		mDataBase.delete(DarkroomDBHelper.FILMFRAME_EXPOSURE_TABLE_NAME, KEY_FRAMEEXPOSURE_FILMFRAMEID + "=" + getFilmFrameExposureId(inFilmId, inFrameNumber) , null);
		return mDataBase.delete(DarkroomDBHelper.FILMFRAME_TABLE_NAME, KEY_FILMFRAME_FILMID + "=" + inFilmId + " and " + KEY_FILMFRAME_FRAME + "=" + inFrameNumber, null) > 0;
	}
	
	public boolean removeFilmFrame(long inId) {
		Logger.d("Removing film frame id " + inId);
		//Remove associated exposure
		mDataBase.delete(DarkroomDBHelper.FILMFRAME_EXPOSURE_TABLE_NAME, KEY_FRAMEEXPOSURE_FILMFRAMEID + "=" + inId, null);
		return mDataBase.delete(DarkroomDBHelper.FILMFRAME_TABLE_NAME, KEY_ID + "=" + inId, null) > 0;
		
	}
	
	public Cursor getAllFilmFrameCursor(long inFilmId, String [] inColumns) {
		return mDataBase.query(DarkroomDBHelper.FILMFRAME_TABLE_NAME, addKeyId(inColumns), KEY_FILMFRAME_FILMID + "=" + inFilmId, null, null, null, DarkroomDBHelper.KEY_FILMFRAME_FRAME);
	}
	
	public long getFilmFrameId(long inFilmId, int inFrameNumber) {
		Cursor lCursor = mDataBase.query(true, DarkroomDBHelper.FILMFRAME_TABLE_NAME, new String[]{KEY_ID},
				KEY_FILMFRAME_FILMID + "=" + inFilmId + " AND " + KEY_FILMFRAME_FRAME + "=" + inFrameNumber, null, null, null, null, null);
		if ((lCursor.getCount() == 0) || !lCursor.moveToFirst()) {
			return -1;
		}
		return lCursor.getLong(lCursor.getColumnIndex(KEY_ID));
	}
	
	public FilmFrame getFilmFrame(long inId) {
		Cursor lCursor = mDataBase.query(true, DarkroomDBHelper.FILMFRAME_TABLE_NAME, null,
                KEY_ID + "=" + inId, null, null, null, null, null);
		if ((lCursor.getCount() == 0) || !lCursor.moveToFirst()) {
			throw new SQLException("No film frame found for row: " + inId);
		}
		
		int lFrameNumber = lCursor.getInt(lCursor.getColumnIndex(KEY_FILMFRAME_FRAME));
		long lFilmId = lCursor.getLong(lCursor.getColumnIndex(KEY_FILMFRAME_FILMID));
		long lDate = lCursor.getLong(lCursor.getColumnIndex(KEY_FILMFRAME_DATE));
		FilmFrame lFrame = new FilmFrame(lFilmId, lFrameNumber);
		lFrame.setId(inId);
		lFrame.setDate(new Date(lDate));
		lFrame.setDescription(lCursor.getString(lCursor.getColumnIndex(KEY_FILMFRAME_DESC)));
		lFrame.setImagePath(lCursor.getString(lCursor.getColumnIndex(KEY_FILMFRAME_PICTURE_PATH)));
		
		lFrame.setExposure(getFilmFrameExposureId(inId));
		
		return lFrame;
	}
	
	/**
	 * Get all film frame associated to inFilm. Frame will be added to inFilm.
	 */
	private void getFilmFrame(Film inFilm) {
		Cursor lCursor = mDataBase.query(true, DarkroomDBHelper.FILMFRAME_TABLE_NAME, null,
				KEY_FILMFRAME_FILMID + "=" + inFilm.getId(), null, null, null, null, null); 
		if ((lCursor.getCount() == 0) || !lCursor.moveToFirst()) {
			//throw new SQLException("No frame found for film : " + inFilm.getId());
			Logger.v("No frame found for film : " + inFilm.getId());
			lCursor.close();
			return;
		}
		
		if (lCursor.moveToFirst()) {
			do {
				int lFrameNumber =  lCursor.getInt(lCursor.getColumnIndex(KEY_FILMFRAME_FRAME));;
				String lDescription = lCursor.getString(lCursor.getColumnIndex(KEY_FILMFRAME_DESC));
				long lFilmId = lCursor.getLong(lCursor.getColumnIndex(KEY_FILMFRAME_FILMID));
				long lDate = lCursor.getLong(lCursor.getColumnIndex(KEY_FILMFRAME_DATE));
				FilmFrame lFrame = new FilmFrame(lFilmId, lFrameNumber);
				lFrame.setId(lCursor.getLong(lCursor.getColumnIndex(KEY_ID)));
				lFrame.setDescription(lDescription);
				lFrame.setDate(new Date(lDate));
				lFrame.setImagePath(lCursor.getString(lCursor.getColumnIndex(KEY_FILMFRAME_PICTURE_PATH)));
				inFilm.addFrame(lFrame);
			} while (lCursor.moveToNext());
		}
		lCursor.close();
	}
	
	public long getFilmFrameExposureId(long inFilmId, long inFrameNumber) {
		String lQuery = "SELECT " + FILMFRAME_EXPOSURE_TABLE_NAME+"."+KEY_ID + " FROM " + FILMFRAME_TABLE_NAME;
		lQuery += " JOIN " + FILMFRAME_EXPOSURE_TABLE_NAME 
					+ " ON (" + FILMFRAME_EXPOSURE_TABLE_NAME + "." + KEY_FRAMEEXPOSURE_FILMFRAMEID + "=" + FILMFRAME_TABLE_NAME + "." + KEY_ID + ")";
		lQuery += " WHERE " + FILMFRAME_TABLE_NAME + "." +KEY_FILMFRAME_FILMID + "=" + inFilmId 
					+ " and " + FILMFRAME_TABLE_NAME + "." + KEY_FILMFRAME_FRAME + "=" + inFrameNumber;
		Cursor lCursor = mDataBase.rawQuery(lQuery, null);
		
		//TODO Verify
		
		if ((lCursor.getCount() == 0) || !lCursor.moveToFirst()) {
			Logger.v("No frame exposure found for frame " + inFrameNumber + " of film id " + inFilmId);
			lCursor.close();
			return -1;
		}
		return lCursor.getLong(lCursor.getColumnIndex(FILMFRAME_EXPOSURE_TABLE_NAME+"."+KEY_ID));
	}
	
	public long getFilmFrameExposureId(long inFilmFrameId) {
		Cursor lCursor = mDataBase.query(true, DarkroomDBHelper.FILMFRAME_EXPOSURE_TABLE_NAME, new String[]{KEY_ID},
				KEY_FRAMEEXPOSURE_FILMFRAMEID + "=" + inFilmFrameId, null, null, null, null, null); 
		if ((lCursor.getCount() == 0) || !lCursor.moveToFirst()) {
			Logger.v("No frame exposure found for film frame : " + inFilmFrameId);
			lCursor.close();
			return -1;
		}
		return lCursor.getLong(lCursor.getColumnIndex(KEY_ID));
	}
	
	public long getFilmFrameFilmId(long inFilmFrameId) {
		Cursor lCursor = mDataBase.query(true, DarkroomDBHelper.FILMFRAME_TABLE_NAME, new String[]{KEY_FILMFRAME_FILMID},
				KEY_ID + "=" + inFilmFrameId, null, null, null, null, null); 
		if ((lCursor.getCount() == 0) || !lCursor.moveToFirst()) {
			Logger.v("No film id found for film frame : " + inFilmFrameId);
			lCursor.close();
			return -1;
		}
		return lCursor.getLong(lCursor.getColumnIndex(KEY_FILMFRAME_FILMID));
	}
	
	//////////////////////////////////
	// Film Frame Exposure
	//////////////////////////////////
	
	private ContentValues fillFilmFrameExpsosurValue(FilmFrameExposure inValue) {
		ContentValues newValues = new ContentValues();
		newValues.put(KEY_FRAMEEXPOSURE_FILMFRAMEID, inValue.mFilmFrameId);
		newValues.put(KEY_FRAMEEXPOSURE_LENS, inValue.mLens);
		newValues.put(KEY_FRAMEEXPOSURE_BELLOWEXT, inValue.mBellowExtension);
		newValues.put(KEY_FRAMEEXPOSURE_BELLOWFACTOR, inValue.mBellowFactor.toStringShort());
		newValues.put(KEY_FRAMEEXPOSURE_HIGHLIGHT, inValue.mHighlight.toStringShort());
		newValues.put(KEY_FRAMEEXPOSURE_SHADOW, inValue.mShadow.toStringShort());
		newValues.put(KEY_FRAMEEXPOSURE_EXPOSURE, inValue.mExposure.toStringShort());
		newValues.put(KEY_FRAMEEXPOSURE_APERTURE, inValue.mAperture);
		newValues.put(KEY_FRAMEEXPOSURE_SHUTTER, inValue.mShutterSpeed.toStringShort());
		newValues.put(KEY_FRAMEEXPOSURE_RECIPROCITY, inValue.mReciprocity);
		newValues.put(KEY_FRAMEEXPOSURE_EXPOSURELOCK, inValue.mHasExposureLock ? 1 : 0);
		newValues.put(KEY_FRAMEEXPOSURE_NOTES, inValue.mNotes);
		
//		String lFilters = "";
//		if(inValue.mFilters.size() > 0) {
//			lFilters = String.valueOf(inValue.mFilters.get(0));
//			for(int i = 1; i < inValue.mFilters.size(); ++i) {
//				lFilters += "," + String.valueOf(inValue.mFilters.get(i));
//			}
//		}
//		newValues.put(KEY_FRAMEEXPOSURE_FILTERS, lFilters);
		
		return newValues;
	}
	
	public FilmFrameExposure getFilmFrameExposure(long inId) {
		Cursor lCursor = mDataBase.query(true, DarkroomDBHelper.FILMFRAME_EXPOSURE_TABLE_NAME, null,
				KEY_ID + "=" + inId, null, null, null, null, null);
		if ((lCursor.getCount() == 0) || !lCursor.moveToFirst()) {
			throw new SQLException("No film frame exposure found for id: " + inId);
		}
		
		FilmFrameExposure lValue = new FilmFrameExposure(lCursor.getLong(lCursor.getColumnIndex(KEY_FRAMEEXPOSURE_FILMFRAMEID)));
		
		lValue.mId = lCursor.getLong(lCursor.getColumnIndex(KEY_ID));
		lValue.mLens = lCursor.getLong(lCursor.getColumnIndex(KEY_FRAMEEXPOSURE_LENS));
		lValue.mBellowExtension = lCursor.getFloat(lCursor.getColumnIndex(KEY_FRAMEEXPOSURE_BELLOWEXT));
		lValue.mBellowFactor = new Fraction(lCursor.getString(lCursor.getColumnIndex(KEY_FRAMEEXPOSURE_BELLOWFACTOR)));
		lValue.mHighlight = new Fraction(lCursor.getString(lCursor.getColumnIndex(KEY_FRAMEEXPOSURE_HIGHLIGHT)));
		lValue.mShadow = new Fraction(lCursor.getString(lCursor.getColumnIndex(KEY_FRAMEEXPOSURE_SHADOW)));
		lValue.mExposure = new Fraction(lCursor.getString(lCursor.getColumnIndex(KEY_FRAMEEXPOSURE_EXPOSURE)));
		lValue.mAperture = lCursor.getFloat(lCursor.getColumnIndex(KEY_FRAMEEXPOSURE_APERTURE));
		lValue.mShutterSpeed = new Fraction(lCursor.getString(lCursor.getColumnIndex(KEY_FRAMEEXPOSURE_SHUTTER)));
		lValue.mReciprocity = lCursor.getFloat(lCursor.getColumnIndex(KEY_FRAMEEXPOSURE_RECIPROCITY));
		lValue.mHasExposureLock = lCursor.getInt(lCursor.getColumnIndex(KEY_FRAMEEXPOSURE_EXPOSURELOCK)) > 0 ? true : false;
		lValue.mNotes = lCursor.getString(lCursor.getColumnIndex(KEY_FRAMEEXPOSURE_NOTES));
			
//		String lFilters = lCursor.getString(lCursor.getColumnIndex(KEY_FRAMEEXPOSURE_FILTERS));
//		if(!TextUtils.isEmpty(lFilters)) {
//			for(String lFilter : lFilters.split("[,]")) {
//				lValue.mFilters.add(Long.valueOf(lFilter));
//			}
//		}
		
		Cursor lFilterCursor = this.getAllExposedFilterCursor(lValue.mId, new String[]{KEY_ID});
		if((lFilterCursor.getCount() > 0) && (lFilterCursor.moveToFirst())) {
			do {
				lValue.mFilters.add(lFilterCursor.getLong(lFilterCursor.getColumnIndex(KEY_ID)));
			} while (lCursor.moveToNext());
		}
		lFilterCursor.close();
		
		lCursor.close();
		return lValue;
	}
	
	public long insertFilmFrameExposure(FilmFrameExposure  inExposure) {
		ContentValues newValues = fillFilmFrameExpsosurValue(inExposure);
		long lId = mDataBase.insertOrThrow(DarkroomDBHelper.FILMFRAME_EXPOSURE_TABLE_NAME, null, newValues); 
		inExposure.setId(lId);
		Logger.v("Inserted new film frame exposure with id " + lId);
		//Filters need to be already inserted
		return lId;
	}
	
	public boolean updateFilmFrameExposureValue(FilmFrameExposure inExposure) {
		ContentValues newValues = fillFilmFrameExpsosurValue(inExposure);	    
		return mDataBase.update(DarkroomDBHelper.FILMFRAME_EXPOSURE_TABLE_NAME, newValues, KEY_ID + "=" + inExposure.getId(), null) > 0;
	}
	
	/////////////////
	// Paper
	/////////////////
	
	private ContentValues fillPaperValue(Paper inPaper) {
		ContentValues newValues = new ContentValues();
		newValues.put(KEY_PAPER_NAME, inPaper.mName);
		newValues.put(KEY_PAPER_DESC, inPaper.mDescription);
		newValues.put(KEY_PAPER_TYPE, inPaper.mType);
		newValues.put(KEY_PAPER_GRADE, inPaper.mGrade);
		return newValues;
	}
	
	public long insertPaper(Paper inPaper) {
		ContentValues newValues = fillPaperValue(inPaper);
		long lId = mDataBase.insertOrThrow(DarkroomDBHelper.PAPER_TABLE_NAME, null, newValues); 
		inPaper.setId(lId);
		Logger.v("Inserted new paper with id " + lId);
		return lId;
	}
	
	public boolean updatePaperValue(Paper inPaper) {
		ContentValues newValues = fillPaperValue(inPaper);	    
		return mDataBase.update(DarkroomDBHelper.PAPER_TABLE_NAME, newValues, KEY_ID + "=" + inPaper.getId(), null) > 0;
	}
	
	protected long insertNullPaper(SQLiteDatabase inDb) {
		Logger.d("Inserting null emulsion");
		ContentValues newValues = new ContentValues();
		newValues.put(KEY_PAPER_NAME, NOT_SPECIFIED);
		newValues.put(KEY_PAPER_DESC, "");
		newValues.put(KEY_PAPER_TYPE, -1);
		newValues.put(KEY_PAPER_GRADE, -1);
		return  inDb.insertOrThrow(DarkroomDBHelper.PAPER_TABLE_NAME, null, newValues); 
	}
	
	public boolean removePaper(long inPaperId) {
		return mDataBase.delete(DarkroomDBHelper.PAPER_TABLE_NAME, KEY_ID + "=" + inPaperId, null) > 0;
	}
	
	public Cursor getAllPapersCursor() {
		return mDataBase.query(DarkroomDBHelper.PAPER_TABLE_NAME, null, null, null, null, null, null);
	}
	
	public Cursor getAllPapersCursor(String[] inColumns) {
		return mDataBase.query(DarkroomDBHelper.PAPER_TABLE_NAME, addKeyId(inColumns), null, null, null, null, null);
	}
	
	public Paper getPaper(long inPaperId) throws SQLException {
		Cursor lCursor = mDataBase.query(true, DarkroomDBHelper.PAPER_TABLE_NAME, null,
				KEY_ID + "=" + inPaperId, null, null, null, null, null);
		if ((lCursor.getCount() == 0) || !lCursor.moveToFirst()) {
			throw new SQLException("No paper not found for id: " + inPaperId);
		}
		
		Paper lPaper = new Paper();
		lPaper.mName = lCursor.getString(lCursor.getColumnIndex(KEY_PAPER_NAME));
		lPaper.mDescription = lCursor.getString(lCursor.getColumnIndex(KEY_PAPER_DESC));
		lPaper.mType = lCursor.getInt(lCursor.getColumnIndex(KEY_PAPER_TYPE));
		lPaper.mGrade = lCursor.getFloat(lCursor.getColumnIndex(KEY_PAPER_GRADE));
		lCursor.close();
		return lPaper;
	}
	
	/////////////////
	// Lens
	/////////////////
	private ContentValues fillLensValue(Lens inLens) {
		ContentValues newValues = new ContentValues();
		newValues.put(KEY_LENS_NAME, inLens.getName());
		newValues.put(KEY_LENS_FOCAL, inLens.getFocalLength());
		return newValues;
	}
	
	public long insertLens(Lens inLens) {
		ContentValues newValues = fillLensValue(inLens); 
		inLens.setId(mDataBase.insertOrThrow(DarkroomDBHelper.LENS_TABLE_NAME, null, newValues));
		Logger.v("Insert new lens of name " + inLens.getName() + " and id " + inLens.getId());
		return inLens.getId();
	}
	
	protected long insertNullLens(SQLiteDatabase inDb) {
		Logger.d("Inserting null lens");
		ContentValues newValues = new ContentValues();
		newValues.put(KEY_LENS_NAME, NOT_SPECIFIED);
		newValues.put(KEY_LENS_FOCAL, -1);
		return  inDb.insertOrThrow(DarkroomDBHelper.LENS_TABLE_NAME, null, newValues); 
	}
	
	public boolean updateLensValue(Lens inLens) {
		ContentValues newValues = fillLensValue(inLens);	    
		return mDataBase.update(DarkroomDBHelper.LENS_TABLE_NAME, newValues, KEY_ID + "=" + inLens.getId(), null) > 0;
	}
	
	public Cursor getAllLensCursor() {
		return mDataBase.query(LENS_TABLE_NAME, null, null, null, null, null, KEY_LENS_FOCAL + ", " + KEY_LENS_NAME);
	}
	
	public Cursor getAllLensCursor(String[] inColumns) {
		return mDataBase.query(LENS_TABLE_NAME, addKeyId(inColumns), null, null, null, null, KEY_LENS_FOCAL + ", " + KEY_LENS_NAME);
	}
	
	public boolean removeLens(long inId) {
		return mDataBase.delete(LENS_TABLE_NAME, KEY_ID + "=" + inId, null) > 0;
	}
	
	public Lens getLens(long inId) throws SQLException {
		Cursor lCursor = mDataBase.query(true, LENS_TABLE_NAME, null,
				KEY_ID + "=" + inId , null, null, null, null, null);
		if ((lCursor.getCount() == 0) || !lCursor.moveToFirst()) {
			throw new SQLException("No lens found for id : " + inId);
		}
			
		Lens lLens = getLens(lCursor);
		lCursor.close();
		return lLens;
	}
	
	private Lens getLens(Cursor lCursor) throws SQLException {

		final String lName = lCursor.getString(lCursor.getColumnIndex(KEY_LENS_NAME));
		final int lFocal = lCursor.getInt(lCursor.getColumnIndex(KEY_LENS_FOCAL));
		Lens lLens = new Lens(lName, lFocal);
		
		return lLens;
	}
	
	/////////////////
	// Filter
	/////////////////
	private ContentValues fillFilterValue(Filter inFilter) {
		ContentValues newValues = new ContentValues();
		newValues.put(KEY_FILTER_NAME, inFilter.getName());
		newValues.put(KEY_FILTER_FACTOR, inFilter.getFactor().toString());
		if(inFilter.getFilmFrameExposureId() > 0)
			newValues.put(KEY_FILTER_FILMFRAME_EXPOSURE_ID, inFilter.getFilmFrameExposureId());
		return newValues;
	}
	
	public long insertFilter(Filter inFilter, String inTableName) {
		ContentValues newValues = fillFilterValue(inFilter); 
		inFilter.setId(mDataBase.insertOrThrow(inTableName, null, newValues));
		Logger.v("Insert new filter of name " + inFilter.getName() + " and id " + inFilter.getId() + " in " + inTableName);
		return inFilter.getId();
	}
	
//	protected long insertNullFilter(SQLiteDatabase inDb) {
//		Logger.d("Inserting null lens");
//		ContentValues newValues = new ContentValues();
//		newValues.put(KEY_FILTER_NAME, NOT_SPECIFIED);
//		newValues.put(KEY_FILTER_FACTOR, "");
//		return  inDb.insertOrThrow(DarkroomDBHelper.FILTER_TABLE_NAME, null, newValues); 
//	}
	
	public boolean updateFilterValue(Filter inFilter, String inTableName) {
		ContentValues newValues = fillFilterValue(inFilter);	    
		return mDataBase.update(inTableName, newValues, KEY_ID + "=" + inFilter.getId(), null) > 0;
	}
	
	public Cursor getAllFilterCursor(String inTableName) {
		return mDataBase.query(inTableName, null, null, null, null, null, KEY_FILTER_FACTOR + ", " + KEY_FILTER_NAME);
	}
	
//	public Cursor getAllFilterCursor(ArrayList<Long> inIds) {
//		String lSelections = "";
//		if(inIds.size() > 0) {
//			lSelections = KEY_ID + "=" + inIds.get(0);
//			for(int i = 1; i < inIds.size(); ++i) {
//				lSelections += " or " + KEY_ID + "=" + inIds.get(i);
//			}
//		} else {
//			lSelections = KEY_ID + "=-1";
//		}
//		
//		return getAllFilterCursor(lSelections);
//	}
	
	public Cursor getAllFilterCursor(String selection, String inTableName) {
		return mDataBase.query(FILTER_TABLE_NAME, null, selection, null, null, null, KEY_FILTER_FACTOR + ", " + KEY_FILTER_NAME);
	}
	
	public Cursor getAllFilterCursor(String[] inColumns, String inTableName) {
		return mDataBase.query(inTableName, addKeyId(inColumns), null, null, null, null, KEY_FILTER_FACTOR + ", " + KEY_FILTER_NAME);
	}
	
	public Cursor getAllExposedFilterCursor(long inFilmFrameExposureId, String[] inColumns) {
		return mDataBase.query(EXPOSED_FILTER_TABLE_NAME, addKeyId(inColumns), 
				KEY_FILTER_FILMFRAME_EXPOSURE_ID + "=" + inFilmFrameExposureId, 
				null, null, null, KEY_FILTER_FACTOR + ", " + KEY_FILTER_NAME);
	}
	
	public boolean removeFilter(long inId, String inTableName) {
		return mDataBase.delete(inTableName, KEY_ID + "=" + inId, null) > 0;
	}
	
	public Filter getFilter(long inId, String inTableName) throws SQLException {
		Cursor lCursor = mDataBase.query(true, inTableName, null,
				KEY_ID + "=" + inId , null, null, null, null, null);
		if ((lCursor.getCount() == 0) || !lCursor.moveToFirst()) {
			throw new SQLException("No filter found for id : " + inId);
		}
		Filter lFilter = getFilter(lCursor);
		lCursor.close();
		return lFilter;
	}
	
	private Filter getFilter(Cursor lCursor) throws SQLException {

		final String lName = lCursor.getString(lCursor.getColumnIndex(KEY_FILTER_NAME));
		final Fraction lFactor = new Fraction(lCursor.getString(lCursor.getColumnIndex(KEY_FILTER_FACTOR)));
		Filter lFilter = new Filter(lName, lFactor);
		
		return lFilter;
	}
	
	
	/////////////////
	// Camera
	/////////////////
	private ContentValues fillCameraValue(Camera inCamera) {
		ContentValues newCameraValues = new ContentValues();
		newCameraValues.put(KEY_CAMERA_BRAND, inCamera.getBrand());
		newCameraValues.put(KEY_CAMERA_MODEL, inCamera.getModel());
		newCameraValues.put(KEY_CAMERA_SHORTID, inCamera.getShortId());
		newCameraValues.put(KEY_CAMERA_YEAR, inCamera.getYear());
		newCameraValues.put(KEY_CAMERA_SERIAL, inCamera.getSerial());
		newCameraValues.put(KEY_CAMERA_SIZE, inCamera.getSize());
		return newCameraValues;
	}
	
	public long insertCamera(Camera inCamera) {
		ContentValues newCameraValues = fillCameraValue(inCamera); 
		inCamera.setId(mDataBase.insertOrThrow(DarkroomDBHelper.CAMERA_TABLE_NAME, null, newCameraValues));
		Logger.v("Insert new camera of brand " + inCamera.getBrand() + " with serial " + inCamera.getSerial() + " and id " + inCamera.getId());
		return inCamera.getId();
	}
	
	public boolean updateCameraValue(Camera inCamera) {
		ContentValues newValues = fillCameraValue(inCamera);	    
		return mDataBase.update(DarkroomDBHelper.CAMERA_TABLE_NAME, newValues, KEY_ID + "=" + inCamera.getId(), null) > 0;
	}
	
	public Cursor getAllCameraBrandModelCursor() {
		return mDataBase.rawQuery( 
				"SELECT " 
				+ KEY_CAMERA_BRAND + "||' '|| " + KEY_CAMERA_MODEL	+ " AS " + KEY_CAMERA_BRANDMODEL 
				+ ", " + KEY_ID  
				+ ", " + KEY_CAMERA_SIZE
				+ " FROM "+ CAMERA_TABLE_NAME
				+ " ORDER BY " + KEY_CAMERA_BRANDMODEL , null);
	}
	
	public Cursor getAllCameraCursor() {
		return mDataBase.query(CAMERA_TABLE_NAME, null, null, null, null, null, KEY_CAMERA_BRAND + ", " + KEY_CAMERA_MODEL);
	}
	
	public Cursor getAllCameraCursor(String[] inColumns) {
		return mDataBase.query(CAMERA_TABLE_NAME, addKeyId(inColumns), null, null, null, null, KEY_CAMERA_BRAND + ", " + KEY_CAMERA_MODEL);
	}
	
	public boolean removeCamera(long inId) {
		return mDataBase.delete(CAMERA_TABLE_NAME, KEY_ID + "=" + inId, null) > 0;
	}
	
	public Camera getCamera(String inBrand, String inSerial) throws SQLException {
		Cursor lCursor = mDataBase.query(true, CAMERA_TABLE_NAME, null,
				KEY_CAMERA_SERIAL + "=" + inSerial + " AND " + KEY_CAMERA_BRAND + "=" + inBrand, null, null, null, null, null);
		if ((lCursor.getCount() == 0) || !lCursor.moveToFirst()) {
			throw new SQLException("No camera found for serial: " + inSerial);
		}
		Camera lCamera = getCamera(lCursor);
		lCursor.close();
		return lCamera;
	}
	
	public Camera getCamera(long inId) throws SQLException {
		Cursor lCursor = mDataBase.query(true, CAMERA_TABLE_NAME, null,
				KEY_ID + "=" + inId , null, null, null, null, null);
		if ((lCursor.getCount() == 0) || !lCursor.moveToFirst()) {
			throw new SQLException("No camera found for id : " + inId);
		}
				
		return getCamera(lCursor);
	}
	
	private Camera getCamera(Cursor lCursor) throws SQLException {

		final String lBrand = lCursor.getString(lCursor.getColumnIndex(KEY_CAMERA_BRAND));
		final String lModel = lCursor.getString(lCursor.getColumnIndex(KEY_CAMERA_MODEL));
		Camera lCamera = new Camera(lBrand, lModel);
		lCamera.setId(lCursor.getLong(lCursor.getColumnIndex(KEY_ID)));
		lCamera.setSerial(lCursor.getString(lCursor.getColumnIndex(KEY_CAMERA_SERIAL)));
		lCamera.setShortId(lCursor.getString(lCursor.getColumnIndex(KEY_CAMERA_SHORTID)));
		lCamera.setYear(lCursor.getInt(lCursor.getColumnIndex(KEY_CAMERA_YEAR)));
		lCamera.setSize(lCursor.getInt(lCursor.getColumnIndex(KEY_CAMERA_SIZE)));
		
		return lCamera;
	}
	
	
	
	
	
	public boolean updateCameraValue(String inSerial, String inWhat, String inNewValue) {
		ContentValues newValue = new ContentValues();
		newValue.put(inWhat, inNewValue);
		return mDataBase.update(DarkroomDBHelper.CAMERA_TABLE_NAME, newValue, KEY_CAMERA_SERIAL + "=" + inSerial, null) > 0;
	}
	
	
	///////////
	// Frame
	///////////
	
	public boolean updateFrameValue(long inFilmId, int inFrameNumber, String inWhat, String inNewValue) {
		ContentValues newValue = new ContentValues();
		newValue.put(inWhat, inNewValue);
		return mDataBase.update(DarkroomDBHelper.FILMFRAME_TABLE_NAME, newValue, 
				KEY_FILMFRAME_FILMID + "=" + inFilmId + " and " 
				+ KEY_FILMFRAME_FRAME + "=" + inFrameNumber, 
				null) > 0;
	}
	
	public boolean updateFrameValue(long inFilmId, FilmFrame inFrame) {
		ContentValues newValue = new ContentValues();
		newValue.put(KEY_FILMFRAME_FILMID, inFilmId);
		newValue.put(KEY_FILMFRAME_FRAME, inFrame.getFrameNumber());
		newValue.put(KEY_FILMFRAME_DESC, inFrame.getDescription());
		return mDataBase.update(DarkroomDBHelper.FILMFRAME_TABLE_NAME, newValue, 
				KEY_FILMFRAME_FILMID + "=" + inFilmId + " and " 
				+ KEY_FILMFRAME_FRAME + "=" + inFrame.getFrameNumber(), 
				null) > 0;
	}
	
	
	
	
	
	
}