package com.darkroom.library.dao;

import com.darkroom.library.Camera;
import com.darkroom.library.Logger;
import com.darkroom.library.R;
import com.jfdupuis.library.screen.ScreenUtil;
import com.jfdupuis.library.string.StringUtil;

import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnKeyListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class AddNewCameraDialog extends AddNewElementDialog {
	
	Spinner mSizeSpinner = null;
	EditText mShortId = null;
	EditText mModel = null;
	
	@Override
	public void onCreate(Bundle inBundle) {
		super.onCreate(inBundle);
		setContentView(R.layout.addnew_camera_dialog);
		
		
		initialize();
		
		mSizeSpinner = (Spinner) findViewById(R.id.size_spinner);
        ArrayAdapter<CharSequence> lAdapter = ArrayAdapter.createFromResource(
        		AddNewCameraDialog.this, R.array.filmsize, android.R.layout.simple_spinner_item);
        lAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSizeSpinner.setAdapter(lAdapter);
        
        mShortId = (EditText) findViewById(R.id.short_id);
        if(isEditing())
        	mShortId.setEnabled(false);
        
        mModel = (EditText) findViewById(R.id.model);
        mModel.setOnKeyListener( new OnKeyListener() {
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if ((event.getAction() == KeyEvent.ACTION_DOWN) && ( (keyCode == KeyEvent.KEYCODE_DPAD_CENTER) || (keyCode == KeyEvent.KEYCODE_ENTER))) {
						if(!isEditing())
							updateShortId();
				    }
				return false;
			}
        	
        });
        mModel.setOnFocusChangeListener(new OnFocusChangeListener() {
			public void onFocusChange(View mArg0, boolean mArg1) {
				updateShortId();
			}
        });
        
        if(isEditing()) {
        	setTitle(R.string.edit_camera_title);
        	loadData();
        } else {
        	setTitle(R.string.addnew_camera_title);
        }
			
        
	}

	
	private void updateShortId() {
		Logger.d("Updating short id");
		if(TextUtils.isEmpty(mShortId.getText().toString())) {
			String lModelStr = mModel.getText().toString();
			mShortId.setText(StringUtil.removeSpaces(lModelStr));
		}
	}
	
	@Override
	protected void loadData() {
		Camera lCamera = mDarkroomDBAdapter.getCamera(mId);
		
		EditText lSerial = (EditText) findViewById(R.id.serial);
		lSerial.setText(lCamera.getSerial());
		EditText lBrand = (EditText) findViewById(R.id.brand);
		lBrand.setText(lCamera.getBrand());
		mModel.setText(lCamera.getModel());
		mShortId.setText(lCamera.getShortId());
		
		EditText lYear = (EditText) findViewById(R.id.year);
		if(lCamera.getYear() > 0) {
			lYear.setText(Integer.toString(lCamera.getYear()));
		}
		
		for(int i = 0; i < mSizeSpinner.getAdapter().getCount(); ++i) {
			if(mSizeSpinner.getItemAtPosition(i).equals( Integer.toString(lCamera.getSize()) )) {
				mSizeSpinner.setSelection(i);
				break;
			}
		}
	}

	public void onClick(View v) {
		//Safe the filled content in the database
		final String lSerial = ((EditText) findViewById(R.id.serial)).getText().toString();
		final String lBrand = ((EditText) findViewById(R.id.brand)).getText().toString();
		final String lModel = ((EditText) findViewById(R.id.model)).getText().toString();
		final String lShortId = mShortId.getText().toString();
		
		final String lYearStr = ((EditText) findViewById(R.id.year)).getText().toString();
		final int lYear = (TextUtils.isEmpty(lYearStr)) ? -1 : Integer.valueOf(lYearStr);
		final int lSize = Integer.valueOf( (String) mSizeSpinner.getSelectedItem() );
		
		if(TextUtils.isEmpty(lBrand)) {
			ScreenUtil.displayMessage(AddNewCameraDialog.this, "Brand can't be empty !", Toast.LENGTH_SHORT);
			return;
		}
		
		if(TextUtils.isEmpty(lModel)) {
			ScreenUtil.displayMessage(AddNewCameraDialog.this, "Model can't be empty !", Toast.LENGTH_SHORT);
			return;
		}
		
		if(TextUtils.isEmpty(lShortId)) {
			ScreenUtil.displayMessage(AddNewCameraDialog.this, "Short ID can't be empty !", Toast.LENGTH_SHORT);
			return;
		}
		
		
		Camera lCamera = new Camera(lBrand, lModel);
		lCamera.setSerial(lSerial);
		lCamera.setYear(lYear);
		lCamera.setSize(lSize);
		lCamera.setShortId(lShortId);
		
		try {
			if(isEditing()) {
				lCamera.setId(mId);
				mDarkroomDBAdapter.updateCameraValue(lCamera);
			} else {
				mDarkroomDBAdapter.insertCamera(lCamera);
			}
			setResult(RESULT_OK, new Intent().putExtra("id",lCamera.getId()));
			finish();
		} catch (SQLiteConstraintException e) {
			Logger.v(e.getMessage());
			ScreenUtil.displayMessage(AddNewCameraDialog.this, "Camera of brand " + lCamera.getBrand() + " with serial " + lCamera.getSerial() + " already exist", Toast.LENGTH_SHORT);
		}
	}


	


}