package com.darkroom.library.dao;

import com.darkroom.library.Collection;
import com.darkroom.library.R;
import com.jfdupuis.library.screen.ScreenUtil;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class AddNewCollectionDialog extends AddNewElementDialog {
	static final int DATE_DIALOG_ID = 0;
	
	EditText mCollectionName = null;
	protected TextView mDateDisplay;
    protected Button mPickDate;
    protected int mYear;
    protected int mMonth;
    protected int mDay;
    
	// the callback received when the user "sets" the date in the dialog
    protected DatePickerDialog.OnDateSetListener mDateSetListener =
            new DatePickerDialog.OnDateSetListener() {

                public void onDateSet(DatePicker view, int year, 
                                      int monthOfYear, int dayOfMonth) {
                    mYear = year;
                    mMonth = monthOfYear;
                    mDay = dayOfMonth;
                    Calendar calendar = new GregorianCalendar(mYear, mMonth, mDay);
                    mDateDisplay.setText(String.format("%tF", calendar));
                }
            };
	
	@Override
	public void onCreate(Bundle inBundle) {
		super.onCreate(inBundle);
		setContentView(R.layout.addnew_collection_dialog);
		
		initialize();
		
		mCollectionName = (EditText) findViewById(R.id.collection_name);
		mDateDisplay = (TextView) findViewById(R.id.date);
        mPickDate = (Button) findViewById(R.id.select_date_btn);
        mPickDate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                showDialog(DATE_DIALOG_ID);
            }
        });
        
		
		if(isEditing()){  
			setTitle(R.string.edit_collection_title);
			loadData();
		}
		else {
			setTitle(R.string.addnew_collection_title);
			final Calendar lCalendar = Calendar.getInstance();
			mYear = lCalendar.get(Calendar.YEAR);
	        mMonth = lCalendar.get(Calendar.MONTH);
	        mDay = lCalendar.get(Calendar.DAY_OF_MONTH);
	        mDateDisplay.setText(String.format("%tF", lCalendar));
		}
		
		
	}
	
	@Override
	protected void loadData() {
		Collection lCollection = mDarkroomDBAdapter.getCollection(mId);
		mCollectionName.setText(lCollection.getName());
		
		final Calendar lCalendar = Calendar.getInstance();
		lCalendar.setTime(lCollection.getDate());
		mYear = lCalendar.get(Calendar.YEAR);
        mMonth = lCalendar.get(Calendar.MONTH);
        mDay = lCalendar.get(Calendar.DAY_OF_MONTH);
        mDateDisplay.setText(String.format("%tF", lCalendar));
	}

	public void onClick(View v) {
		//Safe the filled content in the database
		final String lString = mCollectionName.getText().toString();
		final Calendar lCalendar = new GregorianCalendar(mYear, mMonth, mDay);
		
		if(TextUtils.isEmpty(lString)) {
			setResult(RESULT_CANCELED);
			finish();
		}
				
		try {	
			com.darkroom.library.Collection lCollection = new com.darkroom.library.Collection(lString);
			lCollection.setDate(lCalendar.getTime());
			if(isEditing()) {
				lCollection.setId(mId);
				mDarkroomDBAdapter.updateCollectionValue(lCollection);
			} else {
				mDarkroomDBAdapter.insertCollection(lCollection);
			}
			setResult(RESULT_OK, new Intent().putExtra("id",lCollection.getId()));
			finish();
		} catch (SQLiteConstraintException e) {
			ScreenUtil.displayMessage(AddNewCollectionDialog.this, "Collection " + lString + " already exist", Toast.LENGTH_SHORT);
		}
	}
	
	@Override
	protected Dialog onCreateDialog(int id) {
	    switch (id) {
	    case DATE_DIALOG_ID:
	        return new DatePickerDialog(this, mDateSetListener, mYear, mMonth, mDay);
	    }
	    return null;
	}

}
