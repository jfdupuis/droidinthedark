package com.darkroom.library.dao;

import com.darkroom.library.Agitation;
import com.darkroom.library.R;
import com.jfdupuis.library.screen.ScreenUtil;
import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class AddNewAgitationDialog extends AddNewElementDialog {
		
	@Override
	public void onCreate(Bundle inBundle) {
		super.onCreate(inBundle);
		setContentView(R.layout.addnew_agitation_dialog);
		
		initialize();
   
        if(isEditing()) {
        	setTitle(R.string.edit_agitation_title);
			loadData();
        } else {
        	setTitle(R.string.addnew_agitation_title);
        }
	}
	
	@Override
	protected void loadData() {
		Agitation lAgitation = mDarkroomDBAdapter.getAgitation(mId);
		
		EditText lEditText = (EditText) findViewById(R.id.initial);
		lEditText.setText(Integer.toString(lAgitation.getInitial()));
		
		lEditText = (EditText) findViewById(R.id.interval);
		lEditText.setText(Integer.toString(lAgitation.getInterval()));
		
		lEditText = (EditText) findViewById(R.id.duration);
		lEditText.setText(Integer.toString(lAgitation.getDuration()));
	}


	public void onClick(View v) {
		//Safe the filled content in the database
		final EditText lInterval = (EditText) findViewById(R.id.interval);
		final EditText lInitial = (EditText) findViewById(R.id.initial);
		final EditText lDuration = (EditText) findViewById(R.id.duration);

		if(TextUtils.isEmpty(lInterval.getText().toString())) {
			ScreenUtil.displayMessage(AddNewAgitationDialog.this, "Interval time can't be empty !", Toast.LENGTH_SHORT);
			return;
		}
		
		int lInitialValue;
		int lIntervalValue;
		int lDurationValue;
		try{
			String lText = lInitial.getText().toString();
			lInitialValue = Integer.valueOf(TextUtils.isEmpty(lText) ? "0" : lText );
		} catch(NumberFormatException e) {
			ScreenUtil.displayMessage(AddNewAgitationDialog.this, "Invalid initial time value !", Toast.LENGTH_SHORT);
			return;
		}
		
		try{
			lIntervalValue = Integer.valueOf(lInterval.getText().toString());
		} catch(NumberFormatException e) {
			ScreenUtil.displayMessage(AddNewAgitationDialog.this, "Invalid intervale time value !", Toast.LENGTH_SHORT);
			return;
		}
		
		try{
			String lText = lDuration.getText().toString();
			lDurationValue = Integer.valueOf(TextUtils.isEmpty(lText) ? "0" : lText );
		} catch(NumberFormatException e) {
			ScreenUtil.displayMessage(AddNewAgitationDialog.this, "Invalid duration time value !", Toast.LENGTH_SHORT);
			return;
		}
		
		Agitation lNew = new Agitation(lInitialValue, lIntervalValue, lDurationValue);
			
		try {	
			if(isEditing()) {
				lNew.setId(mId);
				mDarkroomDBAdapter.updateAgitationValue(lNew);
			} else {
				mDarkroomDBAdapter.insertAgitation(lNew);
			}
			setResult(RESULT_OK, new Intent().putExtra("id",lNew.getId()));
			finish();
		} catch (SQLiteConstraintException e) {
			ScreenUtil.displayMessage(AddNewAgitationDialog.this, "Agitation already exist", Toast.LENGTH_SHORT);
		}
	}

}
