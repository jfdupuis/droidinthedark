package com.darkroom.library.dao;

import com.darkroom.library.R;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public abstract class AddNewElementDialog extends Activity implements OnClickListener {
	protected DarkroomDBAdapter mDarkroomDBAdapter = null;
	private boolean mEditing = false;
	private Button mAddButton;
	protected long mId;
	
	@Override
	public void onCreate(Bundle inBundle) {
		super.onCreate(inBundle);
		this.setTheme(android.R.style.Theme_Dialog);
		
		mDarkroomDBAdapter = new DarkroomDBAdapter(this);
		mDarkroomDBAdapter.open();
		
		setResult(RESULT_CANCELED);
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		// Close the database
		mDarkroomDBAdapter.close();
	}
	
	protected void initialize() {
		
		//Connect buttons
        mAddButton = (Button) findViewById(R.id.add_btn);
        mAddButton.setOnClickListener((OnClickListener) this);
        
        final Button lCancelButton = (Button) findViewById(R.id.cancel_btn);
        lCancelButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	setResult(RESULT_CANCELED);
            	finish();
            }
        });
        
        mId = getIntent().getLongExtra("id", -1);
		if(mId > 0)
			setEditingMode(true);
	}
	
	abstract protected void loadData();
	abstract public void onClick(View v);
	
	protected void setEditingMode(boolean inEditing) {
		mEditing = inEditing;
		if(mEditing)
			mAddButton.setText(R.string.edit);
		else
			mAddButton.setText(R.string.add);
	}
	
	protected boolean isEditing() {
		return mEditing;
	}
	
//	abstract public void onClick(DialogInterface mArg0, int mArg1);
}
