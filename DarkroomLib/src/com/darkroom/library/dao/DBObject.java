package com.darkroom.library.dao;

import android.os.Parcel;

import java.util.ArrayList;


public class DBObject {

	protected long mId = -1;//SQL table id
	
	public void setId(long inId) {	this.mId = inId; }
	public long getId() { return mId; }
	
	protected DBObject() {

	}
	
	protected DBObject(DBObject inObject) {
		this.mId = inObject.mId;
	}
	
	static public int find(ArrayList<DBObject> inArray, DBObject inObject) {
		for(int i = 0; i < inArray.size(); ++i) {
			if(inArray.get(i).mId == inObject.mId)
				return i;
		}
		return -1;
	}
	
	public void writeToParcel(Parcel outParcel, int inFlags) {
		outParcel.writeLong(mId);
	}
	
    public void readFromParcel(Parcel inParcel) {
    	mId = inParcel.readLong();
	}
	
	
}
