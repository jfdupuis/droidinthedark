package com.darkroom.library.dao;


import com.jfdupuis.library.dao.DBHelper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase.CursorFactory;


public class DarkroomDBHelper extends DBHelper {
	public static String NOT_SPECIFIED = "Not Specified";
	public static String NOT_INCOLLECTION = "Not In Collection";
	
	public static String AGITATION_STRING_CATEGORY = "Agitation";
	public static String DILUTION_STRING_CATEGORY = "Dilution";
	
	
	/////////////////////
	public static final int DATABASE_VERSION_NUMBER = 3;
	////////////////////
	
	DarkroomDBHelper(Context inContext) {
		this(inContext, "darkroom.DataBase", null, DATABASE_VERSION_NUMBER);
	}
	
	public DarkroomDBHelper(Context inContext, String name, CursorFactory factory, int version) {
		super(inContext, name, factory, version);
		DEMO_VERSION_PACKAGE_NAME = "com.darkroom.demo.manager";
	}

	public static final String DILUTION_TABLE_NAME = "dilution";
	public static final String EMULSION_DEVTIME_TABLE_NAME = "emulsion_devtime";
	public static final String COLLECTIONPRINT_TABLE_NAME = "collection_print";
	public static final String PRINT_TABLE_NAME = "prints";
	public static final String EMULSION_TABLE_NAME = "emulsion";
	public static final String FILMFRAME_TABLE_NAME = "filmframe";
	public static final String FILMFRAME_EXPOSURE_TABLE_NAME = "frame_exposure";
	public static final String EXPOSURE_TABLE_NAME = "exposure";
	public static final String EXPOSUREITEM_TABLE_NAME = "exposure_item";
	public static final String PAPER_TABLE_NAME = "paper";
	public static final String FILM_TABLE_NAME = "film";
	public static final String CAMERA_TABLE_NAME = "camera";
	public static final String CHEMICAL_TABLE_NAME = "chemical";
	public static final String AGITATION_TABLE_NAME = "agitation";
	public static final String COLLECTION_TABLE_NAME = "collection";
	public static final String LENS_TABLE_NAME = "lens";
	public static final String FILTER_TABLE_NAME = "filter";
	public static final String EXPOSED_FILTER_TABLE_NAME = "exposed_filter";
    
	
	////////////////////////////
	// Emulsion Development Time
	////////////////////////////
	public static final String KEY_DEVTIME_USER = "user";
    public static final String KEY_DEVTIME_EMULSIONID = "emulsion"; //Emulsion
	public static final String KEY_DEVTIME_SHOTISO = "shot_iso"; //ISO used
	public static final String KEY_DEVTIME_DEVELOPERID = "developer"; //Developer
	public static final String KEY_DEVTIME_DILUTIONID = "dilution"; //Developer
	public static final String KEY_DEVTIME_N = "N"; //Development alteration N, N-1, N+1, etc
	public static final String KEY_DEVTIME_EMULSIONFORMAT = "format"; //135, 120, Sheet
	public static final String KEY_DEVTIME_TEMP = "temp"; //Development time [C]
	public static final String KEY_DEVTIME_TIME = "time"; //Development time [s]
	public static final String KEY_DEVTIME_SOURCE = "source"; //Development time source {user, massive}
	public static final String KEY_DEVTIME_AGITATIONID = "agitation"; //Agitation used
	public static final String KEY_DEVTIME_NOTE = "note"; //Agitation used
	
	
	protected static final String CREATE_DEVTIME_TABLE = "create table "
		+ EMULSION_DEVTIME_TABLE_NAME + " (" 
		+ KEY_ID + " integer primary key autoincrement, "
		+ KEY_DEVTIME_EMULSIONID + " INTEGER not null REFERENCES " + EMULSION_TABLE_NAME + "(" + KEY_ID + "), "
		+ KEY_DEVTIME_DEVELOPERID + " INTEGER not null REFERENCES " + CHEMICAL_TABLE_NAME + "(" + KEY_ID + "), "
		+ KEY_DEVTIME_AGITATIONID + " INTEGER not null REFERENCES " + AGITATION_TABLE_NAME + "(" + KEY_ID + "), "
		+ KEY_DEVTIME_DILUTIONID + " text not null, "
		+ KEY_DEVTIME_SHOTISO + " integer not null, "
		+ KEY_DEVTIME_N + " integer not null, "
		+ KEY_DEVTIME_TEMP + " real not null, "
		+ KEY_DEVTIME_EMULSIONFORMAT + " text not null, "
		+ KEY_DEVTIME_SOURCE + " text not null, "
		+ KEY_DEVTIME_TIME + " integer not null, "
		+ KEY_DEVTIME_NOTE + " string not null, "
		+ "UNIQUE (" + KEY_DEVTIME_EMULSIONID + ", " + KEY_DEVTIME_DEVELOPERID + ", " + KEY_DEVTIME_TEMP + ", "
					 + KEY_DEVTIME_DILUTIONID + ", " + KEY_DEVTIME_AGITATIONID + ", " + KEY_DEVTIME_SOURCE + ", "
					 + KEY_DEVTIME_EMULSIONFORMAT + ", " + KEY_DEVTIME_SHOTISO + ", " + KEY_DEVTIME_N  + ") "
		+ ");";
	
	protected static final String CREATE_DEVTIME_NOUNIQUE_TABLE = "create table "
		+ EMULSION_DEVTIME_TABLE_NAME + " (" 
		+ KEY_ID + " integer primary key autoincrement, "
		+ KEY_DEVTIME_EMULSIONID + " INTEGER not null REFERENCES " + EMULSION_TABLE_NAME + "(" + KEY_ID + "), "
		+ KEY_DEVTIME_DEVELOPERID + " INTEGER not null REFERENCES " + CHEMICAL_TABLE_NAME + "(" + KEY_ID + "), "
		+ KEY_DEVTIME_AGITATIONID + " INTEGER not null REFERENCES " + AGITATION_TABLE_NAME + "(" + KEY_ID + "), "
		+ KEY_DEVTIME_DILUTIONID + " INTEGER not null REFERENCES " + DILUTION_TABLE_NAME + "(" + KEY_ID + "), "
		+ KEY_DEVTIME_SHOTISO + " integer not null, "
		+ KEY_DEVTIME_N + " INTEGER not null, "
		+ KEY_DEVTIME_TEMP + " REAL not null, "
		+ KEY_DEVTIME_EMULSIONFORMAT + " text not null, "
		+ KEY_DEVTIME_SOURCE + " TEXT not null, "
		+ KEY_DEVTIME_TIME + " INTEGER not null, "
		+ KEY_DEVTIME_NOTE + " TEXT not null "
		+ ");";
	
    ////////////
	// Emulsion
	////////////
    public static final String KEY_EMULSION_NAME = "namecode"; //Short name of the film type
	public static final String KEY_EMULSION_BRAND = "brand"; //Longer film name
	public static final String KEY_EMULSION_ISO = "iso"; //Box iso
	public static final String KEY_EMULSION_ISCOLOR = "color"; //Is the film color
	
	protected static final String CREATE_EMULSION_TABLE = "create table "
		+ EMULSION_TABLE_NAME + " (" 
		+ KEY_ID + " integer primary key autoincrement, "
		+ KEY_EMULSION_NAME + " text not null, "
		+ KEY_EMULSION_BRAND + " text not null, "
		+ KEY_EMULSION_ISO + " integer not null, "
		+ KEY_EMULSION_ISCOLOR + " integer not null, "
		+ "UNIQUE (" + KEY_EMULSION_NAME + ")"
		+ ");";
	
	////////////
	// Film
	////////////
	public static final String KEY_FILM_USERID = "film_userid"; //User identifier, the film_ is added so that cursor don't get confused when joining with print table
	public static final String KEY_FILM_USERID_ISCUSTOM = "userid_iscustom"; //boolean to say if the userid generated, custom = 1
	public static final String KEY_FILM_NAME = "name"; //Short name of the film type
	public static final String KEY_FILM_DESC = "desc"; //Longer film name
	public static final String KEY_FILM_SHOTISO = "shot_iso"; //Iso shot at
	public static final String KEY_FILM_EMULSIONID = "emulsion"; //Film emulsion 
	public static final String KEY_FILM_CAMERAID = "camera"; //Camera used to shot this film
	public static final String KEY_FILM_DATE = "film_date"; //Date at which the film was finish
	public static final String KEY_FILM_DATENUM = "datenumber"; //Unique day number for this camera
	public static final String KEY_FILM_DEVELOPMENTID = "development"; //Development time used
	//TODO replace with emulsion_dev_time
//	public static final String KEY_FILM_DEVID = "developper"; //Developer used
//	public static final String KEY_FILM_DEV_NOTES = "dev_notes"; //General development notes
//	public static final String KEY_FILM_DEV_TEMP = "dev_temp"; //Developer temperature [C]
//	public static final String KEY_FILM_DEV_TIME = "dev_time"; //Development time [sec]
//	public static final String KEY_FILM_DEV_AGITATIONID = "dev_agi"; //Development agitation pattern
	/////////
	
	public static final String ALL_FILM_TABLE_KEY = KEY_ID + ", " + KEY_FILM_USERID + ", " + KEY_FILM_USERID_ISCUSTOM 
			+ ", " + KEY_FILM_NAME + ", " + KEY_FILM_DESC + ", " + KEY_FILM_EMULSIONID 
			+ ", " + KEY_FILM_SHOTISO + ", " + KEY_FILM_CAMERAID + ", " + KEY_FILM_DATE 
			+ ", " + KEY_FILM_DATENUM + ", " + KEY_FILM_DEVELOPMENTID;
	
//	protected static final String CREATE_FILM_TABLE = "create table "
//		+ FILM_TABLE_NAME + " (" 
//		+ KEY_ID + " integer primary key autoincrement, "
//		+ KEY_FILM_USERID + " text not null, "
//		+ KEY_FILM_USERID_ISCUSTOM + " integer not null, "
//		+ KEY_FILM_NAME + " text not null, "
//		+ KEY_FILM_DESC + " text not null, "
//		+ KEY_FILM_EMULSIONID + " INTEGER not null REFERENCES " + EMULSION_TABLE_NAME + "(" + KEY_ID + "), "
//		+ KEY_FILM_SHOTISO + " integer not null, "
//		+ KEY_FILM_CAMERAID + " INTEGER not null REFERENCES " + CAMERA_TABLE_NAME + "(" + KEY_ID + "), "
//		+ KEY_FILM_DATE + " integer not null, "
//		+ KEY_FILM_DATENUM + " integer not null, "
//		+ KEY_FILM_DEVID + " INTEGER not null  REFERENCES " + CHEMICAL_TABLE_NAME + "(" + KEY_ID + "), "
//		+ KEY_FILM_DEV_TEMP + " text not null, "
//		+ KEY_FILM_DEV_TIME + " text not null, "
//		+ KEY_FILM_DEV_AGITATIONID + " INTEGER not null REFERENCES " + AGITATION_TABLE_NAME + "(" + KEY_ID + "), "
//		+ KEY_FILM_DEV_NOTES + " text not null "
//		+ ", UNIQUE (" + KEY_FILM_USERID + ")"
//		+ ");";
	
	protected static final String CREATE_FILM_TABLE = "create table "
		+ FILM_TABLE_NAME + " (" 
		+ KEY_ID + " integer primary key autoincrement, "
		+ KEY_FILM_USERID + " text not null, "
		+ KEY_FILM_USERID_ISCUSTOM + " integer not null, "
		+ KEY_FILM_NAME + " text not null, "
		+ KEY_FILM_DESC + " text not null, "
		+ KEY_FILM_EMULSIONID + " INTEGER not null REFERENCES " + EMULSION_TABLE_NAME + "(" + KEY_ID + "), "
		+ KEY_FILM_SHOTISO + " integer not null, "
		+ KEY_FILM_CAMERAID + " INTEGER not null REFERENCES " + CAMERA_TABLE_NAME + "(" + KEY_ID + "), "
		+ KEY_FILM_DATE + " integer not null, "
		+ KEY_FILM_DATENUM + " integer not null, "
		+ KEY_FILM_DEVELOPMENTID + " integer not null REFERENCES " + EMULSION_DEVTIME_TABLE_NAME + "(" + KEY_ID + "), "
//		+ KEY_FILM_DEVID + " INTEGER not null  REFERENCES " + CHEMICAL_TABLE_NAME + "(" + KEY_ID + "), "
//		+ KEY_FILM_DEV_TEMP + " text not null, "
//		+ KEY_FILM_DEV_TIME + " text not null, "
//		+ KEY_FILM_DEV_AGITATIONID + " INTEGER not null REFERENCES " + AGITATION_TABLE_NAME + "(" + KEY_ID + "), "
//		+ KEY_FILM_DEV_NOTES + " text not null "
		+ " UNIQUE (" + KEY_FILM_USERID + ")"
		+ ");";
	
	//////////////
	// Film Frame
	//////////////
	public static final String KEY_FILMFRAME_FILMID = "film_id"; //Film to which this frame belong
	public static final String KEY_FILMFRAME_FRAME = "frame"; //Frame number on the film
	public static final String KEY_FILMFRAME_DESC = "desc"; //Content description
	public static final String KEY_FILMFRAME_DATE = "date"; //Data at which the frame was shot
	public static final String KEY_FILMFRAME_PICTURE_PATH = "path"; //Linked picture path
	//public static final String KEY_FILMFRAME_EXPOSUREID = "exposure_id"; //
	
	protected static final String CREATE_FILMFRAME_TABLE = "create table "
		+ FILMFRAME_TABLE_NAME + " (" 
		+ KEY_ID + " integer primary key autoincrement, " //Not used, except for the cursor
		+ KEY_FILMFRAME_FILMID + " INTEGER NOT NULL REFERENCES " + FILM_TABLE_NAME + "(" + KEY_ID + "), "
		//+ KEY_FILMFRAME_EXPOSUREID + " INTEGER NOT NULL REFERENCES " + FILMFRAME_EXPOSURE_TABLE_NAME + "(" + KEY_ID + "), "
		+ KEY_FILMFRAME_FRAME + " integer not null, "
		+ KEY_FILMFRAME_DESC + " text not null, "
		+ KEY_FILMFRAME_DATE + " integer not null, "
		+ KEY_FILMFRAME_PICTURE_PATH + " text not null, "
		+ "UNIQUE (" + KEY_FILMFRAME_FILMID + ", " + KEY_FILMFRAME_FRAME + ") "
		+ ");";
	
	////////////////////////////
	// Film Frame Exposure
	////////////////////////////
	public static final String KEY_FRAMEEXPOSURE_FILMFRAMEID = "filmframe_id"; //Frame to which this frame belong
	public static final String KEY_FRAMEEXPOSURE_LENS = "lens_id"; 
	public static final String KEY_FRAMEEXPOSURE_BELLOWEXT = "bellow_extension";
	public static final String KEY_FRAMEEXPOSURE_BELLOWFACTOR = "bellow_factor";
	public static final String KEY_FRAMEEXPOSURE_HIGHLIGHT = "hightlight";
	public static final String KEY_FRAMEEXPOSURE_SHADOW = "shadow";
	public static final String KEY_FRAMEEXPOSURE_EXPOSURE = "exposure";
	public static final String KEY_FRAMEEXPOSURE_APERTURE = "aperture";
	public static final String KEY_FRAMEEXPOSURE_SHUTTER = "shutter_speed";
	public static final String KEY_FRAMEEXPOSURE_RECIPROCITY = "reciprocity"; 
	public static final String KEY_FRAMEEXPOSURE_EXPOSURELOCK = "EV_exposed";
	public static final String KEY_FRAMEEXPOSURE_NOTES = "notes";
	
	//public static final String KEY_FRAMEEXPOSURE_FILTERS = "filters"; //comma seperated value of the filter used
	
	protected static final String CREATE_FILMFRAME_EXPOSURE_TABLE = "create table "
		+ FILMFRAME_EXPOSURE_TABLE_NAME + " (" 
		+ KEY_ID + " integer primary key autoincrement, " //Not used, except for the cursor
		+ KEY_FRAMEEXPOSURE_FILMFRAMEID + " INTEGER NOT NULL REFERENCES " + FILMFRAME_TABLE_NAME + "(" + KEY_ID + "), "
		+ KEY_FRAMEEXPOSURE_LENS + " INTEGER NOT NULL REFERENCES " + LENS_TABLE_NAME + "(" + KEY_ID + "), "
		+ KEY_FRAMEEXPOSURE_BELLOWEXT + " real not null, "
		+ KEY_FRAMEEXPOSURE_BELLOWFACTOR + " text not null, "
		+ KEY_FRAMEEXPOSURE_HIGHLIGHT + " text not null, "
		+ KEY_FRAMEEXPOSURE_SHADOW + " text not null, "
		+ KEY_FRAMEEXPOSURE_EXPOSURE + " text not null, "
		+ KEY_FRAMEEXPOSURE_APERTURE + " real not null, "
		+ KEY_FRAMEEXPOSURE_SHUTTER + " text not null, "
		+ KEY_FRAMEEXPOSURE_RECIPROCITY + " real not null, "
		+ KEY_FRAMEEXPOSURE_EXPOSURELOCK + " integer not null, "
		+ KEY_FRAMEEXPOSURE_NOTES + " text not null "
		//+ KEY_FRAMEEXPOSURE_FILTERS + " text not null "
		+ ");";
		
	
	////////////
	// Print
	////////////
	public static final String KEY_PRINT_USERID = "print_userid"; //User identifier
	public static final String KEY_PRINT_USERID_ISCUSTOM = "userid_iscustom"; //boolean to say if the userid os generated, custom = 1
	public static final String KEY_PRINT_DATE = "print_date"; //Date printed
	public static final String KEY_PRINT_IDNUM = "print_idnumber"; //print number id
	public static final String KEY_PRINT_FILMID = "film_id"; //Film printed
	public static final String KEY_PRINT_SIZE_H = "size_h"; //horizontal size
	public static final String KEY_PRINT_SIZE_V = "size_v"; //vertical size
	public static final String KEY_PRINT_FRAMEID = "frame_id"; //Film frame printed
	public static final String KEY_PRINT_TITLE = "title"; //print title
	public static final String KEY_PRINT_DESC = "desc"; //Short print description
	//public static final String KEY_PRINT_COLLECTIONID = "collection"; //book, exhibition or else in which this print belong
	public static final String KEY_PRINT_NOTES = "notes"; //General notes
	public static final String KEY_PRINT_PAPERID = "paper_id"; //Paper used
	public static final String KEY_PRINT_DEVID = "developer"; //Developer used
	public static final String KEY_PRINT_TONERID = "toner"; //Toner used
	
	//Column Aliases 
	public static final String KEY_PRINT_SIZE = "size"; //size_h x size_v
	
	protected static final String CREATE_PRINT_TABLE = "create table "
			+ PRINT_TABLE_NAME + " (" 
			+ KEY_ID + " integer primary key autoincrement, " 
			+ KEY_PRINT_USERID + " text not null, "
			+ KEY_PRINT_USERID_ISCUSTOM + " integer not null, "
			+ KEY_PRINT_SIZE_H + " real not null, "
			+ KEY_PRINT_SIZE_V + " real not null, "
			+ KEY_PRINT_DATE + " integer not null, "
			+ KEY_PRINT_IDNUM + " integer not null, "
			+ KEY_PRINT_FILMID + " integer not null, "
			+ KEY_PRINT_FRAMEID + " integer not null, "
			//+ KEY_PRINT_COLLECTIONID + " INTEGER not null REFERENCES " + COLLECTION_TABLE_NAME + "(" + KEY_ID + "), "
			+ KEY_PRINT_TITLE + " text not null, "
			+ KEY_PRINT_DESC + " text not null, "
			+ KEY_PRINT_NOTES + " text not null, "
			+ KEY_PRINT_PAPERID + " INTEGER not null REFERENCES " + PAPER_TABLE_NAME + "(" + KEY_ID + "), "
			+ KEY_PRINT_DEVID + " INTEGER not null REFERENCES " + CHEMICAL_TABLE_NAME + "(" + KEY_ID + "), "
			+ KEY_PRINT_TONERID + " INTEGER not null REFERENCES " + CHEMICAL_TABLE_NAME + "(" + KEY_ID + "), "
			+ "UNIQUE (" + KEY_PRINT_USERID + ") "
			+ "FOREIGN KEY (" + KEY_PRINT_FILMID + ", " + KEY_PRINT_FRAMEID + " ) REFERENCES " + FILMFRAME_TABLE_NAME + " (" + KEY_FILMFRAME_FILMID + ", " + KEY_FILMFRAME_FRAME + ") "
			+ ");";
	
	////////////
	// Exposure
	////////////
	public static final String KEY_EXPOSURE_PRINTID = "print_id"; // Print to which this exposure belong
	public static final String KEY_EXPOSURE_GRADE = "grade"; //Paper grade exposed , -1 = 00
	public static final String KEY_EXPOSURE_DESC = "desc"; //Short description
	public static final String KEY_EXPOSURE_NOTES = "notes"; //General text notes
	public static final String KEY_EXPOSURE_BASE = "base"; //Base exposure value
	public static final String KEY_EXPOSURE_SKIPBASE = "skipbase"; //This exposure skip the base exposure to just expose the serie of dodge and burn
	public static final String KEY_EXPOSURE_ISLINEAR = "islinear"; //Use linear timing instead of f-stops
	
	//TODO add cmy filter setting
	
	//TODO Lens aperture and focal length
	
	protected static final String CREATE_EXPOSURE_TABLE = "create table "
		+ EXPOSURE_TABLE_NAME + " (" 
		+ KEY_ID + " integer primary key autoincrement, " //Not used, except for the cursor
		+ KEY_EXPOSURE_PRINTID + " INTEGER NOT NULL REFERENCES " + PRINT_TABLE_NAME + "(" + KEY_ID + "), "
		+ KEY_EXPOSURE_GRADE + " real not null, "
		+ KEY_EXPOSURE_DESC + " text not null, "
		+ KEY_EXPOSURE_NOTES + " text not null, "
		+ KEY_EXPOSURE_BASE + " text not null, "
		+ KEY_EXPOSURE_SKIPBASE + " integer not null, "
		+ KEY_EXPOSURE_ISLINEAR + " integer not null, "
		+ "UNIQUE (" + KEY_EXPOSURE_PRINTID + ", " + KEY_EXPOSURE_GRADE + ") "
		+ ");";
	
	////////////////
	// Exposure Item
	////////////////
	public static final String KEY_EXPOSUREITEM_PRINTID = "print_id"; //Print to which this item belong, REF: Print table
	public static final String KEY_EXPOSUREITEM_GRADE = "exposure_grade"; //Exposure grade to which this item belong, REF: Exposure table
	public static final String KEY_EXPOSUREITEM_DESC = "desc"; //Description
	public static final String KEY_EXPOSUREITEM_POSITION = "position"; //position in the sequence
	public static final String KEY_EXPOSUREITEM_VALUE= "value"; //Exposure value in stops
	public static final String KEY_EXPOSUREITEM_TYPE = "type"; //Type of exposure item dodge = 0, burn = 1
	
	protected static final String CREATE_EXPOSUREITEM_TABLE = "create table "
		+ EXPOSUREITEM_TABLE_NAME + " (" 
		+ KEY_ID + " integer primary key autoincrement, " //Not used, except for the cursor
		+ KEY_EXPOSUREITEM_PRINTID + " integer not null, "
		+ KEY_EXPOSUREITEM_GRADE + " real not null, "
		+ KEY_EXPOSUREITEM_DESC + " text not null, "
		+ KEY_EXPOSUREITEM_POSITION + " integer not null, "
		+ KEY_EXPOSUREITEM_VALUE + " string not null, "
		+ KEY_EXPOSUREITEM_TYPE + " integer not null, "
		+ "UNIQUE (" + KEY_EXPOSUREITEM_PRINTID + ", " + KEY_EXPOSUREITEM_GRADE + ", " + KEY_EXPOSUREITEM_TYPE + "," + KEY_EXPOSUREITEM_POSITION + ") "
		+ "FOREIGN KEY (" + KEY_EXPOSUREITEM_PRINTID + ", " + KEY_EXPOSUREITEM_GRADE + " ) REFERENCES " + EXPOSURE_TABLE_NAME + " (" + KEY_EXPOSURE_PRINTID + ", " + KEY_EXPOSURE_GRADE + ") "
		+ ");";
	
	
	////////////
	// Paper
	////////////
	public static final String KEY_PAPER_NAME = "name"; //Short paper name
	public static final String KEY_PAPER_DESC = "desc"; //Long paper name
	public static final String KEY_PAPER_TYPE = "type"; //Short paper name, 0 = RC, 1 = FB
	public static final String KEY_PAPER_GRADE = "grade"; //Paper grade, -2 = multi, -1 = 00
	
	protected static final String CREATE_PAPER_TABLE = "create table "
		+ PAPER_TABLE_NAME + " (" 
		+ KEY_ID + " integer primary key autoincrement, "
		+ KEY_PAPER_NAME + " text not null, "
		+ KEY_PAPER_DESC + " text not null, "
		+ KEY_PAPER_TYPE + " integer not null, "
		+ KEY_PAPER_GRADE + " real not null, "
		+ "UNIQUE (" + KEY_PAPER_NAME + ")"
		+ ");";
	
	
	//////////////
	//	Lens
	//////////////
	public static final String KEY_LENS_NAME = "name"; 
	public static final String KEY_LENS_FOCAL = "focal_length"; 
	protected static final String CREATE_LENS_TABLE = "create table "
		+ LENS_TABLE_NAME + " (" 
		+ KEY_ID + " integer primary key autoincrement, " //Not used, except for the cursor
		+ KEY_LENS_NAME + " text not null, "
		+ KEY_LENS_FOCAL + " real not null, "
		+ "UNIQUE (" + KEY_LENS_NAME + ")); ";
	
	//////////////
	//	Filter
	//////////////
	public static final String KEY_FILTER_NAME = "name"; 
	public static final String KEY_FILTER_FACTOR = "factor";
	public static final String KEY_FILTER_FILMFRAME_EXPOSURE_ID = "filmframe"; 
	
	//Create the table of all filter
	protected static final String CREATE_FILTER_TABLE = "create table "
		+ FILTER_TABLE_NAME + " (" 
		+ KEY_ID + " integer primary key autoincrement, " //Not used, except for the cursor
		+ KEY_FILTER_NAME + " text not null, "
		+ KEY_FILTER_FACTOR + " text not null, "
		+ "UNIQUE (" + KEY_FILTER_NAME + ")); ";
	
	protected static final String CREATE_EXPOSED_FILTER_TABLE = "create table "
		+ EXPOSED_FILTER_TABLE_NAME + " (" 
		+ KEY_ID + " integer primary key autoincrement, " //Not used, except for the cursor
		+ KEY_FILTER_FILMFRAME_EXPOSURE_ID + " INTEGER NOT NULL REFERENCES " + FILMFRAME_EXPOSURE_TABLE_NAME + "(" + KEY_ID + "), "
		+ KEY_FILTER_NAME + " text not null, "
		+ KEY_FILTER_FACTOR + " text not null "
		+ "); ";
	
	
	
	//////////////
	//	Camera
	//////////////
	public static final String KEY_CAMERA_BRAND = "brand"; 
	public static final String KEY_CAMERA_MODEL = "model"; 
	public static final String KEY_CAMERA_YEAR = "year"; //Manufacturing year
	public static final String KEY_CAMERA_SERIAL = "serial"; //Serial number
	public static final String KEY_CAMERA_SIZE = "size"; //Film size, 135, 120, 45, 810
	public static final String KEY_CAMERA_SHORTID = "shortid"; //Camera id to be used in film id
	//Column Aliases 
	public static final String KEY_CAMERA_BRANDMODEL = "BrandModel"; //Used to construct brand model table
	
	protected static final String CREATE_CAMERA_TABLE = "create table "
		+ CAMERA_TABLE_NAME + " (" 
		+ KEY_ID + " integer primary key autoincrement, " //Not used, except for the cursor
		+ KEY_CAMERA_SHORTID + " text not null, "
		+ KEY_CAMERA_BRAND + " text not null, "
		+ KEY_CAMERA_MODEL + " text not null, "
		+ KEY_CAMERA_YEAR + " text not null, "
		+ KEY_CAMERA_SERIAL + " text not null, "
		+ KEY_CAMERA_SIZE + " integer not null, "
		+ "UNIQUE (" + KEY_CAMERA_SHORTID + "), "
		+ "UNIQUE (" + KEY_CAMERA_BRAND + ", " + KEY_CAMERA_SERIAL + "));";
	
	//////////////
	//	Chemical
	//////////////
	public static final String KEY_CHEMICAL_NAME = "name"; 
	public static final String KEY_CHEMICAL_FORMULA = "formula"; 
	public static final String KEY_CHEMICAL_CATEGORY = "category"; //toner, developer, fixer

	protected static final String CREATE_CHEMICAL_TABLE = "create table "
		+ CHEMICAL_TABLE_NAME + " (" 
		+ KEY_ID + " integer primary key autoincrement, "
		+ KEY_CHEMICAL_NAME + " text not null, "
		+ KEY_CHEMICAL_FORMULA + " text not null, "
		+ KEY_CHEMICAL_CATEGORY + " text not null, "
		+ "UNIQUE (" + KEY_CHEMICAL_NAME + ", " + KEY_CHEMICAL_CATEGORY + ")" 
		+ ");";

	
	/////////////////////
	//	StringData, aka dilution
	/////////////////////
	public static final String KEY_STRING = "text";
	public static final String KEY_CATEGORY = "category";
	
	protected static final String CREATE_DILUTION_TABLE = "create table "
		+ DILUTION_TABLE_NAME + " (" 
		+ KEY_ID + " integer primary key autoincrement, "
		+ KEY_STRING + " string not null, "
		+ "UNIQUE (" + KEY_STRING + ")"
		+ ");";
	
	/////////////////////
	//	Agitation 
	/////////////////////
	public static final String KEY_AGITATION_INITIAL = "agitation_initial"; //Initial agitation [s]
	public static final String KEY_AGITATION_INTERVAL = "agitation_interval"; //Time between agitation [s]
	public static final String KEY_AGITATION_DURATION = "agitation_duration"; //Duration of each agitation [s]
	
	protected static final String CREATE_AGITATION_TABLE = "create table "
		+ AGITATION_TABLE_NAME + " (" 
		+ KEY_ID + " integer primary key autoincrement, "
		+ KEY_STRING + " string not null, " //Backward compatibility
		+ KEY_AGITATION_INITIAL + " integer not null, "
		+ KEY_AGITATION_INTERVAL + " integer not null, "
		+ KEY_AGITATION_DURATION + " integer not null, "
		+ KEY_CATEGORY + " string not null, " //Backward compatibility
		+ "UNIQUE (" + KEY_AGITATION_INITIAL + ", " + KEY_AGITATION_INTERVAL + ", " + KEY_AGITATION_DURATION + ")"
		+ ");";
	
//	protected static final String CREATE_OLD_AGITATION_TABLE = "create table "
//		+ AGITATION_TABLE_NAME + " (" 
//		+ KEY_ID + " integer primary key autoincrement, "
//		+ KEY_STRING + " string not null, "
//		+ KEY_CATEGORY + " string not null, "
//		+ "UNIQUE (" + KEY_STRING + ", " + KEY_CATEGORY + ")"
//		+ ");";
	
	////////////
	// Collection
	////////////
	public static final String KEY_COLLECTION_NAME = "name"; //Name of the collection
	public static final String KEY_COLLECTION_DESC = "description"; //Name of the collection
	public static final String KEY_COLLECTION_DATE = "date"; //Date of the publication
	
	protected static final String CREATE_COLLECTION_TABLE = "create table "
		+ COLLECTION_TABLE_NAME + " (" 
		+ KEY_ID + " integer primary key autoincrement, "
		+ KEY_COLLECTION_NAME + " text not null, "
		+ KEY_COLLECTION_DESC + " text not null, "
		+ KEY_COLLECTION_DATE + " integer not null, "
		+ "UNIQUE (" + KEY_COLLECTION_NAME + ")"
		+ ");";
	
	public static final String KEY_COLLECTIONPRINT_PRINTID = "print";
	public static final String KEY_COLLECTIONPRINT_COLLECTIONID = "collection";
	protected static final String CREATE_COLLECTIONPRINT_TABLE = "create table "
		+ COLLECTIONPRINT_TABLE_NAME + " (" 
		+ KEY_ID + " integer primary key autoincrement, "
		+ KEY_COLLECTIONPRINT_COLLECTIONID + " INTEGER NOT NULL REFERENCES " + COLLECTION_TABLE_NAME + "(" + KEY_ID + "), "
		+ KEY_COLLECTIONPRINT_PRINTID + " INTEGER NOT NULL REFERENCES " + PRINT_TABLE_NAME + "(" + KEY_ID + "), "
		+ "UNIQUE (" + KEY_COLLECTIONPRINT_COLLECTIONID + "," + KEY_COLLECTIONPRINT_PRINTID + ")"
		+ ");";

	

//	// Called when no database exists in disk and the helper class needs
//	// to create a new one.
//	@Override
//	public void onCreate(SQLiteDatabase _db) {
////		_db.execSQL(CREATE_PRINT_TABLE);
////		_db.execSQL(CREATE_EXPOSURE_TABLE);
////		_db.execSQL(CREATE_EXPOSUREITEM_TABLE);
////		_db.execSQL(CREATE_FILM_TABLE);
////		_db.execSQL(CREATE_FILMFRAME_TABLE);
////		_db.execSQL(CREATE_CAMERA_TABLE);
////
////		_db.execSQL(CREATE_PAPER_TABLE);
////		_db.execSQL(CREATE_CHEMICAL_TABLE);
////		_db.execSQL(CREATE_AGITATION_TABLE);
////		_db.execSQL(CREATE_EMULSION_TABLE);
////		_db.execSQL(CREATE_COLLECTION_TABLE);
////		_db.execSQL(CREATE_COLLECTIONPRINT_TABLE);
////		
////		_db.execSQL(CREATE_DEVTIME_TABLE);
////		
////		_db.execSQL(CREATE_LENS_TABLE);
////		_db.execSQL(CREATE_FILTER_TABLE);
////		_db.execSQL(CREATE_DILUTION_TABLE);
//	}

	// Called when there is a database version mismatch meaning that the version
	// of the database on disk needs to be upgraded to the current version.
//	@Override
//	public void onUpgrade(SQLiteDatabase _db, int _oldVersion, int _newVersion) {
//		// Log the version upgrade.
//		Log.w("Darkroom_DAOHelper", "Upgrading from version " + _oldVersion + " to " + _newVersion);
//
//		// Upgrade the existing database to conform to the new version.
//		// Multiple previous versions can be handled by comparing _oldVersion
//		// and
//		// _newVersion values.
//		
//		//TODO Handle the change of agitation from stringdata
//		//TODO Handle film reference to devtime
//		//TODO Handle dilution of developer from developer name
//		//TODO  make better transition
//		//TODO change initially created agitation with "Agitation.NOTSPECIFIEDKEY" as interval
//		
//		if((_oldVersion == 1 || _oldVersion == 0) && _newVersion == 2) {
//			_db.execSQL(CREATE_DEVTIME_TABLE);
//			_db.execSQL(CREATE_LENS_TABLE);
//			_db.execSQL(CREATE_FILTER_TABLE);
//			return;
//		} else {
//			//Create a new one.
//			// The simplest case is to drop the old table and create a new one.
////			_db.execSQL("DROP TABLE IF EXISTS " + PRINT_TABLE_NAME);
////			_db.execSQL("DROP TABLE IF EXISTS " + EXPOSURE_TABLE_NAME);
////			_db.execSQL("DROP TABLE IF EXISTS " + EXPOSUREITEM_TABLE_NAME);
////			_db.execSQL("DROP TABLE IF EXISTS " + PAPER_TABLE_NAME);
////			_db.execSQL("DROP TABLE IF EXISTS " + FILM_TABLE_NAME);
////			_db.execSQL("DROP TABLE IF EXISTS " + FILMFRAME_TABLE_NAME);
////			_db.execSQL("DROP TABLE IF EXISTS " + CAMERA_TABLE_NAME);
////			_db.execSQL("DROP TABLE IF EXISTS " + CHEMICAL_TABLE_NAME);
////			_db.execSQL("DROP TABLE IF EXISTS " + AGITATION_TABLE_NAME);
////			_db.execSQL("DROP TABLE IF EXISTS " + EMULSION_TABLE_NAME);
////			_db.execSQL("DROP TABLE IF EXISTS " + COLLECTION_TABLE_NAME);
////			_db.execSQL("DROP TABLE IF EXISTS " + COLLECTIONPRINT_TABLE_NAME);
////			_db.execSQL("DROP TABLE IF EXISTS " + CREATE_DEVTIME_TABLE);
//			onCreate(_db);
//		}
//	}

	
}
