package com.darkroom.library.dao;

import com.darkroom.library.R;
import com.jfdupuis.library.screen.ScreenUtil;

import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

/**
 * 
 * Expect in intent: table, title, category, havecategory (boolean)
 * 
 *
 */
public class AddNewStringDataDialog extends AddNewElementDialog {
	
	EditText mInputString = null;
	EditText mCategory = null;
	String mTableName = null;
	boolean mHaveCategory = true;
	
	@Override
	public void onCreate(Bundle inBundle) {
		super.onCreate(inBundle);
		setContentView(R.layout.addnew_stringdata_dialog);
		
		setResult(RESULT_CANCELED);
		
		Intent lIntent = getIntent();
		mTableName = lIntent.getStringExtra("table");
		setTitle(lIntent.getStringExtra("title"));
		
		mHaveCategory = lIntent.getBooleanExtra("havecategory", true);
		
		initialize();
		
		mInputString = (EditText) findViewById(R.id.string);
		mCategory = (EditText) findViewById(R.id.category);
		
		if(mHaveCategory) {
			final String lCategory = lIntent.getStringExtra("category");
			mCategory.setText(lCategory);
			if(lCategory != null) {
				mCategory.setEnabled(false);
			}
		}
		else {
			mCategory.setVisibility(View.GONE);
			findViewById(R.id.category_header).setVisibility(View.GONE);
		}
		
		if(isEditing()) {
			setTitle("Edit string");
			loadData();
		} else {
			setTitle("Add new string");
		}
	}

	protected void loadData() {
		StringData lStringData = mDarkroomDBAdapter.getStringData(mTableName, mId);
		mInputString.setText(lStringData.getString());
		if(lStringData.getCategory() != null)
			mCategory.setText(lStringData.getCategory());
		else 
			mCategory.setText("");
	}

	public void onClick(View v) {
		//Safe the filled content in the database
		final String lString = mInputString.getText().toString();
		String lCategory = mCategory.getText().toString();
		
		
		if(lString.length() == 0) {
			setResult(RESULT_CANCELED);
			finish();
		}
		
		if(mHaveCategory) {
			if(lCategory.length() == 0) {
				ScreenUtil.displayMessage(AddNewStringDataDialog.this, "A category is needed", Toast.LENGTH_SHORT);
			}
		} else {
			lCategory = null;
		}
				
		try {		
			if(isEditing()) {
				StringData lStringData = new StringData(lString, lCategory);
				lStringData.setId(mId);
				mDarkroomDBAdapter.updateStringData(mTableName, lStringData);
			} else {
				mId = mDarkroomDBAdapter.insertStringData(mTableName, new StringData(lString, lCategory) );
			}
			setResult(RESULT_OK, new Intent().putExtra("id",mId));
			finish();
		} catch (SQLiteConstraintException e) {
			ScreenUtil.displayMessage(AddNewStringDataDialog.this, "String " + lString + " already exist", Toast.LENGTH_SHORT);
		}
	}


}