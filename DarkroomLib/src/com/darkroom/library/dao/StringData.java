package com.darkroom.library.dao;

public class StringData extends DBObject {
	
	String mValue = "";
	String mCategory = null;
	
	public StringData(String inValue) {
		mValue = inValue;
		mCategory = null;
	}
	
	public StringData(String inValue, String inCategory) {
		mValue = inValue;
		mCategory = inCategory;
	}
	
	public void setString(String inValue) {
		this.mValue = inValue;
	}
	public String getString() {
		return mValue;
	}
	
	public void setCategory(String inValue) {
		this.mCategory = inValue;
	}
	public String getCategory() {
		return mCategory;
	}
}
