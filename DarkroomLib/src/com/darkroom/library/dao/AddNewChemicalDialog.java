package com.darkroom.library.dao;

import com.darkroom.library.Chemical;
import com.darkroom.library.Logger;
import com.darkroom.library.R;
import com.jfdupuis.library.screen.ScreenUtil;

import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class AddNewChemicalDialog extends AddNewElementDialog {
	
	Spinner mSpinner;	
	
	@Override
	public void onCreate(Bundle inBundle) {
		super.onCreate(inBundle);
		setContentView(R.layout.addnew_chemical_dialog);
		
		
		initialize();
    	
    	mSpinner = (Spinner) findViewById(R.id.chemical_category_spinner);
        ArrayAdapter<CharSequence> lAdapter = ArrayAdapter.createFromResource(
        		AddNewChemicalDialog.this, R.array.chemical_category, android.R.layout.simple_spinner_item);
        lAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(lAdapter);
        
        //Set the selection to the category sent by the intent
        Intent lIntent = getIntent();
        final String lCategory = lIntent.getStringExtra("category");
        
        int lPos = lAdapter.getPosition(lCategory);
        Logger.v("Receive chemical category " + lCategory + ", which is in position " + lPos);
        mSpinner.setSelection(lPos);
        
        if(lCategory != null) {
        	boolean lHideCategory = lIntent.getBooleanExtra("hidecategory", false);
        	if( lHideCategory) {
        		mSpinner.setVisibility(View.GONE);
        		findViewById(R.id.chemical_category_text).setVisibility(View.GONE);
        	}
        }
        
        
        
        if(isEditing()) {
        	setTitle(R.string.edit_chemical_title);
        	loadData();
        } else {
        	setTitle(R.string.addnew_chemical_title);
        }
	}
	
	@Override
	protected void loadData() {
		Chemical lChemical = mDarkroomDBAdapter.getChemical(mId);
		
		EditText lChemicalNameText = (EditText) findViewById(R.id.chemical_name);
		lChemicalNameText.setText(lChemical.getName());
		EditText lChemicalFormulaText = (EditText) findViewById(R.id.chemical_formula);
		lChemicalFormulaText.setText(lChemical.getFormula());
		
		lChemical.getCategory();
		@SuppressWarnings("unchecked")
		final int lPosition = ((ArrayAdapter<CharSequence>)(mSpinner.getAdapter())).getPosition(lChemical.getCategory());
		mSpinner.setSelection(lPosition);
		
	}

	public void onClick(View v) {
		//Safe the filled content in the database
		final String lChemicalNameText = ((EditText) findViewById(R.id.chemical_name)).getText().toString();
		if(TextUtils.isEmpty(lChemicalNameText)) {
			ScreenUtil.displayMessage(AddNewChemicalDialog.this, "Chemical name can't be empty !", Toast.LENGTH_SHORT);
			return;
		}
			
		Chemical lChemical = new Chemical(lChemicalNameText);
		final EditText lChemicalFormulaText = (EditText) findViewById(R.id.chemical_formula);
		lChemical.setFormula(lChemicalFormulaText.getText().toString());
		lChemical.setCategory((String) mSpinner.getSelectedItem());
		try{
			if(isEditing()) {
				lChemical.setId(mId);
				mDarkroomDBAdapter.updateChemicalValue(lChemical);
			} else {
				mDarkroomDBAdapter.insertChemical(lChemical);
			}
			setResult(RESULT_OK, new Intent().putExtra("id",lChemical.getId()));
			finish();
		} catch (SQLiteConstraintException e) {
			ScreenUtil.displayMessage(AddNewChemicalDialog.this, "Chemical " + lChemical.getName() + " already exist", Toast.LENGTH_SHORT);
		}
	}

	public void setNameText(int inResString) {
		TextView lChemicalNameText = (TextView) findViewById(R.id.chemical_name_text);
		lChemicalNameText.setText(inResString);
	}

}
