package com.darkroom.library.dao;

import com.darkroom.library.Paper;
import com.darkroom.library.R;
import com.jfdupuis.library.screen.ScreenUtil;

import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;



public class AddNewPaperDialog extends AddNewElementDialog {
	
	Spinner mGradeSpinner = null;
	Spinner mTypeSpinner = null;
	
	@Override
	public void onCreate(Bundle inBundle) {
		super.onCreate(inBundle);
		setContentView(R.layout.addnew_paper_dialog);
		
		
		initialize();
		
		mGradeSpinner = (Spinner) findViewById(R.id.paper_grade_spinner);
        ArrayAdapter<CharSequence> lGradeAdapter = ArrayAdapter.createFromResource(
        		AddNewPaperDialog.this, R.array.paper_grade, android.R.layout.simple_spinner_item);
        lGradeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mGradeSpinner.setAdapter(lGradeAdapter);
        
        mTypeSpinner = (Spinner) findViewById(R.id.paper_type_spinner);
        ArrayAdapter<CharSequence> lTypeAdapter = ArrayAdapter.createFromResource(
        		AddNewPaperDialog.this, R.array.paper_type, android.R.layout.simple_spinner_item);
        lTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mTypeSpinner.setAdapter(lTypeAdapter);
        
        if(isEditing()) {
        	setTitle(R.string.edit_paper_title);
			loadData();
        } else {
        	setTitle(R.string.addnew_paper_title);
        }
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void loadData() {
		Paper lPaper = mDarkroomDBAdapter.getPaper(mId);
		EditText lName = (EditText) findViewById(R.id.paperdia_name);
		lName.setText(lPaper.mName);
		EditText lDescription = (EditText) findViewById(R.id.paperdia_description);
		lDescription.setText(lPaper.mDescription);
		
		int lPosition = ((ArrayAdapter<CharSequence>)(mTypeSpinner.getAdapter())).getPosition(Integer.toString(lPaper.mType));
		mTypeSpinner.setSelection(lPosition);
		
		lPosition = ((ArrayAdapter<CharSequence>)(mGradeSpinner.getAdapter())).getPosition(Float.toString(lPaper.mGrade));
		mGradeSpinner.setSelection(lPosition);
	}


	public void onClick(View v) {
		//Safe the filled content in the database
		final EditText lName = (EditText) findViewById(R.id.paperdia_name);
		final EditText lDescription = (EditText) findViewById(R.id.paperdia_description);

		if(TextUtils.isEmpty(lName.getText().toString())) {
			ScreenUtil.displayMessage(AddNewPaperDialog.this, "Paper name can't be empty !", Toast.LENGTH_SHORT);
			return;
		}
		
		Paper lPaper = new Paper();
		lPaper.mDescription = lDescription.getText().toString();
		lPaper.mName = lName.getText().toString();
		lPaper.setType((String) mTypeSpinner.getSelectedItem());
		lPaper.setGrade((String) mGradeSpinner.getSelectedItem());
		
		try {	
			if(isEditing()) {
				lPaper.setId(mId);
				mDarkroomDBAdapter.updatePaperValue(lPaper);
			} else {
				mDarkroomDBAdapter.insertPaper(lPaper);
			}
			setResult(RESULT_OK, new Intent().putExtra("id",lPaper.getId()));
			finish();
		} catch (SQLiteConstraintException e) {
			ScreenUtil.displayMessage(AddNewPaperDialog.this, "Paper already exist", Toast.LENGTH_SHORT);
		}
	}


}