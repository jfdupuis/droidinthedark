package com.darkroom.library.dao;

import com.darkroom.library.Emulsion;
import com.darkroom.library.R;
import com.jfdupuis.library.screen.ScreenUtil;

import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.Toast;

public class AddNewEmulsionDialog extends AddNewElementDialog {
	
	Spinner mSpinner = null;
	SimpleCursorAdapter mAdapter = null;
	
	@Override
	public void onCreate(Bundle inBundle) {
		super.onCreate(inBundle);
		setContentView(R.layout.addnew_emulsion_dialog);
		
		
		initialize();
		
		mSpinner = (Spinner) findViewById(R.id.iso_spinner);
		ArrayAdapter<CharSequence> lAdapter = ArrayAdapter.createFromResource(
				  AddNewEmulsionDialog.this, R.array.iso, android.R.layout.simple_spinner_item);
		lAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mSpinner.setAdapter(lAdapter);

		if(isEditing()) {
			setTitle(R.string.edit_emulsion_title);
			loadData();
		} else {
			setTitle(R.string.addnew_emulsion_title);
		}
	}
	
	@Override
	protected void loadData() {
		Emulsion lEmulsion = mDarkroomDBAdapter.getEmulsion(mId);
		EditText lCodeName = (EditText) findViewById(R.id.codename);
		lCodeName.setText(lEmulsion.getName());
		EditText lBrand = (EditText)findViewById(R.id.brand);
		lBrand.setText(lEmulsion.mBrand);
		CheckBox lColor = (CheckBox)findViewById(R.id.iscolor);
		lColor.setChecked(lEmulsion.mColor);
		
		@SuppressWarnings("unchecked")
		final int lPosition = ((ArrayAdapter<CharSequence>)(mSpinner.getAdapter())).getPosition(Integer.toString(lEmulsion.mISO));
		mSpinner.setSelection(lPosition);
	}

	public void onClick(View v) {
		//Safe the filled content in the database
		final EditText lCodeName = (EditText) findViewById(R.id.codename);
		final EditText lBrand = (EditText)findViewById(R.id.brand);
		//final EditText lName = (EditText) findViewById(R.id.name);
		final CheckBox lColor = (CheckBox)findViewById(R.id.iscolor);
		
		if(TextUtils.isEmpty(lCodeName.getText().toString())) {
			ScreenUtil.displayMessage(AddNewEmulsionDialog.this, "Name code can't be empty !", Toast.LENGTH_SHORT);
			return;
		}
		
		Emulsion lEmulsion = new Emulsion(lCodeName.getText().toString());
		lEmulsion.mBrand = lBrand.getText().toString();
		//lEmulsion.mName = lName.getText().toString();
		lEmulsion.mISO = Integer.valueOf( (String) mSpinner.getSelectedItem() );
		lEmulsion.mColor = lColor.isChecked();
		
		try {		
			if(isEditing()) {
				lEmulsion.setId(mId);
				mDarkroomDBAdapter.updateEmulsionValue(lEmulsion);
			} else {
				mDarkroomDBAdapter.insertEmulsion(lEmulsion);
			}
			setResult(RESULT_OK, new Intent().putExtra("id",lEmulsion.getId()));
			finish();
		} catch (SQLiteConstraintException e) {
			ScreenUtil.displayMessage(AddNewEmulsionDialog.this, "Emulsion " + lEmulsion.getName() + " already exist", Toast.LENGTH_SHORT);
		}
		
	}


}