package com.darkroom.library;

import com.darkroom.library.dao.DBObject;

import android.os.Parcel;
import android.os.Parcelable;


public class DevelopmentTime extends DBObject implements Parcelable{
	
	public String mFormat;
	public long mEmulsion;
	public long mAgitation;
	public long mDilution;
	public long mDeveloper;
	
	public int mISO;
	public float mTemp; //[C]
	public int mTime; // [s]
	public int mAlteration; // 0->N, 1 -> N+1, -1 -> N-1
	public int mFixTime;
	public String mSource; //Source where the dev time was taken
	public String mNotes; //General notes
	
	
	public DevelopmentTime(String inSource) {
		mSource = inSource;
		mNotes = "";
		mEmulsion = 1;
		mAgitation = 1;
		mDeveloper = 1;
		mDilution = 1;
	}
	
	public DevelopmentTime(DevelopmentTime inValue) {
		mFormat = inValue.mFormat;
    	mEmulsion = inValue.mEmulsion;
    	mAgitation = inValue.mAgitation;
    	mISO = inValue.mISO;
    	mDeveloper = inValue.mDeveloper;
    	mTime = inValue.mTime;
    	mFixTime = inValue.mFixTime;
    	mAlteration = inValue.mAlteration;
    	mTemp = inValue.mTemp;
    	mDilution = inValue.mDilution;
		mSource = inValue.mSource;
		mNotes = inValue.mNotes;
	}
	
	///////Parcel related methods
	public static final Parcelable.Creator<DevelopmentTime> CREATOR = new
	Parcelable.Creator<DevelopmentTime>() {
	        public DevelopmentTime createFromParcel(Parcel inParcel) {
	            return new DevelopmentTime(inParcel);
	        }

	        public DevelopmentTime[] newArray(int size) {
	            return new DevelopmentTime[size];
	        }
	    };

	    private DevelopmentTime(Parcel inParcel) {
	        readFromParcel(inParcel);
	    }
	
	public int describeContents() {
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel outParcel, int inFlags) {
		super.writeToParcel(outParcel, inFlags);
		outParcel.writeString(mFormat);
		outParcel.writeLong(mEmulsion);
		outParcel.writeLong(mAgitation);
		outParcel.writeInt(mISO);
		outParcel.writeLong(mDeveloper);
		outParcel.writeInt(mTime);
		outParcel.writeInt(mFixTime);
		outParcel.writeInt(mAlteration);
		outParcel.writeFloat(mTemp);
		outParcel.writeLong(mDilution);
		outParcel.writeString(mSource);
		outParcel.writeString(mNotes);
	}

	@Override
    public void readFromParcel(Parcel inParcel) {
    	super.readFromParcel(inParcel);
    	
    	mFormat = inParcel.readString();
    	mEmulsion = inParcel.readLong();
    	mAgitation = inParcel.readLong();
    	mISO = inParcel.readInt();
    	mDeveloper = inParcel.readLong();
    	mTime = inParcel.readInt();
    	mFixTime = inParcel.readInt();
    	mAlteration = inParcel.readInt();
    	mTemp = inParcel.readFloat();
    	mDilution = inParcel.readLong();
    	mSource = inParcel.readString();
    	mNotes = inParcel.readString();
    }
	
}
