package com.darkroom.library;

import java.util.ArrayList;
import java.util.Iterator;



import android.os.Parcel;
import android.os.Parcelable;

import com.darkroom.library.dao.DBObject;
import com.jfdupuis.library.math.Fraction;
import com.jfdupuis.library.math.FractionConversionException;

public class Exposure extends DBObject implements Parcelable{
	private String mDescription = null;
	private Fraction mBaseExposure = null;
	private ArrayList<ExposureItem> mDodgeExposureItems = null;
	private ArrayList<ExposureItem> mBurnExposureItems = null;
	private float mGrade = -3;
	private String mNotes = "";
	private boolean mSkipBase = false; //TODO support within sequence computation
	private boolean mLinear = false; //Use linear timing instead of f-stops //TODO support within sequence computation
		
	
	/**
	 *  Get exposure time from f-stop number
	 *  This return the total time computed from a base time
	 *  of 2 sec. To get a time difference, one should call
	 *  this method twice.
	 *  t = a*r^(n-1), where a=2 and r=2.
	 *  @param f-stop number
	 *  @return number of seconds
	 */
	public static float getTime(Fraction inStop) {
		return (float) (2 * java.lang.Math.pow ( 2.0, inStop.asDouble() - 1.0));
			
	}
	
	/**
	 * Get the f-number of an exposure expressed in seconds
	 * n = ln(t/a)/ln(r) + 1, where a=2 and r=2
	 * @param inTime Time in seconds
	 * @return F-Stop number
	 */
	public static Fraction getStop(double inTime) {
		try {
			if(inTime == 0)
				return new Fraction(0);
			else
				return new Fraction(java.lang.Math.log(inTime/2)/java.lang.Math.log(2) + 1);
		} catch (FractionConversionException e) {
			Logger.e("Can't convert " + inTime + " to f-stop number.", e);
			return null;
		}
	}
	
	/**
	 * Get the exposure time relative to a base time.
	 * @param inBase Base time
	 * @param inStop Exposure relative to the base time
	 * @param inIsAdded Specified if the inStop is added or subtracted from the base time
	 * @return
	 */
	public static float getRelativeTime(Fraction inBase, Fraction inStop, boolean inIsAdded) {
		if(inIsAdded) {
			//Burn
			return getTime(inBase.plus(inStop)) - getTime(inBase);
		} else {
			//Dodge
			return getTime(inBase) - getTime(inBase.minus(inStop));
		}
	}
	
	public static Fraction getStop(double inBaseTime, double inTime, boolean inIsAdded) {
		if(inIsAdded) {
			//Burn
			return getStop(inTime + inBaseTime).minus( getStop(inBaseTime) );
		} else {
			//Dodge
			return getStop(inBaseTime).minus(getStop(inBaseTime-inTime));
		}
	}
	
	public Exposure(float inGrade, String inDescription) {
		mGrade = inGrade;
		mDescription = inDescription;
		setBaseExposure(new Fraction(3));
		mDodgeExposureItems = new ArrayList<ExposureItem>();
		mBurnExposureItems = new ArrayList<ExposureItem>();
	}
	
	public Exposure(float inGrade) {
		this(inGrade,"");
	}
	
	public Exposure(Exposure inExposure) {
		super(inExposure);
		this.mDescription = inExposure.mDescription;
		this.mBaseExposure = inExposure.mBaseExposure;
		this.mGrade = inExposure.mGrade;
		this.mNotes = inExposure.mNotes;
		
		this.mDodgeExposureItems = new ArrayList<ExposureItem>();
		for(ExposureItem lItem : inExposure.mDodgeExposureItems)
			this.mDodgeExposureItems.add(new ExposureItem(lItem));
		
		this.mBurnExposureItems = new ArrayList<ExposureItem>();
		for(ExposureItem lItem : inExposure.mBurnExposureItems)
			this.mBurnExposureItems.add(new ExposureItem(lItem));
	}
	
	/**
	 * Compare two exposure to check if they are the same. 
	 */
	public boolean isEqual(Exposure inExposure) {
		if( !mBaseExposure.equals(inExposure.mBaseExposure) )
			return false;
		
		//TODO take into account position field
		if(mDodgeExposureItems.size() != inExposure.mDodgeExposureItems.size())
			return false;
		
		if(mBurnExposureItems.size() != inExposure.mBurnExposureItems.size())
			return false;
		
		for(int i = 0; i < mDodgeExposureItems.size(); ++i) {
			if(!mDodgeExposureItems.get(i).getValue().equals( inExposure.mDodgeExposureItems.get(i).getValue()) )
				return false;
		}
		
		for(int i = 0; i < mBurnExposureItems.size(); ++i) {
			if(!mBurnExposureItems.get(i).getValue().equals(inExposure.mBurnExposureItems.get(i).getValue()))
				return false;
		}
		
		return true;
	}
	
	public float getGrade() { return mGrade; }
	public void setGrade(float inGrade) { mGrade = inGrade;	}
	public String getNotes() { return mNotes; }
	public void setNotes(String inNotes) { mNotes = inNotes; }
	
	
	public void setBurnExposureItems(ArrayList<ExposureItem> mBurnExposureItems) {
		this.mBurnExposureItems = mBurnExposureItems;
	}
	public void setDodgeExposureItems(ArrayList<ExposureItem> mDodgeExposureItems) {
		this.mDodgeExposureItems = mDodgeExposureItems;
	}
	
	public ArrayList<ExposureItem> getBurnExposureItems() {
		return mBurnExposureItems;
	}
	public ArrayList<ExposureItem> getDodgeExposureItems() {
		return mDodgeExposureItems;
	}
	
	public void addBurnExposureItem(ExposureItem inItem){
		mBurnExposureItems.add(inItem);
		inItem.setPosition(mBurnExposureItems.size());
	}
	public void addDodgeExposureItem(ExposureItem inItem){
		mDodgeExposureItems.add(inItem);
		inItem.setPosition(mDodgeExposureItems.size());
	}
	
	public void removeBurnExposureItem(int inPosition) {
		mBurnExposureItems.remove(inPosition);
		for(int i = inPosition; i < mBurnExposureItems.size(); ++i)
			mBurnExposureItems.get(i).setPosition(i);
	}
	public void removeDodgeExposureItem(int inPosition) {
		mDodgeExposureItems.remove(inPosition);
		for(int i = inPosition; i < mDodgeExposureItems.size(); ++i)
			mDodgeExposureItems.get(i).setPosition(i);
	}
	
	public void insertBurnExposureItem(int inAtPosition, ExposureItem inItem) {
		mBurnExposureItems.add(inAtPosition, inItem);
		for(int i = inAtPosition; i < mBurnExposureItems.size(); ++i)
			mBurnExposureItems.get(i).setPosition(i);
	}
	public void insertDodgeExposureItem(int inAtPosition, ExposureItem inItem) {
		mDodgeExposureItems.add(inAtPosition, inItem);
		for(int i = inAtPosition; i < mBurnExposureItems.size(); ++i)
			mDodgeExposureItems.get(i).setPosition(i);
	}
	
	
	
	
	

	public void setBaseExposure(Fraction mBaseExposure) {
		this.mBaseExposure = mBaseExposure;
	}

	public Fraction getBaseExposure() {
		return mBaseExposure;
	}
	
	public void setDescription(String inDescription) {
		this.mDescription = inDescription;
	}

	public String getDescription() {
		return mDescription;
	}
	
	public void setSkipBase(boolean inSkipBase) {
		this.mSkipBase = inSkipBase;
	}

	public boolean isSkipBase() {
		return mSkipBase;
	}
	
	public void setLinear(boolean inLinear) {
		this.mLinear = inLinear;
	}

	public boolean isLinear() {
		//TODO handle convertion
		
		return mLinear;
	}

	public float computeTotalDodgingTime(float inBaseTime, boolean inRemoveNullExposure) {
		float lSum = 0;
		for (Iterator<ExposureItem> lIter = mDodgeExposureItems.iterator(); lIter.hasNext(); ) {
			ExposureItem lItem = lIter.next();
			if(inRemoveNullExposure && lItem.getValue().equals(0)) {
				lIter.remove();
			} else {
				lSum += inBaseTime - getTime(mBaseExposure.minus(lItem.getValue()));
			}
		}
		return lSum;
	}
	
	public boolean checkBaseExposure() {
		float lBaseExposureTime = getTime(mBaseExposure);
		if(computeTotalDodgingTime(lBaseExposureTime,false) > lBaseExposureTime) {
			return false;
		}
		else
			return true;
	}
	
	/** Compute base exposure time sequence in seconds
	 * 
	 */
	public float[] getBaseTimeSequence() {
		float lBaseExposureTime = getTime(mBaseExposure);
		float lSum = computeTotalDodgingTime(lBaseExposureTime,true);
		
		int lSize = mDodgeExposureItems.size();
		
		float[] lTimeSequence = new float[lSize];
		if(lSize == 0) return lTimeSequence;
		
		float lClearTime = lBaseExposureTime - lSum;
		if(lClearTime < 0) {
			
			throw new RuntimeException("Dodge time is bigger than base time");
		}
	
		lTimeSequence[0] = lClearTime;
		int i = 1;
		for(int k = 0; k < mDodgeExposureItems.size() && i < lTimeSequence.length;++k) {
			if(mDodgeExposureItems.get(k).getValue().equals(0))
				continue;
			else
				lTimeSequence[i++] = lBaseExposureTime - getTime(mBaseExposure.minus(mDodgeExposureItems.get(k).getValue()));
		}
		return lTimeSequence;
	}
	
	/** Compute burn exposure time sequence in seconds
	 * 
	 */
	public float[] getBurnTimeSequence() {
		for (Iterator<ExposureItem> lIter = mBurnExposureItems.iterator(); lIter.hasNext(); ) {
			if(lIter.next().getValue().equals(0)) {
				lIter.remove();
			} 
		}
		int lSize = mBurnExposureItems.size();

		
		float[] lTimeSequence = new float[lSize];
		if(lSize == 0) return lTimeSequence;
	
		int i = 0;
		for(int k = 0; k < mBurnExposureItems.size() && i < lTimeSequence.length;++k) {
			if(mBurnExposureItems.get(k).getValue().equals(0))
				continue;
			else
				lTimeSequence[i++] = getTime(mBaseExposure.plus(mBurnExposureItems.get(k).getValue())) - getTime(mBaseExposure);
		}
		return lTimeSequence;
	}
	
	///////Parcel related methods
	public static final Parcelable.Creator<Exposure> CREATOR = new
	Parcelable.Creator<Exposure>() {
	        public Exposure createFromParcel(Parcel inParcel) {
	            return new Exposure(inParcel);
	        }

	        public Exposure[] newArray(int size) {
	            return new Exposure[size];
	        }
	    };

	    private Exposure(Parcel inParcel) {
	    	mDodgeExposureItems = new ArrayList<ExposureItem>();
			mBurnExposureItems = new ArrayList<ExposureItem>();
	        readFromParcel(inParcel);
	    }
	
	public int describeContents() {
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel outParcel, int inFlags) {
		super.writeToParcel(outParcel, inFlags);
		outParcel.writeString(mDescription);
		outParcel.writeParcelable(mBaseExposure,inFlags);
		outParcel.writeFloat(mGrade);
		outParcel.writeString(mNotes);
		outParcel.writeBooleanArray(new boolean[]{mSkipBase,mLinear});
		
		int lDSize = mDodgeExposureItems.size();
		outParcel.writeInt(lDSize);
		
		int lBSize = mBurnExposureItems.size();
		outParcel.writeInt(lBSize);
		
        for (int i = 0; i < lDSize; i++) {
            outParcel.writeParcelable(mDodgeExposureItems.get(i),inFlags);
        }
        for (int i = 0; i < lBSize; i++) {
            outParcel.writeParcelable(mBurnExposureItems.get(i),inFlags);
        }
		
	}

	@Override
    public void readFromParcel(Parcel inParcel) {
    	super.readFromParcel(inParcel);
    	mDescription = inParcel.readString();
    	mBaseExposure = inParcel.readParcelable(Fraction.class.getClassLoader());
    	mGrade = inParcel.readFloat();
    	mNotes = inParcel.readString();
    	boolean[] lBools = new boolean[2];
    	inParcel.readBooleanArray(lBools);
    	mSkipBase = lBools[0];
    	mLinear = lBools[1];
    	
    	int lDSize = inParcel.readInt();
    	int lBSize = inParcel.readInt();
		
    	mDodgeExposureItems.clear(); 
    	mBurnExposureItems.clear();
    	for (int i = 0; i < lDSize; i++) {
    		ExposureItem lItem = inParcel.readParcelable(ExposureItem.class.getClassLoader());
    		mDodgeExposureItems.add( lItem );
        }
    	for (int i = 0; i < lBSize; i++) {
    		mBurnExposureItems.add( (ExposureItem) inParcel.readParcelable(ExposureItem.class.getClassLoader()) );
        }

    }

	

}
