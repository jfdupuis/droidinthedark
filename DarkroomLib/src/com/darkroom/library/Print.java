package com.darkroom.library;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.jfdupuis.library.math.Unit;

import android.os.Parcel;
import android.os.Parcelable;

/*! Print Class
 *  This class define the description of a print that can
 *  contain many exposures.
 */
public class Print extends DarkroomDatedObject implements Parcelable {
	
	ArrayList<Exposure> mExposures = null;
	
//	private static final long serialVersionUID = 1L;

	private long mFilmId = -1;
	private String mFilmUserId = null;
	
	private int mFrameNumber = -1;
	
	private float mSizeH = 0; //[mm]
	private float mSizeV = 0; //[mm]
	
	private ArrayList<Long> mCollectionId;
	private String mTitle = "";
	private String mDescription = "";
	private String mNotes = ""; //Development notes
	private long mTonerId = -1;
	private long mPaperId = -1;
	private long mDeveloperId = -1;
	private int mCurrentExposure = 0;
	
	
	public Print(Print inPrint) {
		super(inPrint);
		this.mCollectionId = inPrint.mCollectionId;
		this.mExposures = inPrint.mExposures;
		this.mFilmUserId = inPrint.mFilmUserId;
		this.mSizeH = inPrint.mSizeH;
		this.mSizeV = inPrint.mSizeV;
		this.mFilmId = inPrint.mFilmId;
		this.mFrameNumber = inPrint.mFrameNumber;
		this.mTitle = inPrint.mTitle;
		this.mDescription = inPrint.mDescription;
		this.mNotes = inPrint.mNotes;
		this.mTonerId = inPrint.mTonerId;
		this.mPaperId = inPrint.mPaperId;
		this.mDeveloperId = inPrint.mDeveloperId;
		this.mCurrentExposure = inPrint.mCurrentExposure;
	}
	
	public Print(long inFilmId, int inFrameNumber, int inIDNumber, Date inDate){
		mIDNumber = inIDNumber;
		mDate = inDate;
		mFilmId = inFilmId;
		mFrameNumber = inFrameNumber;
		mExposures = new ArrayList<Exposure>();
		mCollectionId = new ArrayList<Long>();
		generateUniqueId();
	}

	
	public Print(long inFilmId, int inFrameNumber, int inIDNumber){
		this(inFilmId, inFrameNumber, inIDNumber,Calendar.getInstance().getTime());
	}
	
	public Print(){
		this(-1,-1,-1,Calendar.getInstance().getTime());
	}
	
	public boolean hasSameExposure(Print inPrint) {
		if(inPrint == null)
			return false;
		if(this.mFilmId != inPrint.mFilmId)
			return false;
		if(this.mFrameNumber != inPrint.mFrameNumber)
			return false;
		
		if(mExposures.size() != inPrint.mExposures.size())
			return false;
		
		for(Exposure lExposure: mExposures) {
			int i = 0;
			for(; i < inPrint.mExposures.size(); ++i) {
				//TODO remove previously found exposures
				if(lExposure.isEqual(inPrint.mExposures.get(i))) {
					break;
				}
			}
			if(i == mExposures.size()) 
				return false;
		}
		
		return true;
	}
	
	public void setFilmUserId(String inFilmUserId) {
		mFilmUserId = inFilmUserId;
	}
	
	public String getFilmUserId() {
		return mFilmUserId;
	}
	
	public void setCollectionId(ArrayList<Long> inCollection) {
		mCollectionId = inCollection;
	}
	
	public void addCollectionId(long inCollectionId) {
		mCollectionId.add(inCollectionId);
	}
	
	public void removeCollectionId(long inCollectionId) {
		mCollectionId.remove(new Long(inCollectionId));
	}
	
	public final ArrayList<Long> getCollectionId() {
		return mCollectionId;
	}
	
	@Override
	protected String generateUniqueId() {
		Calendar lCalendar = Calendar.getInstance();
        lCalendar.setTime(mDate);
        if(mFilmUserId != null)
        	mUserId = String.format("%s-%d-%d", mFilmUserId,mFrameNumber, mIDNumber);
        else
        	mUserId = String.format("%d-%d-%d", mFilmId,mFrameNumber, mIDNumber);
        	//throw new RuntimeException("FilmUserId should not be null in print.");
        
		Logger.d("New print user id: " + mUserId);
		return mUserId;
	}
	
	public void setCurrentExposure(int inCurrentExposure) {
		this.mCurrentExposure = inCurrentExposure;
	}
	
	public Exposure getCurrentExposure() {
		return mExposures.get(mCurrentExposure);
	}
	
	public void addExposure(Exposure inExposure, int inIndex) {
		mExposures.add(inIndex, inExposure);
	}
	
	public void addExposure(Exposure inExposure) {
		mExposures.add(inExposure);
	}
	
	public Exposure getExposure(int inIdx) {
		return mExposures.get(inIdx);
	}
	
	public ArrayList<Exposure> getExposures() {
		return mExposures;
	}
	
	public void setFilmFrameNumber(int inFilmFrame) {
		this.mFrameNumber = inFilmFrame;
	}
	public int getFilmFrameNumber() {
		return mFrameNumber;
	}
	
	public void setFilmId(long inFilmId) {
		this.mFilmId = inFilmId;
	}
	public long getFilmId() {
		return mFilmId;
	}
		
	public void setDescription(String inDescription) {
		this.mDescription = inDescription;
	}

	public String getDescription() {
		return mDescription;
	}
	
	public void setTitle(String inDescription) {
		this.mTitle = inDescription;
	}

	public String getTitle() {
		return mTitle;
	}
	
	public void setNotes(String inNotes) {
		this.mNotes = inNotes;
	}

	public String getNotes() {
		return mNotes;
	}
	
	public void setPaper(long inPaperId) {
		this.mPaperId = inPaperId;
	}

	public long getPaper() {
		return mPaperId;
	}
	
	
	public void setDeveloper(long inDeveloperId) {
		this.mDeveloperId = inDeveloperId;
	}

	public long getDeveloper() {
		return mDeveloperId;
	}
	public void setToner(long inTonerId) {
		this.mTonerId = inTonerId;
	}

	public long getToner() {
		return mTonerId;
	}
	
	public void setSize(float inSizeH, float inSizeV) {
		this.mSizeH = inSizeH;
		this.mSizeV = inSizeV;
	}
	
	public void setSize(float inSizeH, float inSizeV, String inUnits) {
		this.mSizeH = Unit.convertToMM(inSizeH, inUnits);
		this.mSizeV = Unit.convertToMM(inSizeV, inUnits);
	}
	
	public float getSizeH() {
		return mSizeH;
	}
	
	public float getSizeV() {
		return mSizeV;
	}
	
	public float getSizeH(String inUnits) {
		return Unit.convertFromMM(mSizeH, inUnits);
	}
	
	public float getSizeV(String inUnits) {
		return Unit.convertFromMM(mSizeV, inUnits);
	}
	
	
	///////Parcel related methods
	public static final Parcelable.Creator<Print> CREATOR = new	Parcelable.Creator<Print>() {
	        public Print createFromParcel(Parcel inParcel) {
	            return new Print(inParcel);
	        }

	        public Print[] newArray(int size) {
	            return new Print[size];
	        }
	    };

	    private Print(Parcel inParcel) {
	        readFromParcel(inParcel);
	    }
	
	public int describeContents() {
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel outParcel, int inFlags) {
		super.writeToParcel(outParcel, inFlags);
		
		outParcel.writeLong(mFilmId);
		outParcel.writeString(mFilmUserId);
		outParcel.writeInt(mFrameNumber);
		
		outParcel.writeFloat(mSizeH);
		outParcel.writeFloat(mSizeV);
		Object[] lCollections = (Object[]) mCollectionId.toArray();
		outParcel.writeArray(lCollections);
		outParcel.writeString(mTitle);
		outParcel.writeString(mDescription);
		outParcel.writeString(mNotes);
		outParcel.writeLong(mTonerId);
		outParcel.writeLong(mPaperId);
		outParcel.writeLong(mDeveloperId);
		outParcel.writeInt(mCurrentExposure);
		
		int lSize = mExposures.size();
		outParcel.writeInt(lSize);
		
        for (int i = 0; i < lSize; i++) {
            outParcel.writeParcelable(mExposures.get(i),inFlags);
        }
	}

	@Override
    public void readFromParcel(Parcel inParcel) {
    	super.readFromParcel(inParcel);
    	
    	mFilmId = inParcel.readLong();
    	mFilmUserId = inParcel.readString();
    	mFrameNumber = inParcel.readInt();
    	
    	mSizeH = inParcel.readFloat();
    	mSizeV = inParcel.readFloat();
    	
    	Object[] lCollections = (Object[]) inParcel.readArray(Long.class.getClassLoader());
    	mCollectionId = new ArrayList<Long>();
    	for(Object lItem : lCollections)
    		mCollectionId.add((Long) lItem);
    	
    	mTitle = inParcel.readString();
    	mDescription = inParcel.readString();
    	mNotes = inParcel.readString();
    	mTonerId = inParcel.readLong();
    	mPaperId = inParcel.readLong();
    	mDeveloperId = inParcel.readLong();
    	mCurrentExposure = inParcel.readInt();
    	
    	int lSize = inParcel.readInt();
    			
    	mExposures = new ArrayList<Exposure>();
    	for (int i = 0; i < lSize; i++) {
    		Exposure lItem = inParcel.readParcelable(Exposure.class.getClassLoader());
    		mExposures.add( lItem );
        }
    }

	
	
	
	
}
