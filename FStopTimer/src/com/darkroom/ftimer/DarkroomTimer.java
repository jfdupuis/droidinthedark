package com.darkroom.ftimer;

import com.darkroom.ftimer.widget.CustomMenu;
import com.darkroom.ftimer.widget.CustomMenu.OnMenuItemSelectedListener;
import com.darkroom.ftimer.widget.CustomMenuItem;
import com.darkroom.ftimer.widget.GradeWheelAdapter;
import com.darkroom.ftimer.widget.WheelViewCustomizable;
import com.darkroom.library.Exposure;
import com.darkroom.library.ExposureItem;
import com.darkroom.library.Print;
import com.jfdupuis.library.screen.ScreenUtil;
import com.jfdupuis.bluetooth.arduino.ArduinoServiceBinder;
import com.util.help.SimpleUsage;
import com.jfdupuis.library.math.Fraction;
import com.jfdupuis.library.math.FractionConversionException;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import com.jfdupuis.library.widget.wheel.OnWheelChangedListener;
import com.jfdupuis.library.widget.wheel.OnWheelClickedListener;
import com.jfdupuis.library.widget.wheel.WheelView;




public class DarkroomTimer extends ArduinoServiceBinder implements OnMenuItemSelectedListener {
	
//	private static final String TAG = "DarkroomTimer";
	
	static final int MINIMUM_INCREMENTS = 48;
	static final int MAXIMUM_INCREMENTS = 2;
	
	static final int MAXIMUN_NB_GRADE = 4;
	
	static final int DIALOG_EXIT_ID = 0;
	static final int DIALOG_SETTING_ID = 1;
	static final int DIALOG_SAVE_ID = 2;
	
	public static final int MENU_ITEM_ADD_DODGE = 1;
	public static final int MENU_ITEM_ADD_BURN = 2;
	public static final int MENU_ITEM_REMOVE = 3;
	public static final int MENU_GRADE_ADD= 4;
	public static final int MENU_GRADE_REMOVE = 5;
	public static final int MENU_ITEM_SAVE = 6;
	public static final int MENU_ITEM_SCAN = 7;
	public static final int MENU_ITEM_SETTINGS = 8;
	
	GradeCompensation mGradeCompTable = null;
	
	private boolean mAddingNew = false;
	
	private CustomMenu mMenu = null;
	
	private Print mPrint = null;
	boolean mExitOnSave = false;
	
	private ListView mDodgeList = null;
	private ListView mBurnList = null;
	
	private ExposureArrayAdapter mDodgeAdapter = null;
	private ExposureArrayAdapter mBurnAdapter = null;
	
	private FNumberPicker mIncrementPicker = null;
	private FNumberPicker mBasePicker = null;
	
	private TextView mBaseTimeDisplay = null;
	
	CheckBox mSkipBaseExposureBox = null;
	
	GradeWheelAdapter mGradeAdapter = null;
	LinearLayout mGradeLayout;
	
	final private OnWheelClickedListener mExposureChangedListener = new OnWheelClickedListener() {
		@Override
		public void onItemClicked(WheelView inWheel, int inItemIndex) {
			changedExposure(((ViewGroup) inWheel.getParent()).indexOfChild(inWheel));
		}
	};
	
	final private OnWheelChangedListener mGradeChangedListener = new OnWheelChangedListener() {
		@Override
		public void onChanged(WheelView inWheel, int inOldValue, int inNewValue) {
			mPrint.getExposures().get(((ViewGroup) inWheel.getParent()).indexOfChild(inWheel)).setGrade(mGradeAdapter.getValue(inNewValue));
			
			//Apply compensation
			applyGradeCompensation(mGradeAdapter.getValue(inNewValue), mGradeAdapter.getValue(inOldValue));
		}
	};
	
	void applyGradeCompensation(float inNewGrade, float inOldGrade) {
		double lCompensation = mGradeCompTable.getCompensation(inNewGrade, inOldGrade);
		Fraction lFComp;
		try {
			lFComp = new Fraction(lCompensation);
			
		} catch (FractionConversionException e) {
			lFComp = new Fraction(0); //TODO think of a better handling than ignoring compensation
			e.printStackTrace();
		}
		Logger.d("Apply grade compensation from " + String.valueOf(inNewGrade) + " to " + String.valueOf(inOldGrade) + " of : " + String.valueOf(lFComp.asDouble()) );
		mBasePicker.changeCurrent(mBasePicker.getCurrent().plus(lFComp));
	}
	
	void setDecimals(boolean inState) {
		mIncrementPicker.setDecimal(inState);
		mBasePicker.setDecimal(inState);
		if(inState) {
			mIncrementPicker.setRange(new Fraction(1,(long)100),new Fraction(1), mIncrementPicker.getCurrent());
		} else {
			mIncrementPicker.setRange(new Fraction(1,(long)MINIMUM_INCREMENTS),new Fraction(MAXIMUM_INCREMENTS), mIncrementPicker.getCurrent());
		}
		mDodgeAdapter.notifyDataSetChanged();
        mBurnAdapter.notifyDataSetChanged();
	}
	

	/** Create the activity
	 * 
	 */
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	requestWindowFeature(Window.FEATURE_NO_TITLE); 
//    	getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
//							 WindowManager.LayoutParams.FLAG_FULLSCREEN);
    	super.onCreate(savedInstanceState);
    	
    	// Inflate your view
        setContentView(R.layout.main);
        
        // Get the GUI items
        mBaseTimeDisplay = (TextView) findViewById(R.id.printingtime_display);
        mBasePicker = (FNumberPicker) findViewById(R.id.BaseTimePicker);
        mIncrementPicker = (FNumberPicker) findViewById(R.id.FStopIncrementPicker);
        
        
        
        //Setup increments picker
        mIncrementPicker.setRange(new Fraction(1,(long)MINIMUM_INCREMENTS),new Fraction(MAXIMUM_INCREMENTS));
        mIncrementPicker.setFractionPicker(true);
        
       //Setup base time picker
        mBasePicker.setIncrementPicker(mIncrementPicker);
        mBasePicker.setOnChangeListener(new FNumberPicker.OnChangedListener() {
		    public void onChanged(FNumberPicker inSpinner, Fraction oldVal, Fraction newVal) {
		    	mPrint.getCurrentExposure().setBaseExposure(newVal);
		    	if(!mPrint.getCurrentExposure().checkBaseExposure()) {
		    		inSpinner.setCurrent(oldVal);
		    		mPrint.getCurrentExposure().setBaseExposure(oldVal);	
		    		ScreenUtil.displayMessage(DarkroomTimer.this,getString(R.string.error_dodgetimetoobig),Toast.LENGTH_SHORT);
		    	}
	    		updateTimeDisplay();		    	
		    }
		  });
        
        
        
        //Setup skip exposure box
        mSkipBaseExposureBox = (CheckBox) findViewById(R.id.SkipBaseExposureBox);
        mSkipBaseExposureBox.setOnCheckedChangeListener(new OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(CompoundButton inButtonView, boolean inIsChecked) {
				mPrint.getCurrentExposure().setSkipBase(inIsChecked);
			}
        	
        });
        
        //Setup grade widget
        mGradeLayout = (LinearLayout) findViewById(R.id.grade_list); 
        mGradeAdapter = new GradeWheelAdapter(this, 0.5f, null);
        mGradeAdapter.setItemResource(R.layout.grade_wheel_item);
             
        //Setup the print
        if (savedInstanceState != null){
        	//Get the saved print 
            mPrint = savedInstanceState.getParcelable("print");
            mIncrementPicker.setCurrent((Fraction) savedInstanceState.getParcelable("increment"));
            mBasePicker.setCurrent(mPrint.getCurrentExposure().getBaseExposure());
            for(int i = 0; i < mPrint.getExposures().size(); ++i) {
    			addGrade(mPrint.getExposure(i).getGrade(), i);
    		}
            
        } else if(getIntent().getLongExtra("print_id", -1) > 0) {
        	//Get the print put in the intent
        	mExitOnSave = true;
        	mPrint = getIntent().getParcelableExtra("print");
        	if(mPrint.getExposures().size() < 1) {
	    		addExposure(2.0f);
	    	} else {
	    		for(int i = 0; i < mPrint.getExposures().size(); ++i) {
	    			addGrade(mPrint.getExposure(i).getGrade(), i);
	    		}
	    	}
            mBasePicker.setCurrent(mPrint.getCurrentExposure().getBaseExposure());
            //Set to default increments
            if(mIncrementPicker.isDecimal())
            	mIncrementPicker.setCurrent(new Fraction(1,(long)10));
            else
            	mIncrementPicker.setCurrent(new Fraction(1,(long)3));
        } else {
        	//Create a new one
        	mPrint = new Print();
        	addExposure(2.0f);
	        
        	if(mIncrementPicker.isDecimal())
            	mIncrementPicker.setCurrent(new Fraction(1,(long)10));
            else
            	mIncrementPicker.setCurrent(new Fraction(1,(long)3));
	        mBasePicker.setCurrent(new Fraction(3));
	        mPrint.getCurrentExposure().setBaseExposure(mBasePicker.getCurrent());
        }
        
        mSkipBaseExposureBox.setChecked(mPrint.getCurrentExposure().isSkipBase());
        
        //Setup lists
        final int resID = R.layout.exposurelist_item;
        mDodgeList = (ListView)findViewById(R.id.DodgeExposureList);  
        mBurnList = (ListView)findViewById(R.id.BurnExposureList);
        mBurnAdapter =  new ExposureArrayAdapter(this, resID, mPrint.getCurrentExposure().getBurnExposureItems(), mBurnList, mIncrementPicker);
        mDodgeAdapter = new ExposureArrayAdapter(this, resID, mPrint.getCurrentExposure().getDodgeExposureItems(), mDodgeList, mIncrementPicker);
        mDodgeAdapter.setOnChangeListener(new ExposureArrayAdapter.OnChangedListener() {
		    public void onChanged(int inPosition, Fraction mPrevious, Fraction mCurrent) {
		    	checkDodgeExposures(inPosition, mPrevious, mCurrent);
		    }
		  });
        
        mDodgeList.setAdapter(mDodgeAdapter);
        mBurnList.setAdapter(mBurnAdapter);
        
        //Set selected grade
        final int lCurrentExposureIndex =mPrint.getExposures().indexOf(mPrint.getCurrentExposure());
        for(int i = 0; i < mGradeLayout.getChildCount(); ++ i) {
			mGradeLayout.getChildAt(i).setSelected(i == lCurrentExposureIndex);
		}
        
        //Connect buttons
        final Button lStartButton = (Button) findViewById(R.id.startstop_button);
        lStartButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	startstopTimer();
            }
        });
        
        
        final Button lDevTimerButton = (Button) findViewById(R.id.devtimer_button);
        if(lDevTimerButton != null) {
        	lDevTimerButton.setOnClickListener(new View.OnClickListener() {
	            public void onClick(View v) {
	                startDevTimer();
	            }
	        });
        }
        
        
        
		//Initialize the menu
        mMenu = new CustomMenu(this, this, getLayoutInflater());
        mMenu.setHideOnSelect(true);
        mMenu.setItemsPerLineInPortraitOrientation(2);
        mMenu.setItemsPerLineInLandscapeOrientation(8);
        createMenuItems();
        
        updateTimeDisplay();
        
        new SimpleUsage(this).show();
    }
    
    /** Save the current print
     * 	To handle the change in orientation, the current print
     * 	need to be saved in order to be restored when calling
     *  onCreate() again. 
     */
    @Override
    protected void onSaveInstanceState(Bundle saveState) {
        super.onSaveInstanceState(saveState);
        saveState.putParcelable("print",mPrint);
        saveState.putParcelable("increment",mIncrementPicker.getCurrent());
    }
    
    @Override
    protected void onResume() {
    	super.onResume();
    	
    	//Set screen brightness
    	float lValue = (float)(DarkroomTimerPrefs.getScreenBrightness(this));
    	ScreenUtil.setBrightness(DarkroomTimer.this, lValue/100.0f, 0);
    	
    	//Set f-stop decimal preferences
    	setDecimals(DarkroomTimerPrefs.showDecimals(this));
    	
    	//Get the grade exposure compensation table
    	mGradeCompTable = DarkroomTimerPrefs.getGradeCompensation(this);
    	
    	
       	//Initialize bluetooth
    	if( DarkroomTimerPrefs.haveBluetooth(this) ) {
    		//Binding need to be made in this activity so that the device don't
    		//disconnect between exposure.
    		//Using a dummy handler is need to avoid null pointer exception.
    		//This handler is changed in the exposure timer
    		mHandler = new EnlargerHandler(DarkroomTimer.this);
    		bind();
    	} else {
    		unbind();
    		mHandler = null;
    	}
    }
    
    @Override 
    protected void onDestroy() {
    	super.onDestroy();
    	unbind(); //Unbind the bluetooth service
    }

    /** Fill exposure list
     * 	Add exposure into a list with value of 0.
     */
    private void fillEmptyList(ArrayList<ExposureItem> inList, int inNb, int inType) {
        for(int i = 0; i < inNb; ++i) {
        	inList.add(new ExposureItem(inList.size(),inType,"",new Fraction(0)));
        }
    }
    
    
	
	/** Check dodge exposure values 
	 * 	This function is called when the value of dodge adapter are 
	 *  modified. This is to verify that the new dodging time doesn't
	 *  exceed the selected base exposure time.
	 */
	public void checkDodgeExposures(int inPosition, Fraction mPrevious, Fraction mCurrent) {
		if(!mPrint.getCurrentExposure().checkBaseExposure()) {
			ScreenUtil.displayMessage(DarkroomTimer.this,getString(R.string.error_dodgetimetoobig),Toast.LENGTH_SHORT);
			mPrint.getCurrentExposure().getDodgeExposureItems().get(inPosition).setValue(mPrevious);
			mDodgeAdapter.notifyDataSetChanged();
		}
	}
	
	    
    /** Change the current exposure
     * 	Called by mExposureChangedListener when the change the exposure
     * 	selection button have been triggered. 
     */
    private void changedExposure(int inExposure) {
//    	Log.v(TAG,String.format("Exposure changed to %d",inExposure));
		mPrint.setCurrentExposure(inExposure);
		mSkipBaseExposureBox.setChecked(mPrint.getCurrentExposure().isSkipBase());
		updatePickerValues();
		
		for(int i = 0; i < mGradeLayout.getChildCount(); ++ i) {
			mGradeLayout.getChildAt(i).setSelected(i == inExposure);
		}
	}
    
    /** Update the displayed exposure value
     * 	Called to update the GUI value according to the new
     * 	active exposure value when an change of exposure was
     *  made.
     */
    protected void updatePickerValues() {
    	final int resID = R.layout.exposurelist_item;
    	mDodgeAdapter = new ExposureArrayAdapter(this, resID, mPrint.getCurrentExposure().getDodgeExposureItems(), mDodgeList, mIncrementPicker);
        mBurnAdapter =  new ExposureArrayAdapter(this, resID, mPrint.getCurrentExposure().getBurnExposureItems(), mBurnList, mIncrementPicker);
        mDodgeList.setAdapter(mDodgeAdapter);
        mBurnList.setAdapter(mBurnAdapter);
        mDodgeAdapter.notifyDataSetChanged();
        mBurnAdapter.notifyDataSetChanged();
        
        mBasePicker.setCurrent(mPrint.getCurrentExposure().getBaseExposure());
        updateTimeDisplay();
    }
    
    @Override
    protected Dialog onCreateDialog(int inId) {
    	SafeDialog lDialog = null;
        switch(inId) {
        case DIALOG_EXIT_ID:

        	lDialog = new SafeDialog(this);
        	//lDialog.setTitle(getString(R.string.confirm_exit_title)); //Can't get the text color right. Text disappear
        	lDialog.setImageResource(R.drawable.safe_icon_light_alert);
        	lDialog.setText(getString(R.string.confirm_exit_message));
        	
        	lDialog.setPositiveButton(getString(android.R.string.ok), new DialogInterface.OnClickListener() {
    		public void onClick(DialogInterface dialog, int whichButton) {
    			DarkroomTimer.this.finish();
    		}});
        	lDialog.setNegativeButton(getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
        		public void onClick(DialogInterface dialog, int whichButton) {
        	}});

            break;
        case DIALOG_SETTING_ID:
        	lDialog = new SafeDialog(this);
        	lDialog.setImageResource(R.drawable.safe_icon_light_alert);
        	lDialog.setText(getString(R.string.confirm_white_light));
        	
        	lDialog.setPositiveButton(getString(android.R.string.ok), new DialogInterface.OnClickListener() {
    		public void onClick(DialogInterface dialog, int whichButton) {
    			DarkroomTimer.this.startActivity(new Intent(DarkroomTimer.this, DarkroomTimerPrefs.class));
    		}});
        	lDialog.setNegativeButton(getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
        		public void onClick(DialogInterface dialog, int whichButton) {
        	}});
            break;
        case DIALOG_SAVE_ID:
        	lDialog = new SafeDialog(this);
        	lDialog.setImageResource(R.drawable.safe_icon_light_alert);
        	lDialog.setText(getString(R.string.confirm_white_light));
        	
        	lDialog.setPositiveButton(getString(android.R.string.ok), new DialogInterface.OnClickListener() {
    		public void onClick(DialogInterface dialog, int whichButton) {
    			Intent lIntent = new Intent();
    			lIntent.setAction( "com.darkroom.saveprint" );
    			lIntent.putExtra("print", mPrint);
    			lIntent.putExtra("print_id", mPrint.getId());
    			try {
    				startActivity(lIntent);
    				if(mExitOnSave) finish();
    			} catch(ActivityNotFoundException inException) {
    				ScreenUtil.displayMessage(DarkroomTimer.this, "Unable to start the database application", Toast.LENGTH_SHORT);
    			}
    		}});
        	lDialog.setNegativeButton(getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
        		public void onClick(DialogInterface dialog, int whichButton) {
        	}});
            break;
        default:
        	lDialog = null;
        }
        return lDialog;
    }
    
    
    /** Display exit dialog
     *  Ask an exit confirmation before exiting. This is asked to prefer
     *  white light exposure from accidental back button hit.
     *  @see android.app.Activity#onBackPressed()
     */
    @Override
    public void onBackPressed() {
    	showDialog(DIALOG_EXIT_ID);
        return;
    }
    
    /** Intercept the menu key
     */
	public boolean onKeyDown(int keyCode, KeyEvent event) { 
	    if (keyCode == KeyEvent.KEYCODE_MENU) {
	    	doMenu();
	    	return true; //always eat it!
	    }
		return super.onKeyDown(keyCode, event); 
	} 
 	
	public void MenuItemSelectedEvent(CustomMenuItem mSelection) {
	      switch (mSelection.getId()) {
	    	case MENU_ITEM_SETTINGS:
	    		displaySettings();
	    		return;
			case MENU_ITEM_ADD_DODGE:
				addNewDodgeItem();
				return; 
			
			case MENU_ITEM_REMOVE :
				if (mAddingNew) {
					mAddingNew = false;;
				} 
				else {
					removeItem();
				}
				return;
	        
			case MENU_GRADE_ADD:
				changedExposure(addExposure(2.0f));
				return; 
			
			case MENU_GRADE_REMOVE :
				removeGrade();
				return;
	        
			case MENU_ITEM_ADD_BURN :
				addNewBurnItem();
				return; 
			
			case MENU_ITEM_SAVE :
				savePrint();
				return; 
			
			case MENU_ITEM_SCAN:
	    		// Launch the DeviceListActivity to see devices and do scan
	            Intent serverIntent = new Intent(this, SafeDeviceListActivity.class);
	            startActivityForResult(serverIntent, ArduinoServiceBinder.REQUEST_CONNECT_DEVICE);
	            return;
	    }		
	}
	
	/**
     * Load up our menu.
     */
	private void createMenuItems() {
		//This is kind of a tedious way to load up the menu items.
		//Am sure there is room for improvement.
		ArrayList<CustomMenuItem> menuItems = new ArrayList<CustomMenuItem>();
		CustomMenuItem cmi = new CustomMenuItem();
		cmi.setCaption(getString(R.string.menu_add_dodge_item));
		cmi.setImageResourceId(R.drawable.ic_menu_add_invert);
		cmi.setId(MENU_ITEM_ADD_DODGE);
		menuItems.add(cmi);
		
		cmi = new CustomMenuItem();
		cmi.setCaption(getString(R.string.menu_add_burn_item));
		cmi.setImageResourceId(R.drawable.ic_menu_add_invert);
		cmi.setId(MENU_ITEM_ADD_BURN);
		menuItems.add(cmi);
		
		cmi = new CustomMenuItem();
		cmi.setCaption(getString(R.string.menu_remove_item));
		cmi.setImageResourceId(R.drawable.ic_menu_delete_invert);
		cmi.setId(MENU_ITEM_REMOVE);
		menuItems.add(cmi);
		
		cmi = new CustomMenuItem();
		cmi.setCaption(getString(R.string.menu_add_grade));
		cmi.setImageResourceId(R.drawable.ic_menu_add_invert);
		cmi.setId(MENU_GRADE_ADD);
		menuItems.add(cmi);
		
		cmi = new CustomMenuItem();
		cmi.setCaption(getString(R.string.menu_remove_grade));
		cmi.setImageResourceId(R.drawable.ic_menu_delete_invert);
		cmi.setId(MENU_GRADE_REMOVE);
		menuItems.add(cmi);
		
		cmi = new CustomMenuItem();
		cmi.setCaption(getString(R.string.menu_save_item));
		cmi.setImageResourceId(R.drawable.ic_menu_save_invert);
		cmi.setId(MENU_ITEM_SAVE);
		menuItems.add(cmi);
		
		cmi = new CustomMenuItem();
		cmi.setCaption(getString(R.string.connect));
		cmi.setImageResourceId(R.drawable.menu_icon_btsearch);
		cmi.setId(MENU_ITEM_SCAN);
		menuItems.add(cmi);
		
		cmi = new CustomMenuItem();
		cmi.setCaption(getString(R.string.menu_setting_item));
		cmi.setImageResourceId(R.drawable.ic_menu_preferences_invert);
		cmi.setId(MENU_ITEM_SETTINGS);
		menuItems.add(cmi);
		
		
		
		if (!mMenu.isShowing())
		try {
			mMenu.setMenuItems(menuItems);
		} catch (Exception e) {
			AlertDialog.Builder alert = new AlertDialog.Builder(this);
			alert.setTitle("Error!");
			alert.setMessage(e.getMessage());
			alert.show();
		}
	}
	
	/** Toggle our menu on user pressing the menu key.
     * 
     */
	private void doMenu() {
		if (mMenu.isShowing()) {
			mMenu.hide();
		} else {
			
			//Show the remove item only if an item is selected
			CustomMenuItem removeRemoveItem = mMenu.getMenuItem(MENU_ITEM_REMOVE);
			removeRemoveItem.setVisible(mAddingNew || mDodgeList.getSelectedItemPosition() > -1 || mBurnList.getSelectedItemPosition() > -1);
			
			mMenu.getMenuItem(MENU_GRADE_REMOVE).setVisible(mGradeLayout.getChildCount() > 1);
			mMenu.getMenuItem(MENU_GRADE_ADD).setVisible(mGradeLayout.getChildCount() < MAXIMUN_NB_GRADE);
			
			
			mMenu.getMenuItem(MENU_ITEM_SCAN).setVisible( DarkroomTimerPrefs.haveBluetooth(this) );
			
			//Note it doesn't matter what widget you send the menu as long as it gets view.
			mMenu.show(findViewById(R.id.printingtime_display));
		}
	}
	
	/**
	 * Save the print in the database by sending it to the darkroom manager
	 */
	private void savePrint() {
		showDialog(DIALOG_SAVE_ID);
	}

	/**
	 * Add a new exposure to the print
	 */
	private int addExposure(float inExposureGrade) {
		final int lIndex = addGrade(inExposureGrade);
		mPrint.addExposure(new Exposure(inExposureGrade), lIndex);
		fillEmptyList(mPrint.getExposure(lIndex).getDodgeExposureItems(),3, ExposureItem.DODGEEXPOSURE);
        fillEmptyList(mPrint.getExposure(lIndex).getBurnExposureItems(),3, ExposureItem.BURNEXPOSURE);
        //changedExposure(lIndex);
        return lIndex;
	}
	
	private int addGrade(float inGradeValue) {
		int lIndex = 0;
		if(mPrint.getExposures().size() > 0)
			lIndex = mPrint.getExposures().indexOf(mPrint.getCurrentExposure()) + 1;

		addGrade(inGradeValue, lIndex);
		return lIndex;
	}
	
	/**
	 * Add a new existing exposure grade
	 */
	private void addGrade(float inGradeValue, int inIndex) {
		WheelViewCustomizable lGrade = new WheelViewCustomizable(this);
        lGrade.setViewAdapter(mGradeAdapter);
        lGrade.setBackgroundDrawable(R.drawable.safe_wheel_bg);
        lGrade.setCenterDrawable(R.drawable.safe_wheel_val);
        lGrade.setShadowColor(new int[] { 0xFF111111,	0x00444444, 0x00444444 });
        if ((getResources().getConfiguration().screenLayout & 
        	    Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_SMALL) {
        	lGrade.setVisibleItems(3);
        } else {
        	lGrade.setVisibleItems(5);	
        }
        
        
        lGrade.setCurrentItem(mGradeAdapter.getValueIndex(inGradeValue));
        lGrade.addClickingListener(mExposureChangedListener);
        lGrade.addChangingListener(mGradeChangedListener);
        
        mGradeLayout.addView(lGrade, inIndex);
        
        ViewGroup.LayoutParams lParameters = lGrade.getLayoutParams();
        lParameters.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 50, getResources().getDisplayMetrics());
        lGrade.setLayoutParams(lParameters);
	}
	
	private void removeGrade() {
		if(mGradeLayout.getChildCount() > 1) {
			final int lIndex = mPrint.getExposures().indexOf(mPrint.getCurrentExposure());
			mGradeLayout.removeViewAt(lIndex);
			mPrint.getExposures().remove(lIndex);
			changedExposure(lIndex > 0 ? lIndex-1 : 0);
		}
	}
	
    /** Add a new dodging exposure
     *  
     */
    private void addNewDodgeItem() {
		mAddingNew = true;
		ExposureItem newItem = new ExposureItem(mDodgeAdapter.getCount()+1,ExposureItem.DODGEEXPOSURE,"", new Fraction(0));
		mPrint.getCurrentExposure().getDodgeExposureItems().add(newItem);
		mDodgeAdapter.notifyDataSetChanged();
		mAddingNew = false;;
    }
    
    /** Add a new burning exposure
     * 
     */
	private void addNewBurnItem() {
	    mAddingNew = true;
		ExposureItem newItem = new ExposureItem(mBurnAdapter.getCount()+1,ExposureItem.BURNEXPOSURE,"", new Fraction(0));
		mPrint.getCurrentExposure().getBurnExposureItems().add(newItem);
	    mBurnAdapter.notifyDataSetChanged();
	    mAddingNew = false;;
	}
	

	/** Remove the selected exposure
	 * 	This function will remove the selected exposure from
	 * 	the dodging or burning list.
	 */
	private void removeItem() {
		
		if(mBurnList.getSelectedItemPosition() >= 0) {
			mPrint.getCurrentExposure().getBurnExposureItems().remove(mBurnList.getSelectedItemPosition());
		  	mBurnAdapter.notifyDataSetChanged();
		} 
		else if(mDodgeList.getSelectedItemPosition() >= 0) {
			mPrint.getCurrentExposure().getDodgeExposureItems().remove(mDodgeList.getSelectedItemPosition());
	    	mDodgeAdapter.notifyDataSetChanged();
		}
		  
	}
	
	/** Update base exposure time
	 * 	This function is call display the new base exposure time
	 * 	after base exposure time picker as been modified.
	 */
	private void updateTimeDisplay() {
		mBaseTimeDisplay.setText(String.format("%.1f",Exposure.getTime(mPrint.getCurrentExposure().getBaseExposure())));
	}
	
	/** Start the timer activity
	 * 	When the start button is triggered, the ExposureTimer activity is started.
	 *  The current exposure is pass to the timer.
	 */
	private void startstopTimer() {
		Intent lIntent = new Intent(this, ExposureTimer.class);
		lIntent.putExtra("exposure", mPrint.getCurrentExposure());
		final Fraction lIncrement =  mIncrementPicker.getCurrent();
		lIntent.putExtra("increment", (Parcelable) lIncrement );
      
		final CheckBox lTestStripBox = (CheckBox) findViewById(R.id.TestStripExposureBox);
		
				
		int lMode = ExposureTimer.MODE_NORMAL;
		boolean lSkipBase = false;
		if (lTestStripBox.isChecked()) {
			lMode = ExposureTimer.MODE_TESTSTRIPE;
		} 
		if (mSkipBaseExposureBox.isChecked()) {
			lSkipBase = true;
		}
		lIntent.putExtra("mode", lMode);
		lIntent.putExtra("skipbase", lSkipBase);

		try {
			startActivity(lIntent);
		} catch (Exception e) {
			AlertDialog.Builder alert = new AlertDialog.Builder(this);
			alert.setTitle("Error!");
			alert.setMessage(e.getMessage());
			alert.show();
		}
	}
	
	
	/** Start the preference screen
	 */
	protected void displaySettings() {
		showDialog(DIALOG_SETTING_ID);
//        startActivity(new Intent(this, DarkroomTimerPrefs.class));
    }
	

	

	/** Start the devtimer activity
	 * 	When the start button is triggered, the DevelopingTimer activity is started.
	 */
	private void startDevTimer() {
		Intent lIntent = new Intent(this, DevelopingTimer.class);
		try {
			startActivity(lIntent);
		} catch (Exception e) {
			AlertDialog.Builder alert = new AlertDialog.Builder(this);
			alert.setTitle("Error!");
			alert.setMessage(e.getMessage());
			alert.show();
		}
	}
	
}

