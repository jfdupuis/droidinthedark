package com.darkroom.ftimer;

import com.darkroom.ftimer.R;
import com.jfdupuis.library.screen.ScreenUtil;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

public class DevelopingTimer extends Activity implements OnClickListener {
	private static final int DRIFT_CORRECTION_INTERVAL = 5;
	
	private float mDriftStartValue = 0;
	private int mDriftCounter = 0;
	
	private PowerManager.WakeLock mWakeLock = null;
	
	private static MediaPlayer mLapsSound = null;
	private static MediaPlayer mMetronomSound = null;
    private static MediaPlayer mFinishSound = null;
    
    private int mTimerLapsSoundInterval = 30000;
    private int mMetronomInterval = 1000;
    private boolean mHaveMetronom = true;
    private boolean mHaveSounds = true;
    private boolean mFinished = false;
    private boolean mRunning = false;
    private Timer mTimer = null;
    
    View mStartButton = null;
    View mResetButton = null;
    
    private float mRemainingSeconds = 0; //[sec]
	
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.devtimer);
        
        setVolumeControlStream(AudioManager.STREAM_MUSIC);

        loadState();
        initializeSounds();
        initializeButtonListeners();
        initializeTimerValues();
        
        
        updateDisplay();
    }
	
	/**
	 * While exposing, the activity should not be restarted
	 */
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		Logger.v("Configuration Changed");
		super.onConfigurationChanged(newConfig);
		updateTimeHandler.sendEmptyMessage(0);
	}
	
	@Override
    protected void onResume() {
        super.onResume();
        acquireWakeLock();
        ScreenUtil.setBrightness(DevelopingTimer.this,(float)(DarkroomTimerPrefs.getScreenBrightness(this))/100.0f, 0);
    }
	
	@Override
    protected void onPause() {
        super.onPause();

        synchronized(this) {
            cancelTimer();
            releaseWakeLock();
        }
        
    }
	
	/**
	 * Avoid the device to go to sleep while computing the exposure.
	 */
	private void acquireWakeLock() {
        if (mWakeLock == null) {
            Logger.v("Acquiring wake lock");
            PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, this.getClass().getCanonicalName());
            mWakeLock.acquire();
        }
    }
	
	/**
	 * Once the exposre computing task is done, the device can sleep again.
	 */
    private void releaseWakeLock() {
        if (mWakeLock != null && mWakeLock.isHeld()) {
            Logger.v("Releasing wake lock");
            mWakeLock.release();
            mWakeLock = null;
        }
    }
	
    /**
     * Handle the back key for proper exit.
     */
	@Override
    public boolean onKeyDown(int keyCode, KeyEvent keyEvent) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
        	Logger.d("Back button pressed");
            // Shutdown the timer when back is pressed.  Let the framework
            // handle the back behavior.
        	shutdownTimer();
        }
        
        return super.onKeyDown(keyCode, keyEvent);
    }
	
	/**
	 * Handle the start/stop button click
	 */
	public void onClick(View v) {
        switch (v.getId()) {
	        case R.id.timer_startstop_button:
	            processStartStopButtonClick();
	            break;
	        case R.id.timer_reset_button:
	            processResetButtonClick();
	            break;
	        default:
	        	Logger.d("Ignored click.");
	        	break;
        }

    }

	/**
	 * Initialize the sound resources
	 */
    private void initializeSounds() {
        if (mMetronomSound == null && mHaveMetronom && mHaveSounds) {
            Logger.v("Loading the metronom sound");
            mMetronomSound = MediaPlayer.create(this, R.raw.metronom_klack);
        }
        
        if (mLapsSound == null && mTimerLapsSoundInterval > 0 && mHaveSounds) {
            Logger.v("Loading the laps sound");
            mLapsSound = MediaPlayer.create(this, R.raw.metronom_pling);
        }

        if (mFinishSound == null && mHaveSounds) {
            Logger.v("Loading the finish sound");
            mFinishSound = MediaPlayer.create(this, R.raw.metronom_pling); 
            mFinishSound.setOnErrorListener(new OnErrorListener(){
				public boolean onError(MediaPlayer mArg0, int mArg1, int mArg2) {
					Logger.v("Finishing sound error");
					if(mFinished)
                		terminateTimer();
					return false;
				}
            });
            
            mFinishSound.setOnCompletionListener(new OnCompletionListener(){
                public void onCompletion(MediaPlayer arg0) {
               		//setNextExposure();
                	if(mFinished)
                		terminateTimer();
                }
            });
        }
    }

    private void initializeButtonListeners() {
        mStartButton = findViewById(R.id.timer_startstop_button);
        mStartButton.setOnClickListener(this);
        
        mResetButton = findViewById(R.id.timer_reset_button);
        mResetButton.setOnClickListener(this);
    }
    
    private void initializeTimerValues() {
    	
    }
    
    
	private Handler updateTimeHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            updateDisplay();
        }
    };
    
    /**
     * Update the displayed time
     */
    protected synchronized void updateDisplay() {
    	//Logger.d(String.format("Update time display to: %f", mRemainingSeconds));
        TextView lTotalTimeRemaining = (TextView) findViewById(R.id.timer_printingtime_display);
        lTotalTimeRemaining.setText(String.format("%.1f", mRemainingSeconds));
    }
    
    /**
     * Start the exposure timers
     * 
     * stopTimer: This timer is used to trigger the end of the exposure.
     * This timer will not be used if connected to and external device. 
     * 
     * updateTimerValue: This timer is used to update the time display
     * on the user interface. Drift on this timer will not affect the 
     * overall exposure.
     * 
     * correctDisplayDrift: As the displayed time can drift due to 
     * delay in the timer callback, this can be disturbing to the 
     * user. Therefore, computation of the actual exposed time on
     * longer interval can improve the accuracy of the displayed
     * time.
     * 
     * playMetronomSound: Play a metronom sound on a regular basis.
     * 
     * If the timer is connect to an external microcontroller, this
     * method will start this timer. The end of exposure will be 
     * handled by the external microcontroller for improved accuracy.
     * 
     */
    private synchronized void startTimer() {
        Logger.d("Starting a new timer");

        TimerTask correctDisplayDriftTask = new TimerTask() {
            @Override
            public void run() {
            	correctTimerDrift();
            }
        };
        
        TimerTask updateTimerValuesTask = new TimerTask() {
            @Override
            public void run() {
                updateTimerValues();
            }
        };
        TimerTask playMetronomSoundTask = new TimerTask() {
            @Override
            public void run() {
            	playMetronomSound();
            }
        };
        TimerTask playLapsSoundTask = new TimerTask() {
            @Override
            public void run() {
            	playLapsSound();
            }
        };
//        TimerTask stopTimerTask = new TimerTask() {
//            @Override
//            public void run() {
//            	exposureEnd();
//            }
//        };
        
        mTimer = new Timer();
        mDriftCounter = 0;
        mDriftStartValue = mRemainingSeconds;
        mTimer.schedule(correctDisplayDriftTask, 0, 1000*DRIFT_CORRECTION_INTERVAL);
        
//        if(mExposureDuration.getValue() > 0) {
//    		Logger.v("Starting software timer");
//    		mTimer.schedule(stopTimerTask, mExposureDuration.getValue());	
//        }
        
        mTimer.schedule(updateTimerValuesTask, 100, 100);
        if(mHaveSounds && mHaveMetronom)
        	mTimer.schedule(playMetronomSoundTask, 0, mMetronomInterval);
        if(mHaveSounds && mTimerLapsSoundInterval > 0) {
        	Logger.d(String.format("Scheduling laps sound to %d",mTimerLapsSoundInterval));
        	mTimer.schedule(playLapsSoundTask, mTimerLapsSoundInterval, mTimerLapsSoundInterval);
        }
        	
        
        mRunning = true;
    }
    

    
    /**
     * Correct displayed time.
     * Sounds and display refresh can drift due to delay in the execution of
     * the time display update which decrease the remaining time. Using longer
     * timed interval reduce the accumulated delays error.
     */
    private synchronized void correctTimerDrift() {
 		Logger.d("Correcting Timer Drift, compute longer time lapse");
		//Compute remaining time from longer interval
		mRemainingSeconds = mDriftStartValue + (mDriftCounter++)*DRIFT_CORRECTION_INTERVAL;
    }
    
    /**
     * Stops the running timers
     * Once stopped, the remaining time is updated from the value of the 
     * physical enlarger timer value.
     */
    private synchronized void cancelTimer() {
    	Logger.v("Cancelling timer");
        if (mTimer != null) {
            //Logger.v("Cancelling timer, stopping software timer");
            mTimer.cancel();
            mTimer = null;
        }
        mRunning = false;
    }
    
//    /**
//     * The end of an exposure have been reach.
//     * Timers are stop and the remaining time is corrected in order to give the
//     * next exposure the correct starting time. This starting time need to be
//     * corrected according to the print sequence as errors might have accumulate.
//     */
//    private synchronized void exposureEnd() {
//    	if(mRunning){
//	    	cancelTimer();
//    	}
//    }

    /**
     * Software timer value update.
     * Update the time display and serve as a timer count down. However, the
     * end of the exposure is determined by an independent timer.
     */
    protected synchronized void updateTimerValues() {
        mRemainingSeconds+= 0.1;
        updateTimeHandler.sendEmptyMessage(0);
    }
    
    
    /**
     * Handle the termination of this activity
     */
    private void terminateTimer() {
    	shutdownTimer();
    	finish();
    }
    
    /**
     * Free resources prior to shutdown.
     */
    private synchronized void shutdownTimer() {
    	cancelTimer();
        destroySounds();
        mFinished = true;
    }

    /**
     * Free sound resources
     */
    private static void destroySounds() {
    	Logger.d("destroying sounds");
    	if (mMetronomSound != null) {
	    	mMetronomSound.stop();
	    	mMetronomSound.release();
	    	mMetronomSound = null;
    	}
    	
    	if (mLapsSound != null) {
	    	mLapsSound.stop();
	    	mLapsSound.release();
	    	mLapsSound = null;
    	}
    	
    	if(mFinishSound != null) {
	    	mFinishSound.stop();
	    	mFinishSound.release();
	    	mFinishSound = null;
    	}
    }

    protected synchronized void playMetronomSound() {
    	Logger.d("Playing metronome sound");
    	if(mMetronomSound != null)
    		playSound(mMetronomSound);
    }

    protected synchronized void playLapsSound() {
    	Logger.d("Playing laps sound");
    	if(mLapsSound != null)
    		playSound(mLapsSound);
    }
    
    protected synchronized void playFinishedSound() {
    	Logger.d("Playing finish sound");
    	if(mFinishSound != null)
    		playSound(mFinishSound);
    }

    private synchronized void playSound(MediaPlayer mp) {
    	try{    		
    		 mp.seekTo(0);
    		 mp.start();
    	} catch(Exception e){
			Logger.v("Exception catched: " + e.getMessage());
			if(mFinished)
				terminateTimer();
		}
    }
    
    /**
     * Handle the triggering of the reset button
     */
    private synchronized void processResetButtonClick () {
    	boolean lIsRunning = mRunning;
    	if(lIsRunning){
    		cancelTimer();
    	}
    	mRemainingSeconds = 0;
    	updateTimeHandler.sendEmptyMessage(0);
    	if(lIsRunning) {
    		startTimer();
    	}
	}
    
    /**
     * Handle the triggering of the start/stop button
     */
    private synchronized void processStartStopButtonClick() {
    	if(mRunning){
    		cancelTimer();
    	}
    	else{
    		startTimer();
    	}
	}

    /**
     * Load the saved preference settings
     */
    protected synchronized void loadState() {
    	mHaveMetronom = DarkroomTimerPrefs.playMetronomSounds(this);
    	mMetronomInterval = DarkroomTimerPrefs.getMetronomTime(this);
    	mHaveSounds = DarkroomTimerPrefs.playSounds(this);
    	mTimerLapsSoundInterval = DarkroomTimerPrefs.getDevTimerLapsTime(this)*1000;
    }
    

    
    // The Handler that gets information back from the bluetooth communication thread
    protected class TimerHandler extends EnlargerHandler {
    	TimerHandler(Context inContext) {
			super(inContext);
		}
    };
    
}
