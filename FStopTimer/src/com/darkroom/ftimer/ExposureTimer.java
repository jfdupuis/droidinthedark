package com.darkroom.ftimer;

import com.darkroom.library.Exposure;
import com.darkroom.library.ExposureItem;
import com.jfdupuis.library.screen.ScreenUtil;
import com.jfdupuis.bluetooth.BluetoothChatThreads;
import com.jfdupuis.bluetooth.BluetoothService;
import com.darkroom.ftimer.Logger;
import com.jfdupuis.bluetooth.R;
import com.jfdupuis.bluetooth.arduino.ArduinoServiceBinder;
import com.jfdupuis.library.math.Fraction;

import android.bluetooth.BluetoothAdapter;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.PowerManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

public class ExposureTimer extends ArduinoServiceBinder implements OnClickListener/*, OnMenuItemSelectedListener*/ {
	
	private static final int DRIFT_CORRECTION_INTERVAL = 5;
	
	public static final int MODE_NORMAL = 0;
	public static final int MODE_TESTSTRIPE = 1;
	
	private static final int EXPOSURE_BASE = 0;
	private static final int EXPOSURE_BURN = 1; //treated as base
	private static final int EXPOSURE_DODGE = 2;
	
	private int mMode = MODE_NORMAL;
	private boolean mSkipBaseExposure = false;
	private Exposure mExposure = null;
	private Fraction mIncrement = null;
	
	private float mDriftStartValue = 0;
	private int mDriftCounter = 0;
	
	private PowerManager.WakeLock mWakeLock = null;
	
	private static MediaPlayer mMetronomSound = null;
    private static MediaPlayer mFinishSound = null;
    
    private int mLampDelayCompensation = 0; //Milliseconds
    private int mMetronomInterveal = 1000; //Milliseconds
    private boolean mHaveMetronom = true;
    private boolean mHaveSounds = true;
    private boolean mFinished = false;
    private boolean mRunning = false;
    private Timer mTimer = null;
    
    View mStartButton = null;
    View mFocusButton = null;
    private boolean mFocusOn = false;
    
    //Exposure in milliseconds
    private float [] mBurnTimeSequence; 
    private float [] mBaseTimeSequence;
    private float mBaseExposure;
    
    private int mExposureCount = 0;
    private int mExposureTotal = 0;
    private float mRemainingSeconds = 0; //[sec]
    private float mExposedTime = 0; //[sec]
    
    
    private String mExposureDescription = null;
    ExposureDuration mExposureDuration = new ExposureDuration();
   
  
    //// Bluetooth related
    private int mExpectUpdate = 0;
    private boolean mHaveBluetooth = false;
    
    
    
    /**
     * Encapsulate the exposure duration computation and assignment
     */
    class ExposureDuration {
    	 private long mmExposureDuration = 0; //milliseconds
    	 private long mmBaseExposureDuration = 0; //milliseconds
    	 private int mmExposureType = 0;
    	 
    	 public void reset() {
    		 if(mHaveBluetooth) {
	    		 long lTmpBaseExposure = mmBaseExposureDuration; //Since modified in setExpDur
	    		 if((mExposureCount == 0) && (mmExposureType != EXPOSURE_BASE) && (mmExposureType != EXPOSURE_BURN)) {
	    			 Logger.v(String.format("Resend device base exposure time to %d",mmExposureDuration));
	    			 sendData('b',mmBaseExposureDuration);
	    		 } 
	    		 setExposureDuration(mmExposureDuration,mmExposureType);
	    		 mmBaseExposureDuration = lTmpBaseExposure;
    		 }
    	 }
    	 
    	 public long getValue() { return mmExposureDuration; }
    	 
    	 public void setExposureDuration(long inValue, int inExposureType) {
	    	mmExposureDuration = inValue;
	    	mmExposureType = inExposureType;
	    	
	    	if(mHaveBluetooth) {
	    		//Update device remaining time
	    		switch(inExposureType) {
	    		case EXPOSURE_BASE:
	    		case EXPOSURE_BURN:
	    			Logger.v(String.format("Send device base exposure time to %d",mmExposureDuration));
	        		sendData('b',mmExposureDuration);
	        		mmBaseExposureDuration = mmExposureDuration; 
	        		break;
	    		case EXPOSURE_DODGE:
	    			Logger.v(String.format("Send device dodge exposure time to %d",mmExposureDuration));
	        		sendData('d',mmExposureDuration);
	        		break;
	    		}
	    		Logger.v("Getting time update after exposure duration set");
	    		requestTimeUpdate();
	    	}
	    }
    };
    
//    @Override
    protected void displayMessage(String inMessage, int inLength) {
		final LayoutInflater inflater = getLayoutInflater();
		View layout = inflater.inflate(R.layout.toast_layout,
		                               (ViewGroup) findViewById(R.id.toast_layout_root));
//		ImageView image = (ImageView) layout.findViewById(R.id.image);
//		image.setImageResource(R.drawable.android);
		TextView text = (TextView) layout.findViewById(R.id.text);
		text.setText(inMessage);
		
		Toast toast = new Toast(getApplicationContext());
//		toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
		toast.setDuration(inLength);
		toast.setView(layout);
		toast.show();
	}
    
    public ExposureTimer() {
    	
    	mConnection = new ServiceConnection() {
			public void onServiceConnected(ComponentName className, IBinder service) {
				Logger.d("onServiceConnected");
				mBTService = ((BluetoothService.LocalBinder) service).getService();

				// Ignore any related bluetooth functionality if the device
				// have no bluetooth
				if (!mBTService.haveBluetooth()) {
					Logger.d("Bluetooth is not available");
					return;
				}

				// If BT is not on, request that it be enabled.
				// setupChat() will then be called during onActivityResult
				if (!mBTService.getBluetoothAdapter().isEnabled()) {
					Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
					startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
				} else {
					mBTService.setupChat(mHandler);
				}
				
		        //Display a message to the user that want to use bluetooth
		        //on a device that doesn't have bluetooth
		        if(mHaveBluetooth && !haveBluetooth() ) {
		        	Logger.d("mHaveBluetooth == " + mHaveBluetooth + ", haveBluetooth() == " + haveBluetooth());
		        	
		        	displayMessage(getString(R.string.bt_not_available),Toast.LENGTH_LONG);
		        	mHaveBluetooth = false;
		        }
		        
		        //Check if a device is connected
		        if( mBTService.getState() != BluetoothChatThreads.STATE_CONNECTED ) {
		        	displayMessage(getString(R.string.bt_not_device_connected), Toast.LENGTH_LONG);
		        	mHaveBluetooth = false;
		        }

				// Tell the user about this for our demo.
				mStartButton.setEnabled(true);
				//displayMessage(getString(R.string.bt_service_connected), Toast.LENGTH_SHORT);
				
				setNextExposure();
			}

			public void onServiceDisconnected(ComponentName className) {
				mBTService = null;
				//displayMessage.makeText(ExposureTimer.this, R.string.bt_service_disconnected, Toast.LENGTH_SHORT).show();
			}
		};
    }
    

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.timer);
        
        setVolumeControlStream(AudioManager.STREAM_MUSIC);

        loadState();
        initializeSounds();
        initializeButtonListeners();
        initializeTimerValues();
        
        
        updateDisplay();
    }
	
	/**
	 * While exposing, the activity should not be restarted
	 */
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		Logger.v("Configuration Changed");
		super.onConfigurationChanged(newConfig);
		updateTimeHandler.sendEmptyMessage(0);
    	updateDescriptionHandler.sendEmptyMessage(0);
	}
	
	@Override
    protected void onResume() {
        super.onResume();
        acquireWakeLock();
        ScreenUtil.setBrightness(ExposureTimer.this,(float)(DarkroomTimerPrefs.getScreenBrightness(this))/100.0f, 0);
        
        
		// Initialise bluetooth
		if (mHaveBluetooth) {
			mStartButton.setEnabled(false);
			mHandler = new TimerHandler(ExposureTimer.this);
			bind();
        } else {
        	setNextExposure(); //With BT, this is called in onServiceConnected
        }
    }
	
	@Override
    protected void onPause() {
        super.onPause();

        synchronized(this) {
            cancelTimer();
            releaseWakeLock();
        }
        
        //Unbind the bluetooth service
        unbind();
    }
	
	@Override 
    protected void onDestroy() {
    	super.onDestroy();
    	unbind(); //Unbind the bluetooth service
    }
	
	/**
	 * Avoid the device to go to sleep while computing the exposure.
	 */
	private void acquireWakeLock() {
        if (mWakeLock == null) {
            Logger.v("Acquiring wake lock");
            PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, this.getClass().getCanonicalName());
            mWakeLock.acquire();
        }
    }
	
	/**
	 * Once the exposre computing task is done, the device can sleep again.
	 */
    private void releaseWakeLock() {
        if (mWakeLock != null && mWakeLock.isHeld()) {
            Logger.v("Releasing wake lock");
            mWakeLock.release();
            mWakeLock = null;
        }
    }
	
    /**
     * Handle the back key for proper exit.
     */
	@Override
    public boolean onKeyDown(int keyCode, KeyEvent keyEvent) {
//		if (keyCode == KeyEvent.KEYCODE_MENU) {
//			Logger.d("Menu button pressed");
//	    	doMenu();
//	    	return true; //always eat it!
//	    }
		
        if (keyCode == KeyEvent.KEYCODE_BACK) {
        	Logger.d("Back button pressed");
            // Shutdown the timer when back is pressed.  Let the framework
            // handle the back behavior.
        	shutdownExposureTimer();
        }
        
        return super.onKeyDown(keyCode, keyEvent);
    }
	
	/**
	 * Handle the start/stop button click
	 */
	public void onClick(View v) {
        switch (v.getId()) {
	        case R.id.timer_startstop_button:
	            processStartStopButtonClick();
	            break;
	        case R.id.enlargerlight_button:
	        	processFocusButtonClick();
	            break;
        }

    }

	/**
	 * Initialize the sound resources
	 */
    private void initializeSounds() {
        if (mMetronomSound == null && mHaveMetronom && mHaveSounds) {
            Logger.v("Loading the metronom sound");
            mMetronomSound = MediaPlayer.create(this, R.raw.metronom_klack);
        }

        if (mFinishSound == null && mHaveSounds) {
            Logger.v("Loading the finish sound");
            mFinishSound = MediaPlayer.create(this, R.raw.metronom_pling); 
            mFinishSound.setOnErrorListener(new OnErrorListener(){
				public boolean onError(MediaPlayer mArg0, int mArg1, int mArg2) {
					Logger.v("Finishing sound error");
					if(mFinished)
                		terminateExposureTimer();
					return false;
				}
            });
            
            mFinishSound.setOnCompletionListener(new OnCompletionListener(){
                public void onCompletion(MediaPlayer arg0) {
               		//setNextExposure();
                	if(mFinished)
                		terminateExposureTimer();
                }
            });
        }
    }

    private void initializeButtonListeners() {
        mStartButton = findViewById(R.id.timer_startstop_button);
        mStartButton.setOnClickListener(this);
        
        mFocusButton = findViewById(R.id.enlargerlight_button);
        mFocusButton.setOnClickListener(this);
        if(!mHaveBluetooth) {
        	//If bluetooth is not available, focus is not usefull
        	Logger.d("Desactivating focus button.");
        	mFocusButton.setVisibility(View.GONE);
        	mFocusButton.setEnabled(false);
        }
        mFocusButton.setEnabled(true);
        
    }

    /**
     * Create the exposure time sequence.
     */
    private void initializeTimerValues() {
    	try{
	    	mExposure = getIntent().getParcelableExtra("exposure");
	    	mIncrement = getIntent().getParcelableExtra("increment");
	    	mMode = getIntent().getIntExtra("mode", 0);
	    	mSkipBaseExposure = getIntent().getBooleanExtra("skipbase", false);
	    	Logger.v( String.format("ExposureTimer: Mode: %d Skipbase: %s Increment: "+mIncrement.toString(), mMode, String.valueOf(mSkipBaseExposure)));
	        
	        mRemainingSeconds =  Exposure.getTime(mExposure.getBaseExposure());
	        
	        mBaseExposure = mRemainingSeconds;
	    	mBaseTimeSequence = mExposure.getBaseTimeSequence();
	    	mBurnTimeSequence = mExposure.getBurnTimeSequence();

	    	//Apply lamp fade compensation.
	    	if(mLampDelayCompensation > 0) {
	    		float lCompensation = mLampDelayCompensation/1000.0f;
	    		mBaseExposure += lCompensation; //Correction of the total exposure time
	    		Logger.d(String.format("Base time corrected from %f to %f",mBaseExposure-lCompensation,mBaseExposure));
	    		if(mBaseTimeSequence.length > 0) {
	    			//dodge should not have any compensation as the dodging card is not fading in or out.
	    			mBaseTimeSequence[0] += lCompensation; //Apply correction in the first base exposure
	    		}
	    		if(mBurnTimeSequence.length > 0) {
	    			for(int i = 0; i < mBurnTimeSequence.length; ++i) {
	    				mBurnTimeSequence[i] += lCompensation;
	    				Logger.d(String.format("Burn time corrected from %f to %f",mBurnTimeSequence[i] - lCompensation, mBurnTimeSequence[i]));
	    			}
	    		}
	    	}
	    	
	    	mExposureTotal = 1 + mBaseTimeSequence.length + mBurnTimeSequence.length;
	    	mExposureCount = 0;
	    	
	    	if(mSkipBaseExposure) {
	    		mExposureCount = 1 + mBaseTimeSequence.length;
	    		if((mExposureCount >= mExposureTotal) && (mMode != MODE_TESTSTRIPE) ) {
	    			displayMessage(getString(R.string.error_noburnexposure), Toast.LENGTH_SHORT);
	    			mFinished = true;
	    			terminateExposureTimer();
	    		}
	    		if(mMode == MODE_TESTSTRIPE)
	    			mExposureCount = 1;
	    	}
	    	
    	} catch(RuntimeException inException) {
    		displayMessage(inException.getMessage(), Toast.LENGTH_SHORT);
    		terminateExposureTimer();
    	}
    }
    
    /**
     * Orient the preparation of the next exposure according to the current 
     * operation mode.
     */
    private synchronized void setNextExposure() {
    	if(mMode == MODE_TESTSTRIPE)
    		setNextTestStripExposure();
    	else
    		setNextDBExposure();
    }
    
    /**
     * Prepare the next test strip exposure
     */
    private synchronized void setNextTestStripExposure() {
    	if(mExposureCount != 0) {
    		if(mHaveSounds) {
    			if(! (mSkipBaseExposure && mExposureCount == (1 + mBaseTimeSequence.length)) ) {
	    			try{
	    				playFinishedSound();
	    			} catch(IllegalStateException e){
	    				Logger.v("IllegalStateException Exception catched: " + e.getMessage());
	    			} 
	    			catch(Exception e){
	    				Logger.v("Exception catched: " + e.getMessage());
	    			}
    			}
    		}

    		final Fraction lNewBaseExposure =   mExposure.getBaseExposure().plus(mIncrement.times(mExposureCount-1));
	    	mRemainingSeconds = Exposure.getTime(lNewBaseExposure.plus(mIncrement)) - Exposure.getTime(lNewBaseExposure) + mLampDelayCompensation/1000.0f;
	    	mExposureDuration.setExposureDuration((long)(1000*mRemainingSeconds),EXPOSURE_BASE);
			Logger.v(String.format("Teststrip exposure %d of %.1f seconds", mExposureCount, mRemainingSeconds));
			
			mExposureDescription = String.format("%s %d", getString(R.string.teststrip_exposure), mExposureCount);
	    	
    	}
    	else {
    		Logger.v(String.format("Teststrip base exposure of %.1f seconds", mBaseExposure));
    		mExposureDescription = getString(R.string.teststrip_baseexposure);
			mExposedTime = 0;
			mRemainingSeconds = mBaseExposure; //Base exposure has lamp compensation already applied in initializeTimerValues
			mFinished = false;
			mExposureDuration.setExposureDuration((long)(1000*mRemainingSeconds),EXPOSURE_BURN);
			mRunning = false;
    	}
    	
    	Logger.v("setNextTestStripExposure update display");
    	updateTimeHandler.sendEmptyMessage(0);
    	updateDescriptionHandler.sendEmptyMessage(0);
    	
    }
    
    /**
     * Prepare the next normal exposure containing dodging and burning. 
     */
    private synchronized void setNextDBExposure() {
    	if(mExposureCount >= mExposureTotal) {
    		mFinished = true;
    		if(!mHaveSounds) {
    			terminateExposureTimer();
    		}
    	} 
    	
    	if(mExposureCount != 0 && mHaveSounds) {
			if(! (mSkipBaseExposure && mExposureCount == (1 + mBaseTimeSequence.length)) ) {
    			try{
    				playFinishedSound();
    			} catch(IllegalStateException e){
    				Logger.v("IllegalStateException Exception catched: " + e.getMessage());
    			} 
    			catch(Exception e){
    				Logger.v("Exception catched: " + e.getMessage());
    			}
			}
    	}
    	
    	
    	ExposureItem lItem;
    	mExposureDescription = "";
    	if(mExposureCount < mBaseTimeSequence.length) {
    		
    		if(mExposureCount == 0) {
    			Logger.v(String.format("Base exposure of %.1f seconds", mBaseTimeSequence[mExposureCount]));
    			
    			mRemainingSeconds = mBaseExposure;
    			mExposureDuration.setExposureDuration((long)(1000*mBaseExposure), EXPOSURE_BASE);
    			mExposureDuration.setExposureDuration((long)(1000*mBaseTimeSequence[mExposureCount]), EXPOSURE_DODGE);
    			mExposureDescription = getString(R.string.normal_baseexposure);
    			
    			mExposedTime = 0;
    			mFinished = false;
    			mRunning = false;

    		} else {
    			Logger.v(String.format("Dodge exposure of %.1f seconds", mBaseTimeSequence[mExposureCount]));
    			mExposureDuration.setExposureDuration((long)(1000*mBaseTimeSequence[mExposureCount]), EXPOSURE_DODGE);
    			lItem = mExposure.getDodgeExposureItems().get(mExposureCount-1);
    			mExposureDescription = String.format("%s %d", getString(R.string.normal_dodgeexposure), lItem.getPosition()+1);
    			if(lItem.getDescription() != null)
    				mExposureDescription += ": " + lItem.getDescription();
    		}
    		
    		
    	} else if(mExposureCount == mBaseTimeSequence.length) {
    		Logger.v(String.format("Last dodge exposure of %.1f seconds", mBaseExposure - mExposedTime)); //TODO with BT, mExposedTime is not updated
    		
    		if(mBaseTimeSequence.length == 0) {
    			mExposureDuration.setExposureDuration((long)(1000*(mBaseExposure - mExposedTime)), EXPOSURE_BASE); //TODO wrong value
    			mExposureDescription = "Base exposure";
    			mExposedTime = 0;
    			mRemainingSeconds = mBaseExposure;
    			mRunning = false;
    		} else {
    			mExposureDuration.setExposureDuration((long)(1000*(mBaseExposure - mExposedTime)), EXPOSURE_DODGE);
				lItem = mExposure.getDodgeExposureItems().get(mExposureCount-1);
				mExposureDescription = String.format("%s %d", getString(R.string.normal_dodgeexposure), lItem.getPosition()+1);
				if(lItem.getDescription() != null)
					mExposureDescription += ": " + lItem.getDescription();
    		}
			
    	} else if(mExposureCount < mBaseTimeSequence.length + mBurnTimeSequence.length + 1){
    		mRemainingSeconds = mBurnTimeSequence[mExposureCount - mBaseTimeSequence.length - 1];
    		mExposureDuration.setExposureDuration((long)(1000*mRemainingSeconds), EXPOSURE_BURN);
    		Logger.v(String.format("Burn exposure of %.1f seconds", mRemainingSeconds));
    		
    		lItem = mExposure.getBurnExposureItems().get(mExposureCount - mBaseTimeSequence.length - 1);
    		mExposureDescription = String.format("%s %d", getString(R.string.normal_burnexposure), lItem.getPosition()+1);
			if(lItem.getDescription() != null)
				mExposureDescription += ": " + lItem.getDescription();
    	} 


    	Logger.v("Text displayed: " + mExposureDescription);
    	

    	//Update display
    	Logger.v(String.format("setNextDBExposure update display with %f",mRemainingSeconds));
    	updateTimeHandler.sendEmptyMessage(0);
    	updateDescriptionHandler.sendEmptyMessage(0);
    } 
    
   
    private void requestTimeUpdate() {
    	++mExpectUpdate;
    	Logger.v(String.format("Requesting time update from bluetooth timer, %d", mExpectUpdate));
		sendData('u',true);
    }
    
    /**
     * Receive the remaining exposure time from remote device
     * @param inValue Number of milliseconds remaining
     */
    private synchronized void receiveTimeUpdate(long inValue) {
   		mRemainingSeconds = (inValue < 0) ? 0 : (float)(inValue)/1000;
   		Logger.d(String.format("Received time update: %d, remaining seconds: %f",inValue, mRemainingSeconds));
    	updateTimeHandler.sendEmptyMessage(0);

    }
    
    /**
     * Recompute the remaining exposure duration base on the exposure sequence and the remaining time.
     * 
     * The remaining time should have been corrected prior to call this method. This method is called 
     * after being stop in the middle of an exposure.
     */
    private synchronized void recomputeExposureDuration() {
    	if(mHaveBluetooth) {
    		//The chip has a more accurate timing, let do it determine time left
    		//However, will send light fade compensation
    		if(mLampDelayCompensation > 0) {
    			Logger.v(String.format("Send light fade compensation of %d",mLampDelayCompensation));
    			sendData('c',mLampDelayCompensation);
    			requestTimeUpdate();
    		}
    		return; //Handled by the arduino
    	} else {
	    	Logger.v(String.format("Recompute exposure duration, including a light fade compensation of %d", mLampDelayCompensation));
	    	mRemainingSeconds += mLampDelayCompensation/1000.0f; //Add light fade compensation
	    	if(mExposureCount < mBaseTimeSequence.length) {
	    		//We are in a base or dodge exposure time
	    		float lDone = mBaseExposure - mExposedTime - mRemainingSeconds; 
	    		mExposureDuration.setExposureDuration( (long)(1000*(mBaseTimeSequence[mExposureCount] - lDone)), (mExposureCount == 0) ? EXPOSURE_BASE : EXPOSURE_DODGE);
	    	} else {
	    		//We are in a burn exposure
	    		mExposureDuration.setExposureDuration((long)(1000*mRemainingSeconds + mLampDelayCompensation),EXPOSURE_BURN);
	    	}
    	}
    	updateTimeHandler.sendEmptyMessage(0);
    }
    
    /**
     * Get the correct remaining time according to the exposure sequence.
     * Called only at the end of an exposure.
     */
    private synchronized void correctRemaningTime() {
    	Logger.d(String.format("Correcting remaining time from %f",mRemainingSeconds));
    	if(mHaveBluetooth) {
    		requestTimeUpdate();
    	} else {
	    	if(mExposureCount < mBaseTimeSequence.length) {
	    		mExposedTime += mBaseTimeSequence[mExposureCount];
	    		mRemainingSeconds = mBaseExposure - mExposedTime;
	    	} else  {
	    		mRemainingSeconds = 0;
	    	}
    	}
    	Logger.d(String.format("New remaining time %f", mRemainingSeconds));
    	updateTimeHandler.sendEmptyMessage(0);
    }
    
	private Handler updateTimeHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            updateDisplay();
        }
    };
    
    private Handler updateDescriptionHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            updateDescription();
        }
    };

    /**
     * Update the displayed time
     */
    protected synchronized void updateDisplay() {
    	//Logger.d(String.format("Update time display to: %f", mRemainingSeconds));
        TextView lTotalTimeRemaining = (TextView) findViewById(R.id.timer_printingtime_display);
        lTotalTimeRemaining.setText(String.format("%.1f",mRemainingSeconds));
        final TextView lExposureDescription = (TextView) findViewById(R.id.exposure_description);
    	lExposureDescription.setText(mExposureDescription);
    }
    
    /**
     * Update the displayed exposure description
     */
    protected synchronized void updateDescription() {
        final TextView lExposureDescription = (TextView) findViewById(R.id.exposure_description);
    	lExposureDescription.setText(mExposureDescription);
    }

    /**
     * Start the exposure timers
     * 
     * stopTimer: This timer is used to trigger the end of the exposure.
     * This timer will not be used if connected to and external device. 
     * 
     * updateTimerValue: This timer is used to update the time display
     * on the user interface. Drift on this timer will not affect the 
     * overall exposure.
     * 
     * correctDisplayDrift: As the displayed time can drift due to 
     * delay in the timer callback, this can be disturbing to the 
     * user. Therefore, computation of the actual exposed time on
     * longer interval can improve the accuracy of the displayed
     * time.
     * 
     * playMetronomSound: Play a metronom sound on a regular basis.
     * 
     * If the timer is connect to an external microcontroller, this
     * method will start this timer. The end of exposure will be 
     * handled by the external microcontroller for improved accuracy.
     * 
     */
    private synchronized void startTimer() {
        Logger.d("Starting a new timer");

        TimerTask correctDisplayDriftTask = new TimerTask() {
            @Override
            public void run() {
            	correctTimerDrift();
            }
        };
        
        TimerTask updateTimerValuesTask = new TimerTask() {
            @Override
            public void run() {
                updateTimerValues();
            }
        };
        TimerTask playMetronomSoundTask = new TimerTask() {
            @Override
            public void run() {
            	playMetronomSound();
            }
        };
        TimerTask stopTimerTask = new TimerTask() {
            @Override
            public void run() {
            	exposureEnd();
            }
        };
        
        mTimer = new Timer();
        mDriftCounter = 0;
        mDriftStartValue = mRemainingSeconds;
        mTimer.schedule(correctDisplayDriftTask, 0, 1000*DRIFT_CORRECTION_INTERVAL);
        
        if(mExposureDuration.getValue() > 0) {
        	if(mHaveBluetooth) {
//        		Logger.v(String.format("Reset and starting bluetooth timer for an exposure of %d ms", mExposureDuration.getValue()));//TODO
//        		mExposureDuration.reset(); //TODO: not sure it's needed
        		sendData('s',true);
        	} else {
        		Logger.v(String.format("Starting software timer for %d milliseconds", mExposureDuration.getValue()));
        		mTimer.schedule(stopTimerTask, mExposureDuration.getValue());	
        	}
        } else {
        	throw new RuntimeException(String.format("Exposure duration <= 0: %d", mExposureDuration.getValue()));
        }
        mTimer.schedule(updateTimerValuesTask, 100, 100);
        if(mHaveSounds && mHaveMetronom)
        	mTimer.schedule(playMetronomSoundTask, 0, mMetronomInterveal);
        mRunning = true;
    }
    

    
    /**
     * Correct displayed time.
     * Sounds and display refresh can drift due to delay in the execution of
     * the time display update which decrease the remaining time. Using longer
     * timed interval reduce the accumulated delays error.
     */
    private synchronized void correctTimerDrift() {
    	Logger.d("Correcting Timer Drift");
    	if(mHaveBluetooth) {
    		Logger.d("Correcting Timer Drift, get time update from bluetooth");
    		requestTimeUpdate();
    	} else {
    		Logger.d("Correcting Timer Drift, computer longer time lapse");
    		//Compute remaining time from longer interval
    		mRemainingSeconds = mDriftStartValue - (mDriftCounter++)*DRIFT_CORRECTION_INTERVAL;
    		if(mRemainingSeconds < 0) mRemainingSeconds = 0;
    	}
    }
    
    /**
     * The end of an exposure have been reach.
     * Timers are stop and the remaining time is corrected in order to give the
     * next exposure the correct starting time. This starting time need to be
     * corrected according to the print sequence as errors might have accumulate.
     */
    private synchronized void exposureEnd() {
    	if(mRunning){
	    	cancelTimer();
			correctRemaningTime();
			mExposureCount++;
	   		setNextExposure();
    	}
    }

    /**
     * Stops the running timers
     * Once stopped, the remaining time is updated from the value of the 
     * physical enlarger timer value.
     */
    private synchronized void cancelTimer() {
    	Logger.v("Cancelling timer");
    	if(mHaveBluetooth) {
    		//Logger.v("Cancelling timer, stopping bluetooth timer");
    		sendData('x',true); //Stop remote timer and turn off the enlarger
    	}
    	
        if (mTimer != null) {
            //Logger.v("Cancelling timer, stopping software timer");
            mTimer.cancel();
            mTimer = null;
        }
        
        mRunning = false;
        
        //Get the correct remaining time.
        if(mHaveBluetooth) {
        	Logger.d("Requesting the correct remaining time after timer cancel");
        	requestTimeUpdate();
    	}
        
    }

    /**
     * Software timer value update.
     * Update the time display and serve as a timer count down. However, the
     * end of the exposure is determined by an independent timer.
     */
    protected synchronized void updateTimerValues() {
    	if (mRemainingSeconds > 0)
            mRemainingSeconds-= 0.1;
        if(mRemainingSeconds <= 0) {
        	mRemainingSeconds = 0;
        }
        updateTimeHandler.sendEmptyMessage(0);
    }
    
    
    /**
     * Handle the termination of this activity
     */
    private void terminateExposureTimer() {
    	shutdownExposureTimer();
    	finish();
    }
    
    /**
     * Free resources prior to shutdown.
     */
    private synchronized void shutdownExposureTimer() {
    	cancelTimer();
        destroySounds();
        mFinished = true;
    }

    /**
     * Free sound resources
     */
    private static void destroySounds() {
    	Logger.v("destroying sounds");
    	if (mMetronomSound != null) {
	    	mMetronomSound.stop();
	    	mMetronomSound.release();
	    	mMetronomSound = null;
    	}
    	
    	if(mFinishSound != null) {
	    	mFinishSound.stop();
	    	mFinishSound.release();
	    	mFinishSound = null;
    	}
    }

    protected synchronized void playMetronomSound() {
    	Logger.d("Playing metronome sound");
    	if(mMetronomSound != null)
    		playSound(mMetronomSound);
    }

    protected synchronized void playFinishedSound() {
    	Logger.d("Playing finish sound");
    	if(mFinishSound != null)
    		playSound(mFinishSound);
    }

    private synchronized void playSound(MediaPlayer mp) {
    	try{    		
    		 mp.seekTo(0);
    		 mp.start();
    	} catch(Exception e){
			Logger.v("Exception catched: " + e.getMessage());
			if(mFinished)
				terminateExposureTimer();
		}
    }
    
    /**
     * Handle the triggering of the start/stop button
     */
    private synchronized void processStartStopButtonClick() {
    	Logger.d("Disable focus button.");
    	mFocusButton.setEnabled(false); //Disable focus button after exposure started
    	if(mRunning){
    		cancelTimer();
    		recomputeExposureDuration();
    	}
    	else{
    		try{
    			startTimer();
    		} catch(RuntimeException e) {
    			Logger.e(e.getMessage());
    			terminateExposureTimer();
    		}
    	}
	}
    
    /**
     * Handle the triggering of the focus button
     */
    private synchronized void processFocusButtonClick() {
    	if(mFocusOn){
    		sendData('e',true); //Turn off the enlarger light
    		mStartButton.setEnabled(true); 
    		mFocusOn = false;
    	}
    	else {
    		mStartButton.setEnabled(false); //Don't allow start of the timer when focusing
    		sendData('E',true); //Turn on the enlarger light
    		mFocusOn =true;
    	}
    		
	}

    /**
     * Load the saved preference settings
     */
    protected synchronized void loadState() {
    	mHaveMetronom = DarkroomTimerPrefs.playMetronomSounds(this);
    	mMetronomInterveal = DarkroomTimerPrefs.getMetronomTime(this);
    	mHaveSounds = DarkroomTimerPrefs.playSounds(this);
    	mHaveBluetooth = DarkroomTimerPrefs.haveBluetooth(this); 
    	mLampDelayCompensation = DarkroomTimerPrefs.getEnlargerLampCompensationTime(this); 
    }
    

    
    // The Handler that gets information back from the bluetooth communication thread
    protected class TimerHandler extends EnlargerHandler {
    	TimerHandler(Context inContext) {
			super(inContext);
		}

		/**
    	 * Receive the remaining time
    	 */
		@Override
		public void messageReceived(String inMessage) {
			Logger.v("Received message: " + inMessage);
			try{
				long lValue = Long.valueOf(inMessage);
				if(lValue < 0) {
					//Receive end of exposure
					exposureEnd();
				} else if(mExpectUpdate > 0) { 
					Logger.v(String.format("Received time update, %d",mExpectUpdate));
					--mExpectUpdate;
					receiveTimeUpdate(lValue);
				} else {
					Logger.v("Ignoring unexpected value");
				}
			}
			catch(NumberFormatException inException) {
				Logger.e("Can't convert value to long");
			}
		}
    };
	
 
}
