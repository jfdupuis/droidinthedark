package com.darkroom.ftimer;

import com.darkroom.ftimer.R;
import com.jfdupuis.bluetooth.Logger;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;


public class DarkroomTimerPrefs extends PreferenceActivity {
	private static final String DECIMALS = "decimal";
    private static final boolean DECIMALS_DEFAULT = false;
	
	private static final String SOUNDS = "sounds";
    private static final boolean SOUNDS_DEFAULT = true;
    
    private static final String METRONOM_SOUNDS = "metronom_sounds";
    private static final boolean METRONOM_SOUNDS_DEFAULT = true;
    
    private static final String METRONOM_TIME = "metronom_intervals";
    private static final int METRONOM_TIME_DEFAULT = 1000; //In milliseconds
    
    private static final String DEVTIMER_TIME = "devtimer_laps_intervals";
    private static final int DEVTIMER_LAPSTIME_DEFAULT = 30; //In seconds
    
    private static final String LAMP_COMP_TIME = "enlarger_lamp_delay_compensation";
    private static final int LAMP_COMP_TIME_DEFAULT = 0; //In milliseconds
    
    
    private static final String SCREEN_BRIGHTNESS = "screen_brightness";
    private static final int SCREEN_BRIGHTNESS_DEFAULT = 50;
    
    private static final String USE_BLUETOOTH = "have_bluetooth";
    private static final boolean USE_BLUETOOTH_DEFAULT = false;
	
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.layout.settings);
        
        //Verify that bluetooth is available on this device.
        //Disable the relevant option accordingly.
        Preference lBluetoothPrefs = (Preference) findPreference(USE_BLUETOOTH);
        BluetoothAdapter lBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (lBluetoothAdapter == null) {
        	lBluetoothPrefs.setEnabled(false);
        	setHaveBlueTooth(DarkroomTimerPrefs.this,false);
        	lBluetoothPrefs.setSummary(R.string.no_bluetooth_enlarger_summary);
        }
        
        //Deactivate bluetooth, uncomment to disable bluetooth
//        lBluetoothPrefs.setEnabled(false);
//        lBluetoothPrefs.setSummary(R.string.no_bluetooth_version_summary);
    }
	
	public static boolean showDecimals(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(DECIMALS, DECIMALS_DEFAULT);
    }

    public static void setShowDecimals(Context context, boolean value) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(DECIMALS, value).commit();
    }
	
	
	public static boolean playSounds(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(SOUNDS, SOUNDS_DEFAULT);
    }

    public static void setPlaySounds(Context context, boolean value) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(SOUNDS, value).commit();
    }
    
    public static boolean playMetronomSounds(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(METRONOM_SOUNDS, METRONOM_SOUNDS_DEFAULT);
    }

    public static void setMetronomSounds(Context context, boolean value) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(METRONOM_SOUNDS, value).commit();
    }

    public static int getMetronomTime(Context context) {
        String value = PreferenceManager.getDefaultSharedPreferences(context).getString(METRONOM_TIME, Integer.toString(METRONOM_TIME_DEFAULT));
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
        	Logger.d("Error formating metronome time interval, setting back to default.");
            setMetronomTime(context, METRONOM_TIME_DEFAULT);
            return METRONOM_TIME_DEFAULT;
        }
    }
    public static void setMetronomTime(Context context, int inMetronomTime) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(METRONOM_TIME, Integer.toString(inMetronomTime)).commit();
    }
    
    public static int getDevTimerLapsTime(Context context) {
        String value = PreferenceManager.getDefaultSharedPreferences(context).getString(DEVTIMER_TIME, Integer.toString(DEVTIMER_LAPSTIME_DEFAULT));
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
        	Logger.d("Error formating dev timer laps time interval, setting back to default.");
        	setDevTimerLapsTime(context, DEVTIMER_LAPSTIME_DEFAULT);
            return DEVTIMER_LAPSTIME_DEFAULT;
        }
    }
    public static void setDevTimerLapsTime(Context context, int inTime) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(DEVTIMER_TIME, Integer.toString(inTime)).commit();
    }

    public static void setScreenBrightness(Context context, int inScreenBrightness) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putInt(SCREEN_BRIGHTNESS, inScreenBrightness).commit();
    }
    public static int getScreenBrightness(Context context) {
    	try{
    		return PreferenceManager.getDefaultSharedPreferences(context).getInt(SCREEN_BRIGHTNESS, SCREEN_BRIGHTNESS_DEFAULT);
    	} catch(ClassCastException inE) {
    		String value = PreferenceManager.getDefaultSharedPreferences(context).getString(SCREEN_BRIGHTNESS, Integer.toString(SCREEN_BRIGHTNESS_DEFAULT));
    		try {
                int lIntValue =  Integer.parseInt(value);
                PreferenceManager.getDefaultSharedPreferences(context).edit().putInt(SCREEN_BRIGHTNESS, lIntValue).commit();
                return lIntValue;
            } catch (NumberFormatException e) {
            	setScreenBrightness(context, SCREEN_BRIGHTNESS_DEFAULT);
                return SCREEN_BRIGHTNESS_DEFAULT;
            }
    	}
    }
    
    public static GradeCompensation getGradeCompensation(Context context) {
    	String lStrTable = PreferenceManager.getDefaultSharedPreferences(context).getString("grade_compensation", "");
    	return new GradeCompensation(lStrTable);
    	
    }
    
    public static boolean haveBluetooth(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(USE_BLUETOOTH, USE_BLUETOOTH_DEFAULT);
    }
    public static void setHaveBlueTooth(Context context, boolean value) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(USE_BLUETOOTH, value).commit();
    }

    public static int getEnlargerLampCompensationTime(Context context) {
        String value = PreferenceManager.getDefaultSharedPreferences(context).getString(LAMP_COMP_TIME, Integer.toString(LAMP_COMP_TIME_DEFAULT));
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
        	Logger.d("Error formating lamp comp. time, setting back to default.");
        	setDevTimerLapsTime(context, LAMP_COMP_TIME_DEFAULT);
            return LAMP_COMP_TIME_DEFAULT;
        }
    }
    public static void setEnlargerLampCompensationTime(Context context, int inTime) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(LAMP_COMP_TIME, Integer.toString(inTime)).commit();
    }
    
}
