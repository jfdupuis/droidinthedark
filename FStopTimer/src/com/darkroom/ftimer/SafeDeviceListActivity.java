package com.darkroom.ftimer;

import com.jfdupuis.bluetooth.DeviceListActivity;
import com.jfdupuis.bluetooth.R;

import android.os.Bundle;

public class SafeDeviceListActivity extends DeviceListActivity {

	@Override
    protected void onCreate(Bundle savedInstanceState) {
		mLayout = R.layout.safe_device_list;
        super.onCreate(savedInstanceState);
    }
}
