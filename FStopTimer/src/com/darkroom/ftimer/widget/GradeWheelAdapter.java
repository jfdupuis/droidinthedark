package com.darkroom.ftimer.widget;

import android.content.Context;

public class GradeWheelAdapter extends FloatWheelAdapter {
	
	public GradeWheelAdapter(Context context, float inIncrements, String inFormat) {
		this(context, 0, 5, inIncrements, inFormat);
	}
	
	public GradeWheelAdapter(Context context, float minValue, float maxValue, float increment, String format) {
		super(context, minValue, maxValue, increment, format);
	}
	
	@Override
	public int getValueIndex(float inValue) {
		if(inValue == -1)
			return 0; 
    	return (int)((inValue-minValue)/incrementValue) + 1;
    }
	
	@Override
	public float getValue(int index) {
		if(index == 0)
			return -1;
    	return minValue + (index-1)*incrementValue;
    }
	
	@Override
    public CharSequence getItemText(int index) {
        if (index >= 0 && index < getItemsCount()) {
            if(index == 0) 
            	return "00";
            else {
            	float value = getValue(index);
               	return format != null ? String.format(format, value) : Float.toString(value);
            }
        } 
        return null;
    }
	
	@Override
    public int getItemsCount() {
        return (int) ((maxValue - minValue)/incrementValue) + 2; //Add the special value 00 as -1
    }
}
