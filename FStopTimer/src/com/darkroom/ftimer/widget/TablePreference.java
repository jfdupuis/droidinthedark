package com.darkroom.ftimer.widget;

import com.darkroom.ftimer.Logger;
import com.darkroom.library.R;

import android.content.Context;
import android.preference.DialogPreference;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class TablePreference extends DialogPreference{
	
	// Namespaces to read attributes
	private static final String ANDROID_NS = "http://schemas.android.com/apk/res/android";

	// Attribute names
	private static final String ATTR_DEFAULT_VALUE = "defaultValue";
	
	// Default values for defaults
	private static final String DEFAULT_CURRENT_VALUE = "";
	
	// Real defaults
	private String mDefaultValue;
	
	protected List<EditText> editTextList = new ArrayList<EditText>();
	protected List<TextView> textViewList = new ArrayList<TextView>();
	
	// Current value
	private TableValues[] mCurrentData;
	
	public static class TableValues {
    	public String mEntry;
    	public String mValue;
    }
    
	//Extract table value from string
    public static TableValues[] extractValues(String inStringValues) {
    	String[] sArray = inStringValues.split(";");
    	
    	TableValues[] lValues = new TableValues[sArray.length];
    	
    	for(int i = 0; i < sArray.length; ++i) {    		
    		String[] lStrValues = sArray[i].split(",");
    		assert(lStrValues.length == 2);
    		lValues[i] = new TableValues();
    		lValues[i].mEntry = lStrValues[0];
    		lValues[i].mValue = lStrValues[1];
    	}
    	
    	return lValues;
    }
    
    //Format the table value to string
    protected String formatValues(TableValues[] inData) {
    	String lStringValues = inData[0].mEntry + "," + inData[0].mValue;
    	for(int i = 1; i < inData.length; ++i) {
    		lStringValues += ";" + inData[i].mEntry + "," + inData[i].mValue;
    	}
    	return lStringValues;
    }
    
    protected void fillTable(TableValues[] inData) {
    	Logger.d("fillTable");
    	for(int i = 0; i < inData.length; ++i) {
    		textViewList.get(i).setText(inData[i].mEntry);
    		editTextList.get(i).setText(inData[i].mValue);
    	}
    }
    
    protected TableValues[] collectTableData() {
    	TableValues[] lData = new TableValues[editTextList.size()];
    	for(int i = 0; i < editTextList.size(); ++i) {
    		lData[i] = new TableValues();
    		lData[i].mEntry = textViewList.get(i).getText().toString();
    		lData[i].mValue = editTextList.get(i).getText().toString();
    	}
    	return lData;
    }
	
    public TablePreference(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        
        //Read parameters from attributes
		mDefaultValue = attrs.getAttributeValue(ANDROID_NS, ATTR_DEFAULT_VALUE);
		if(mDefaultValue == "")
			mDefaultValue = DEFAULT_CURRENT_VALUE;
    }
	
	public TablePreference(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		//Read parameters from attributes
		mDefaultValue = attrs.getAttributeValue(ANDROID_NS, ATTR_DEFAULT_VALUE);
		if(mDefaultValue == "")
			mDefaultValue = DEFAULT_CURRENT_VALUE;
	}
	

	@Override
	protected View onCreateDialogView() {
		Logger.d("onCreateDialogView()");
		// Get current value from preferences
		mCurrentData = extractValues(getPersistedString(mDefaultValue));

		// Inflate layout
		LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.table_preference_dialog, null);

		LinearLayout lLayout = ((LinearLayout) view.findViewById(R.id.main_layout));
		lLayout.addView(tableLayout(mCurrentData.length, 2));
		
		fillTable(mCurrentData);
		
		return view;
	}
	
	private TableLayout tableLayout(int count, int nbcols) {
        TableLayout tableLayout = new TableLayout(getContext());
        tableLayout.setStretchAllColumns(true);
        int noOfRows = count / nbcols;
        for (int i = 0; i < noOfRows; i++) {
            int rowId = nbcols*2 * i; //*2 as the textview is needed
            tableLayout.addView(createOneFullRow(rowId, nbcols));
        }
        int individualCells = count % nbcols;
        tableLayout.addView(createLeftOverCells(individualCells, count));
        return tableLayout;
    }
	
	private TableRow createOneFullRow(int rowId, int nbcols) {
        TableRow tableRow = new TableRow(getContext());
        tableRow.setPadding(0, 10, 0, 0);
        for (int i = 1; i <= nbcols*2; i+=2) {
        	tableRow.addView(textView(rowId+i));
        	tableRow.addView(editText(rowId+i+1));
        }
        return tableRow;
    }
	
	private TableRow createLeftOverCells(int individualCells, int count) {
        TableRow tableRow = new TableRow(getContext());
        tableRow.setPadding(0, 10, 0, 0);
        int rowId = count - individualCells;
        for (int i = 1; i <= individualCells*2; i+=2) {
        	tableRow.addView(textView(rowId+i));
            tableRow.addView(editText(rowId+i+1));
        }
        return tableRow;
    }
	
	private TextView textView(int id/*, String value*/) {
        TextView textView = new TextView(getContext());
        textView.setId(id);
        //textView.setText(value);
        textViewList.add(textView);
        return textView;
    }
 
    private EditText editText(int id/*, String value*/) {
        EditText editText = new EditText(getContext());
        editText.setId(id);
        //editText.setRawInputType(InputType.number|numberSigned|numberDecimal);
        editText.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_DECIMAL|InputType.TYPE_NUMBER_FLAG_SIGNED);
        editText.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        //editText.setText(value);
        editTextList.add(editText);
        return editText;
    }

	@Override
	protected void onDialogClosed(boolean positiveResult) {
		super.onDialogClosed(positiveResult);

		// Return if change was cancelled
		if (!positiveResult) {
			editTextList.clear(); //TODO could reuse array
			textViewList.clear();
			return;
		}

		// Persist current value if needed
		if (shouldPersist()) {
			persistString(formatValues(collectTableData()));
			editTextList.clear();
			textViewList.clear();
		}

		// Notify activity about changes (to update preference summary line)
		notifyChanged();
		
	}
	

}
