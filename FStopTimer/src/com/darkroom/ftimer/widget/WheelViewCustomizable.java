package com.darkroom.ftimer.widget;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.jfdupuis.library.R;
import com.jfdupuis.library.widget.wheel.WheelView;

public class WheelViewCustomizable extends WheelView {
	
	protected int[] mShadowColors = new int[] { 0xFF111111,	0x00AAAAAA, 0x00AAAAAA };
	
	protected int mCenterDrawable = R.drawable.wheel_val;
	protected int mBackgroundDrawable = R.drawable.wheel_bg;
	
	public WheelViewCustomizable(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public WheelViewCustomizable(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public WheelViewCustomizable(Context context) {
		super(context);
	}
	
	public void setShadowColor(int [] inColors) {
		mShadowColors = inColors;
	}
	
	public void setCenterDrawable(int resid) {
		mCenterDrawable = resid;
	}
	
	public void setBackgroundDrawable(int resid) {
		mBackgroundDrawable = resid;
	}
	
	@Override
	protected void initResourcesIfNecessary() {
		if (centerDrawable == null) {
			centerDrawable = getContext().getResources().getDrawable(mCenterDrawable);
		}

		if (topShadow == null) {
			topShadow = new GradientDrawable(Orientation.TOP_BOTTOM, mShadowColors);
		}

		if (bottomShadow == null) {
			bottomShadow = new GradientDrawable(Orientation.BOTTOM_TOP, mShadowColors);
		}

		
		setBackgroundResource(mBackgroundDrawable);
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (!isEnabled() || getViewAdapter() == null) {
			return true;
		}
		
		switch (event.getAction()) {
		    case MotionEvent.ACTION_MOVE:
		        if (getParent() != null) {
		            getParent().requestDisallowInterceptTouchEvent(true);
		        }
		        break;
		        
		    case MotionEvent.ACTION_UP:
		        if (!isScrollingPerformed) {
		            int distance = (int) event.getY() - getHeight() / 2;
		            if (distance > 0) {
		                distance += getItemHeight() / 2;
		            } else {
                        distance -= getItemHeight() / 2;
		            }
		            int items = distance / getItemHeight();
		            if (/*items != 0 &&*/ isValidItemIndex(currentItem + items)) {
	                    notifyClickListenersAboutClick(currentItem + items);
		            }
		        }
		        break;
		}

		return scroller.onTouchEvent(event);
	}

	
}
