package com.darkroom.ftimer.widget;

import android.content.Context;

import com.jfdupuis.library.widget.wheel.adapters.AbstractWheelTextAdapter;


public class FloatWheelAdapter extends AbstractWheelTextAdapter {
	 /** The default min value */
    public static final float DEFAULT_MAX_VALUE = 9;

    /** The default max value */
    private static final float DEFAULT_MIN_VALUE = 0;
    
    /** The default increment value */
    private static final float DEFAULT_INC_VALUE = 0.5f;
    
    // Values
    protected float minValue;
    protected float maxValue;
    protected float incrementValue;
    
    // format
    protected String format;
    
    /**
     * Constructor
     * @param context the current context
     */
    public FloatWheelAdapter(Context context) {
        this(context, DEFAULT_MIN_VALUE, DEFAULT_MAX_VALUE, DEFAULT_INC_VALUE);
    }

    /**
     * Constructor
     * @param context the current context
     * @param minValue the wheel min value
     * @param maxValue the wheel max value
     */
    public FloatWheelAdapter(Context context, float minValue, float maxValue, float increment) {
        this(context, minValue, maxValue, increment, null);
    }

    /**
     * Constructor
     * @param context the current context
     * @param minValue the wheel min value
     * @param maxValue the wheel max value
     * @param format the format string
     */
    public FloatWheelAdapter(Context context, float minValue, float maxValue, float increment, String format) {
        super(context);
        
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.incrementValue = increment;
        this.format = format;
    }
    
    public float getValue(int inIndex) {
    	return minValue + inIndex*incrementValue;
    }
    
    public int getValueIndex(float inValue) {
    	return (int)((inValue-minValue)/incrementValue);
    }

    @Override
    public CharSequence getItemText(int index) {
        if (index >= 0 && index < getItemsCount()) {
            float value = getValue(index);
            return format != null ? String.format(format, value) : Float.toString(value);
        }
        return null;
    }

    @Override
    public int getItemsCount() {
        return (int) ((maxValue - minValue)/incrementValue) + 1;
    }    
}
