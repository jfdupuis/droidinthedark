package com.darkroom.ftimer;

import com.darkroom.ftimer.widget.TablePreference;
import com.darkroom.ftimer.widget.TablePreference.TableValues;

import java.util.HashMap;
import java.util.Map;

public class GradeCompensation {
	private Map<Float, Float> mTable;
	
	GradeCompensation(String inStrData) {
		mTable = new HashMap<Float, Float>();
		
		if(inStrData != "") {
			TableValues[] lValues = TablePreference.extractValues(inStrData);
			
			for(int i = 0; i < lValues.length; ++i) {
				mTable.put( Float.valueOf(lValues[i].mEntry), Float.valueOf(lValues[i].mValue) );
			}
		}
	}
	
	public float getCompensation(float inNewGrade, float inOldGrade) {
		Float lNewCompValue = mTable.get(inNewGrade);
		if(lNewCompValue == null)
			lNewCompValue = new Float(0.0);
		
		Float lOldCompValue = mTable.get(inOldGrade);
		if(lOldCompValue == null)
			lOldCompValue = new Float(0.0);
		
		return lNewCompValue - lOldCompValue;
	}
}
