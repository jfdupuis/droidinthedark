package com.darkroom.ftimer;

import java.util.List;

import com.darkroom.library.ExposureItem;
import com.darkroom.ftimer.R;
import com.jfdupuis.library.math.Fraction;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.view.View.OnClickListener;
//import android.util.Log;

public class ExposureArrayAdapter extends ArrayAdapter<ExposureItem> {
//	private static final String TAG = "DarkroomTime_ExposureArrayAdapter";
	
	public interface OnChangedListener {
        void onChanged(int inPosition, Fraction mPrevious, Fraction mCurrent);
    }
	
	private FNumberPicker mIncrementPicker = null;
    private ListView mListView = null;
    int resource = 0;  
    private OnChangedListener mListener;
	
	public ExposureArrayAdapter(Context inContext, int inResource, 
								List<ExposureItem> inItems, ListView inList,
								FNumberPicker inIncrementPicker) {
	    super(inContext, inResource, inItems);
	    resource = inResource;
	    mListView = inList;
	    mIncrementPicker = inIncrementPicker;
	}
	
	public void setOnChangeListener(OnChangedListener listener) {
        mListener = listener;
    }
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
	    
	    //Log.v(TAG, String.format("getView, row %d", position));
	    
	    LinearLayout lExposureView;
	    ExposureItem lItem = getItem(position);
		
	    if (convertView == null) {
	    	lExposureView = new LinearLayout(getContext());
			String inflater = Context.LAYOUT_INFLATER_SERVICE;
			LayoutInflater vi = (LayoutInflater)getContext().getSystemService(inflater);
			vi.inflate(resource, lExposureView, true);

			TextView lValueString = (TextView) lExposureView.findViewById(R.id.fnumberpicker_value);
		    lValueString.setOnClickListener(mOnValueClickListener);
		    
		    ImageButton lIncrementButton = (ImageButton) lExposureView.findViewById(R.id.increment);
		    lIncrementButton.setOnClickListener(mOnIncrementClickListener);

		    ImageButton lDecrementButton = (ImageButton) lExposureView.findViewById(R.id.decrement);
		    lDecrementButton.setOnClickListener(mOnDecrementClickListener);
  
	    } else {
	    	lExposureView = (LinearLayout) convertView;
	    }
		
	    TextView lIdxText = (TextView)lExposureView.findViewById(R.id.ExposureItem_Text);
	    lIdxText.setOnClickListener(mOnValueClickListener);
	    int lPosition = position+1;
	    lIdxText.setText(Integer.toString(lPosition));
	    lItem.setPosition(position);
	    
	    TextView lValueString = (TextView) lExposureView.findViewById(R.id.fnumberpicker_value);
	    if(mIncrementPicker.isDecimal())
	    	lValueString.setText(String.format("%.2f",lItem.getValue().asDouble()));
	    else
	    	lValueString.setText(lItem.getValueStr());
	    
	    
	    return lExposureView;
	}
	
	
	
	private OnClickListener mOnValueClickListener = new OnClickListener() {
        public void onClick(View v) {
            final int position = mListView.getPositionForView((View) v.getParent());
            mListView.requestFocusFromTouch();
            mListView.setSelection(position);
        }
    };
	
    private OnClickListener mOnIncrementClickListener = new OnClickListener() {
        public void onClick(View v) {
            final int position = mListView.getPositionForView((View) v.getParent());
            final Fraction lPrevious =  getItem(position).getValue();
            getItem(position).incValue(mIncrementPicker.getCurrent());
            notifyDataSetChanged();
            
            if(mListener != null)
            	mListener.onChanged(position, lPrevious, getItem(position).getValue());
//            Log.v(TAG, String.format("Increment button clicked, row %d, new value: "+getItem(position).getValueStr(), position));
            
        }
    };
    
    private OnClickListener mOnDecrementClickListener = new OnClickListener() {
        public void onClick(View v) {
            final int position = mListView.getPositionForView((View) v.getParent());
            final Fraction lPrevious =  getItem(position).getValue();
            getItem(position).decValue(mIncrementPicker.getCurrent());
            notifyDataSetChanged();
            
            if(mListener != null)
            	mListener.onChanged(position, lPrevious, getItem(position).getValue());
//            Log.v(TAG, String.format("Decrement button clicked, row %d, new value: "+getItem(position).getValueStr(), position));
        }
    };
    	
}


