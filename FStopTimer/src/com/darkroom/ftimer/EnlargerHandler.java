package com.darkroom.ftimer;

import com.jfdupuis.library.screen.ScreenUtil;
import com.jfdupuis.bluetooth.BluetoothChatThreads;
import com.jfdupuis.bluetooth.R;
import com.jfdupuis.bluetooth.arduino.ArduinoHandler;

import android.content.Context;
import android.widget.Toast;

public class EnlargerHandler extends ArduinoHandler {
	
	private int mCurrentState = -1;
	
	protected EnlargerHandler(Context inContext) {
		super(inContext);
	}
	
	@Override
	public void displayMessage(String inMessage, int inLength) {
		ScreenUtil.displayMessage(mContext,inMessage,inLength);
    }
	
	@Override
	public void messageReceived(String mInMessage) {
		// do nothing
	}
	
	/**
	 * Inform the user when connection change
	 */
	@Override
	public void stateChanged(int inNewState) {
		//Override to do something useful
        switch(inNewState) {
        case BluetoothChatThreads.STATE_CONNECTED:
        	mConnectedDeviceName = mReceivedDeviceName;
        	if(mCurrentState != BluetoothChatThreads.STATE_CONNECTED)
        		displayMessage(mContext.getString(R.string.bt_connected, mConnectedDeviceName), Toast.LENGTH_SHORT);
            break;
        case BluetoothChatThreads.STATE_CONNECTING:
        	if(mCurrentState != BluetoothChatThreads.STATE_CONNECTING)
        		displayMessage(mContext.getString(R.string.bt_connecting, mReceivedDeviceName), Toast.LENGTH_LONG);
            break;
        case BluetoothChatThreads.STATE_LISTEN:
        case BluetoothChatThreads.STATE_NONE:
        	mConnectedDeviceName = null;
        	mReceivedDeviceName = null;
        	if(mCurrentState != BluetoothChatThreads.STATE_LISTEN && mCurrentState != BluetoothChatThreads.STATE_NONE)
        		displayMessage(mContext.getString(R.string.bt_notconnected), Toast.LENGTH_LONG);
            break;
        }
        mCurrentState = inNewState;
	}
}
