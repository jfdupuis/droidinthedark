package com.darkroom.ftimer;

import com.darkroom.ftimer.R;

import java.lang.ref.WeakReference;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


public class SafeDialog extends Dialog {
		
	private TextView mText;
	private ImageView mImage;
	
	private Button mButtonPositive;
    @SuppressWarnings("unused")
	private CharSequence mButtonPositiveText;
    private Message mButtonPositiveMessage;

    private Button mButtonNegative;
    @SuppressWarnings("unused")
	private CharSequence mButtonNegativeText;
    private Message mButtonNegativeMessage;

	
	private Handler mHandler;
	private final DialogInterface mDialogInterface;


	View.OnClickListener mButtonHandler = new View.OnClickListener() {
        public void onClick(View v) {
            Message m = null;
            if (v == mButtonPositive && mButtonPositiveMessage != null) {
                m = Message.obtain(mButtonPositiveMessage);
            } else if (v == mButtonNegative && mButtonNegativeMessage != null) {
                m = Message.obtain(mButtonNegativeMessage);
            } 
            if (m != null) {
                m.sendToTarget();
            }

            // Post a message so we dismiss after the above handlers are executed
            mHandler.obtainMessage(ButtonHandler.MSG_DISMISS_DIALOG, mDialogInterface)
                    .sendToTarget();
        }
    };

    private static final class ButtonHandler extends Handler  {
        // Button clicks have Message.what as the BUTTON{1,2,3} constant
        private static final int MSG_DISMISS_DIALOG = 1;
        
        private WeakReference<DialogInterface> mDialog;

        public ButtonHandler(DialogInterface dialog) {
            mDialog = new WeakReference<DialogInterface>(dialog);
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                
                case DialogInterface.BUTTON_POSITIVE:
                case DialogInterface.BUTTON_NEGATIVE:
                    ((DialogInterface.OnClickListener) msg.obj).onClick(mDialog.get(), msg.what);
                    break;
                    
                case MSG_DISMISS_DIALOG:
                    ((DialogInterface) msg.obj).dismiss();
            }
        }


    }
    
    /**
     * Sets a click listener or a message to be sent when the button is clicked.
     * You only need to pass one of {@code listener} or {@code msg}.
     * 
     * @param whichButton Which button, can be one of
     *            {@link DialogInterface#BUTTON_POSITIVE},
     *            {@link DialogInterface#BUTTON_NEGATIVE}, or
     *            {@link DialogInterface#BUTTON_NEUTRAL}
     * @param text The text to display in positive button.
     * @param listener The {@link DialogInterface.OnClickListener} to use.
     * @param msg The {@link Message} to be sent when clicked.
     */
    public void setButton(int whichButton, CharSequence text,
            DialogInterface.OnClickListener listener, Message msg) {

        if (msg == null && listener != null) {
            msg = mHandler.obtainMessage(whichButton, listener);
        }
        
        switch (whichButton) {

            case DialogInterface.BUTTON_POSITIVE:
                mButtonPositiveText = text;
                mButtonPositiveMessage = msg;
                break;
                
            case DialogInterface.BUTTON_NEGATIVE:
                mButtonNegativeText = text;
                mButtonNegativeMessage = msg;
                break;
                
            default:
                throw new IllegalArgumentException("Button does not exist");
        }
    }
	
	
	public SafeDialog(Context mContext) {
		super(mContext);
		mDialogInterface = this;
		mHandler = new ButtonHandler(this);
		
    	this.setContentView(R.layout.safe_alert_dialog);
    	this.getWindow().setBackgroundDrawableResource(R.drawable.safe_popup_full_dark);
    	//this.getWindow().setTitleColor(R.color.safetext);
    	
    	setCancelable(false);

    	mText = (TextView) this.findViewById(R.id.text);
    	mImage = (ImageView) this.findViewById(R.id.image);
    	
    	mButtonPositive = (Button) this.findViewById(R.id.positive_button);
    	mButtonPositive.setText(android.R.string.ok);
    	mButtonPositive.setOnClickListener(mButtonHandler);

    	mButtonNegative = (Button) this.findViewById(R.id.negative_button);
    	mButtonNegative.setText(android.R.string.cancel);
    	mButtonNegative.setOnClickListener(mButtonHandler);
    	
	}
	
	

	public void setText(String inText) {
		mText.setText(inText);
	}
	
	public void setImageResource(int inRessource) {
		mImage.setImageResource(inRessource);
	}
	
	
	public void setPositiveButton(CharSequence text, OnClickListener listener) {
		this.setButton(DialogInterface.BUTTON_POSITIVE,text,listener,null);
	}
	
	public void setNegativeButton(CharSequence text, OnClickListener listener) {
		
	}

}
