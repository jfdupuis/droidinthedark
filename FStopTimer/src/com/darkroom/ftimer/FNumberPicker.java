/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.darkroom.ftimer;



import android.content.Context;
import android.os.Handler;
//import android.text.InputFilter;
//import android.text.InputType;
//import android.text.Spanned;
//import android.text.method.NumberKeyListener;
import android.util.AttributeSet;
//import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
//import android.view.View.OnFocusChangeListener;
import android.view.View.OnLongClickListener;
//import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.LinearLayout;
//import android.widget.EditText;
import android.view.Gravity
;

import com.darkroom.ftimer.R;
import com.jfdupuis.library.math.Fraction;


/**
 * This class has been pulled from the Android platform source code, its an internal widget that hasn't been
 * made public so its included in the project in this fashion for use with the preferences screen; I have made
 * a few slight modifications to the code here, I simply put a MAX and MIN default in the code but these values
 * can still be set publically by calling code.
 *
 * @author Google
 */
public class FNumberPicker extends LinearLayout implements OnClickListener
//        ,OnFocusChangeListener
        ,OnLongClickListener 
        {

//    private static final String TAG = "Darkroom_FNumberPicker";
    private static final int DEFAULT_MAX = 10;
    private static final int DEFAULT_MIN = 0;

    public interface OnChangedListener {
        void onChanged(FNumberPicker picker, Fraction mPrevious, Fraction mCurrent);
    }

    public interface Formatter {
        String toString(int value);
    }

    /*
     * Use a custom NumberPicker formatting callback to use two-digit
     * minutes strings like "01".  Keeping a static formatter etc. is the
     * most efficient way to do this; it avoids creating temporary objects
     * on every call to format().
     */
    public static final FNumberPicker.Formatter TWO_DIGIT_FORMATTER =
            new FNumberPicker.Formatter() {
                final StringBuilder mBuilder = new StringBuilder();
                final java.util.Formatter mFmt = new java.util.Formatter(mBuilder);
                final Object[] mArgs = new Object[1];
                public String toString(int value) {
                    mArgs[0] = value;
                    mBuilder.delete(0, mBuilder.length());
                    mFmt.format("%02d", mArgs);
                    return mFmt.toString();
                }
        };

    private final Handler mHandler;
    private final Runnable mRunnable = new Runnable() {
        public void run() {
            if (mIncrement) {
                changeCurrent((Fraction) mCurrent.plus(getIncrement()));
                mHandler.postDelayed(this, mSpeed);
            } else if (mDecrement) {
                changeCurrent((Fraction) mCurrent.minus(getIncrement()));
                mHandler.postDelayed(this, mSpeed);
            }
        }
    };

    private final TextView mText;
    

//    private String[] mDisplayedValues;
    protected Fraction mStart;
    protected Fraction mEnd;
    
//    protected Fraction mFractionStart;
//    protected Fraction mFractionEnd;
//    protected float mDecimalStart;
//    protected float mDecimalEnd;
 
    protected Fraction mCurrent;
    protected FNumberPicker mIncrementValuePicker;
    protected Fraction mDefaultIncrementValue;
        
    protected Fraction mPrevious;
    private OnChangedListener mListener;
//    private Formatter mFormatter;
    private long mSpeed = 300;

    private boolean mIncrement;
    private boolean mDecrement;
    
    private boolean mIsDecimal = false;
    private boolean mFractionPicker = false;

    public FNumberPicker(Context context) {
        this(context, null);
    }

    public FNumberPicker(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }
    
    public FNumberPicker(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.fnumber_picker, this, true);
//        Log.v(TAG, String.format("Picker id: " + this.hashCode() + " created"));
        
        //Set alignment according to orientation
        mIncrementButton = (FNumberPickerButton) this.findViewById(R.id.increment);
        mDecrementButton = (FNumberPickerButton) this.findViewById(R.id.decrement);
        mText = (TextView) findViewById(R.id.fnumberpicker_value);
        if(getOrientation() == HORIZONTAL) {
        	setGravity(Gravity.CENTER_VERTICAL);
        } else {
        	setGravity(Gravity.CENTER_HORIZONTAL);
        }
        
        //Connect
        mHandler = new Handler();
        
        mIncrementButton.setOnClickListener(this);
        mIncrementButton.setOnLongClickListener(this);
        mIncrementButton.setNumberPicker(this);
        
        mDecrementButton.setOnClickListener(this);
        mDecrementButton.setOnLongClickListener(this);
        mDecrementButton.setNumberPicker(this);

        if (!isEnabled()) {
            setEnabled(false);
        }

        mCurrent = new Fraction(0);
        mIncrementValuePicker = null;
        mDefaultIncrementValue = new Fraction(1,(long)6);
        mStart = new Fraction(DEFAULT_MIN);
        mEnd = new Fraction(DEFAULT_MAX);
    }
    
    public void setDefaultIncrement(Fraction inValue) {
    	mDefaultIncrementValue = inValue;
    }
    
    public void setIncrementPicker(FNumberPicker inPicker) {
    	mIncrementValuePicker = inPicker;
    }

    public void setFractionPicker(boolean inState) {
    	mFractionPicker = inState;
    }
    
    public void setDecimal(boolean inState) {
    	mIsDecimal = inState;
//    	if(mIsDecimal) {
//    		setDecimalRange(mDecimalStart, mDecimalEnd);
//    	} else {
//    		setRange(mFractionStart, mFractionEnd);
//    	}
    	
    }
    
    
    
    public boolean isDecimal() {
    	return mIsDecimal;
    }
    
    public Fraction getIncrement() {
    	if(mIncrementValuePicker != null) {
    		return mIncrementValuePicker.getCurrent();
    	} else {
    		return mDefaultIncrementValue;
    	}
    }
    

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        mIncrementButton.setEnabled(enabled);
        mDecrementButton.setEnabled(enabled);
        mText.setEnabled(enabled);
    }

    public void setOnChangeListener(OnChangedListener listener) {
        mListener = listener;
    }

//    public void setFormatter(Formatter formatter) {
//        mFormatter = formatter;
//    }

    /**
     * Set the range of numbers allowed for the number picker. The current
     * value will be automatically set to the start.
     *
     * @param start the start of the range (inclusive)
     * @param end the end of the range (inclusive)
     */
    public void setRange(Fraction start, Fraction end) {
    	mStart = start;
        mEnd = end;
        mCurrent = start;
        updateView();
    }
    
    public void setRange(Fraction start, Fraction end, Fraction current) {
    	mStart = start;
        mEnd = end;
        mCurrent = current;
        updateView();
    }
    
    
//    public void setDecimalRange(float start, float end) {
//        mDecimalStart = start;
//        mDecimalEnd = end;
//        try {
//			mCurrent = new Fraction(start);
//		} catch (FractionConversionException e) {
//			mCurrent = new Fraction(1, (long)10);
//		}
//		
//    	try {
//			mStart = new Fraction(start);
//		} catch (FractionConversionException e) {
//			mStart = new Fraction(1);
//		}
//    	try {
//			mEnd = new Fraction(end);
//		} catch (FractionConversionException e) {
//			mEnd = new Fraction(1,(long)100);
//		}
//
//        updateView();
//    }

//    /**
//     * Set the range of numbers allowed for the number picker. The current
//     * value will be automatically set to the start. Also provide a mapping
//     * for values used to display to the user.
//     *
//     * @param start the start of the range (inclusive)
//     * @param end the end of the range (inclusive)
//     * @param displayedValues the values displayed to the user.
//     */
//    public void setRange(Fraction start, Fraction end, String[] displayedValues) {
//        mDisplayedValues = displayedValues;
//        mStart = start;
//        mEnd = end;
//        mCurrent = start;
//        updateView();
//    }

    public void setCurrent(Fraction current) {
        mCurrent = current;
        updateView();
    }

    /**
     * The speed (in milliseconds) at which the numbers will scroll
     * when the the +/- buttons are longpressed. Default is 300ms.
     */
    public void setSpeed(long speed) {
        mSpeed = speed;
    }

    public void onClick(View v) {
        if (!mText.hasFocus()) mText.requestFocus();

        // now perform the increment/decrement
        if (R.id.increment == v.getId()) {
        	if(mFractionPicker) {
        		if(mIsDecimal) {
        			changeCurrent(mCurrent.times(10));
        		} else {
	        		if(mCurrent.compareTo(new Fraction(1,(long)3)) == 0 ) {
	        			changeCurrent(new Fraction(1,(long)2));
	        		} else {
	        			changeCurrent(mCurrent.times(2));
	        		}
        		}
        	}
        	else {
        		changeCurrent(mCurrent.plus(getIncrement()));
        	}
        } else if (R.id.decrement == v.getId()) {
        	if(mFractionPicker) {
        		if(mIsDecimal) {
        			changeCurrent(mCurrent.dividedBy(10));
        		} else {
	        		if(mCurrent.compareTo(new Fraction(1,(long)2)) == 0 ) {
	        			changeCurrent(new Fraction(1,(long)3));
	        		} else {
	        			changeCurrent(mCurrent.dividedBy(2));
	        		}
        		}
        	}
        	else {
        		changeCurrent(mCurrent.minus(getIncrement()));
        	}
        }
    }

//    private String formatNumber(int value) {
//        return (mFormatter != null)
//                ? mFormatter.toString(value)
//                : String.valueOf(value);
//    }

    protected void changeCurrent(Fraction current) {

        // Wrap around the values if we go past the start or end
        if (current.compareTo(mEnd) > 0) {
       		current = mStart;
        } else if (current.compareTo(mStart) < 0) {
            current = mEnd;
        }
        mPrevious = mCurrent;
        mCurrent = current;

//        Log.v(TAG, String.format("Update id:" + this.hashCode() + " current value: " + mCurrent.toString()));
        
        notifyChange();
        updateView();
    }

    protected void notifyChange() {
        if (mListener != null) {
            mListener.onChanged(this, mPrevious, mCurrent);
        }
    }

    protected void updateView() {

//        /* If we don't have displayed values then use the
//         * current number else find the correct value in the
//         * displayed values for the current number.
//         */
//        if (mDisplayedValues == null) {
//            mText.setText(formatNumber(mCurrent));
//        } else {
//            mText.setText(mDisplayedValues[mCurrent.toString()]);
//        }
    	if(mIsDecimal) {
    		mText.setText(String.format("%.2f", mCurrent.asDouble()));
    	}
    	else {
    		mText.setText(mCurrent.toString());
    	}
    	
    }


    /**
     * We start the long click here but rely on the {@link FNumberPickerButton}
     * to inform us when the long click has ended.
     */
    public boolean onLongClick(View v) {

        /* The text view may still have focus so clear it's focus which will
         * trigger the on focus changed and any typed values to be pulled.
         */
        mText.clearFocus();

        if (R.id.increment == v.getId()) {
            mIncrement = true;
            mHandler.post(mRunnable);
        } else if (R.id.decrement == v.getId()) {
            mDecrement = true;
            mHandler.post(mRunnable);
        }
        return true;
    }

    public void cancelIncrement() {
        mIncrement = false;
    }

    public void cancelDecrement() {
        mDecrement = false;
    }


    private FNumberPickerButton mIncrementButton;
    private FNumberPickerButton mDecrementButton;



    /**
     * @return the current value.
     */
    public Fraction getCurrent() {
        return mCurrent;
    }
}