package com.util.help;

import com.darkroom.ftimer.R;
import com.darkroom.ftimer.widget.SafeAlertDialogBuilder;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;

public class SimpleUsage {

	private String INFO_PREFIX = "info_";
	private Activity mActivity; 
	
	public SimpleUsage(Activity context) {
		mActivity = context; 
	}
	
	private PackageInfo getPackageInfo() {
        PackageInfo pi = null;
        try {
             pi = mActivity.getPackageManager().getPackageInfo(mActivity.getPackageName(), PackageManager.GET_ACTIVITIES);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return pi; 
    }

     public void show() {
    	 
    	 
        PackageInfo versionInfo = getPackageInfo();

        // the infoKey changes every time you increment the version number in the AndroidManifest.xml
		final String infoKey = INFO_PREFIX + versionInfo.versionCode;
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mActivity);
        
        // For testing
        //prefs.edit().putBoolean(infoKey, false).commit();
        //////////
        
        
        boolean hasBeenShown = prefs.getBoolean(infoKey, false);
        if(hasBeenShown == false){

        	// Show the Info
            String title = mActivity.getString(R.string.app_name) + " v" + versionInfo.versionName;
            
            //Includes the updates as well so users know what changed. 
            String message = mActivity.getString(R.string.updates) 
            				 + "\n\n" + mActivity.getString(R.string.info)
            				 + "\n";

//          AlertDialog.Builder builder = new AlertDialog.Builder(mActivity)
            SafeAlertDialogBuilder builder = new SafeAlertDialogBuilder(mActivity);
            builder.setTitle(title)
                    .setMessage(message)
                    .setPositiveButton(android.R.string.ok, new Dialog.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            // Mark this version as read.
                            SharedPreferences.Editor editor = prefs.edit();
                            editor.putBoolean(infoKey, true);
                            editor.commit();
                            dialogInterface.dismiss();
                        }
                    });
//                    .setNegativeButton(android.R.string.cancel, new Dialog.OnClickListener() {
//						public void onClick(DialogInterface dialog, int which) {
//							// Close the activity as they have declined the EULA
//							mActivity.finish(); 
//						}
//                    	
//                     }               );
            builder.create().show();
        }
    }
	
}
