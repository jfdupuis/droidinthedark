/*
 *  BTEnlargerAdapter.pde
 *  Copyright 2011 Jean-Francois Dupuis.
 *
 *  
 *  BTEnlargerAdapter is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BTEnlargerAdapter is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BTEnlargerAdapter.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  This file was created by Jean-Francois Dupuis on 10/04/11.
 */
 
#include <MeetAndroid.h>

// declare MeetAndroid so that you can call functions with it
MeetAndroid meetAndroid;

const int LIGHTON = HIGH;
const int LIGHTOFF = LOW;

const int gEnlargerPin = 11;
const int gSafeLightPin = 12;
const int gLightSensor = A0;
const int gButtonPin = 13;

int buttonState;             // the current reading from the input pin
int lastButtonState = LOW;   // the previous reading from the input pin
long lastDebounceTime = 0;  // the last time the output pin was toggled
long debounceDelay = 50;    // the debounce time; increase if the output flickers

long gStopTime = 0;
long gStopBaseTime = 0;
long gStartTime = 0;
long gRemainingTime = 0;
long gExposureTime = 0;


boolean gRunning = false;

void setup()  
{
  // use the baud rate your bluetooth module is configured to 
  // not all baud rates are working well, i.e. ATMEGA168 works best with 57600
  Serial.begin(57600); 
  
  // register callback functions, which will be called when an associated event occurs.
  meetAndroid.registerFunction(receivedTimeUpdateRequest, 'u');
  meetAndroid.registerFunction(receiveStart, 's');
  meetAndroid.registerFunction(receiveStop, 'x');
  meetAndroid.registerFunction(receiveNewExposure, 'b');
  meetAndroid.registerFunction(receiveNewStopPoint, 'd');
  
  meetAndroid.registerFunction(receiveEnlargerLightOn, 'E');
  meetAndroid.registerFunction(receiveEnlargerLightOff, 'e');
  meetAndroid.registerFunction(receiveSafeLightOn, 'L');
  meetAndroid.registerFunction(receiveSafeLightOff, 'l');
  meetAndroid.registerFunction(receiveTimeCorrection, 'c');
  
  meetAndroid.registerFunction(receiveLightMeasurementRequest, 'm');

  // set all color leds as output pins
  pinMode(gEnlargerPin, OUTPUT);
  pinMode(gButtonPin, INPUT);
  
  // just set all leds to high so that we see they are working well
  digitalWrite(gEnlargerPin, LIGHTOFF);

}

void loop() {
  meetAndroid.receive(); // you need to keep this in your loop() to receive message from android
   
  long currentMillis = millis(); 
  if( gRunning && (currentMillis >= gStopTime) ) {
    //Reach the end of current exposure
    digitalWrite(gEnlargerPin, LIGHTOFF);
    
    computeRemainingTime(currentMillis);
    
    gRunning = false;
    meetAndroid.send(long(-1));
  }
  
  //Process the light measurement button
  int lReading = digitalRead(gButtonPin);
  // If the switch changed, due to noise or pressing:
  if (lReading != lastButtonState) {
    // reset the debouncing timer
    lastDebounceTime = millis();
  }
  
  if ((millis() - lastDebounceTime) > debounceDelay) {
    // whatever the reading is at, it's been there for longer
    // than the debounce delay, so take it as the actual current state:
    buttonState = lReading;
  } 
  
  if(buttonState ) {
    sendLightMeasure();
  }
  
  lastButtonState = lReading;
  
}

void computeRemainingTime(long inStopTime) {
    gRemainingTime = gRemainingTime - (inStopTime-gStartTime); 
    if(gRemainingTime < 0) gRemainingTime = 0;
    
    gExposureTime = gExposureTime - (inStopTime-gStartTime); 
    if(gExposureTime <= 0) gExposureTime = gRemainingTime;
    

}

long getRemainingTime() {
  if(gRunning) 
    return gStopBaseTime - millis();
  else
    return gRemainingTime;
}

//Receive a request to receive an update on the remaining exposure time
void receivedTimeUpdateRequest(byte flag, byte numOfValues)
{
  meetAndroid.getInt();
  meetAndroid.send(getRemainingTime());
}

//Receive a new exposure time
void receiveNewExposure(byte flag, byte numOfValues)
{
  digitalWrite(gEnlargerPin, LIGHTOFF);
  gRunning = false;
  gRemainingTime =  meetAndroid.getLong();
  gExposureTime = gRemainingTime;
}

//Receive a new mid-exposure stop time
void receiveNewStopPoint(byte flag, byte numOfValues)
{
  digitalWrite(gEnlargerPin, LIGHTOFF);
  gRunning = false;
  gExposureTime =  meetAndroid.getLong();
}

//Receive a command to start the exposure for the remaining exposure time
void receiveStart(byte flag, byte numOfValues) {
  gStartTime = millis();
  digitalWrite(gEnlargerPin, LIGHTON);
  
  gStopBaseTime = gStartTime + gRemainingTime;
  const long lStopExpTime = gStartTime + gExposureTime;
  
  gStopTime = min(gStopBaseTime, lStopExpTime);
  
  gRunning = true;
  meetAndroid.getInt();
}

//Receive a command to stop the exposure
void receiveStop(byte flag, byte numOfValues) {
  digitalWrite(gEnlargerPin, LIGHTOFF);
  if(gRunning) {
    long lStopTime = millis();
    computeRemainingTime(lStopTime);
  }
  gRunning = false;
  meetAndroid.getInt();
}

void receiveEnlargerLightOn(byte flag, byte numOfValues) {
  digitalWrite(gEnlargerPin, LIGHTON);
  meetAndroid.getInt();
}

void receiveEnlargerLightOff(byte flag, byte numOfValues) {
  digitalWrite(gEnlargerPin, LIGHTOFF);
  meetAndroid.getInt();
}

void receiveSafeLightOn(byte flag, byte numOfValues) {
  digitalWrite(gSafeLightPin, LIGHTON);
  meetAndroid.getInt();
}


void receiveSafeLightOff(byte flag, byte numOfValues) {
  digitalWrite(gSafeLightPin, LIGHTOFF);
  meetAndroid.getInt();
}

void receiveTimeCorrection(byte flag, byte numOfValues)
{
  long lCorrection =  meetAndroid.getLong();
  gRemainingTime += lCorrection;
  gExposureTime += lCorrection;
}

void receiveLightMeasurementRequest(byte flag, byte numOfValues) {
  meetAndroid.getInt();
  sendLightMeasure();
}

void sendLightMeasure() {
  meetAndroid.send('m');
  meetAndroid.send(analogRead(gLightSensor));
}

