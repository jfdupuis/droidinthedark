%Compute f-stop 
%
% Zero of the scale: f-stop = 3 -> 8 sec, so f-stop = 1 -> 2 sec
%
% From wiki: Doubling the f-number increases the necessary exposure time by
% a factor of four.
%
% Geometric progression with ratio of 2


 
a = 2; %Base number
r = 2; %Ratio

n = 1/3; % Number of stop from base number

t = a*r^(n-1)



% reverse
n = log(t/a)/log(r)+1