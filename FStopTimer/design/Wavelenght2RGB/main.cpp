/*
 *  wavelength2RGB.cpp
 *  
 *
 *  Created by Jean-Francois Dupuis on 04/03/11.
 *  Copyright 2011 Jean-Francois Dupuis. All rights reserved.
 *
 *	Source: http://miguelmoreno.net/sandbox/wavelengthtoRGB/
 *	Original Fortran: http://www.midnightkite.com/color.html
 */

#include <iostream>
#include <cstdlib>
#include <getopt.h>
#include <cmath>

using namespace std;

void usage() {
	std::cout << "Usage: wavelength2RBG [options]" << std::endl;
	std::cout << "   -w, --wavelength=W        Wavelength to convert" << std::endl;
	std::cout << "   -h, --help                Display this help and exit" << std::endl;
}


class Color {
public:
	int R;
	int G;
	int B;
	
	Color(int inR=0, int inG=0, int inB=0) : R(inR), G(inG), B(inB) {}
};

int factorAdjust(double Color,
				 double Factor,
				 int IntensityMax,
				 double Gamma){
	if(Color == 0.0){
		return 0;
	}else{
		return (int) round(IntensityMax * pow(Color * Factor, Gamma));
	}
}

Color getColorFromWaveLength(double Wavelength){
	double Gamma = 1.00;
	int IntensityMax = 255;
	double Blue;
	double Green;
	double Red;
	double Factor;
	
	if(Wavelength >= 380 && Wavelength <= 439){
		Red	= -(Wavelength - 440) / (440 - 380);
		Green = 0.0;
		Blue	= 1.0;
	}else if(Wavelength >= 440 && Wavelength <= 489){
		Red	= 0.0;
		Green = (Wavelength - 440) / (490 - 440);
		Blue	= 1.0;
	}else if(Wavelength >= 490 && Wavelength <= 509){
		Red = 0.0;
		Green = 1.0;
		Blue = -(Wavelength - 510) / (510 - 490);
		
	}else if(Wavelength >= 510 && Wavelength <= 579){ 
		Red = (Wavelength - 510) / (580 - 510);
		Green = 1.0;
		Blue = 0.0;
	}else if(Wavelength >= 580 && Wavelength <= 644){
		Red = 1.0;
		Green = -(Wavelength - 645) / (645 - 580);
		Blue = 0.0;
	}else if(Wavelength >= 645 && Wavelength <= 780){
		Red = 1.0;
		Green = 0.0;
		Blue = 0.0;
	}else{
		Red = 0.0;
		Green = 0.0;
		Blue = 0.0;
	}
	
	if(Wavelength >= 380 && Wavelength <= 419){
		Factor = 0.3 + 0.7*(Wavelength - 380) / (420 - 380);
	}else if(Wavelength >= 420 && Wavelength <= 700){
		Factor = 1.0;
	}else if(Wavelength >= 701 && Wavelength <= 780){
		Factor = 0.3 + 0.7*(780 - Wavelength) / (780 - 700);
	}else{
		Factor = 0.0;
	}
	
	int R = factorAdjust(Red, Factor, IntensityMax, Gamma);
	int G = factorAdjust(Green, Factor, IntensityMax, Gamma);
	int B = factorAdjust(Blue, Factor, IntensityMax, Gamma);
	
	return Color(R, G, B);
}


int main (int argc, char * const argv[]) {
	
	// Parse arguments
	struct option longopts[] = {
		{"wavelength", 1, NULL, 'w'},
		{"help", 0, NULL, 'h'},
		{0,0,0,0}
	};
	
	double lWavelength = 0;
	
	int lOption;
	while( (lOption = getopt_long(argc, argv, "w:h", longopts, NULL) ) != -1) {
		switch(lOption) {
			case 'w':
				lWavelength = atof(optarg);
				break;
			case 'h':
				usage();
				exit(EXIT_SUCCESS);
			case ':':
				std::cerr << "Error : Option needs a value" << std::endl;
				exit(EXIT_FAILURE);
			case '?':
				std::cerr << "Error : Unknown option : " << optopt << std::endl;
				exit(EXIT_FAILURE);
		}
	}
	
	std::cout << "Wavelength: " << lWavelength << std::endl;
	
	Color lRGBColor = getColorFromWaveLength(lWavelength);
	
	std::cout << "R=" << hex << lRGBColor.R << " G=" << hex << lRGBColor.G << " B=" << hex << lRGBColor.B << std::endl;

	return 0;
}





