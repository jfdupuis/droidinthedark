A timer to develop your film in the darkroom. This application provides a count up timer with sound indication for the end of development and customizable metronome and agitation interval. 

Even if the phone falls asleep, the end of development will be notified. The timer even allows you to use your phone for other task during the development. 

This timer application is not oriented to provide you an infinite amount of development time, but to store the development time that you use and trust. You will build your own database of processing times that you can export and import in a CSV format for easy edition on a computer in OpenOffice or Excel.

Notable features:
- Zone-system development support
- Custom agitation patterns
- Sound notification
- SQL database with CVS import and export
