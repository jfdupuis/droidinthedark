# Simple HtmlTableParser module
# Author: Ariff Wambeck
# Year: 2010

import lxml.html
import csv

def parse(url, missingCell="NA"):
    """
    Parses all HTML tables found at the given url. Missing data or those
    without text content will be replaced with the missingCell string.

    Returns a list of lists of strings, corresponding to rows within all
    found tables.
    """
    doc = lxml.html.parse(url)
    tableList = doc.xpath("/html//table")
    dataList = []
    for table in tableList:
        dataList.append(parseTable(table, missingCell))
    return dataList

def parseTable(table, missingCell):
    """
    Parses the individual HTML table, returning a list of its rows.
    """
    rowList = []
    for row in table.xpath('.//tr'):
        colList = []
        cells = row.xpath('.//th') + row.xpath('.//td')
        for cell in cells:
            # The individual cell's content
            content = cell.text_content().encode("utf8")
            if content == "":
                content = missingCell
            colList.append(content)
        rowList.append(colList)
    return rowList
    
#import htmlTableParser


#p = htmlTableParser
#tableList = p.parse("http://www.digitaltruth.com/chart/search_text.php?Film=HP5", "-")

if __name__ == '__main__':
    import sys
    import csv
    
    #"http://www.digitaltruth.com/chart/search_text.php?Film=HP5"
    
    if len(sys.argv) != 2 and len(sys.argv) != 3:
        print "html2csv <URL> <CSVOUT>"
        sys.exit()
    
    #print "Processing: %s" % sys.argv[1];
    
    if len(sys.argv) == 3:
        tableList = parse(sys.argv[1], "-")
        csvWriter = csv.writer(open(sys.argv[2], "w"),
                       delimiter=';',
                       quotechar='|',
                       quoting=csv.QUOTE_MINIMAL)
    elif len(sys.argv) == 2:
        tableList = parse(sys.argv[1], "-")
        csvWriter = csv.writer(sys.stdout,
                       delimiter=';',
                       quotechar='|',
                       quoting=csv.QUOTE_MINIMAL)
    
    
    for table in tableList:
        for row in table:
            csvWriter.writerow(row)