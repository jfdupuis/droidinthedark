#!/usr/bin/perl

#convert massive dev chart time to csv


my @films = `perl xurl.pl http://www.digitaltruth.com/chart/print.html | grep search_text.php?Film`;
my @developer = `perl xurl.pl http://www.digitaltruth.com/chart/print.html | grep search_text.php?Developer`;

my $output = "out.csv";

open FILE, ">$output" or die $!;
print FILE $str;

my $index = 0;
foreach $film (@films) {
	chomp($film);
	
	print $.;
	
	#correctring form bad behavior for xurl
	$film =~ s/chart\/chart/chart/g;
	print "Processing: $film\n";
	
	my @csv = `python html2csv.py $film`;
	
	my $header = $csv[0];
	shift @csv;
	
	if ($index == 0) {
		print FILE $header;
	}
	$index++;
		
	print FILE @csv;

}

close FILE;
