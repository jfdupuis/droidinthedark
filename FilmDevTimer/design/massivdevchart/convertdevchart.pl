#!/usr/bin/perl
#
#	Convert Massive Dev Chart in csv with headers:
#   	Film;Developer;Dilution;ASA/ISO;35mm;120;Sheet;Temp;Notes
#
#	to a csv file with the following headers that can be imported
#	in the film developing timer.
#   	"_id","dev_name","formula","brand","film_namecode","film_iso","color","shot_iso","format","temp","N","time"
#

## Take care of the line endings. Should be only LF

##################
## Filters, 
##################

# List the notes you want to ignor the entry
my @skipnotes = ('a','r','u','1a','2','3','4','5','6','7','8','9','10','13',
	'14','15','16','17','18','19','22','23','24','25','25a','25b','28','28a',
	'28b', '31', '33','36','38','39','41','42','43','44','45','47','48','49',
	'50','54','57','59b','60');

# Turn film and developer filter on or off. 0 = ON, 1 = OFF
my $filteroff = 0;

# List the film you want
my @films = (
	"APX 100", 
	"APX 400", 
	"Delta 100 Pro", 
	"Delta 100 Pro",
	"Delta 3200 Pro",
	"Delta 400 Pro",
	"FP4+",
	"HP5+",
	"Ilford Pan 100",
	"Ilford Pan 400",
	"Neopan 100 Acros",
	"Neopan 100ss",
	"Neopan 1600",
	"Neopan 400",
	"Ortho Plus",
	"Pan F+",
	"Plus-X",
	"Rollei Retro 100",
	"Rollei Retro 400",
	"TMax 100",
	"TMax 400",
	"TMax P3200",
	"Tri-X 400"
);

# List the developer you want
my @developers = (
	"Rodinal",
	"Xtol"
);


##################

if( ($#ARGV > 1) || ($#ARGV < 0)) {
	print "\nUsage: convertdevchart src dst\n\n";
	exit 1 ;
}


use Text::CSV_XS;

my $file_in = $ARGV[0];
my $filename_out = ""; 

if( $#ARGV > 0) {
	$filename_out = $ARGV[1];
} else {
	$filename_out = "massiv_export.csv";
}
print "Writing output to: $filename_out\n\n";

my $incsv = Text::CSV_XS->new({ sep_char => ';' });
#$incsv->eol ("\r\n");


my $csv_out = Text::CSV_XS->new ({ binary => 1, eol => $/ });
open my $file_out, ">", "$filename_out" or die "$filename_out: $!";

open(my $data, '<', $file_in) or die "Could not open '$file_in'\n";
my $line_count = 0;
while (my $line = <$data>) {
	chomp $line;
	
	
	if($line_count > 0) {
		if ($incsv->parse($line)) {
		
			my @fields = $incsv->fields(); #incoming fields
			
			#Process only if in the desired list
			my %filmhash = map { $_, 1 } @films;
			my %devhash = map { $_, 1 } @developers;
			if( $filteroff || ($filmhash{$fields[0]} && $devhash{$fields[1]}) ){ 
				for (my $count = 0; $count < 3; $count++) {
		 
					if($fields[$count+4] ne "-") {
						my @output_fields;

						push(@output_fields, "-1"); 		#_id
						push(@output_fields, $fields[1]); 	#developer
						push(@output_fields, ""); 			#formula
						push(@output_fields, $fields[2]); 	#dilution
						push(@output_fields, "-"); 			#emulsion_brand
						push(@output_fields, $fields[0]); 	#emulsion_name
						push(@output_fields, "-"); 			#emulsion_iso
						push(@output_fields, "-"); 			#emulsion_iscolor
						
						#In each line, create 3 entries, one for each format {135, 120, sheet} 
						if($fields[3] ne "n/a"  && $fields[3] ne "?") {
							@isos = split(/-/, $fields[3]);
							foreach $iso (@isos) {
								
								@output_fields2 = @output_fields;
								push(@output_fields2, $iso); 		#shot iso
								
								if($count == 0) { 					#format 
									push(@output_fields2, "135");
								} elsif($count == 1) {
									push(@output_fields2, "120");
								} else {
									push(@output_fields2, "Sheet");
								}
								
								my $temp = $fields[7];				#temp
								#removing units from number
								my $unit = chop($temp);
								if($unit eq "C") {
									push(@output_fields2, $temp);
								} else {
									push(@output_fields2, $fields[7]);
								}
									
								push(@output_fields2, "0"); 		#N, assuming N=0
								
								#enter Not Specified agitation
								push(@output_fields2, "0");			#agitation_initial
								push(@output_fields2, "-2"); 	#agitation_interval, special "Not specified" key
								push(@output_fields2, "0");			#agitation_duration

								push(@output_fields2, $fields[$count+4] * 60); #time for field {4,5,6}, converted to seconds
								
								if($fields[8] eq "-") {
									push(@output_fields2, "");		#note
								} else {
									push(@output_fields2, $fields[8]);	
								}
								
								
								push(@output_fields2, "mdc");		#source
								
								#skip entry with notes that match filter
								my $notes = $fields[8];	
								my $skipentry = 0;
								foreach $note (@skipnotes) {
									if($notes =~ m/\Q[\E$note\Q]\E/) {
										#print "found $note, skipping entry\n";
										$skipentry = 1;
										last;
									}
								}
								
								if($skipentry == 0 ) {
									if ($csv_out->combine (@output_fields2)) {
										print $file_out $csv_out->string;
									}
									else {
										$csv_out->error_input, "\n";
									}
								}
								
							}
						}
					}
				}
			}
		
		} else {
			warn "Line could not be parsed: $line\n";
		}
	} else {
		#insert headers
		my $header = "\"_id\",\"name\",\"formula\",\"brand\",\"namecode\",\"iso\",\"color\",\"shot_iso\",\"format\",\"temp\",\"N\",\"time\",\"source\"\n";
		print $file_out $header;
	}
	$line_count++;
}

close $file_out or die "$filename_out: $!";



