package com.darkroom.filmdevtimer;

import com.darkroom.library.DevelopmentTime;
import com.darkroom.library.dao.DarkroomDBAdapter;
import com.darkroom.library.dao.DarkroomDBHelper;

import android.database.Cursor;
import android.database.SQLException;

import java.util.ArrayList;

public class TemperatureInterpolator {
	DarkroomDBAdapter mDarkroomDBAdapter;
	DevelopmentTime mDevelopmentTime;
	
	ArrayList<Float> mTemperatures;
	ArrayList<Float> mTimes;
	
//	HashMap<Float, Integer> mTempTimeMap; 
	
	public TemperatureInterpolator(DarkroomDBAdapter inDBAdapter){
		mDarkroomDBAdapter = inDBAdapter;
		mTemperatures = new ArrayList<Float>();
		mTimes = new ArrayList<Float>();
	}
	
	public void setDevelopmentTime(DevelopmentTime inValue) {
		mDevelopmentTime = inValue;
		mTemperatures.clear();
		mTimes.clear();
		
		
		//TODO get all temperature for this configuration
		String where =
			DarkroomDBHelper.KEY_DEVTIME_DEVELOPERID + "=" + inValue.mDeveloper
			+ " and "+ DarkroomDBHelper.KEY_DEVTIME_DILUTIONID + "=" + inValue.mDilution
			+ " and "+ DarkroomDBHelper.KEY_DEVTIME_EMULSIONID + "=" + inValue.mEmulsion
			+ " and "+ DarkroomDBHelper.KEY_DEVTIME_AGITATIONID + "=" + inValue.mAgitation
			+ " and "+ DarkroomDBHelper.KEY_DEVTIME_EMULSIONFORMAT + "=\"" + inValue.mFormat + "\""
			+ " and "+ DarkroomDBHelper.KEY_DEVTIME_SHOTISO + "=" + inValue.mISO
			+ " and "+ DarkroomDBHelper.KEY_DEVTIME_N + "=" + inValue.mAlteration;
			//+ " and "+ DarkroomDBHelper.KEY_DEVTIME_TEMP + "=" + inValue.mTemp
			//+ " and "+ DarkroomDBHelper.KEY_DEVTIME_TIME + "=" + inValue.mTime;
			//+ " and "+ DarkroomDBHelper.KEY_DEVTIME_SOURCE + "=" + inValue.mSource; 
			//TODO handle source
			//Note
		
		try {
			Cursor lCursor = mDarkroomDBAdapter.getAllEmulsionDevTimeCursor(where, null, DarkroomDBHelper.KEY_DEVTIME_TEMP);
			int lIdx = 0;
			if (lCursor.moveToFirst()) {
				do {
					mTemperatures.add(lIdx, lCursor.getFloat(lCursor.getColumnIndex(DarkroomDBHelper.KEY_DEVTIME_TEMP)));
					mTimes.add(lIdx, lCursor.getFloat(lCursor.getColumnIndex(DarkroomDBHelper.KEY_DEVTIME_TIME)));
					lIdx++;
				} while (lCursor.moveToNext());
			}
			lCursor.close();
			
		} catch(SQLException e) {
			//Logger.e("No development time found in database!",e);
			Logger.d("No development time found in database!");
		}
	}
	
	/**
	 * Interpolate the development time between the available time
	 * in the database. 
	 * @return Time value in seconds, 0 if not found
	 */
	float getInterpolatedTime(float inTemperature) {
		
		//Find if there is inTemperature is inbetween known value
		int lIdx = -1;
		for(int i = 0; i < mTemperatures.size() - 1; ++i) {
			if(mTemperatures.get(i) == inTemperature)
				return inTemperature;
			
			if(mTemperatures.get(i) <= inTemperature  && mTemperatures.get(i+1) > inTemperature) {
				lIdx = i;
				break;
			}
		}
		
		if(lIdx < 0)
			return 0;
		
		float dx = inTemperature-mTemperatures.get(lIdx);
		return (dx*mTimes.get(lIdx+1) - dx*mTimes.get(lIdx))/(mTemperatures.get(lIdx+1) - mTemperatures.get(lIdx)) + mTimes.get(lIdx);
	}
}
