package com.darkroom.filmdevtimer;

import com.darkroom.filmdevtimer.dao.FilmDevTimerDBAdapter;
import com.darkroom.library.Agitation;
import com.darkroom.library.Chemical;
import com.darkroom.library.DevelopmentTime;
import com.darkroom.library.Emulsion;
import com.darkroom.library.dao.DarkroomDBHelper;
import com.darkroom.library.dao.StringData;

import com.jfdupuis.library.dao.CSVReader;
import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ProgressBar;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;

public class DBImporter extends Activity {
	//private final static int ID = 0;
	private final static int DEVNAME = 1;
	private final static int DEVFORMULA = 2;
	private final static int DEVDILUTION = 3;
	private final static int EMULBRAND = 4;
	private final static int EMULNAME = 5;
	private final static int EMULISO = 6;
	private final static int EMULCOLOR = 7;
	private final static int ISO = 8;
	private final static int FORMAT = 9;
	private final static int TEMP = 10;
	private final static int ALTER = 11;
	private final static int AINITIAL = 12;
	private final static int AINTERVAL = 13;
	private final static int ADURATION = 14;
	private final static int TIME = 15;
	private final static int NOTE = 16;
	private final static int SOURCE = 17;
	
	
	FilmDevTimerDBAdapter mDarkroomDBAdapter = null;
	String mFilename = null;
	private ProgressBar mProgress;
    private Handler mHandler = new Handler();
    
    private long mFileLength = 1;
    private long mProgressStatus = 0;
    
    private boolean mStopImport = false;
    Context mContext;
	
    Thread mImportThread = null;
    
    private boolean mOverwrite = true;
    
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.db_importer_csv);
		mProgress = (ProgressBar) findViewById(R.id.progressBar2);
		this.setTitle(R.string.importing_db);
		mContext = this;
		
		
		mFilename = getIntent().getStringExtra("filename");
		mOverwrite = getIntent().getBooleanExtra("overwrite", false);
		
		try {
			LineNumberReader  lnr = new LineNumberReader(new FileReader(new File(mFilename)));
			lnr.skip(Long.MAX_VALUE);
			mFileLength = lnr.getLineNumber();
			mProgress.setMax((int) mFileLength);
		} catch (IOException e) {
			Logger.e("Can open file " + mFilename, e);
		}
		
		mImportThread = new Thread(new importDBTask());
		mImportThread.start();
		
		
	}
	
	@Override
	public void onBackPressed() {
		mStopImport = true;
	}
	
	protected class importDBTask implements Runnable {

		public void run() {
			Logger.d("Import database from " + mFilename);
			
			mDarkroomDBAdapter = new FilmDevTimerDBAdapter(mContext);
			mDarkroomDBAdapter.open();
			
			
			String lPath = null;
			File file = new File(lPath, mFilename);
			try {
				CSVReader csvReader = new CSVReader(new FileReader(file), ',', '"', 1);
				
				StringData lDilution = null;
				Chemical lDeveloper = null;
				Emulsion lEmulsion = null;
				Agitation lAgitation = null;
				DevelopmentTime lDevTime = null;
				String[] line = null;
				while( ((line = csvReader.readNext()) != null) && !mStopImport) {
					mProgressStatus++;
					mHandler.post(new Runnable() {
						public void run() {
							mProgress.setProgress( (int) mProgressStatus );
						}
					});
					
					try {
						//Developer
						if(line[DEVNAME].equals("-")) throw new IOException("Developper need a name!");
						lDeveloper = new Chemical(line[DEVNAME]);
						lDeveloper.setFormula(line[DEVFORMULA].equals("-") ? line[DEVFORMULA] : "");
						if(lDeveloper.getName().equals(DarkroomDBHelper.NOT_SPECIFIED))
							lDeveloper.setCategory("");
						else
							lDeveloper.setCategory(Chemical.DEVELOPER);
						long lDevId = findOrInsertChemical(lDeveloper);
						
						//Agitation
						lAgitation = new Agitation(Integer.valueOf(line[AINITIAL]), Integer.valueOf(line[AINTERVAL]), Integer.valueOf(line[ADURATION]));
						long lAgiId = findOrInsertAgitation(lAgitation);
						
						//Dilution
						lDilution = new StringData(line[DEVDILUTION]);
						long lDilutionId = findOrInsertDilution(lDilution);
						
						//Emulsion
						if(line[EMULNAME].equals(DarkroomDBHelper.NOT_SPECIFIED)) {
							lEmulsion = new Emulsion(DarkroomDBHelper.NOT_SPECIFIED);
							lEmulsion.mBrand = "";
						}else{
							lEmulsion = new Emulsion(line[EMULNAME].equals("-") ? "" : line[EMULNAME]);
							lEmulsion.mBrand = line[EMULBRAND].equals("-") ? "" : line[4];
						}
						try{
							lEmulsion.mISO = Integer.valueOf( line[EMULISO] );
						} catch(NumberFormatException e) {
							lEmulsion.mISO = 0;
						}
						
						if(line[EMULCOLOR].equals("0") ||  line[EMULCOLOR].equals("-"))
							lEmulsion.mColor = false;
						else 
							lEmulsion.mColor = true;
						long lEmulsionId = findOrInsertEmulsion(lEmulsion);
						
						//Dev time
						lDevTime = new DevelopmentTime((String)null);
						lDevTime.mFormat = line[FORMAT];
						lDevTime.mEmulsion = lEmulsionId; //find emulsion
						lDevTime.mISO = Integer.valueOf(line[ISO]);
						lDevTime.mDeveloper = lDevId;
						lDevTime.mAgitation = lAgiId;
						lDevTime.mTemp = Float.valueOf(line[TEMP]); //float
						lDevTime.mTime = Integer.valueOf(line[TIME]);
						lDevTime.mAlteration = Integer.valueOf(line[ALTER]);
						lDevTime.mSource = line[SOURCE];
						lDevTime.mNotes = line[NOTE];
						lDevTime.mDilution = lDilutionId;
						lDevTime.mFixTime = 0;
						findOrInsertDevTime(lDevTime);	
						
					}  catch (NumberFormatException e){
							Logger.w("Ignoring bad entry for "
									+ lEmulsion.getName() 
									+ " in " + lDeveloper.getName() 
									+ " at " + lDevTime.mISO	
									+ " for " + lDevTime.mFormat 
									);
					}
				} //End while
				setResult(Activity.RESULT_OK);
			} catch (FileNotFoundException e) {
				Logger.e("File not found, can't import.", e);
				setResult(Activity.RESULT_CANCELED);
			} catch (IOException e) {
				Logger.e("Problem reading file. Abort import.", e);
				setResult(Activity.RESULT_CANCELED);
			} finally {
				if(mDarkroomDBAdapter != null)
					mDarkroomDBAdapter.close();
				finish();
			}
			
		}
	}
	
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		
	}
	
	protected long findOrInsertDilution(StringData inValue) {
		String where = DarkroomDBHelper.KEY_STRING + "=\"" + inValue.getString() + "\"";
		Cursor lCursor = mDarkroomDBAdapter.getDataBase().query(DarkroomDBHelper.DILUTION_TABLE_NAME, null, where, null, null, null, null);

		if ((lCursor.getCount() > 1)) {
			throw new SQLException(String.format("Multiple dilution found for same values for : " + inValue.getString()));
		}

		if ((lCursor.getCount() == 0) || !lCursor.moveToFirst()) {
			lCursor.close();
			return mDarkroomDBAdapter.insertStringData(DarkroomDBHelper.DILUTION_TABLE_NAME, inValue);
		} else {
			long lId = lCursor.getLong(lCursor.getColumnIndex(DarkroomDBHelper.KEY_ID));
			lCursor.close();
			return lId;
		}
	}
	
	protected long findOrInsertAgitation(Agitation inValue) {
		String where = DarkroomDBHelper.KEY_AGITATION_INITIAL + "=\"" + inValue.getInitial() + "\""
					 + " and " + DarkroomDBHelper.KEY_AGITATION_INTERVAL + "=\"" + inValue.getInterval() + "\""
					 + " and " + DarkroomDBHelper.KEY_AGITATION_DURATION + "=\"" + inValue.getDuration() + "\"";
		Cursor lCursor = mDarkroomDBAdapter.getDataBase().query(DarkroomDBHelper.AGITATION_TABLE_NAME, null, where, null, null, null, null);
		
		if ((lCursor.getCount() > 1) ) {
			throw new SQLException(String.format("Multiple agitation found for same values for : " + inValue.toString()));
		}
		
		if ((lCursor.getCount() == 0) || !lCursor.moveToFirst()) {
			lCursor.close();
			return mDarkroomDBAdapter.insertAgitation(inValue);
		} else {
			long lId = lCursor.getLong(lCursor.getColumnIndex(DarkroomDBHelper.KEY_ID));
			lCursor.close();
			return lId;
		}
	}
	
	protected long findOrInsertChemical(Chemical inValue) {
		String where = DarkroomDBHelper.KEY_CHEMICAL_NAME + "=\"" + inValue.getName() + "\""
					 + " and " + DarkroomDBHelper.KEY_CHEMICAL_CATEGORY + "=\"" + inValue.getCategory() + "\"";
		Cursor lCursor = mDarkroomDBAdapter.getDataBase().query(DarkroomDBHelper.CHEMICAL_TABLE_NAME, null, where, null, null, null, null);
		
		if ((lCursor.getCount() > 1) ) {
			throw new SQLException(String.format("Multiple chemical found for same values for : " + inValue.getName()));
		}
		
		if ((lCursor.getCount() == 0) || !lCursor.moveToFirst()) {
			lCursor.close();
			return mDarkroomDBAdapter.insertChemical(inValue);
		} else {
			long lId = lCursor.getLong(lCursor.getColumnIndex(DarkroomDBHelper.KEY_ID));
			lCursor.close();
			return lId;
		}
	}
	
	protected long findOrInsertEmulsion(Emulsion inValue) {
		String where = DarkroomDBHelper.KEY_EMULSION_NAME + "=\"" + inValue.getName() + "\""; 
		Cursor lCursor = mDarkroomDBAdapter.getDataBase().query(DarkroomDBHelper.EMULSION_TABLE_NAME, null, where, null, null, null, null);

		if ((lCursor.getCount() > 1) ) {
			throw new SQLException(String.format("Multiple emulsion found with same name for : " + inValue.getName()));
		}
		
		if ((lCursor.getCount() == 0) || !lCursor.moveToFirst()) {
			lCursor.close();
			return mDarkroomDBAdapter.insertEmulsion(inValue);
		} else {
			long lId = lCursor.getLong(lCursor.getColumnIndex(DarkroomDBHelper.KEY_ID));
			lCursor.close();
			return lId; 
		}
	}
	
	protected long findOrInsertDevTime(DevelopmentTime inValue) {
		String where = DarkroomDBHelper.KEY_DEVTIME_DEVELOPERID + "=" + inValue.mDeveloper
		+ " and "+ DarkroomDBHelper.KEY_DEVTIME_DILUTIONID + "=" + inValue.mDilution
		+ " and "+ DarkroomDBHelper.KEY_DEVTIME_EMULSIONFORMAT + "=\"" + inValue.mFormat + "\""
		+ " and "+ DarkroomDBHelper.KEY_DEVTIME_EMULSIONID + "=" + inValue.mEmulsion
		+ " and "+ DarkroomDBHelper.KEY_DEVTIME_AGITATIONID + "=" + inValue.mAgitation
		+ " and "+ DarkroomDBHelper.KEY_DEVTIME_N + "=" + inValue.mAlteration
		+ " and "+ DarkroomDBHelper.KEY_DEVTIME_SHOTISO + "=" + inValue.mISO
		+ " and "+ DarkroomDBHelper.KEY_DEVTIME_TEMP + "=" + inValue.mTemp
		//+ " and "+ DarkroomDBHelper.KEY_DEVTIME_TIME + "=" + inValue.mTime
		+ " and "+ DarkroomDBHelper.KEY_DEVTIME_SOURCE + "=\"" + inValue.mSource + "\"";
		//NOTE
		Cursor lCursor = mDarkroomDBAdapter.getDataBase().query(DarkroomDBHelper.EMULSION_DEVTIME_TABLE_NAME, null, where, null, null, null, null);
		
		if ((lCursor.getCount() > 1) ) {
			throw new SQLException(String.format("Error multiple development time found, database is corrupted."));
		}
		
		
		
		if ((lCursor.getCount() == 0) || !lCursor.moveToFirst()) {
			//No entry in the database match the new one. 
			lCursor.close();
			try {
				return mDarkroomDBAdapter.insertEmulsionDevTime(inValue);
			} catch(SQLiteConstraintException e) {
				mDarkroomDBAdapter.getChemical(inValue.mDeveloper);
				mDarkroomDBAdapter.getEmulsion(inValue.mEmulsion);
				Logger.w("Error while inserting new development time. Ignoring duplicated entry for "
						+ mDarkroomDBAdapter.getEmulsion(inValue.mEmulsion).getName()
						+ " in " + mDarkroomDBAdapter.getChemical(inValue.mDeveloper).getName()
						+ " at " + inValue.mISO	
						+ " for " + inValue.mFormat
				);
				return -1;
			}
		} else {
			//An entry that match was found.
			long lId = lCursor.getLong(lCursor.getColumnIndex(DarkroomDBHelper.KEY_ID));
			inValue.setId(lId);
			///TODO Should we update the values
			if(mOverwrite) {
				mDarkroomDBAdapter.updateEmulsionDevTimeValue(inValue);
			}
						
			lCursor.close();
			return lId;
		}
	}

}
