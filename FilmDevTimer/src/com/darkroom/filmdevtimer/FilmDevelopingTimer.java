package com.darkroom.filmdevtimer;


import java.util.ArrayList;
import java.text.ParseException;

import com.darkroom.filmdevtimer.R;
import com.darkroom.filmdevtimer.dao.FilmDevTimerDBAdapter;
import com.darkroom.filmdevtimer.preference.FilmDevTimerPrefs;
import com.darkroom.filmdevtimer.Logger;
import com.darkroom.library.Agitation;
import com.darkroom.library.Chemical;
import com.darkroom.library.Emulsion;
import com.darkroom.library.DevelopmentTime;
import com.darkroom.library.dao.AddNewAgitationDialog;
import com.darkroom.library.dao.AddNewChemicalDialog;
import com.darkroom.library.dao.AddNewEmulsionDialog;
import com.darkroom.library.dao.AddNewStringDataDialog;
import com.darkroom.library.dao.DarkroomDBAdapter;
import com.darkroom.library.dao.DarkroomDBHelper;
import com.jfdupuis.library.ArrayUtil;
import com.jfdupuis.library.string.StringUtil;
import com.jfdupuis.library.help.SimpleUsage;
import com.jfdupuis.library.widget.DBSpinnerDialog;
import com.jfdupuis.library.widget.DBSpinner;
import com.jfdupuis.library.widget.SoundTimerAlarm;
import com.jfdupuis.library.widget.SoundTimerAlarm.OnTimerEventListener;
import com.jfdupuis.library.widget.voice.VoiceCommander.onCommandListener;
import com.jfdupuis.library.widget.wheel.WheelView;
import com.jfdupuis.library.widget.wheel.adapters.AbstractWheelTextAdapter;
import com.jfdupuis.library.widget.wheel.adapters.ArrayWheelAdapter;
import com.jfdupuis.library.widget.wheel.adapters.FloatWheelAdapter;
import com.jfdupuis.library.widget.wheel.OnWheelChangedListener;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.PowerManager;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnKeyListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;



/**
 * 
 * Future Features
 * - Delete entries
 * - Temperature interpolation
 * - Multiple film in one session
 * - Communication with notekeeper
 * - Custom list of step
 * 
 * @author jfdupuis
 */
public class FilmDevelopingTimer extends Activity {//implements OnClickListener {
	static final int REQUEST_ADD_EMULSION = 3;
	static final int REQUEST_ADD_DEVELOPER = 0;
	static final int REQUEST_ADD_DILUTION = 1;
	static final int REQUEST_ADD_AGITATION = 2;
	static final int REQUEST_MANAGEDB = 4;
	
	static final float MINIMUM_TEMPERATURE = 15;
	static final float MAXIMUM_TEMPERATURE = 45;
	static final float INCREMENT_TEMPERATURE = 0.5f;
	
	static final String TEMPERATURE_INTERPOLATED = "x";
	
	private PowerManager.WakeLock mWakeLock = null;
   
	TimerVoiceCommand mTimerVoiceCommand = null;
	
	private static MediaPlayer mToggleSound = null;
    
    EditText mDevTime = null;
    EditText mFixTime = null;
    RadioButton mCheckDev = null;
    RadioButton mCheckFix = null;
    
    CheckBox mDevTimeLock;
    
    FilmDevTimerDBAdapter mDarkroomDBAdapter;
    
    WheelView mTempWheel;
    WheelView mFormatWheel;
    WheelView mIsoWheel;
    ArrayWheelAdapter<Integer> mIsoAdapter;
    WheelView mDevAlterWheel;
	DBSpinner mEmulsionSpinner;
	DBSpinner mDeveloperSpinner;
	DBSpinner mAgitationSpinner;
	DBSpinner mDilutionSpinner;
    
	FilmDevTimerAlarm mDevTimer;
	TemperatureInterpolator mTempInterpol;
	
	DevelopmentTime mDevelopmentTime = null;
	
	int mCurrentEditTime = 0;
	
	boolean mSelectionLockState = false;
	ArrayList<View> mLockableView = null;
		
	protected class SourcedTime {
		public int mTime;
		public String mSource = null;
		public SourcedTime(int inTime, String inSource) {
			mTime = inTime; mSource = inSource;
		}
	}
    
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.filmdevtimer);
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
            		
        
        mLockableView = new ArrayList<View>();
        mSelectionLockState = true;
        
        mDarkroomDBAdapter = new FilmDevTimerDBAdapter(this);
        mDarkroomDBAdapter.open();
        
        mTempInterpol = new TemperatureInterpolator(mDarkroomDBAdapter);
        
        mDevTimeLock = (CheckBox) findViewById(R.id.devtime_lock);
        mDevTimeLock.setChecked(false);
        
        mDevTimer = (FilmDevTimerAlarm) findViewById(R.id.devtimer);
        mDevTimer.setOnTimerEventListener(new OnTimerEventListener() {
			public void onEvent(int inEvent) {
				processTimerEvent(inEvent);
			}
        });
        
        mCheckDev = (RadioButton) findViewById(R.id.checkDevTime);
        mCheckDev.setChecked(true);
        mCheckDev.setOnCheckedChangeListener( new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked && mToggleSound != null && FilmDevTimerPrefs.playSounds(FilmDevelopingTimer.this)) { 
					playSound(mToggleSound);
				}
				updateTimerValue();
			}
        });
        
        
        mCheckFix = (RadioButton) findViewById(R.id.checkFixTime);
        mCheckFix.setOnCheckedChangeListener( new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked && mToggleSound != null && FilmDevTimerPrefs.playSounds(FilmDevelopingTimer.this)) { 
					playSound(mToggleSound);
				}
				updateTimerValue();
			}
        });
        
        mDevTime = (EditText) findViewById(R.id.DevTime);
        mDevTime.setText(StringUtil.doubleToString(0.0));
        mDevTime.setOnKeyListener( new OnKeyListener() {
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if ((event.getAction() == KeyEvent.ACTION_DOWN) && ( (keyCode == KeyEvent.KEYCODE_DPAD_CENTER) || (keyCode == KeyEvent.KEYCODE_ENTER))) {
					int lNewTime = 0;
					try{
						lNewTime = (int)(StringUtil.stringToDouble(mDevTime.getText().toString())*60);
					} 
					catch (ParseException e) {
						Logger.e("Bad time entered. Setting to 0",e);
						lNewTime = 0;
						mDevTime.setText(StringUtil.doubleToString(0.0));
					}
					if(lNewTime != mCurrentEditTime) {
						updateTimeColor(new SourcedTime(lNewTime, null));
					}
					updateTimerValue();
			    }
				return false;
			}
        	
        });
        mDevTime.setOnFocusChangeListener(new OnFocusChangeListener() {
			public void onFocusChange(View mArg0, boolean mArg1) {
				int lNewTime = 0;
				try{
					lNewTime = (int)(StringUtil.stringToDouble(mDevTime.getText().toString())*60);
				} 
				catch (ParseException e) {
					Logger.e("Bad time entered. Setting to 0",e);
					lNewTime = 0;
					mDevTime.setText(StringUtil.doubleToString(0.0));
				}
				if(lNewTime != mCurrentEditTime) {
					updateTimeColor(new SourcedTime(lNewTime, null) );
				}
				updateTimerValue();
			}
        });
        
        mFixTime = (EditText) findViewById(R.id.FixTime);
        mFixTime.setText(FilmDevTimerPrefs.getDefaultFixTime(this));
        mFixTime.setOnKeyListener( new OnKeyListener() {
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if ((event.getAction() == KeyEvent.ACTION_DOWN) && ( (keyCode == KeyEvent.KEYCODE_DPAD_CENTER) || (keyCode == KeyEvent.KEYCODE_ENTER))) {
						updateTimerValue();
				    }
				return false;
			}
        	
        });
        mFixTime.setOnFocusChangeListener(new OnFocusChangeListener() {
			public void onFocusChange(View mArg0, boolean mArg1) {
				updateTimerValue();
			}
        });
        
        
		setupSpinners();
        loadSavedInstance(savedInstanceState);
        changedSelection(0);
        mDevTimer.mUpdateTimeHandler.sendEmptyMessage(0);
        
        //mTimerVoiceCommand = new TimerVoiceCommand(this);
        mTimerVoiceCommand = (TimerVoiceCommand) findViewById(R.id.voice_command);
        mTimerVoiceCommand.setCommandListener(new onCommandListener() {
			@Override
			public void onCommand(int inCommand) {
				processCommand(inCommand);
			}
        });
        
        
        
        new SimpleUsage(this).show(getString(R.string.updates) 
				 + "\n\n" + getString(R.string.info)
				 + "\n");
    }
	
	private void initializeSounds() {
        if (mToggleSound == null && FilmDevTimerPrefs.playSounds(this)) {
        	mToggleSound = MediaPlayer.create(this, R.raw.rocker_switch);
        }
	}
	
	private synchronized void playSound(MediaPlayer mp) {
		try {
			mp.seekTo(0);
			mp.start();
		} catch (Exception e) {
			Logger.v("Error while playing sound!" + e.getMessage());
		}
	}
	
	protected void processTimerEvent(int inEvent) {
		Logger.d("Processing timer event");
		if(inEvent == SoundTimerAlarm.TIMER_START) {
			if(FilmDevTimerPrefs.useVoiceRecognitionOnlyStopped(FilmDevelopingTimer.this)) {
				Logger.d("Stopping voice recognition");
				mTimerVoiceCommand.stopListening();
			}
			
			mDevelopmentTime = getDevelopmentSelection();
			updateTimerValue(); //Make sure the current edited value are sent to the timer
			
		} else if(inEvent == SoundTimerAlarm.TIMER_STOP) {
			if(FilmDevTimerPrefs.useVoiceRecognition(this)) {
				Logger.d("Starting voice recognition");
				mTimerVoiceCommand.startListening();
			}
		}
	}
	
	protected void processCommand(int inCommand) {
		switch(inCommand) {
		case TimerVoiceCommand.START_COMMAND:
		case TimerVoiceCommand.STOP_COMMAND:
			mDevTimer.processStartStopButtonClick();
			break;
		case TimerVoiceCommand.RESET_COMMAND:
			mDevTimer.processResetButtonClick();
			break;
		case TimerVoiceCommand.NEXT_COMMAND:
			//TODO
			Logger.d("Next command received");
			break;
		case TimerVoiceCommand.DEV_COMMAND:
			//TODO
			Logger.d("Dev command received");
			mCheckDev.setChecked(true);
			break;
		case TimerVoiceCommand.FIX_COMMAND:
			//TODO
			Logger.d("Fix command received");
			mCheckFix.setChecked(true);
			break;
		}
		
	}
	
	@Override
    protected void onSaveInstanceState(Bundle saveState) {
        super.onSaveInstanceState(saveState);
        Logger.d("onSaveInstanceState");
        mDevelopmentTime = getDevelopmentSelection();
        saveState.putParcelable("devtime", mDevelopmentTime);
    }
    
	protected void loadSavedInstance(Bundle savedInstanceState) {
		Logger.d("loadSavedInstance");
        //Setup the devtime
        if (savedInstanceState != null){
        	//Get the saved devtime 
        	Logger.d("Loading saved instance");
        	mDevelopmentTime = savedInstanceState.getParcelable("devtime");
        } else if(getIntent().getLongExtra("devtime_id", -1) > 0) {
        	//Get the devtime put in the intent
        	Logger.d("Loading from intent");
        	mDevelopmentTime = getIntent().getParcelableExtra("devtime");
        } else {
        	//Create a new one
        	Logger.d("Dev time is null");
        	mDevelopmentTime = null;
        }
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		Logger.d("onCreateOptionsMenu");
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.filmdevtimer, menu);
	    return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    case R.id.save_id:
	    	//Save modified development time
	    	saveTime();
	    	return true;
	    case R.id.lock_view_id:
	    	lockViews();
	    	return true;
	    case R.id.show_settings_id:
	    	showSettings();
	    	return true;
	    case R.id.manage_db_id:
	    	manageDB();
	    	return true;
	    default:
	        return super.onOptionsItemSelected(item);
	    }
	}
	
	protected void manageDB() {
		Intent intent = new Intent(this, DBManager.class);
        startActivityForResult(intent, REQUEST_MANAGEDB);
	}
	
	protected void showSettings() {
		startActivity(new Intent(this, FilmDevTimerPrefs.class));
	}
	
//	/**
//	 * While exposing, the activity should not be restarted
//	 */
//	@Override
//	public void onConfigurationChanged(Configuration newConfig) {
//		Logger.v("Configuration Changed"); 
//		super.onConfigurationChanged(newConfig);
//		//mDevTimer.mUpdateTimeHandler.sendEmptyMessage(0);
//		
//		setContentView(R.layout.filmdevtimer);
//	    flipper = (ViewFlipper) findViewById(R.id.filmdevtimer);
//	}
	
	@Override
    protected void onPause() {
		super.onPause();
		mTimerVoiceCommand.stopListening();
	}
	
	@Override
    protected void onResume() {
        super.onResume();
        Logger.d("onResume");
        if(mDevelopmentTime == null)
        	Logger.d("Dev time is null");
        
        //Update display
        mDevTimer.mUpdateTimeHandler.sendEmptyMessage(0);
        updateTimerValue();
        
        acquireWakeLock();
        //ScreenUtil.setBrightness(FilmDevelopingTimer.this,(float)(FilmDevTimerPrefs.getScreenBrightness(this))/100.0f, 0);
        
        if(FilmDevTimerPrefs.useVoiceRecognition(this))
        	mTimerVoiceCommand.startListening();
        
        initializeSounds();
    }
	
	
	@Override 
	protected void onDestroy() {
		super.onDestroy();
		Logger.d("onDestroy");
		releaseWakeLock();
		mDevTimer.shutdownTimer();
		mTimerVoiceCommand.destroy();
		try{
        	mDarkroomDBAdapter.close();
        } catch(NullPointerException e) {
        	Logger.e("DBAdapter is null !", e);
        }
	}
	
	/**
	 * Avoid the device to go to sleep while computing the exposure.
	 */
	private void acquireWakeLock() {
        if (mWakeLock == null) {
            Logger.v("Acquiring wake lock");
            PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, this.getClass().getCanonicalName());
            mWakeLock.acquire();
        }
    }
	
	/**
	 * Once the exposre computing task is done, the device can sleep again.
	 */
    private void releaseWakeLock() {
        if (mWakeLock != null && mWakeLock.isHeld()) {
            Logger.v("Releasing wake lock");
            mWakeLock.release();
            mWakeLock = null;
        }
    }
	
   
    private void setupSpinners() {
    	// Agitation spinner
		mAgitationSpinner = (DBSpinner) findViewById(R.id.agitation_spinner);
		mAgitationSpinner.setAdapter(
        		mDarkroomDBAdapter.getAllAgitationCursor(new String[]{DarkroomDBHelper.KEY_STRING}, DarkroomDBHelper.KEY_STRING), 
				DarkroomDBHelper.KEY_STRING);
		mAgitationSpinner.setOnActionRequestHandler(new DBSpinner.OnActionRequest(){
			public void onActionRequest(Spinner mInSpinner, int inAction, int inPosition, long inId) {
				Intent lIntent = new Intent(FilmDevelopingTimer.this, AddNewAgitationDialog.class);
				switch(inAction) {
				case DBSpinnerDialog.ADD_REQUEST:
	            	break;
				case DBSpinnerDialog.EDIT_REQUEST:
					lIntent.putExtra("id", inId);
					break;
				case DBSpinnerDialog.ITEM_SELECTION:
					changedSelection(mInSpinner.getId());
					return;
				}
				startActivityForResult(lIntent,REQUEST_ADD_AGITATION);
			}
		});
        if(mDevelopmentTime == null)
        	try {
        		mAgitationSpinner.setPositionById(FilmDevTimerPrefs.getDefaultAgitation(this));
        	} catch(RuntimeException e) {
        		Logger.e("Can't set default agitation!",e);
        	}
		else {
			mAgitationSpinner.setPositionById(mDevelopmentTime.mAgitation);
		}
        mLockableView.add(mAgitationSpinner);
        
        
        // Agitation spinner
		mDilutionSpinner = (DBSpinner) findViewById(R.id.dilution_spinner);
		mDilutionSpinner.setAdapter(
        		mDarkroomDBAdapter.getAllStringDataCursor(DarkroomDBHelper.DILUTION_TABLE_NAME), 
				DarkroomDBHelper.KEY_STRING);
		mDilutionSpinner.setOnActionRequestHandler(new DBSpinner.OnActionRequest(){
			public void onActionRequest(Spinner mInSpinner, int inAction, int inPosition, long inId) {
				Intent lIntent = new Intent(FilmDevelopingTimer.this, AddNewStringDataDialog.class);
				lIntent.putExtra("table", DarkroomDBHelper.DILUTION_TABLE_NAME);
				lIntent.putExtra("havecategory", false);
				switch(inAction) {
				case DBSpinnerDialog.ADD_REQUEST:
					lIntent.putExtra("title", getString(R.string.edit_dilution_title));
	            	break;
				case DBSpinnerDialog.EDIT_REQUEST:
					lIntent.putExtra("title", getString(R.string.addnew_dilution_title));
					lIntent.putExtra("id", inId);
					break;
				case DBSpinnerDialog.ITEM_SELECTION:
					changedSelection(mInSpinner.getId());
					return;
				}
				startActivityForResult(lIntent,REQUEST_ADD_DILUTION);
			}
		});
        if(mDevelopmentTime == null)
        	try{
        		mDilutionSpinner.setPositionById(FilmDevTimerPrefs.getDefaultDilution(this));
        	} catch(RuntimeException e) {
        		Logger.e("Can't set default dilution!", e);
        	}
		else {
			mDilutionSpinner.setPositionById(mDevelopmentTime.mDilution);
		}
        mLockableView.add(mDilutionSpinner);
    	
    	// Developer spinner
		mDeveloperSpinner = (DBSpinner) findViewById(R.id.dev_spinner);
		mDeveloperSpinner.setAdapter(
				mDarkroomDBAdapter.getAllChemicalCursor( 
						new String[]{DarkroomDBHelper.KEY_CHEMICAL_NAME}, 
						DarkroomDBHelper.KEY_CHEMICAL_NAME,Chemical.DEVELOPER),
				DarkroomDBHelper.KEY_CHEMICAL_NAME);
		mDeveloperSpinner.setOnActionRequestHandler(new DBSpinner.OnActionRequest(){
			public void onActionRequest(Spinner mInSpinner, int inAction, int inPosition, long inId) {
				Intent lIntent = new Intent(FilmDevelopingTimer.this, AddNewChemicalDialog.class);
            	lIntent.putExtra("category", Chemical.DEVELOPER);
            	lIntent.putExtra("hidecategory", true);
				switch(inAction) {
				case DBSpinnerDialog.ADD_REQUEST:
	            	break;
				case DBSpinnerDialog.EDIT_REQUEST:
					lIntent.putExtra("id", inId);
					break;
				case DBSpinnerDialog.ITEM_SELECTION:
					//changedDeveloper(inId);
					changedSelection(mInSpinner.getId());
					return;
				}
				startActivityForResult(lIntent,REQUEST_ADD_DEVELOPER);
			}
		});
		if(mDevelopmentTime == null)
			try {
				mDeveloperSpinner.setPositionById(FilmDevTimerPrefs.getDefaultDeveloper(this));
			} catch(RuntimeException e) {
        		Logger.e("Can't set default developer!", e);
        	}
		else {
			mDeveloperSpinner.setPositionById(mDevelopmentTime.mDeveloper);
		}
        mLockableView.add(mDeveloperSpinner);
		
		//Emulsion spinner
        mEmulsionSpinner = (DBSpinner) findViewById(R.id.emulsion_spinner);
        mEmulsionSpinner.setAdapter(
        		mDarkroomDBAdapter.getAllEmulsionCursor(new String[]{DarkroomDBHelper.KEY_EMULSION_NAME}, DarkroomDBHelper.KEY_EMULSION_NAME), 
				DarkroomDBHelper.KEY_EMULSION_NAME);
        mEmulsionSpinner.setOnActionRequestHandler(new DBSpinner.OnActionRequest(){
			public void onActionRequest(Spinner mInSpinner, int inAction, int inPosition, long inId) {
				Intent lIntent = new Intent(FilmDevelopingTimer.this, AddNewEmulsionDialog.class);
				switch(inAction) {
				case DBSpinnerDialog.ADD_REQUEST:
	            	break;
				case DBSpinnerDialog.EDIT_REQUEST:
					lIntent.putExtra("id", inId);
					break;
				case DBSpinnerDialog.ITEM_SELECTION:
					matchShotISOToEmulsion();
					changedSelection(mInSpinner.getId());
					return;
				}
				startActivityForResult(lIntent,REQUEST_ADD_EMULSION);
			}
		});
        if(mDevelopmentTime == null)
        	try {
        		mEmulsionSpinner.setPositionById(FilmDevTimerPrefs.getDefaultEmulsion(this));
        	} catch(RuntimeException e) {
        		Logger.e("Can't set default emulsion!", e);
        	}
        
		else {
			mEmulsionSpinner.setPositionById(mDevelopmentTime.mEmulsion);
		}
        mLockableView.add(mEmulsionSpinner);
        
    	
        //Temperature Wheel
		mTempWheel = (WheelView) findViewById(R.id.temperature_wheel);
		mTempWheel.setViewAdapter(new FloatWheelAdapter(this, MINIMUM_TEMPERATURE, MAXIMUM_TEMPERATURE, INCREMENT_TEMPERATURE));
		((AbstractWheelTextAdapter) mTempWheel.getViewAdapter()).setItemResource(R.layout.wheel_item);
		
		if (mDevelopmentTime == null)
			mTempWheel.setCurrentItem(((FloatWheelAdapter)(mTempWheel.getViewAdapter())).getValueIndex(Float.valueOf((String) FilmDevTimerPrefs.getDefaultTemp(this))));
		else {
			mTempWheel.setCurrentItem(((FloatWheelAdapter)(mTempWheel.getViewAdapter())).getValueIndex(Float.valueOf(mDevelopmentTime.mTemp)));
		}
		mLockableView.add(mTempWheel);
        
		//ISO Wheel
		mIsoWheel = (WheelView) findViewById(R.id.iso_wheel);
		Integer[] lIso = ArrayUtil.convertString2Integer(this.getResources().getStringArray(R.array.iso)); 
		mIsoAdapter = new ArrayWheelAdapter<Integer>( this, lIso);
		mIsoWheel.setViewAdapter(mIsoAdapter);
		((AbstractWheelTextAdapter) mIsoWheel.getViewAdapter()).setItemResource(R.layout.wheel_item);
		
		if (mDevelopmentTime == null)
			matchShotISOToEmulsion();
		else {
			mIsoWheel.setCurrentItem(mIsoAdapter.getItemIndex(mDevelopmentTime.mISO));
		}
		mLockableView.add(mIsoWheel);
        
		//Format wheel
		mFormatWheel = (WheelView) findViewById(R.id.format_wheel);
		ArrayWheelAdapter<String> lFormatAdapter = new ArrayWheelAdapter<String>( this, this.getResources().getStringArray(R.array.emulsion_format));
		mFormatWheel.setViewAdapter(lFormatAdapter);
		((AbstractWheelTextAdapter) mFormatWheel.getViewAdapter()).setItemResource(R.layout.wheel_item);
		
		if (mDevelopmentTime == null)
			mFormatWheel.setCurrentItem(lFormatAdapter.getItemIndex((String) FilmDevTimerPrefs.getDefaultFormat(this)));
		else {
			mFormatWheel.setCurrentItem(lFormatAdapter.getItemIndex(mDevelopmentTime.mFormat));
		}
		mLockableView.add(mFormatWheel);

		//Developing alteration wheel
		mDevAlterWheel = (WheelView) findViewById(R.id.devalter_wheel);
		ArrayWheelAdapter<String> lNAdapter = new ArrayWheelAdapter<String>( this, this.getResources().getStringArray(R.array.devalteration));
		mDevAlterWheel.setViewAdapter(lNAdapter);
		((AbstractWheelTextAdapter) mDevAlterWheel.getViewAdapter()).setItemResource(R.layout.wheel_item);
		
		if (mDevelopmentTime == null)
			mDevAlterWheel.setCurrentItem(lNAdapter.getItemIndex("N"));
		else {
			int lN = mDevelopmentTime.mAlteration;
        	if(lN == 0)
        		mDevAlterWheel.setCurrentItem(lNAdapter.getItemIndex("N"));
	    	else
	    		mDevAlterWheel.setCurrentItem(lNAdapter.getItemIndex(lN < 0 ? String.format("N%d", lN) : String.format("N+%d", lN)));
		}
		mLockableView.add(mDevAlterWheel);
		
		addWheelListener();
	}
    
    void addWheelListener() {
    	mDevAlterWheel.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView inWheel, int inOldValue, int inNewValue) {
				changedSelection(inWheel.getId());
			}
		});
    	mFormatWheel.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView inWheel, int inOldValue, int inNewValue) {
				changedSelection(inWheel.getId());
			}
		});
    	mIsoWheel.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView inWheel, int inOldValue, int inNewValue) {
				changedSelection(inWheel.getId());
			}
		});
    	mTempWheel.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView inWheel, int inOldValue, int inNewValue) {
				changedSelection(inWheel.getId());
			}
		});
    }
	
	void matchShotISOToEmulsion() {
		final long lEmulsionId = mEmulsionSpinner.getSelectedItemId();
		int lPos = mIsoAdapter.getItemIndex(0);
		try {
			Emulsion lSelectedEmulsion = mDarkroomDBAdapter.getEmulsion(lEmulsionId);
			lPos = mIsoAdapter.getItemIndex(lSelectedEmulsion.mISO);
		} catch (SQLException inException) {
			Logger.v("Film editor: No emulsion found in database");
		}
		mIsoWheel.setCurrentItem(lPos);
	}
	
	void lockViews() {
		mSelectionLockState = !mSelectionLockState;
		for(View lView : mLockableView) {
			lView.setEnabled(mSelectionLockState);
		}
	}
	
	@SuppressWarnings("unchecked")
	DevelopmentTime getDevelopmentSelection() {
		DevelopmentTime lTime = new DevelopmentTime((String) null);
		
		lTime.mNotes = ""; //TODO future feature
		
		Cursor lCursor1 = (Cursor) mDilutionSpinner.getSelectedItem();
		lTime.mDilution = lCursor1.getInt(lCursor1.getColumnIndex(DarkroomDBAdapter.KEY_ID));
		
		Cursor lCursor2 = (Cursor) mAgitationSpinner.getSelectedItem();
		lTime.mAgitation = lCursor2.getInt(lCursor2.getColumnIndex(DarkroomDBAdapter.KEY_ID));
		
		Cursor lCursor3 = (Cursor) mEmulsionSpinner.getSelectedItem();
		lTime.mEmulsion = lCursor3.getInt(lCursor3.getColumnIndex(DarkroomDBAdapter.KEY_ID));
		 
		Cursor lCursor4 = (Cursor) mDeveloperSpinner.getSelectedItem(); //TODO get separate dilution dilution
		lTime.mDeveloper = lCursor4.getInt(lCursor4.getColumnIndex(DarkroomDBAdapter.KEY_ID));
		
		lTime.mISO = mIsoAdapter.getValue(mIsoWheel.getCurrentItem());
		lTime.mFormat = ((ArrayWheelAdapter<String>) mFormatWheel.getViewAdapter()).getValue(mFormatWheel.getCurrentItem());
		
		lTime.mTemp = ((FloatWheelAdapter) mTempWheel.getViewAdapter()).getValue(mTempWheel.getCurrentItem());
		
		String lNString = ((ArrayWheelAdapter<String>) mDevAlterWheel.getViewAdapter()).getValue(mDevAlterWheel.getCurrentItem());
		try {
			if(lNString.compareTo("N") == 0)
				lTime.mAlteration = 0;
			else {
				if(lNString.charAt(1) == '+')
					lTime.mAlteration = Integer.valueOf(lNString.substring(2));
				else
					lTime.mAlteration = Integer.valueOf(lNString.substring(1));
				
			}
		} catch(NumberFormatException e) {
			Logger.e("Time alteration formating error, assigning 0", e);
			lTime.mAlteration = 0;
		}
		
		try {
			lTime.mFixTime = (int)(StringUtil.stringToDouble(mFixTime.getText().toString())*60);
		} 
		catch (ParseException e) {
			Logger.e("Bad time entered. Setting to 0",e);
			lTime.mFixTime = 0;
			mFixTime.setText(StringUtil.doubleToString(0.0));
		}
		
		try {
			lTime.mTime = (int)(StringUtil.stringToDouble(mDevTime.getText().toString())*60); // = 0;
		}
		catch (ParseException e) {
			Logger.e("Bad time entered. Setting to 0",e);
			lTime.mTime = 0;
			mDevTime.setText(StringUtil.doubleToString(0.0));
		}
		
		return lTime;
	}
	
	void saveTime() {
		Logger.w("Saving current values");
		
		DevelopmentTime lDevTime = getDevelopmentSelection();
		lDevTime.mSource = "user";
		
		try {
			//mDarkroomDBAdapter.getAllEmulsionDevTimeCursor(lDevTime); //Check if entry exist, will throw exception if not
			mDarkroomDBAdapter.updateEmulsionDevTimeValue(lDevTime);
		} catch(SQLException e) {
			Logger.d("Entry exist doesn't exist, creating a new one");
			mDarkroomDBAdapter.insertEmulsionDevTime(lDevTime);
		}
		
		//Update time color
		
		changedSelection(0);
		updateTimerValue();
	}
	
	void changedSelection(long inId) {
		Logger.d("Changed selection");
		
		SourcedTime lSourcedTime = new SourcedTime(0, null);
		Cursor lCursor = null;
		try {
			DevelopmentTime lSelectedDevTime = getDevelopmentSelection();
			//Update interpolator
			if(inId != R.id.temperature_wheel)
				mTempInterpol.setDevelopmentTime(getDevelopmentSelection());
			
			//Find time from data base.
			lCursor = mDarkroomDBAdapter.getAllEmulsionDevTimeAllSourceCursor(lSelectedDevTime, null); 
			if (lCursor.getCount() > 0) {
				//Get user defined
				if (lCursor.moveToFirst()) {
					do {
						lSourcedTime.mSource = lCursor.getString(lCursor.getColumnIndex(DarkroomDBHelper.KEY_DEVTIME_SOURCE));
						lSourcedTime.mTime = lCursor.getInt(lCursor.getColumnIndex(DarkroomDBHelper.KEY_DEVTIME_TIME));
						if(lSourcedTime.mSource.equals(DarkroomDBHelper.KEY_DEVTIME_USER)) {
							break;
						}
					} while (lCursor.moveToNext());
				}
			}
			
		} catch(SQLException e) {
			//No time found
			lSourcedTime.mTime = 0;
			lSourcedTime.mSource = null;
		} finally {
			Logger.d("Closing cursor.");
			if(lCursor != null)
				lCursor.close();
		}
		
		if(lSourcedTime.mTime == 0 && FilmDevTimerPrefs.useTemperatureInterpolation(this)) {
			//Not time found in database, check with interpolation
			lSourcedTime = getTimeInterpolation(); 
		}
				
		updateTimeColor(lSourcedTime);
		updateTimerValue();
	}
	
	SourcedTime getTimeInterpolation() {
		
		float lSelectedTemp = ((FloatWheelAdapter) mTempWheel.getViewAdapter()).getValue(mTempWheel.getCurrentItem());
		int lInterpolatedTime = (int) mTempInterpol.getInterpolatedTime(lSelectedTemp);
		if(lInterpolatedTime != 0) {
			Logger.d("Found interpolated time: " + lInterpolatedTime);
			return new SourcedTime(lInterpolatedTime, TEMPERATURE_INTERPOLATED);
		}
		
		return new SourcedTime(0, null);
	}
	
	protected void updateTimeColor(SourcedTime inSourcedTime) {
		//Set color depending on time source
		if(!mDevTimeLock.isChecked())
			mDevTime.setText(String.format("%.2f", ((double)inSourcedTime.mTime)/60));
		
		mCurrentEditTime = inSourcedTime.mTime;
		if(inSourcedTime.mTime == 0)
			mDevTime.setTextColor(getResources().getColor(R.color.notime));
		else if(inSourcedTime.mSource == null)
			mDevTime.setTextColor(getResources().getColor(R.color.source_none));
		else if(inSourcedTime.mSource.equals(DarkroomDBHelper.KEY_DEVTIME_USER))
			mDevTime.setTextColor(getResources().getColor(R.color.source_user));
		else if(inSourcedTime.mSource.equals(TEMPERATURE_INTERPOLATED))
			mDevTime.setTextColor(getResources().getColor(R.color.source_interpolated));
		else
			mDevTime.setTextColor(getResources().getColor(R.color.source_untrust));
	}
	
	
	protected void updateTimerValue() {
		//Get development time
		float lValue = 0;
		Agitation lAgitation = new Agitation(0, FilmDevTimerPrefs.getDefaultFixAgitationTime(this), 0);
		try {
			if (mCheckDev.isChecked()) {
				lAgitation = mDarkroomDBAdapter.getAgitation(mAgitationSpinner.getSelectedItemId());
				try{
					lValue = (int)(StringUtil.stringToDouble(mDevTime.getText().toString())*60);
				} 
				catch (ParseException e) {
					Logger.e("Bad time entered. Setting to 0",e);
					lValue = 0;
					mDevTime.setText(StringUtil.doubleToString(0.0));
				}
			} else if (mCheckFix.isChecked()) {
				try{
					lValue = (int)(StringUtil.stringToDouble(mFixTime.getText().toString())*60);
				} 
				catch (ParseException e) {
					Logger.e("Bad time entered. Setting to 0",e);
					lValue = 0;
					mFixTime.setText(StringUtil.doubleToString(0.0));
				}
			}
		} catch(NumberFormatException e) {
			lValue = 0;
		}
		
		Logger.d(String.format("Setting timer duration to %f seconds",lValue));
		mDevTimer.setTimerDuration(lValue);
		
		Logger.d(String.format("Setting timer agitation initial to %d seconds",lAgitation.getInitial()));
		mDevTimer.setLapStart(lAgitation.getInitial()*1000); //Pass value in ms
		
		Logger.d(String.format("Setting timer agitation interval to %d seconds",lAgitation.getInterval()));
		mDevTimer.setLapInterval(lAgitation.getInterval()*1000);
		
		Logger.d(String.format("Setting timer agitation duration to %d seconds",lAgitation.getDuration()));
		mDevTimer.setLapDuration(lAgitation.getDuration()*1000);
	}
	
	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Logger.v("Activity return from request " + requestCode);
		switch(requestCode) {
		case REQUEST_ADD_DEVELOPER:
			mDeveloperSpinner.onAddReturn(resultCode, data);
			changedSelection(R.id.dev_spinner);
			break;
		case REQUEST_ADD_DILUTION:
			mDilutionSpinner.onAddReturn(resultCode, data);
			changedSelection(R.id.dilution_spinner);
			break;
		case REQUEST_ADD_AGITATION:
			mAgitationSpinner.onAddReturn(resultCode, data);
			changedSelection(R.id.agitation_spinner);
			break;
		case REQUEST_ADD_EMULSION:
			mEmulsionSpinner.onAddReturn(resultCode, data);
			matchShotISOToEmulsion();
			changedSelection(R.id.emulsion_spinner);
			break;
		case REQUEST_MANAGEDB:
			Logger.d("Data imported, requery cursor");
			mEmulsionSpinner.getCursorAdapter().getCursor().requery(); //TODO doesn't seem to work
			mDeveloperSpinner.getCursorAdapter().getCursor().requery();
			mDilutionSpinner.getCursorAdapter().getCursor().requery();
			mAgitationSpinner.getCursorAdapter().getCursor().requery();
			changedSelection(0);
			break;
		}
    }
	
	@Override
	public void onBackPressed() {
		if (mDevTimer.isTimerRunning()) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(R.string.exit_warning)
			.setCancelable(false)
			.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					mDevTimer.shutdownTimer();
					FilmDevelopingTimer.this.finish();
				}})
			.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dialog.cancel();
				}});
			
			AlertDialog alert = builder.create();
			alert.show();
		} else {
			FilmDevelopingTimer.this.finish();
		}
	}
    
}
