package com.darkroom.filmdevtimer;

import android.os.Handler;

public class Timer {
	private int mInterval;
	private Handler mHandler;
	private Runnable mTickHandler;
	private Runnable mDelegate;
	private boolean mTicking;

	public int getInterval() {
		return mInterval;
	}

	public void setInterval(int delay) {
		mInterval = delay;
	}

	public boolean getIsTicking() {
		return mTicking;
	}

	public Timer(int interval) {
		mInterval = interval;
		mHandler = new Handler();
	}

	public Timer(int interval, Runnable onTickHandler) {
		mInterval = interval;
		setOnTickHandler(onTickHandler);
		mHandler = new Handler();
	}

	public void start(int interval, Runnable onTickHandler) {
		if (mTicking)
			return;
		mInterval = interval;
		setOnTickHandler(onTickHandler);
		mHandler.postDelayed(mDelegate, mInterval);
		mTicking = true;
	}

	public void start() {
		if (mTicking)
			return;
		mHandler.postDelayed(mDelegate, mInterval);
		mTicking = true;
	}

	public void stop() {
		mHandler.removeCallbacks(mDelegate);
		mTicking = false;
	}

	public void setOnTickHandler(Runnable onTickHandler) {
		if (onTickHandler == null)
			return;

		mTickHandler = onTickHandler;

		mDelegate = new Runnable() {
			public void run() {
				if (mTickHandler == null)
					return;
				mTickHandler.run();
				mHandler.postDelayed(mDelegate, mInterval);
			}
		};
	}
}