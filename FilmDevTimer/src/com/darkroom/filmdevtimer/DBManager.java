package com.darkroom.filmdevtimer;

import com.darkroom.filmdevtimer.Logger;
import com.darkroom.filmdevtimer.dao.FilmDevTimerDBAdapter;
import com.darkroom.library.dao.DarkroomDBAdapter;
import com.jfdupuis.library.dao.ExportCSV;
import com.jfdupuis.library.string.StringUtil;
import com.jfdupuis.library.widget.FileDialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteQueryBuilder;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class DBManager extends Activity {
	static final int REQUEST_SELECT_IMPORT_FILE = 257;
	static final int REQUEST_SELECT_EXPORT_FILE = 113;
	static final int REQUEST_IMPORT_FILE = 3478;
	static final int REQUEST_EXPORT_FILE = 4934;
	static final int REQUEST_IMPORT_OVERWRITE = 30;
	static final int DIALOG_OVERWRITE_ID = 31;
	
	FilmDevTimerDBAdapter mDarkroomDBAdapter;
	String mQuery = "";
	
	String mFilename = null;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Logger.d("DBManager.onCreate");
		setContentView(R.layout.dbmanager);
		this.setTitle(R.string.dbmanager_title);
		
		Button lImportBtn = (Button) findViewById(R.id.import_btn);
		lImportBtn.setOnClickListener(new OnClickListener(){
			public void onClick(View v) {
				importDB();
			}
		});
        
		Button lExportBtn = (Button) findViewById(R.id.export_btn);
		lExportBtn.setOnClickListener(new OnClickListener(){
			public void onClick(View v) {
				exportDB();
			}
		});
		
		Button lDoneBtn = (Button) findViewById(R.id.done_btn);
		lDoneBtn.setOnClickListener(new OnClickListener(){
			public void onClick(View v) {
				finish();
			}
		});
	}
	
	@Override
	public void onStart() {
		super.onStart();
		Logger.d("DBManager.onStart");
		mDarkroomDBAdapter = new FilmDevTimerDBAdapter(this);
        mDarkroomDBAdapter.open();
	}
	
	@Override
	public void onStop() {
		super.onStop();
		if(mDarkroomDBAdapter != null)
			mDarkroomDBAdapter.close();
	}
	
	protected Dialog onCreateDialog(int id) {
		AlertDialog dialog;
	    switch(id) {
	    case DIALOG_OVERWRITE_ID:
	    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    	builder.setMessage(getString(R.string.overwrite_warning))
	    	       .setCancelable(false)
	    	       .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
	    	           public void onClick(DialogInterface dialog, int id) {
	    	        	   dialog.dismiss();
	    	        	   importDB(mFilename, true);
	    	           }
	    	       })
	    	       .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
	    	           public void onClick(DialogInterface dialog, int id) {
	    	        	   dialog.dismiss();
	    	        	   importDB(mFilename, false);
	    	           }
	    	       });
	    	dialog = builder.create();
	        break;
	    default:
	        dialog = null;
	    }
	    return dialog;
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Logger.v("Activity return from request " + requestCode);
		switch(requestCode) {
		case REQUEST_SELECT_IMPORT_FILE:
			if (resultCode == Activity.RESULT_OK) {
				String filePath = data.getStringExtra(FileDialog.RESULT_PATH);
				mFilename = filePath;
				showDialog(DIALOG_OVERWRITE_ID);
			} else if (resultCode == Activity.RESULT_CANCELED) {
				Logger.d("file not selected");
			}
			break;
		case REQUEST_SELECT_EXPORT_FILE:
			if (resultCode == Activity.RESULT_OK) {
				String filePath = data.getStringExtra(FileDialog.RESULT_PATH);
				filePath = StringUtil.stripExtension(filePath);
				exportDB(filePath+".csv");

			} else if (resultCode == Activity.RESULT_CANCELED) {
				Logger.d("file not selected");
			}
			break;
		case REQUEST_EXPORT_FILE:
			if (resultCode == Activity.RESULT_OK) {
				Toast.makeText(DBManager.this, "Export successful!", Toast.LENGTH_SHORT).show();
			}else if (resultCode == Activity.RESULT_CANCELED) {
				Toast.makeText(DBManager.this, "Export failed", Toast.LENGTH_SHORT).show();
			}
			break;
		case REQUEST_IMPORT_FILE:
			if (resultCode == Activity.RESULT_OK) {
				Toast.makeText(DBManager.this, "Import successful!", Toast.LENGTH_SHORT).show();
			}else if (resultCode == Activity.RESULT_CANCELED) {
				Toast.makeText(DBManager.this, "Import failed", Toast.LENGTH_SHORT).show();
			}
			break;			
		}
    }
	
	protected void exportDB() {
		Logger.d("Exporting database");
		String[] lColumns = new String[] {
				DarkroomDBAdapter.EMULSION_DEVTIME_TABLE_NAME + "." + DarkroomDBAdapter.KEY_ID ,
				DarkroomDBAdapter.CHEMICAL_TABLE_NAME + "." + DarkroomDBAdapter.KEY_CHEMICAL_NAME + " AS developer ",
				DarkroomDBAdapter.CHEMICAL_TABLE_NAME + "." + DarkroomDBAdapter.KEY_CHEMICAL_FORMULA ,
				DarkroomDBAdapter.DILUTION_TABLE_NAME + "." + DarkroomDBAdapter.KEY_STRING + " AS dilution ",
				DarkroomDBAdapter.EMULSION_TABLE_NAME + "." + DarkroomDBAdapter.KEY_EMULSION_BRAND + " AS emulsion_brand ",
				DarkroomDBAdapter.EMULSION_TABLE_NAME + "." + DarkroomDBAdapter.KEY_EMULSION_NAME + " AS emulsion_name ",
				DarkroomDBAdapter.EMULSION_TABLE_NAME + "." + DarkroomDBAdapter.KEY_EMULSION_ISO + " AS emulsion_iso ",
				DarkroomDBAdapter.EMULSION_TABLE_NAME + "." + DarkroomDBAdapter.KEY_EMULSION_ISCOLOR + " AS emulsion_iscolor ",
				DarkroomDBAdapter.EMULSION_DEVTIME_TABLE_NAME + "." + DarkroomDBAdapter.KEY_DEVTIME_SHOTISO ,
				DarkroomDBAdapter.EMULSION_DEVTIME_TABLE_NAME + "." + DarkroomDBAdapter.KEY_DEVTIME_EMULSIONFORMAT ,
				DarkroomDBAdapter.EMULSION_DEVTIME_TABLE_NAME + "." + DarkroomDBAdapter.KEY_DEVTIME_TEMP ,
				DarkroomDBAdapter.EMULSION_DEVTIME_TABLE_NAME + "." + DarkroomDBAdapter.KEY_DEVTIME_N ,
				DarkroomDBAdapter.AGITATION_TABLE_NAME + "." + DarkroomDBAdapter.KEY_AGITATION_INITIAL ,
				DarkroomDBAdapter.AGITATION_TABLE_NAME + "." + DarkroomDBAdapter.KEY_AGITATION_INTERVAL ,
				DarkroomDBAdapter.AGITATION_TABLE_NAME + "." + DarkroomDBAdapter.KEY_AGITATION_DURATION ,
				DarkroomDBAdapter.EMULSION_DEVTIME_TABLE_NAME + "." + DarkroomDBAdapter.KEY_DEVTIME_TIME ,
				DarkroomDBAdapter.EMULSION_DEVTIME_TABLE_NAME + "." + DarkroomDBAdapter.KEY_DEVTIME_NOTE ,
				DarkroomDBAdapter.EMULSION_DEVTIME_TABLE_NAME + "." + DarkroomDBAdapter.KEY_DEVTIME_SOURCE
		}; 
		
		mQuery = SQLiteQueryBuilder.buildQueryString(false, DarkroomDBAdapter.EMULSION_DEVTIME_TABLE_NAME, lColumns, null, null, null, null, null);
		mQuery += " JOIN " + DarkroomDBAdapter.EMULSION_TABLE_NAME 
					+ " ON (" + DarkroomDBAdapter.EMULSION_TABLE_NAME + "." + DarkroomDBAdapter.KEY_ID + "="
					+ DarkroomDBAdapter.EMULSION_DEVTIME_TABLE_NAME + "." + DarkroomDBAdapter.KEY_DEVTIME_EMULSIONID + ") ";
		
		mQuery += " JOIN " + DarkroomDBAdapter.CHEMICAL_TABLE_NAME 
					+ " ON (" + DarkroomDBAdapter.CHEMICAL_TABLE_NAME + "." + DarkroomDBAdapter.KEY_ID + "="
					+ DarkroomDBAdapter.EMULSION_DEVTIME_TABLE_NAME + "." + DarkroomDBAdapter.KEY_DEVTIME_DEVELOPERID + ") ";
		
		mQuery += " JOIN " + DarkroomDBAdapter.AGITATION_TABLE_NAME 
					+ " ON (" + DarkroomDBAdapter.AGITATION_TABLE_NAME + "." + DarkroomDBAdapter.KEY_ID + "="
					+ DarkroomDBAdapter.EMULSION_DEVTIME_TABLE_NAME + "." + DarkroomDBAdapter.KEY_DEVTIME_AGITATIONID + ") ";
		
		mQuery += " JOIN " + DarkroomDBAdapter.DILUTION_TABLE_NAME 
					+ " ON (" + DarkroomDBAdapter.DILUTION_TABLE_NAME + "." + DarkroomDBAdapter.KEY_ID + "="
					+ DarkroomDBAdapter.EMULSION_DEVTIME_TABLE_NAME + "." + DarkroomDBAdapter.KEY_DEVTIME_DILUTIONID + ") ";
		
		mQuery += " ORDER BY " 
					+ DarkroomDBAdapter.KEY_DEVTIME_EMULSIONID + ", " + DarkroomDBAdapter.KEY_DEVTIME_DEVELOPERID + ", " 
					+ DarkroomDBAdapter.EMULSION_DEVTIME_TABLE_NAME + "." +DarkroomDBAdapter.KEY_DEVTIME_SHOTISO + ", " 
					+ DarkroomDBAdapter.KEY_DEVTIME_EMULSIONFORMAT + ", " + DarkroomDBAdapter.KEY_DEVTIME_TEMP + ", " + DarkroomDBAdapter.KEY_DEVTIME_N;
		
		
		
		Logger.d("Select file to export");
		Intent intent = new Intent(this, FileDialog.class);
        intent.putExtra(FileDialog.START_PATH, "/sdcard");
        
        //can user select directories or not
        intent.putExtra(FileDialog.CAN_SELECT_DIR, false);
        
        //alternatively you can set file filter
        intent.putExtra(FileDialog.FORMAT_FILTER, new String[] { "csv", "CSV" });
        intent.putExtra(FileDialog.SELECTION_MODE, FileDialog.SelectionMode.MODE_CREATE);
        
        startActivityForResult(intent, REQUEST_SELECT_EXPORT_FILE);
		
		
	}
	
	protected void exportDB(String inFilename) {
		Logger.d("Export to file: " + inFilename);
		Intent lIntent = new Intent(this, ExportCSV.class);
		lIntent.putExtra("dbname", mDarkroomDBAdapter.getDataBaseName());
		lIntent.putExtra("query", mQuery);
		lIntent.putExtra("filename", inFilename);
		startActivityForResult(lIntent, REQUEST_EXPORT_FILE);
	}
	
	protected void importDB() {
		Logger.d("Select file to import");
		Intent intent = new Intent(this, FileDialog.class);
        intent.putExtra(FileDialog.START_PATH, "/sdcard");
        
        //can user select directories or not
        intent.putExtra(FileDialog.CAN_SELECT_DIR, false);
        
        //alternatively you can set file filter
        intent.putExtra(FileDialog.FORMAT_FILTER, new String[] { "csv", "CSV" });
        intent.putExtra(FileDialog.SELECTION_MODE, FileDialog.SelectionMode.MODE_OPEN);
        
        startActivityForResult(intent, REQUEST_SELECT_IMPORT_FILE);
	}
	
	protected void importDB(String inFilename, boolean inOverwrite) {
		Logger.d("Import from file: " + inFilename);
		Intent lIntent = new Intent(this, DBImporter.class);
		lIntent.putExtra("filename", inFilename);
		lIntent.putExtra("overwrite", inOverwrite);
		startActivityForResult(lIntent, REQUEST_IMPORT_FILE);
	}
	
	
	
	
}
