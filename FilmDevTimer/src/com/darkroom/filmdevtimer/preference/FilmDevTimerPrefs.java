package com.darkroom.filmdevtimer.preference;

import com.darkroom.filmdevtimer.Logger;
import com.darkroom.filmdevtimer.R;
import com.darkroom.filmdevtimer.TimerVoiceCommand;
import com.darkroom.filmdevtimer.dao.FilmDevTimerDBAdapter;
import com.darkroom.library.Chemical;
import com.darkroom.library.dao.DarkroomDBHelper;
import com.jfdupuis.library.preferences.CursorListPreference;
import com.jfdupuis.library.string.StringUtil;
import com.jfdupuis.library.widget.SoundTimerPrefs;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;


public class FilmDevTimerPrefs extends SoundTimerPrefs {
//	private static final String UNITS_TEMP = "units_temp";
//    private static final String UNITS_TEMP_DEFAULT = "C";
	
//    private static final String SCREEN_BRIGHTNESS = "screen_brightness";
//    private static final int SCREEN_BRIGHTNESS_DEFAULT = 50;
    
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.layout.settings);
        addPreferencesFromResource(R.layout.timer_preferences);
        
        //Populate with database
        PreferenceScreen preferenceScreen = getPreferenceScreen();
        
        FilmDevTimerDBAdapter lDarkroomDBAdapter = new FilmDevTimerDBAdapter(this);
        lDarkroomDBAdapter.open();
        Cursor lCursor = lDarkroomDBAdapter.getAllEmulsionCursor(
        		new String[]{DarkroomDBHelper.KEY_EMULSION_NAME}, DarkroomDBHelper.KEY_EMULSION_NAME);
        ((CursorListPreference) preferenceScreen.findPreference("emulsion")).setData(lCursor, DarkroomDBHelper.KEY_EMULSION_NAME);
        
        
        lCursor = lDarkroomDBAdapter.getAllChemicalCursor( 
				new String[]{DarkroomDBHelper.KEY_CHEMICAL_NAME}, 
				DarkroomDBHelper.KEY_CHEMICAL_NAME,Chemical.DEVELOPER);
        ((CursorListPreference) preferenceScreen.findPreference("developer")).setData(lCursor, DarkroomDBHelper.KEY_CHEMICAL_NAME);
        
        
        lCursor = lDarkroomDBAdapter.getAllAgitationCursor(
        		new String[]{DarkroomDBHelper.KEY_STRING}, DarkroomDBHelper.KEY_STRING);
        ((CursorListPreference) preferenceScreen.findPreference("agitation")).setData(lCursor, DarkroomDBHelper.KEY_STRING);
        
        
        lCursor = lDarkroomDBAdapter.getAllStringDataCursor(DarkroomDBHelper.DILUTION_TABLE_NAME);
        ((CursorListPreference) preferenceScreen.findPreference("dilution")).setData(lCursor, DarkroomDBHelper.KEY_STRING);
        
        lCursor.close();
        
        Preference lClearLearningPref = (Preference) findPreference("clear_learning");
        lClearLearningPref.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
            	TimerVoiceCommand lCommander = new TimerVoiceCommand(FilmDevTimerPrefs.this);
            	lCommander.clearLearning();
            	lCommander.destroy();
            	return true;
            }
        });
        
        
        lDarkroomDBAdapter.close();
    }
	
	public static boolean learnVoice(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("learn_voice", true);
	}
	
	public static boolean useVoiceRecognition(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("voice_recognition", true);
	}
	
	public static boolean useVoiceRecognitionOnlyStopped(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("voice_recognition_only_stopped", true);
	}
	
	public static boolean useTemperatureInterpolation(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("temperature_interpolation", true);
	}
	
	public static int getDefaultEmulsion(Context context) {
		return Integer.valueOf(PreferenceManager.getDefaultSharedPreferences(context).getString("emulsion", "1"));
	}
	public static int getDefaultDeveloper(Context context) {
		return Integer.valueOf(PreferenceManager.getDefaultSharedPreferences(context).getString("developer", "1"));
	}
	
	public static int getDefaultAgitation(Context context) {
		return Integer.valueOf(PreferenceManager.getDefaultSharedPreferences(context).getString("agitation", "1"));
	}
	public static int getDefaultDilution(Context context) {
		return Integer.valueOf(PreferenceManager.getDefaultSharedPreferences(context).getString("dilution", "1"));
	}
	
	public static CharSequence getDefaultFormat(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context).getString("format", "1");
	}
	
	public static CharSequence getDefaultTemp(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context).getString("temperature", "20");
	}
	
	
//	public static char getDefaultUnitsTemp(Context context) {
//    	return PreferenceManager.getDefaultSharedPreferences(context).getString(UNITS_TEMP, UNITS_TEMP_DEFAULT).charAt(0);
//	}
	
	public static int getDefaultFixAgitationTime(Context context) {
		try {
			return Integer.valueOf(PreferenceManager.getDefaultSharedPreferences(context).getString("fixer_laps_intervals", "30"));
		} catch(NumberFormatException e) {
			Logger.e("Bad number as default fixer agitation time. Using default of 30", e);
			return 30;
		}
		
	}
	
	public static String getDefaultFixTime(Context context) {
    	return PreferenceManager.getDefaultSharedPreferences(context).getString("fixtime", StringUtil.doubleToString(4.0));
	}
	
//    public static void setScreenBrightness(Context context, int inScreenBrightness) {
//        PreferenceManager.getDefaultSharedPreferences(context).edit().putInt(SCREEN_BRIGHTNESS, inScreenBrightness).commit();
//    }
//    public static int getScreenBrightness(Context context) {
//    	try{
//    		return PreferenceManager.getDefaultSharedPreferences(context).getInt(SCREEN_BRIGHTNESS, SCREEN_BRIGHTNESS_DEFAULT);
//    	} catch(ClassCastException inE) {
//    		String value = PreferenceManager.getDefaultSharedPreferences(context).getString(SCREEN_BRIGHTNESS, Integer.toString(SCREEN_BRIGHTNESS_DEFAULT));
//    		try {
//                int lIntValue =  Integer.parseInt(value);
//                PreferenceManager.getDefaultSharedPreferences(context).edit().putInt(SCREEN_BRIGHTNESS, lIntValue).commit();
//                return lIntValue;
//            } catch (NumberFormatException e) {
//            	setScreenBrightness(context, SCREEN_BRIGHTNESS_DEFAULT);
//                return SCREEN_BRIGHTNESS_DEFAULT;
//            }
//    	}
//    }

    
}
