package com.darkroom.filmdevtimer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import com.darkroom.filmdevtimer.preference.FilmDevTimerPrefs;
import com.jfdupuis.library.widget.voice.VoiceCommander;

import android.content.Context;
import android.util.AttributeSet;

public class TimerVoiceCommand extends VoiceCommander {
	static final public int NB_COMMAND = 6;
	static final public int START_COMMAND = 0;
	static final public int RESET_COMMAND = 1;
	static final public int STOP_COMMAND = 2;
	static final public int NEXT_COMMAND = 3;
	static final public int DEV_COMMAND = 4;
	static final public int FIX_COMMAND = 5;
	
	public TimerVoiceCommand(Context context) {
        this(context, null);
    }

    public TimerVoiceCommand(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TimerVoiceCommand(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
	}
	
    
    @Override
    protected void fillInitialCommands() {
    	Logger.d("Inserting initial commands");
    	mCommandDictionary.insertCommand("go", TimerVoiceCommand.START_COMMAND);
    	mCommandDictionary.insertCommand("google", TimerVoiceCommand.START_COMMAND);
    	mCommandDictionary.insertCommand("girl", TimerVoiceCommand.START_COMMAND);
    	
		mCommandDictionary.insertCommand("top", TimerVoiceCommand.STOP_COMMAND);
		mCommandDictionary.insertCommand("stop", TimerVoiceCommand.STOP_COMMAND);
		mCommandDictionary.insertCommand("stock", TimerVoiceCommand.STOP_COMMAND);
		mCommandDictionary.insertCommand("stunk", TimerVoiceCommand.STOP_COMMAND);
		
		mCommandDictionary.insertCommand("next", TimerVoiceCommand.NEXT_COMMAND);
		mCommandDictionary.insertCommand("reset", TimerVoiceCommand.RESET_COMMAND);
		
		mCommandDictionary.insertCommand("dev", TimerVoiceCommand.DEV_COMMAND);
		mCommandDictionary.insertCommand("developer", TimerVoiceCommand.DEV_COMMAND);
		mCommandDictionary.insertCommand("develop", TimerVoiceCommand.DEV_COMMAND);
		mCommandDictionary.insertCommand("developing", TimerVoiceCommand.DEV_COMMAND);
		
		mCommandDictionary.insertCommand("fix", TimerVoiceCommand.FIX_COMMAND);
		mCommandDictionary.insertCommand("fixed", TimerVoiceCommand.FIX_COMMAND);
		mCommandDictionary.insertCommand("fixer", TimerVoiceCommand.FIX_COMMAND);
		mCommandDictionary.insertCommand("fixing", TimerVoiceCommand.FIX_COMMAND);
		mCommandDictionary.insertCommand("6", TimerVoiceCommand.FIX_COMMAND);

    }
	
	/**
	 * Identify the command received
	 * @return -1 if the command was not identify
	 */
    @Override
	protected int identifyCommand(ArrayList<String> inCommands) {
		
		ArrayList<Integer> lPossibleCommands = new ArrayList<Integer>();
		//Get all command associated to that word
		for(String lCommand : inCommands) {
			lPossibleCommands.addAll(mCommandDictionary.getAllCommands(lCommand));
		}
		
		if(lPossibleCommands.size() > 0) {
			//Get the most represented command
			//ArrayList<Integer> lCommandCount = new ArrayList<Integer>(NB_COMMAND);
			Integer[] lCommandCount = new Integer[NB_COMMAND];
			for(int i = 0; i < lCommandCount.length; ++i)
				lCommandCount[i] = 0;

			for(Integer lValue : lPossibleCommands) {
				lCommandCount[lValue]++;
			}
			
			ArrayList<Integer> lCommandCountArray = new ArrayList<Integer>(Arrays.asList(lCommandCount));
			int lIdCommand = lCommandCountArray.indexOf(Collections.max(lCommandCountArray));
			
			Logger.d("Command identified as: " + lIdCommand);
			
			//Learn
			if(FilmDevTimerPrefs.learnVoice(getContext())) {
				for(String lCommand : inCommands) {
					mCommandDictionary.insertCommand(lCommand, lIdCommand);
				}
			}
				
			return lIdCommand;
		} else {
			//No match was found
			return -1;
		}
		
		
//		if(lCommand.equals("go")) {
//			return START_COMMAND;
//		} else if( lCommand.contains("top") ) { // stop
//			return STOP_COMMAND;
//		} else if( lCommand.contains("next")) {
//			return NEXT_COMMAND;
//		} else if( lCommand.contains("reset")) {
//			return RESET_COMMAND;
//		} else if( lCommand.contains("dev")) {
//			return DEV_COMMAND;
//		} else if( lCommand.contains("fix") || lCommand.contains("6")) {
//			return FIX_COMMAND;
//		}
//		return -1;
			
		
		
	}
}