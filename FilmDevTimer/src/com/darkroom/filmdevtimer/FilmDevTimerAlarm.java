package com.darkroom.filmdevtimer;

import com.jfdupuis.library.Logger;
import com.jfdupuis.library.widget.SoundTimerAlarm;
import com.jfdupuis.library.widget.SoundTimerPrefs;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.AttributeSet;
import android.widget.Toast;


/**
 * 
 * Don't forget in manifest:
 * <receiver android:name=".FilmDevTimerAlarm$EndTimeReceiver"/>
 * <receiver android:name=".FilmDevTimerAlarm$LapTimeReceiver"/>
 * <receiver android:name=".FilmDevTimerAlarm$SecondaryLapTimeReceiver"/>
 * <receiver android:name=".FilmDevTimerAlarm$InitialLapTimeReceiver"/>
 */
public class FilmDevTimerAlarm extends SoundTimerAlarm {
	
	static final Class<?> END_RECEIVER = FilmDevTimerAlarm.EndTimeReceiver.class;
	static final Class<?> END_WARNING_RECEIVER = FilmDevTimerAlarm.EndWarningReceiver.class;
	static final Class<?> INITIAL_LAP_RECEIVER = FilmDevTimerAlarm.InitialLapTimeReceiver.class;
	static final Class<?> PRIMARY_LAP_RECEIVER = FilmDevTimerAlarm.LapTimeReceiver.class;
	static final Class<?> SECONDARY_LAP_RECEIVER = FilmDevTimerAlarm.SecondaryLapTimeReceiver.class;
	
	
	public FilmDevTimerAlarm(Context context) {
        this(context, null);
    }

    public FilmDevTimerAlarm(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }
	
	public FilmDevTimerAlarm(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	

	static public class EndTimeReceiver extends AlarmTimeReceiver {
		@Override
		protected String makeMessage(Context inContext) {
			return inContext.getString(R.string.end_development);
		}

		@Override
		protected Intent makeIntent(Context inContext) {
			Intent intent2open = new Intent(inContext, FilmDevelopingTimer.class);
	        intent2open.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	        intent2open.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			return intent2open;
		}

		@Override
		protected Uri getSoundRecessource(Context inContext) {
			return Uri.parse("android.resource://com.darkroom.filmdevtimer/raw/alarm_ring");
		}
		@Override
	    public void onReceive(Context context, Intent inIntent) {
			Logger.d("Film end alarm");
			super.onReceive(context, inIntent);
	    }

		@Override
		protected void cancelRelatedAlarms(Context inContext) {
			AlarmManager am = (AlarmManager)inContext.getSystemService(Context.ALARM_SERVICE);
		    am.cancel(getPrimaryLapIntent(inContext));
		    am.cancel(getSecondaryLapIntent(inContext));
		    am.cancel(getEndWarningIntent(inContext));
		}
	}
	
	static public class EndWarningReceiver extends AlarmTimeReceiver {
		@Override
		protected String makeMessage(Context inContext) {
			return null;
		}

		@Override
		protected Intent makeIntent(Context inContext) {
			Intent intent2open = new Intent(inContext, FilmDevelopingTimer.class);
	        intent2open.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	        intent2open.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			return intent2open;
		}

		@Override
		protected Uri getSoundRecessource(Context inContext) {
			return Uri.parse("android.resource://com.darkroom.filmdevtimer/raw/end_warning");
		}
		@Override
	    public void onReceive(Context context, Intent inIntent) {
			Logger.d("Timer end warning alarm");
			super.onReceive(context, inIntent);
	    }

		@Override
		protected void cancelRelatedAlarms(Context inContext) {	}
	}
	
	static public class LapTimeReceiver extends AlarmTimeReceiver {
		@Override
		protected String makeMessage(Context inContext) {
			return inContext.getString(R.string.start_agitation);
		}

		@Override
		protected Intent makeIntent(Context inContext) {
			return null;
		}

		@Override
		protected Uri getSoundRecessource(Context inContext) {
			return Uri.parse("android.resource://com.darkroom.filmdevtimer/raw/metronom_pling"); //TODO Use the raw from library
		}
		@Override
	    public void onReceive(Context context, Intent inIntent) {
			Logger.d("Film Received lap alarm");
			super.onReceive(context, inIntent);
	    }

		@Override
		protected void cancelRelatedAlarms(Context inContext) {	}
	}
		
	static public class SecondaryLapTimeReceiver extends AlarmTimeReceiver {
		@Override
		protected String makeMessage(Context inContext) {
			return inContext.getString(R.string.stop_agitation);
		}

		@Override
		protected Intent makeIntent(Context inContext) {
			return null;
		}

		@Override
		protected Uri getSoundRecessource(Context inContext) {
			return Uri.parse("android.resource://com.darkroom.filmdevtimer/raw/metronom_pling"); //TODO Use the raw from library
		}
		
		@Override
	    public void onReceive(Context context, Intent inIntent) {
			Logger.d("Film Received secondary alarm");
			super.onReceive(context, inIntent);
		}

		@Override
		protected void cancelRelatedAlarms(Context inContext) {	}
		
	}
	
	static public class InitialLapTimeReceiver extends SecondaryLapTimeReceiver {
		//Do the same thing
	}
	
	////////////////////////////////////////////////////
	/// Rest below doesn't need to be modified
	////////////////////////////////////////////////////
	static protected PendingIntent getEndIntent(Context context) {
		return PendingIntent.getBroadcast(context, 0, new Intent(context, END_RECEIVER), 0);
	}
	static protected PendingIntent getEndWarningIntent(Context context) {
		return PendingIntent.getBroadcast(context, 0, new Intent(context, END_WARNING_RECEIVER), 0);
	}
	
	
	static protected PendingIntent getPrimaryLapIntent(Context context) {
		return PendingIntent.getBroadcast(context, 0, new Intent(context, PRIMARY_LAP_RECEIVER), 0);
	}
	static protected PendingIntent getSecondaryLapIntent(Context context) {
		return PendingIntent.getBroadcast(context, 0, new Intent(context, SECONDARY_LAP_RECEIVER), 0);
	}
	static protected PendingIntent getInitialLapIntent(Context context) {
		return PendingIntent.getBroadcast(context, 0, new Intent(context, INITIAL_LAP_RECEIVER), 0);
	}
	
	@Override
	protected void setPrimaryLapTimeAlarm(int inMilli, int inStart) {
	    sendRepeatingAlarm(getPrimaryLapIntent(getContext()), inMilli, inStart);
	}
	
	@Override
	protected void setSecondaryLapTimeAlarm(int inMilli, int inStart) {
		sendRepeatingAlarm(getSecondaryLapIntent(getContext()), inMilli, inStart);
	}
	
	@Override
	protected void setFirstLapTimeAlarm(int inMilli) {
		sendAlarm(getInitialLapIntent(getContext()), inMilli);
	}
	
	@Override
	protected void setEndTimeAlarm(int inMilli) {
		sendAlarm(getEndIntent(getContext()), inMilli);
	}
	
	@Override
	protected void setEndWarningAlarm(int inMilli) {
		sendAlarm(getEndWarningIntent(getContext()), inMilli);
	}
	
	
	
	@Override
	protected void cancelAlarms() {
		AlarmManager am = (AlarmManager)getContext().getSystemService(Context.ALARM_SERVICE);
		am.cancel(getInitialLapIntent(getContext()));
	    am.cancel(getPrimaryLapIntent(getContext()));
	    am.cancel(getSecondaryLapIntent(getContext()));
	    am.cancel(getEndIntent(getContext()));
	    am.cancel(getEndWarningIntent(getContext()));
	}
	
	private final static int NOTIFICATION_ID = 0;
	abstract static public class AlarmTimeReceiver extends BroadcastReceiver {
		
		abstract protected String makeMessage(Context context);
		abstract protected Intent makeIntent(Context context);
		abstract protected void cancelRelatedAlarms(Context context);
		
		/**
		 * For example could return: Uri.parse("android.resource://com.mypackage/raw/mysound");
		 */
		abstract protected Uri getSoundRecessource(Context context);
		
		@Override
	    public void onReceive(Context context, Intent inIntent) {
			Logger.d("AlarmTimeReceiver.onReceive");
	        if(SoundTimerPrefs.playSounds(context)) {
			    NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
			    Notification notification = new Notification();
			    Uri lUri = getSoundRecessource(context);
			    if(lUri != null) {
				    notification.sound = lUri; //TODO Use the raw from library
				    Logger.d("Playing end of time sound");
				    nm.notify(NOTIFICATION_ID, notification);
			    }
	        }
	        
	        String lMessage = makeMessage(context);
	        if(lMessage != null)
	        	Toast.makeText(context, lMessage, Toast.LENGTH_SHORT).show();
	        
	        cancelRelatedAlarms(context);
		    
	        
	        Logger.d("Starting main activiy");
	        Intent lIntent = makeIntent(context);
	        if(lIntent != null)
	        	context.startActivity(lIntent);
	    }
	}
}
