package com.darkroom.filmdevtimer.dao;

import com.darkroom.library.dao.DarkroomDBAdapter;
import com.darkroom.library.dao.StringData;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class FilmDevTimerDBAdapter extends DarkroomDBAdapter {

	public FilmDevTimerDBAdapter(Context inContext) {
		super(inContext);
	}
	
	@Override
	public void onCreate(SQLiteDatabase _db) {
		super.onCreate(_db);
		
		_db.execSQL(CREATE_CHEMICAL_TABLE);
		_db.execSQL(CREATE_AGITATION_TABLE);
		_db.execSQL(CREATE_EMULSION_TABLE);
		_db.execSQL(CREATE_DEVTIME_TABLE);
		_db.execSQL(CREATE_DILUTION_TABLE);
		
		//Ensure 1 index for all not specified
		insertNullChemical(_db);
		insertNullEmulsion(_db);
		insertNullAgitation(_db);
		insertNullStringData(_db, DILUTION_TABLE_NAME, new StringData("Stock", null));
		
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase _db, int _oldVersion, int _newVersion) {
		// Log the version upgrade.
		Log.w("Darkroom_DAOHelper", "Upgrading from version " + _oldVersion + " to " + _newVersion);

		// Upgrade the existing database to conform to the new version.
		// Multiple previous versions can be handled by comparing _oldVersion
		// and
		// _newVersion values.
		
		//TODO Handle the change of agitation from stringdata
		//TODO Handle film reference to devtime
		//TODO Handle dilution of developer from developer name
		//TODO  make better transition
		//TODO change initially created agitation with "Agitation.NOTSPECIFIEDKEY" as interval
		
//		if((_oldVersion == 1 || _oldVersion == 0) && _newVersion == 2) {
//			_db.execSQL(CREATE_DEVTIME_TABLE);
//			_db.execSQL(CREATE_LENS_TABLE);
//			_db.execSQL(CREATE_FILTER_TABLE);
//			return;
//		} else {
			//Create a new one.
			// The simplest case is to drop the old table and create a new one.
//			_db.execSQL("DROP TABLE IF EXISTS " + PRINT_TABLE_NAME);
//			_db.execSQL("DROP TABLE IF EXISTS " + EXPOSURE_TABLE_NAME);
//			_db.execSQL("DROP TABLE IF EXISTS " + EXPOSUREITEM_TABLE_NAME);
//			_db.execSQL("DROP TABLE IF EXISTS " + PAPER_TABLE_NAME);
//			_db.execSQL("DROP TABLE IF EXISTS " + FILM_TABLE_NAME);
//			_db.execSQL("DROP TABLE IF EXISTS " + FILMFRAME_TABLE_NAME);
//			_db.execSQL("DROP TABLE IF EXISTS " + CAMERA_TABLE_NAME);
//			_db.execSQL("DROP TABLE IF EXISTS " + CHEMICAL_TABLE_NAME);
//			_db.execSQL("DROP TABLE IF EXISTS " + AGITATION_TABLE_NAME);
//			_db.execSQL("DROP TABLE IF EXISTS " + EMULSION_TABLE_NAME);
//			_db.execSQL("DROP TABLE IF EXISTS " + COLLECTION_TABLE_NAME);
//			_db.execSQL("DROP TABLE IF EXISTS " + COLLECTIONPRINT_TABLE_NAME);
//			_db.execSQL("DROP TABLE IF EXISTS " + CREATE_DEVTIME_TABLE);
//			onCreate(_db);
//		}
	}

}
