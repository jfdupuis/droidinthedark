#!/usr/bin/perl

#
# List the films and developer included in the massive dev chart in the csv file
#

my $filename = $ARGV[0];

open my $inFile, "<$filename" or die $!;

my %films;
my %developers;

while(<$inFile>) {
    chomp;
    @fields = split (";",$_);
    
    $films{$fields[0]} = $fields[0] unless exists $films{$fields[0]};
    $developers{$fields[1]} = $fields[1] unless exists $developers{$fields[1]};
}

close $inFile;

print "----- Films ------\n";
foreach ( sort keys %films) {
	print "$_\n";
}

print "\n\n----- Developers ------\n";
foreach ( sort keys %developers) {
	print "$_\n";
}